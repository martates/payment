import { Component } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
import{MatDialog} from '@angular/material/dialog';
import { Menu } from './app-services/app-menu';
import { AppMenuService } from './app-services/app-menu.service';
import { AppNotificationService } from './app-services/app-notification.service';
import { AuthService } from './auth/auth.service';
import { User } from './models/user';
import { LoginComponent } from './auth/login.component';
import { ReplaySubject, BehaviorSubject, Observable, from } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  userSubject: ReplaySubject<User> = new ReplaySubject();
  private currentUserSubject: BehaviorSubject<AuthService>;
  public currentUser: Observable<AuthService>;
  user: User;
  token: string;
  roles: any;
  isAuthenticated: boolean = false;
  menuEntries = new Array<Menu>();

  constructor(public dialog: MatDialog,
    public authService: AuthService,
    private snackBar: MatSnackBar,
    private notificationService: AppNotificationService,
    public menuSvc: AppMenuService,
    private router: Router) {
    this.authService.userSubject.subscribe(user => {
      this.user = user;
    });

    // subscribe for app-wide errors
    this.subscribeForAppErrors();

    // subscribe for app-wide messages
    this.subscribeForAppWarnings();

    // subscribe for app-wide messages
    this.subscribeForAppMessages();

    // subscribe for app-wide menu changes
    menuSvc.menuChanged.subscribe((newMenu: Array<Menu>) => {

      // avoid Angular's ExpressionChangedAfterCheck exception
      setTimeout(() => {
        this.menuEntries = newMenu;
      }, 100);
    });
  }

  subscribeForAppErrors(): void {
    this.notificationService.errorNotification.subscribe(
      issue => {
        this.snackBar.open(issue, '', this.createConfig('error'));
      });
  }

  subscribeForAppWarnings(): void {
    this.notificationService.warningNotification.subscribe(
      message => {
        this.snackBar.open(message, '', this.createConfig('warning'));
      });
  }

  subscribeForAppMessages(): void {
    this.notificationService.messageNotification.subscribe(
      message => {
        this.snackBar.open(message, '', this.createConfig(''));
      });
  }

  private createConfig(type: string) {
    const config = new MatSnackBarConfig();
    config.duration = 8000;
    if (type === 'error') {
      config.panelClass = "color-error";
    } else if (type === 'warning') {
      config.panelClass = "color-warning";
    } else {
      config.panelClass = "color-message";
    }
    return config;
  }

  logout() {
    this.authService.logout();
    this.menuSvc.getMenuEntries('');   
  }

  LoginDialog() {
    setTimeout(() => {
      const dialogRef1 = this.dialog.open(LoginComponent, {
        width: '450px',
        height: '330px',
        data: { message: 'New Remark By' },
        autoFocus: true,
        disableClose: true
      });
    }, 500);
  }
}
