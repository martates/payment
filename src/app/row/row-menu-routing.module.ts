import { RowMenuComponent } from './row-menu.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: RowMenuComponent
  }
];
@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})

export class RowMenuRoutingModule { }
