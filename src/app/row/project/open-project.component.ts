import { ProjectName } from './../../models/row-models';
import { Component, OnInit, NgModule } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ProjectService } from '../Services/project.service';
import { AppNotificationService } from '../../app-services/app-notification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-project',
  templateUrl: './open-project.component.html',
  styleUrls: ['./open-project.component.css']
})
export class ProjectComponent implements OnInit {

  projectNames: ProjectName[];
  selectedProject: ProjectName;

  constructor(private dialogRef: MatDialogRef<ProjectComponent>,
    private projectService: ProjectService,
    private messageNotificationService: AppNotificationService,
    private router: Router) { }

  ngOnInit() {
    this.projectService.getData('project-names', null, null)
      .subscribe(projectName => {
        this.projectNames = projectName
      });
  }

  close() {
    if (!this.selectedProject) {
     this.dialogRef.close();
      return;
    }
    this.projectService.getData('project', this.selectedProject, null)
      .subscribe(project => {
        this.dialogRef.close(project)
       e => this.messageNotificationService.errorNotification.next(e)
      });
  }

}
