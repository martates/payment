import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { AsyncDataFacade } from '../../shared/async-data-facade';

@Injectable()
export class OwnershipService  {

  constructor(private asyncDataFacade: AsyncDataFacade) { }
  getData(query: string, param: any, param2: any): Observable<any> {

    const projectPerKmId: number = <number>param;
    switch (query) {
      case 'ownershipList':
        return this.asyncDataFacade.get('/ownershipList/');

      case 'ownershipNamesPerKm':
        const url = '/PropjectPerKMs/' + projectPerKmId + '/ownershipdrops';
        return this.asyncDataFacade.get(url);

      case 'ownershipNames':
        const url1 = '/ownershipdrop';
        return this.asyncDataFacade.get( url1);

      case 'project-ownership':
        const url2 = '/PropjectPerKms/' + projectPerKmId + '/ownerships';
        return this.asyncDataFacade.get(url2);

      default:
        return Observable.of(null);
    }
  }
  postData(ownership: any): Observable<any> {
    return this.asyncDataFacade.post(`/ownership`, ownership);
  }
  putData(ownership: any, param: any): Observable<any> {
    // api/projectperkm/{projectperkmId}/ownerShip/{ownerShip
    const url = `/projectperkm/${ownership.projectperkmId}/ownerShip/${ownership.propertyId}`;
    return this.asyncDataFacade.put(url, ownership);
  }
  deleteData(Ownership: any, data: any, parm: any): Observable<any> {
    const url = `/deleteownership/${Ownership.ownershipId}`;
    return this.asyncDataFacade.delete(url, {});
  }
}
