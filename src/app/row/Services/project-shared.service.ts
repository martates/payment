import { ProjectComponent } from './../project/open-project.component';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs/Subject';
import { Project, ProjectByKm } from './../../models/row-models';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Payment } from '../../models/payment';

@Injectable()
export class ProjectSharedService {
  project: Project = {
    projectID: 0,
    name: '',
    number: '',
    roadLength: '',
    projectByKm: Array<ProjectByKm>(),
    payement: Array<Payment>(),
  };
  public projectStream: Subject<Project> = null;
  constructor(public dialog: MatDialog,
    private router: Router) {
    this.project = this.initProject();
    this.projectStream = new Subject();
  }

  openProject() {
   setTimeout(() => {
      const dialogRef = this.dialog.open(ProjectComponent, {
        width: '450px',
        data: { message: 'Open Project' },
        autoFocus: true,
        disableClose: true
      });


      dialogRef.afterClosed().subscribe(project => {
        this.project = project ? project : this.initProject();
        this.projectStream.next(this.project);

        if (!this.project.projectID) {
          this.router.navigate(['/home']);
        }
      });
   }, 100)
  }


  initProject(): Project {
    return {
      projectID: 0,
      number: '',
      name: '',
      roadLength: '',
      projectByKm: new Array<ProjectByKm>(),
      payement: Array<Payment>(),
    };
  }
}
