import { ProjectPerKmService } from './project-per-km.service';
import { ProjectSharedService } from './project-shared.service';
import { OwnershipService } from './ownership.services';
import { Ownership, EstimateCommitte, Property, ProjectByKm, ProjectPerKmName, OwnershipName } from './../../models/row-models';
import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';

@Injectable()
export class OwnershipSharedService {
  public projectPerKmStream = new Subject<ProjectPerKmName>();
  public ownershipStream: Subject<OwnershipName> = null;

  ownershipName: OwnershipName = {
    key: 0,
    value: null
  };
  projectPerKmName: ProjectPerKmName = {
    key: 0,
    value: null
  };

  projectByKm: ProjectByKm = {
    projectPerKmId: 0,
    projectId: 0,
    kmStart: 0,
    kmEnd: 0,
    state: null,
    woreda: null,
    zone: null,
    ownership: Array<Ownership>(),
    estimateCommitte: Array<EstimateCommitte>()
  };
  ownership: Ownership = {
    ownershipId: 0,
    ownershipName: null,
    projectPerKmId: 0,
    property: new Array<Property>()
  };

  constructor(private ownershipservice: OwnershipService,
    private projectSharedService: ProjectSharedService,
    private projectPerKmService: ProjectPerKmService  ) {
    this.ownershipStream = new Subject();
    this.projectPerKmStream = new Subject();
  }

  ownershipdrop() {
    this.ownershipservice.getData('ownershipNames', null, null)
      .subscribe(
        ownershiplistdrop => {
          this.ownershipName = ownershiplistdrop;
          this.ownershipStream.next(ownershiplistdrop);
        }
      );
  }
  ProjectPerKmdrop() {
    this.projectPerKmService.getData('project-per-km-drop', this.projectSharedService.project.projectID)
      .subscribe(
        projectPerKmlist => {
          this.projectPerKmName = projectPerKmlist;
          this.projectPerKmStream.next(projectPerKmlist);
        }
      );
  }
  getProjectproperty() {
    return this.ownershipservice.getData('ownershipNames', null, null)
      .subscribe(perKm => {
        this.ownershipStream.next(perKm);
      })
  }  
}

