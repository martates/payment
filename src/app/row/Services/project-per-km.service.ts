import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { ProjectByKm } from '../../models/row-models';
import { AsyncDataFacade } from '../../shared/async-data-facade';

@Injectable()
export class ProjectPerKmService {

  constructor(private asyncDataFacade:AsyncDataFacade) { }

  getData(query: string, param: any): Observable<any> {
    const projectId = <number>param;
    switch (query) {
      case 'project':
        return this.asyncDataFacade.get('/projects/' + projectId);

      case 'project-names':
        return this.asyncDataFacade.get('/projects/');

      case 'project-per-km':
        return this.asyncDataFacade.get('/projectperkm/');

      case 'project-per-kms':
        const url = '/projects/' + projectId + '/projectperkm';
        return this.asyncDataFacade.get(url);

      case 'project-per-km-drop':
      const url2 = '/projects/' + projectId + '/projectperkmdrop';
        return this.asyncDataFacade.get(url2);

      default:
        return Observable.of(null);
    }
  }
  postData(projectperkm: any): Observable<any> {
    return this.asyncDataFacade.post(`/projectperkm`, projectperkm);
  }
  putData(projectperkm: ProjectByKm,data: any, param: any): Observable<any> {
    const url = `/project/${projectperkm.projectId}/projectperkm/${projectperkm.projectPerKmId}`;
    return this.asyncDataFacade.put( url, projectperkm);
  }
  deleteData(projectByKm: ProjectByKm , parm: any): Observable<any> {
    const url = `/projectPerKm/${projectByKm.projectPerKmId}`;
    return this.asyncDataFacade.delete(url, {});  }
}


