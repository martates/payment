import { Subject } from 'rxjs/Subject';
import { OwnershipSharedService } from './ownership-shared.service';
import { Observable } from 'rxjs/Observable';
import { ProjectByKm, Ownership, Property } from './../../models/row-models';
import { Http } from '@angular/http';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class ReportService {
  private readonly ProjectPerKmEndpoint = environment.apiBaseUrl + '/projectperkm/';
  private readonly OwnershipEndpoint = environment.apiBaseUrl + '/ownershipList/';
  private readonly PropertyEndpoint = environment.apiBaseUrl + '/propertys/';

  public propertyReportStream: Subject<Property> = new BehaviorSubject<Property>(null);
  public propertyReportStreamperm: Subject<Property> = new BehaviorSubject<Property>(null);
  public propertyReportStreamtemp: Subject<Property> = new BehaviorSubject<Property>(null);

  constructor(private http: Http,
    private ownershipSharedService: OwnershipSharedService,
  ) { }
  getProjectByKm(param: number): Observable<ProjectByKm> {
    return this.http.get(this.ProjectPerKmEndpoint)
      .map(res => res.json()
        .map(data1 => data1))
    //  this.projectPerKmReportStream.next(data1)))
  }

  getOwner(id: number): Observable<Ownership> {
    return this.http.get(this.OwnershipEndpoint)
      .map(res => res.json()
        .filter((data) => data.projectPerKmId === id)
        .map(data1 => data1))
  }
  getProperty(id: number): Observable<any> {
    return this.http.get(this.PropertyEndpoint)
      .map(res => res.json()
        .filter((data) => data.ownershipId === id)
        .map(data1 => data1));
  }

  ownershiptype(id: number): Observable<any> {
    return this.http.get(this.PropertyEndpoint)
      .map(res => res.json()
        .filter((data) => data.ownershipId === id)
        .map((temp) => temp.type)
      )
  }

  ownershipTotal(id: number): Observable<any> {
    return this.http.get(this.PropertyEndpoint)
      .map(res => res.json()
        .filter((data) => data.ownershipId === id)
        .map((temp) => temp.totalPrice)
      )
  }

  ProjectPropertysharedService() {
    this.ownershipSharedService.ProjectPerKmdrop();
    this.ownershipSharedService.projectPerKmStream.subscribe(projectPerKmName => {
      var newArray = [];
      newArray.push(projectPerKmName)
      for (var i = 0; i < newArray[0].length; i++) {
        var arrayofProjectperkm = newArray[0][i].key
        this.getOwner(arrayofProjectperkm)
          .subscribe(selectedOwnership => {
            var newOwnership = [];
            newOwnership.push(selectedOwnership)
            for (var j = 0; j < newOwnership[0].length; j++) {
              var arrayofOwnership = newOwnership[0][j].ownershipId
              this.getProperty(arrayofOwnership)
                .subscribe(selectedProperty => {
                  // console.log(selectedProperty.length)
                  return this.propertyReportStream.next(selectedProperty)
                })
            }
          })
      }
    });
  }
}
