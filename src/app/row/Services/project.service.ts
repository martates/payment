import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AsyncDataFacade } from '../../shared/async-data-facade';

@Injectable()
export class ProjectService {

  constructor(private http: HttpClient,
    private asyncDataFacade: AsyncDataFacade) { }

  getData(query: string, param: any, param2: any): Observable<any> {
    switch (query) {
      case 'project-names':
      return this.asyncDataFacade.get('/projects/');

      case 'project':
        const projectID = <number>param;
        return this.asyncDataFacade.get('/projects/' + projectID);

      default:
        return Observable.of(null);
    }
  }
  postData(data: any, param: any): Observable<any> {
    throw new Error('Method not implemented.');
  }
  putData(data: any, param: any): Observable<any> {
    throw new Error('Method not implemented.');
  }
  deleteData(data: any, param: any): Observable<any> {
    throw new Error('Method not implemented.');
  }
}
