import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { Ownership } from '../../models/row-models';
import { AsyncDataFacade } from '../../shared/async-data-facade';
import { Http } from '../../../../node_modules/@angular/http';


@Injectable()
export class PropertyService {
  constructor(private asyncDataFacade: AsyncDataFacade, 
    private http2: Http) { }
  getData(query: string, param: any, ownership: Ownership): Observable<any> {
    const contractId: number = <number>param;
    const ownershipId: number = <number>param;
    switch (query) {
      case 'Property-type':
        return this.asyncDataFacade.get(`/propertytypes`);

      case 'ownershipNames':
        return this.asyncDataFacade.get(`/ownerships`);

      case 'contract':
        return this.asyncDataFacade.get( '/contracts/' + contractId);

      case 'property':
        return this.asyncDataFacade.get(`/propertys`);

      case 'selected-property':
        const url = '/ownership/' + ownershipId + '/propertys';
        return this.asyncDataFacade.get(url);

        default:
        return Observable.of(null);
    }
  }
  private readonly PropertyEndpoint = environment.apiBaseUrl + '/propertys';
  getperm() {
    return this.http2.get(this.PropertyEndpoint)
      .map(res => res.json()
        .filter(perm => perm.isPermanent === "Permanent")
        .map((perm) => perm)
      )
  }
  gettemp() {
    return this.http2.get(this.PropertyEndpoint)
      .map(res => res.json()
        .filter(temp => temp.isPermanent === "Temporary")
        .map((temp) => temp
        ))
  }
  ownershiptype() {
    return this.http2.get(this.PropertyEndpoint)
     .map(res => res.json() 
     .map((temp) => temp.type)
    // .map((temp)=>temp.totalPrice)
      )
  }
  ownershipTotal() {
    return this.http2.get(this.PropertyEndpoint)
     .map(res => res.json() 
     .map((temp) => temp.totalPrice)
      )
  }
  postData(property: any): Observable<any> {
    return this.asyncDataFacade.post( `/property`, property);
  }
  putData(property: any, data: any, param: any): Observable<any> {
    const url = `/ownership/${property.ownershipId}/property/${property.propertyId}`;
    return this.asyncDataFacade.put( url, property);
  }
  deleteData(property: any, data: any, parm: any): Observable<any> {
    const url = `/property/${property.propertyId}`;
    return this.asyncDataFacade.delete(url, {});
  }
}
