import { Component, OnInit } from '@angular/core';
import { AppMenuService } from '../app-services/app-menu.service';

@Component({
  selector: 'app-row-menu',
  templateUrl: './row-menu.component.html',
  styleUrls: ['./row-menu.component.css']
})
export class RowMenuComponent implements OnInit {

  constructor(    private menuSvc: AppMenuService) {}

  ngOnInit() {
    this.menuSvc.getMenuEntries('row-management');
  }


}
