import { RowMenuRoutingModule } from './row-menu-routing.module';
import { ProjectSharedService } from './Services/project-shared.service';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ChartsModule, ChartModule } from '@progress/kendo-angular-charts';
import { RowMenuComponent } from './row-menu.component';

// import {
//    MatSelectModule,
//   MatOptionModule,
//   MatPaginatorModule,
//   MatTableModule
// } from '@angular/material';
// import { ChartComponent } from './report/chart/chart.component';
// import { LineChartComponent } from './report/line-chart/line-chart.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { ProjectPerKmService } from './Services/project-per-km.service';
import { PropertyService } from './Services/property.service';
import { AppMaterialModule } from '../app-material.module';
import { MaterialModule } from '../material.module';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule, HttpModule,
    ReactiveFormsModule, FormsModule,
    MaterialModule,
    // AppMaterialModule,
    // MatOptionModule, MatSelectModule,
    // MatPaginatorModule,MatTableModule,
    // MatOptionModule,
    
    RowMenuRoutingModule,
    GridModule,ChartsModule, ChartModule,
  ],

  declarations: [
    RowMenuComponent,
    //ChartComponent,LineChartComponent
  ],
  providers: [
    ProjectSharedService,ProjectPerKmService,PropertyService
  ],

})
export class RowMenuModule { }
