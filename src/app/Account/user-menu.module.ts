import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserMenuComponent } from './user-menu.component';
import { MaterialModule } from '../material.module';
import { UserMenuRoutingModule } from './user-menu-routing.module';



@NgModule({
  imports: [
    CommonModule,
    UserMenuRoutingModule,
    MaterialModule,
  ],
  declarations: [UserMenuComponent
  ],
  // providers:[UserCreateService]
  
})
export class UserMenuModule { }
