import { Injectable } from '@angular/core';
import { User } from '../../models/user';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs';
import { UserCreateService } from './user-create.service';
import { MatDialog } from '@angular/material/dialog';
import { OpenUserComponent } from '../open-user/open-user.component';
import { Router } from '@angular/router';

@Injectable()
export class UserSharedService {
  public userStream: Subject<User> = null; 
  public user: User;
  // user: User = {
  //   userId: '',
  //   userName: '',
  //   firstName: '',
  //   lastName: '',
  //   position: '',
  //   directorate: ''
  // };

  constructor(public dialog: MatDialog,
    private router: Router) {
    this.user = this.initUser();
    this.userStream = new Subject();
  }  

  openUser() {
    setTimeout(() => {
      const dialogRef = this.dialog.open(OpenUserComponent, {
        width: '450px',
        data: { message: 'Open User' },
        autoFocus: true,
        disableClose: true
      });

      dialogRef.afterClosed().subscribe(user => {
        this.user = user ? user : this.initUser();
        this.userStream.next(this.user);

        if (!this.user.userId) {
          this.router.navigate(['/home']);
        }
      });
    }, 100)
  }


  initUser(): User {
    return {
      userId: '',
      userName: '',
      firstName: '',
      lastName: '',
      position: '',
      directorate: ''
    };
  }

}
