import { Injectable } from '@angular/core';
import { AsyncDataFacade } from '../../shared/async-data-facade';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../../auth/auth.service';

@Injectable()
export class UserCreateService {

  constructor(private asyncDataFacade: AsyncDataFacade,) { }
  postData(newuser: any): Observable<any> {
    return this.asyncDataFacade.post(`/newUserCreate`, newuser);
  }

  postMembership(membership: any): Observable<any> {
    return this.asyncDataFacade.post(`/Auth/createMembership`, membership);
  }
  // putData(payment: any, param: any): Observable<any> {
  //   // api/projectperkm/{projectperkmId}/ownerShip/{ownerShip
  //   const url = `/project/${payment.id}/payment/${payment.id}`;
  //   return this.asyncDataFacade.put(url, payment);
  // }
  // deleteData(paymnet: Payment, parm: any): Observable<any> {
  //   const url = `/payment/${paymnet.id}`;
  //   return this.asyncDataFacade.delete(url, {});
  // }
  getData(query: string, param: any): Observable<any> {
    const username = <string>param;
    switch (query) {
      case 'get-user':
        return this.asyncDataFacade.get('/getUser/' + username);

      case 'all-User-List':
        return this.asyncDataFacade.get('/getAllUser/');

      case 'single-User':
        const userId = <number>param;
        return this.asyncDataFacade.get('/getSingleUser/' + userId);
      case 'postition':
        return this.asyncDataFacade.get('/getAllPosition/');

      case 'directorate':
        return this.asyncDataFacade.get('/getAllDirectorate/');

      case 'roleList':
        return this.asyncDataFacade.get('/getAllRole/');
      case 'roleListbasedonUserId':
       const userId1 = <number>param;
        return this.asyncDataFacade.get('/getUserInRole/'+ userId1 );
      default:
        return Observable.of(null);
    }
  }
}
