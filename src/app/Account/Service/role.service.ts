import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AsyncDataFacade } from '../../shared/async-data-facade';
import { AuthService } from '../../auth/auth.service';
import { User } from '../../models/user';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
 // user: User;
  constructor(private asyncDataFacade: AsyncDataFacade,
   // public authService: AuthService,
    ) { }

  // getUserId() {
  //   this.authService.userSubject.subscribe(user => {
  //     this.user = user;
  //   });
  // }


  postData(userInRole: any): Observable<any> {
    return this.asyncDataFacade.post(`/addUserInRole`, userInRole);
  }

  getData(query: string, param: any): Observable<any> {
    const username = <string>param;
    switch (query) {

      case 'roleList':
        return this.asyncDataFacade.get('/getAllRole/');
      // case 'roleListbasedonUserId':
      //   return this.asyncDataFacade.get('/getUserInRole/' + this.user.userId);
      default:
        return Observable.of(null);
    }
  }
}
