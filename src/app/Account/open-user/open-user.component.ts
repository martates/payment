import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription, Subject } from 'rxjs';
import { User } from '../../models/user';
import { UserCreateService } from '../Service/user-create.service';
import { AppNotificationService } from '../../app-services/app-notification.service';

@Component({
  selector: 'app-open-user',
  templateUrl: './open-user.component.html',
  styleUrls: ['./open-user.component.css']
})
export class OpenUserComponent implements OnInit {
  public userStream: Subject<User> = null;
  private subscription: Subscription;
  userNames: User[];
  selectedUser: User;
  //userName: string;
  openUserForm: FormGroup;
  addAmendLabel: string;
  constructor(private formmodel: FormBuilder,
    private dialogRef: MatDialogRef<OpenUserComponent>,
    private userCreateService: UserCreateService,
    private messageNotificationService: AppNotificationService,
  ) { }

  ngOnInit(): void {
    this.getAllUserList();
  }

  close() {
    if (!this.selectedUser) {
      this.dialogRef.close();
      return;
    }
    this.userCreateService.getData('single-User', this.selectedUser)
      .subscribe(user => {
        this.dialogRef.close(user)
        e => this.messageNotificationService.errorNotification.next(e)
      });
  }

  public getAllUserList() {
    this.subscription = this.userCreateService.getData('all-User-List', null).subscribe(
      allUserList => {
        this.userNames = allUserList;
      });
  }

}
