import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { UserCreateRoutingModule } from './user-create-routing.module';
import { MaterialModule } from '../../material.module';
import { UserCreateComponent } from './user-create.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserCreateService } from '../Service/user-create.service';
import { UserSharedService } from '../Service/user-shared.service';



@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    UserCreateRoutingModule,
    MaterialModule
  ],
  declarations: [UserCreateComponent],
  providers:[UserCreateService,DatePipe,UserSharedService ]

})
export class UserCreateModule { }
