import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription, Subject } from 'rxjs';
import { NewUser, MemberShip, User, Position,Directorate } from '../../models/user';
import { AppNotificationService } from '../../app-services/app-notification.service';
import { UserCreateService } from '../Service/user-create.service';
import { DatePipe } from '@angular/common';
import { MatCheckbox } from '@angular/material/checkbox';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  falseValue = '0';
  trueValue = '1';
  checkboxChange(checkbox: MatCheckbox, checked: number) {
    checkbox.value = checked ? this.trueValue : this.falseValue;
  }
  public userStream: Subject<User> = null;
  position: Position = {
    Position: null
  };
  directorate: Directorate = {
    Directorate: null
  };
  user: User = {
    userId: null,
    userName: null,
    firstName: null,
    lastName: null,
    position: null,
    directorate: null,
  };
  public length: number = 5;
  dataSource: any;
  displayedColumns: string[];
  memberShip: MemberShip;
  private subscription: Subscription;
  addAmendLabel: string;
  deleteResetLabel: string;
  userCreateForm: FormGroup;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(private formmodel: FormBuilder,
    private userCreateService: UserCreateService,
    private messageNotificationService: AppNotificationService,
    private datePipe: DatePipe,
  ) { this.userStream = new Subject() }

  private refreshFormAndTable(): void {
    this.addAmendLabel = 'Save';
    this.userCreateForm.reset();
    this.createForm();
  }
  public getCanDelete(): boolean {
    return !this.userCreateForm.pristine;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  ngOnInit() {
    // this.checkbox.value = this.checkbox.checked ? this.trueValue : this.falseValue;
    this.addAmendLabel = 'Save';
    this.deleteResetLabel = 'Clear';
    this.createForm();
    this.getAllUserList();
    this.getAllPositionList();
    this.getAllDirectorateList();
    this.displayedColumns = ['badgeNumber', 'userName', 'firstName', 'lastName', 'directorate', 'position'];
  }

  private createForm(): void {
    this.userCreateForm = this.formmodel.group({
      applicationIdControl: [0, Validators.required],
      //  userIdControlControl: [0, Validators.required],
      firstNameControl: [null, Validators.required],
      lastNameControl: [null, Validators.required],
      userNameControl: [null, Validators.required],
      positionControl: [null, Validators.required],
      directorateControl: [null, Validators.required],
      badgeNumberControl: [null, Validators.required],
      telephoneControl: [null, Validators.required],
      loweredUserNameControl: [null, Validators.required],
      mobileAliasControl: [null, Validators.required],
      isAnonymousControl: [null, Validators.required],
      lastActivityDateControl: this.datePipe.transform(Date.now(), 'MM/dd/yyyy'), //whatever format you need. 
      lastEditedByControl: [null, Validators.required],

      //FormControl Related with MemberShip
      passwordControl: [null, Validators.required],
      emailControl: [null, Validators.required],
      isApprovedControl: [0, Validators.required],
      isLockedOutControl: [0, Validators.required],
      CreateDateControl: this.datePipe.transform(Date.now(), 'MM/dd/yyyy'), //whatever format you need. 
      LastLoginDateControl: this.datePipe.transform(Date.now(), 'MM/dd/yyyy'), //whatever format you need. 
      LastPasswordChangedDateControl: this.datePipe.transform(Date.now(), 'MM/dd/yyyy'), //whatever format you need. 
      LastLockoutDateControl: this.datePipe.transform(Date.now(), 'MM/dd/yyyy'), //whatever format you need. 
      FailedPasswordAttemptCountControl: [null, Validators.required],
      FailedPasswordAttemptWindowStartControl: this.datePipe.transform(Date.now(), 'MM/dd/yyyy'), //whatever format you need. 
      FailedPasswordAnswerAttemptCountControl: [null, Validators.required],
      FailedPasswordAnswerAttemptWindowStartControl: this.datePipe.transform(Date.now(), 'MM/dd/yyyy'), //whatever format you need. 
      CommentControl: [null, Validators.required],
    });
  }

  save(): void {
    const r: NewUser = this.getCreateUserFromFormModel();
    this.subscription = this.userCreateService.postData(r).subscribe(
      remark => {
        this.getuserId();
      },
      e => this.messageNotificationService.errorNotification.next(e.error())
    );

  }
  save2(): void {
    const y: MemberShip = this.getCreateUserFromFormModel2();
    this.subscription = this.userCreateService.postMembership(y).subscribe(
      memberShip => {
        this.messageNotificationService.messageNotification.next('User Create Successfully');
      },
       e => this.messageNotificationService.errorNotification.next(e.error())
    );
    this.refreshFormAndTable();
    this.getAllUserList();
  }

  removeUserFromList(): void {
    if (this.deleteResetLabel === 'Delete') {
      // const r: User = this.getCreateUserFromFormModel();
      //   this.subscription = this.paymentService.deleteData(r, null).subscribe(data => {
      //     this.paymentList();
      //     this.refreshFormAndTable();
      //     this.messageNotificationService.messageNotification.next('Payment Remove from the list');
      //     this.addAmendLabel = 'Save';
      //     this.deleteResetLabel = 'Clear';
      //   },
      //     e => this.messageNotificationService.errorNotification.next(e.error));
    }
    else {
      this.refreshFormAndTable();
    }
  }

  public getuserId(): void {
    this.subscription = this.userCreateService.getData('get-user', this.userCreateForm.controls['userNameControl'].value).subscribe(
      remark => {
        this.user = remark[0];
        this.userStream.next(this.user)
        this.save2()
      });
    console.log(this.user.userId)
  }
  public getAllUserList() {
    this.subscription = this.userCreateService.getData('all-User-List', null).subscribe(
      allUserList => {
        this.dataSource = new MatTableDataSource<User>(allUserList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }
  public getAllPositionList() {
    this.subscription = this.userCreateService.getData('postition', null).subscribe(
      allPostionList => {
        this.position = allPostionList;
      });
  }
  public getAllDirectorateList() {
    this.subscription = this.userCreateService.getData('directorate', null).subscribe(
      allPostionList => {
        this.directorate = allPostionList;
      });
  }
  private getCreateUserFromFormModel(): NewUser {
    // this.userStream.
    const formModel = this.userCreateForm.value;
    const r: NewUser = {
      ApplicationId: "8A9605D1-522A-495E-98E7-795B58D0C496",
      // UserId: "",
      FirstName: formModel.firstNameControl,
      LastName: formModel.lastNameControl,
      UserName: formModel.userNameControl,
      Position: formModel.positionControl,
      Directorate: formModel.directorateControl,
      BadgeNumber: formModel.badgeNumberControl,
      Telephone: formModel.telephoneControl,
      LoweredUserName: formModel.userNameControl,
      MobileAlias: "NULL",
      IsAnonymous: 0,
      LastActivityDate: formModel.lastActivityDateControl,
      LastEditedBy: "Letarikt"
    };
    return r;
  }

  private getCreateUserFromFormModel2(): MemberShip {
    const formModel = this.userCreateForm.value;
    const y: MemberShip = {
      ApplicationId: "8A9605D1-522A-495E-98E7-795B58D0C496",
      UserId: this.user.userId,
      // UserId: this.user.userId,
      Password: formModel.passwordControl,
      PasswordFormat: 1,
      PasswordSalt: null,
      MobilePIN: null,
      Email: formModel.emailControl,
      LoweredEmail: formModel.emailControl,
      PasswordQuestion: null,
      PasswordAnswer: null,
      IsApproved: formModel.isApprovedControl,
      IsLockedOut: formModel.isLockedOutControl,
      CreateDate: formModel.CreateDateControl,
      LastLoginDate: formModel.LastLoginDateControl,
      LastPasswordChangedDate: formModel.LastPasswordChangedDateControl,
      LastLockoutDate: formModel.LastLockoutDateControl,
      FailedPasswordAttemptCount: 0,
      FailedPasswordAttemptWindowStart: formModel.FailedPasswordAttemptWindowStartControl,
      FailedPasswordAnswerAttemptCount: 0,
      FailedPasswordAnswerAttemptWindowStart: formModel.FailedPasswordAnswerAttemptWindowStartControl,
      Comment: "",
    };
    return y;
  }

  public selectUser(user: User): void {
    this.userCreateForm.setValue({
      firstNameControl: user.firstName,
      lastNameControl: user.lastName,
      userNameControl: user.userName,
      positionControl: user.position,
      directorateControl: user.directorate,
    });
    this.userCreateForm.markAsDirty();
    this.addAmendLabel = 'Amend';
    this.deleteResetLabel = 'Delete';
  }

}
