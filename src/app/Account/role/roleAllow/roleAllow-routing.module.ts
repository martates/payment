import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RoleAllowComponent } from './roleAllow.component';

const routes: Routes = [
  {
    path: '',
    component: RoleAllowComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoleAllowRoutingModule { }
