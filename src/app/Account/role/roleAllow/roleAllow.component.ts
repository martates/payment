import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserCreateService } from '../../Service/user-create.service';
import { Role, UserInRole } from '../../../models/user';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { MatCheckbox } from '@angular/material/checkbox';
import { MatDialog } from '@angular/material/dialog';
import { OpenUserComponent } from '../../open-user/open-user.component';
import { RoleNewComponent } from '../roleNew/role-new.component';
import { UserSharedService } from '../../Service/user-shared.service';
import { AppNotificationService } from '../../../app-services/app-notification.service';
import { RoleService } from '../../Service/role.service';

@Component({
  selector: 'app-role',
  templateUrl: './roleAllow.component.html',
  styleUrls: ['./roleAllow.component.css']
})
export class RoleAllowComponent implements OnInit {
  addAmendLabel: string;
  deleteResetLabel: string;
  private subscription: Subscription;
  userRoleForm: FormGroup;
  falseValue = 'Null';
  trueValue = '1';

  role: Role = {
    ApplicationId: null,
    RoleId: null,
    RoleName: null,
    LoweredRoleName: null,
    Description: null
  };

  constructor(public dialog: MatDialog,
    private userCreateService: UserCreateService,
    private formmodel: FormBuilder,
    public userSharedService: UserSharedService,
    private messageNotificationService: AppNotificationService,
    public roleService: RoleService,
  ) { }

  ngOnInit(): void {
    this.addAmendLabel = 'OpenUser';
    this.deleteResetLabel = 'Clear';
    this.getAllRoleList();
    this.createForm();
    this.subscription = this.userSharedService.userStream.subscribe(userResult => {

    });
  }
  openUserDialog(): void {
    this.userSharedService.openUser();
    this.addAmendLabel = "Save"
  }
  init() {
    if (this.userSharedService.user.userId === "") {
      this.openUserDialog();
      return;
    }
  }
  private createForm(): void {
    this.userRoleForm = this.formmodel.group({
      userIdControl: [null, Validators.required],
      userNameListControl: [null, Validators.required],
      roleNameListControl: this.formmodel.array([])
    });
  }

  onChange(roleId: string, isChecked: boolean) {
    const roleIdFormArray = <FormArray>this.userRoleForm.controls.roleNameListControl;
    if (isChecked) {
      roleIdFormArray.push(new FormControl(roleId));
    } else {
      let index = roleIdFormArray.controls.findIndex(x => x.value == roleId)
      roleIdFormArray.removeAt(index);
    }
  }


  public save() {
    if (this.addAmendLabel === 'Save') {

      for (var i = 0; i <= [this.userRoleForm.controls.roleNameListControl.value].length; i++) {
        const formModel = this.userRoleForm.value;
        const r: UserInRole = {
          RoleId: this.userRoleForm.controls.roleNameListControl.value[i],
          UserId: this.userSharedService.user.userId
        };
        this.subscription = this.roleService.postData(r).subscribe(
          remark => {
            this.messageNotificationService.messageNotification.next('Give a role');
          },
          e => this.messageNotificationService.errorNotification.next(e.error())
        );
      }
    }
    else {
      this.openUserDialog();
    }
  }


  public getCanDelete(): boolean {
    return !this.userRoleForm.pristine;
  }
  addroleDialog() {
    setTimeout(() => {
      const dialogRef1 = this.dialog.open(RoleNewComponent, {
        width: '450px',
        data: { message: 'User List' },
        autoFocus: true,
        disableClose: true,
      });
    }, 500);
  }
  public getAllRoleList() {
    this.subscription = this.userCreateService.getData('roleList', null).subscribe(
      allRoleList => {
        this.role = allRoleList;
      });
  }

  public refreshForm(): void {
    this.addAmendLabel = 'OpenUser';
    this.userRoleForm.reset();
    this.createForm();
  }

}
