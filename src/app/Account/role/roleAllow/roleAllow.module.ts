import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { RoleAllowComponent } from './roleAllow.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RoleAllowRoutingModule } from './roleAllow-routing.module';
import { MaterialModule } from '../../../material.module';
import { UserCreateService } from '../../Service/user-create.service';
import { UserSharedService } from '../../Service/user-shared.service';
import { OpenUserComponent } from '../../open-user/open-user.component';
import { RoleNewComponent } from '../roleNew/role-new.component';



@NgModule({
  declarations: [RoleAllowComponent,OpenUserComponent,RoleNewComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RoleAllowRoutingModule,
    MaterialModule
  ],
  providers:[UserCreateService,DatePipe,UserSharedService ],
 entryComponents: [OpenUserComponent,RoleNewComponent],

})
export class RoleAllowModule { }
