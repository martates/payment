import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleAllowComponent } from './roleAllow.component';

describe('RoleComponent', () => {
  let component: RoleAllowComponent;
  let fixture: ComponentFixture<RoleAllowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleAllowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleAllowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
