import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-role-new',
  templateUrl: './role-new.component.html',
  styleUrls: ['./role-new.component.css']
})
export class RoleNewComponent implements OnInit {
  userName: string;
  openUserForm: FormGroup;
  addAmendLabel: string;
  formmodel: any;
  constructor(private dialogRef: MatDialogRef<RoleNewComponent>) { }

  ngOnInit(): void {
  }
  close() {
    if (!this.userName) {
      this.dialogRef.close();
      return;
    }
  }
  private createForm(): void {
    this.openUserForm = this.formmodel.group({
      roleIdControl: [0, Validators.required],
      userNameControl: [null, Validators.required],
    });
  }
  public save() { }
}
