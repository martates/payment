import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
// import { MatProgressSpinnerModule } from '@angular/material';

import { BusyIndicatorComponent } from './busy-indicator.component';
import { BusyIndicatorService } from './busy-indicator.service';
import { AppMaterialModule } from '../app-material.module';
import { MaterialModule } from '../material.module';


@NgModule({
  imports: [
    CommonModule,
    // MatProgressSpinnerModule,
    // AppMaterialModule
    MaterialModule
  ],
  declarations: [BusyIndicatorComponent],
  providers: [BusyIndicatorService],
  entryComponents: [BusyIndicatorComponent],
  exports: [BusyIndicatorComponent]
})
export class BusyIndicatorModule { }
