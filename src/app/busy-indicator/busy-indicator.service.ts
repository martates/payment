import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { BusyIndicatorComponent } from './busy-indicator.component';

@Injectable()
export class BusyIndicatorService {

  private dialogRef: MatDialogRef<BusyIndicatorComponent>;
  private isOpen: boolean = false;

  constructor(private dialog: MatDialog) { }

  showBusySignal() {

    if (this.isOpen) {
      return;
    }

    this.dialogRef = this.dialog.open(BusyIndicatorComponent, {
      width: '150px',
      data: { message: '' },
      autoFocus: true,
      disableClose: true
    });

    this.isOpen = true;

  }

  closeBusySignal() {
    this.dialogRef.close();
    this.isOpen = false;
  }
}
