import { HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppMenuService } from './app-services/app-menu.service';
import { AppNotificationService } from './app-services/app-notification.service';
import { AppErrorHandler } from './app-services/app.error-handler';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { AuthService } from './auth/auth.service';
import { BusyIndicatorModule } from './busy-indicator/busy-indicator.module';
import { ContractDataService } from './contract/contract-data.service';
import { ContractService } from './contract/contract.service';
import { OpenContractComponent } from './contract/open-contract.component';
import { HomeComponent } from './home/home.component';
import { AsyncDataFacade } from './shared/async-data-facade';
import { SharedModule } from './shared/shared.module';
import { AuthGuard } from './auth/auth.guard';
import { LoginComponent } from './auth/login.component';
import { ProjectComponent } from './row/project/open-project.component';
import { RowMenuModule } from './row/row-menu.module';
import { ProjectService } from './row/Services/project.service';
import { PurchaseMenuModule } from './purchase_Order/purchase-menu.module';
import { PaymentMenuModule } from './Payment/payment-menu.module';
import { ReportComponent } from './Payment/report/report.component';
import { PaymentService } from './Payment/Services/payment.service';
import { MatDialogRef } from '@angular/material/dialog';
import { MaterialModule } from './material.module';
import { UserCreateService } from './Account/Service/user-create.service';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    OpenContractComponent,
    ProjectComponent,
    LoginComponent,
    ReportComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    BusyIndicatorModule,
    HttpClientModule,
    MaterialModule,
    SharedModule,
    AppRoutingModule,
    RowMenuModule,
    PurchaseMenuModule,
    PaymentMenuModule
  ],
  providers: [AppNotificationService, AsyncDataFacade, ProjectService, PaymentService,
    { provide: ErrorHandler, useClass: AppErrorHandler },
    { provide: MatDialogRef, useValue: {} },
    AppMenuService,
    ContractDataService,
    ContractService,
    AuthService,
    AuthGuard,
    UserCreateService
  ],
  entryComponents: [OpenContractComponent, ProjectComponent, LoginComponent,
    //RnmdsComponent, SectionComponent, RoadClassComponent
  ],
  // providers: [UserCreateService]

  bootstrap: [AppComponent]
})
export class AppModule { }
