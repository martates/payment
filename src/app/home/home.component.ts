import { Component, OnInit } from '@angular/core';

import { AppMenuService } from '../app-services/app-menu.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private menuSvc: AppMenuService) {
  }

  ngOnInit() {
    this.menuSvc.getMenuEntries('home');
  }

}
