import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { AsyncDataFacade } from './async-data-facade';
import { Deliverable } from '../models/deliverable';
import { SaveConsultantsPerformance } from '../models/save-consultants-performance';

@Injectable()
export class WorkProgressDataService {

  constructor(private asyncDataFacade: AsyncDataFacade) { }

  getData(query: string, param: any): Observable<any> {
    const contractId = <number>param;
    switch (query) {
      case 'contract-deliverables':
        return this.asyncDataFacade.get(`/contracts/${contractId}/deliverables`);
      case 'performances':
        return this.asyncDataFacade.get(`/contracts/${contractId}/consultantsPerformances`);
      case 'programs':
        return this.asyncDataFacade.get(`/contracts/${contractId}/consultantsPrograms`);
        
      default:
        return Observable.of(null);
    }
  }

  postData(query: string, performance: SaveConsultantsPerformance, param: any): Observable<any> {
    const contractId = <number>param;
    return this.asyncDataFacade.post(`/contracts/${contractId}/consultantsPerformances`, performance);
  }

  putData(query: string, performance: SaveConsultantsPerformance, param: any): Observable<any> {
    const contractId = <number>param;
    return this.asyncDataFacade.put(`/contracts/${contractId}/consultantsPerformances`, performance);
  }

  deleteData(query: string, performance: SaveConsultantsPerformance, param: any): Observable<any> {
    const contractId = <number>param;
    return this.asyncDataFacade.delete(`/contracts/${contractId}/consultantsPerformances/${performance.id}`);
  }
}
