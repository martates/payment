import { Component, OnInit } from '@angular/core';

import { ContractService } from '../../contract/contract.service';
import { ContractPhaseComputeService } from '../contract-phase-compute.service';
import { DeliverablesService } from '../deliverables.service';

@Component({
  selector: 'app-basic-side-info',
  templateUrl: './basic-side-info.component.html',
  styleUrls: ['./basic-side-info.component.css']
})
export class BasicSideInfoComponent implements OnInit {

  deliverablesExist: boolean;

  constructor(
    private deliverableDataService: DeliverablesService,
    public contractService: ContractService,
    public computeService: ContractPhaseComputeService) { }

  ngOnInit() {
    this.contractService.contractStream.subscribe(contract => {

      if (contract && contract.id > 0) {
        this.deliverableDataService.getData('deliverables', this.contractService.contract.id)
          .subscribe(data => {
            if (data && data.length > 0) {
              this.deliverablesExist = true;
            }
          });
      }
      
    });

  }

}
