import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { BasicSideInfoComponent } from './basic-side-info.component';
import { PhaseSideInfoComponent } from './phase-side-info/phase-side-info.component';
import { SupplementSideInfoComponent } from './supplement-side-info/supplement-side-info.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [BasicSideInfoComponent, PhaseSideInfoComponent, SupplementSideInfoComponent],
  exports: [BasicSideInfoComponent, PhaseSideInfoComponent, SupplementSideInfoComponent]
})
export class SideInfoModule { }
