import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhaseSideInfoComponent } from './phase-side-info.component';

describe('PhaseSideInfoComponent', () => {
  let component: PhaseSideInfoComponent;
  let fixture: ComponentFixture<PhaseSideInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhaseSideInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhaseSideInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
