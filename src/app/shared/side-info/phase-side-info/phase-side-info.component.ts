import { Component, Input, OnInit } from '@angular/core';

import { ContractService } from '../../../contract/contract.service';
import { ContractPhase } from '../../../models/contract-phase';
import { ContractPhaseComputeService } from '../../contract-phase-compute.service';

@Component({
  selector: 'app-phase-side-info',
  templateUrl: './phase-side-info.component.html',
  styleUrls: ['./phase-side-info.component.css']
})
export class PhaseSideInfoComponent implements OnInit {

  @Input() programExists: boolean;
  phasesValid: boolean;
  validPhases: ContractPhase[];

  @Input()
  set phases(phases) {

    this.validPhases = new Array<ContractPhase>();

    if (phases) {
      phases.forEach(phase => {
        const filtered = phases.filter(p => phase.id === p.contractPhaseId);
        if (filtered.length === 0) {
          this.validPhases.push(phase);
        }
      });
    }
  }
  get phases() {
    return this.validPhases;
  }

  constructor(public contractService: ContractService,
    private phaseComputeService: ContractPhaseComputeService) { }

  ngOnInit() {
    this.phasesValid = this.phaseComputeService.validatePhasesTotal();
  }

}
