import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplementSideInfoComponent } from './supplement-side-info.component';

describe('SupplementSideInfoComponent', () => {
  let component: SupplementSideInfoComponent;
  let fixture: ComponentFixture<SupplementSideInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplementSideInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplementSideInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
