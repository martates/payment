import { Component, Input, OnInit } from '@angular/core';

import { ContractService } from '../../../contract/contract.service';
import { ContractPhase } from '../../../models/contract-phase';
import { ContractPhaseComputeService } from '../../contract-phase-compute.service';

@Component({
  selector: 'app-supplement-side-info',
  templateUrl: './supplement-side-info.component.html',
  styleUrls: ['./supplement-side-info.component.css']
})
export class SupplementSideInfoComponent implements OnInit {

  @Input() supplements;
  phases: ContractPhase[];
  phasesValid: boolean;
  
  constructor(public contractService: ContractService,
    private phaseComputeService: ContractPhaseComputeService) { }

  ngOnInit() {
    this.phasesValid = this.phaseComputeService.validatePhasesTotal();
    this.phases = this.contractService.contract.phases.filter(p => p.supplementId === null);
  }

}
