import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicSideInfoComponent } from './basic-side-info.component';

describe('BasicSideInfoComponent', () => {
  let component: BasicSideInfoComponent;
  let fixture: ComponentFixture<BasicSideInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicSideInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicSideInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
