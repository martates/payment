import { TestBed, inject } from '@angular/core/testing';

import { DeliverablesService } from './deliverables.service';

describe('DeliverablesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeliverablesService]
    });
  });

  it('should be created', inject([DeliverablesService], (service: DeliverablesService) => {
    expect(service).toBeTruthy();
  }));
});
