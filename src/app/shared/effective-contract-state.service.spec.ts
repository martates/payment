import { TestBed, inject } from '@angular/core/testing';

import { EffectiveContractStateService } from './effective-contract-state.service';

describe('EffectiveContractStateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EffectiveContractStateService]
    });
  });

  it('should be created', inject([EffectiveContractStateService], (service: EffectiveContractStateService) => {
    expect(service).toBeTruthy();
  }));
});
