import { TestBed, inject } from '@angular/core/testing';

import { WorkProgressComputeService } from './work-progress-compute.service';

describe('WorkProgressComputeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorkProgressComputeService]
    });
  });

  it('should be created', inject([WorkProgressComputeService], (service: WorkProgressComputeService) => {
    expect(service).toBeTruthy();
  }));
});
