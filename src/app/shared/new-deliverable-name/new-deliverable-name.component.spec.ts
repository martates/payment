import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewDeliverableNameComponent } from './new-deliverable-name.component';

describe('NewDeliverableNameComponent', () => {
  let component: NewDeliverableNameComponent;
  let fixture: ComponentFixture<NewDeliverableNameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewDeliverableNameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewDeliverableNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
