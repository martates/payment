import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

import { AppNotificationService } from '../../app-services/app-notification.service';
import { DeliverablesService } from '../deliverables.service';

@Component({
  selector: 'app-new-deliverable-name',
  templateUrl: './new-deliverable-name.component.html',
  styleUrls: ['./new-deliverable-name.component.css']
})
export class NewDeliverableNameComponent implements OnInit {

  deliverableName: string;

  constructor(private dialogRef: MatDialogRef<NewDeliverableNameComponent>,
    private deliverableDataService: DeliverablesService,
    private messageNotificationService: AppNotificationService) { }

  ngOnInit() {
  }

  close() {
    if (!this.deliverableName) {
      this.dialogRef.close();
      return;
    }

    this.deliverableDataService.postData('deliverableLookups', { Description: this.deliverableName }, null)
      .subscribe(
        deliverableLookup => {
          this.dialogRef.close(deliverableLookup),
        e => this.messageNotificationService.errorNotification.next(e)
      });
  }

}
