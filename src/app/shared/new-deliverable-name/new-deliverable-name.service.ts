import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs/Subject';

import { DeliverableLookup } from '../../models/deliverable-lookup';
import { NewDeliverableNameComponent } from './new-deliverable-name.component';

@Injectable()
export class NewDeliverableNameService {

  public nameStream: Subject<DeliverableLookup> = null;

  constructor(public dialog: MatDialog) {
    this.nameStream = new Subject();
  }

  openDialog() {
    setTimeout(() => {
      const dialogRef = this.dialog.open(NewDeliverableNameComponent, {
        width: '450px',
        data: { message: 'New deliverable' },
        autoFocus: true,
        disableClose: true
      });

      dialogRef.afterClosed().subscribe(deliverableLookup => {
        this.nameStream.next(deliverableLookup);
      });
    }, 500);
  }

}
