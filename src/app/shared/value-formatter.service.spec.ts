import { TestBed, inject } from '@angular/core/testing';

import { ValueFormatterService } from './value-formatter.service';

describe('ValueFormatterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ValueFormatterService]
    });
  });

  it('should be created', inject([ValueFormatterService], (service: ValueFormatterService) => {
    expect(service).toBeTruthy();
  }));
});
