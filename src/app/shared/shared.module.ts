import { CommonModule, CurrencyPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
// import { MatButtonModule, MatDialogModule, MatInputModule, MatSelectModule, MatTableModule } from '@angular/material';

import { ContractPhaseComputeService } from './contract-phase-compute.service';
import { ContractPhaseService } from './contract-phase.service';
import { CurrencyBreakdownComponent } from './currency-breakdown/currency-breakdown.component';
import { CurrencyDataService } from './currency-breakdown/currency-data.service';
import { CurrencyService } from './currency-breakdown/currency.service';
import { DeliverablesComputeService } from './deliverables-compute.service';
import { DeliverablesService } from './deliverables.service';
import { EffectiveContractStateService } from './effective-contract-state.service';
import { NewDeliverableNameComponent } from './new-deliverable-name/new-deliverable-name.component';
import { NewDeliverableNameService } from './new-deliverable-name/new-deliverable-name.service';
import { SideInfoModule } from './side-info/side-info.module';
import { SupplementComputeService } from './supplement-compute.service';
import { SupplementService } from './supplement.service';
import { ValueFormatterService } from './value-formatter.service';
import { WorkProgramComputeService } from './work-program-compute.service';
import { WorkProgramDataService } from './work-program-data.service';
import { WorkProgressComputeService } from './work-progress-compute.service';
import { WorkProgressDataService } from './work-progress-data.service';
import { materialize } from 'rxjs-compat/operator/materialize';
import { MaterialModule } from '../material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    // MatInputModule,
    // MatButtonModule,
    // MatDialogModule,
    // MatSelectModule,
    // MatTableModule,
    SideInfoModule
  ],
  declarations: [NewDeliverableNameComponent, CurrencyBreakdownComponent],
  providers: [
    ContractPhaseComputeService,
    ContractPhaseService,
    DeliverablesService,
    DeliverablesComputeService,
    NewDeliverableNameService,
    WorkProgramComputeService,
    WorkProgressComputeService,
    WorkProgramDataService,
    WorkProgressDataService,
    CurrencyService,
    CurrencyDataService,
    CurrencyPipe,
    ValueFormatterService,
    SupplementComputeService,
    SupplementService,
    EffectiveContractStateService
  ],
  entryComponents: [NewDeliverableNameComponent, CurrencyBreakdownComponent]
})
export class SharedModule { }
