import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { ContractProgress } from '../models/contract-progress';
import { AsyncDataFacade } from './async-data-facade';
@Injectable()
export class ContractProgressService {
  [x: string]: any;
public contractProgress: ContractProgress;
  constructor(private asyncDataFacade: AsyncDataFacade) { }
  
  getData(query: string, param: any): Observable<any> { 
    switch (query) {
      case 'keystaff':
      const staffId = <number>param;
      const staffurl = `/contractprogress/keystaff/${staffId}`;
      return this.asyncDataFacade.get(staffurl);
      case 'progress':
        const contractId = <number>param;
        const url = `/contractprogress/contract/${contractId}`;
        return this.asyncDataFacade.get(url);
      case 'keystaffs':
       const url1 = `/contractprogress/keystaffs/`;
       return this.asyncDataFacade.get(url1);
     
      default:
        return Observable.of(null);
    }
    
  }
 

}
