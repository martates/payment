import { Injectable } from '@angular/core';
import * as _moment from 'moment';
import { Subject } from 'rxjs';

import { ContractService } from '../contract/contract.service';
import { ConsultantsPerformance } from '../models/consultants-performance';
import { ConsultantsProgram } from '../models/consultants-program';
import { Deliverable } from '../models/deliverable';
import { SaveConsultantsPerformance } from '../models/save-consultants-performance';
import { TaskResult } from '../models/task-result';
import { WorkProgressDataService } from './work-progress-data.service';

@Injectable()
export class WorkProgressComputeService {

  contractDeliverables: Deliverable[];
  progressesLoadedSubject: Subject<{ progresses: ConsultantsPerformance[], programs: ConsultantsProgram[] }> = new Subject();

  constructor(private contractService: ContractService,
    private dataService: WorkProgressDataService) { }

  getDressedProgresses(): any {
    this.dataService.getData('contract-deliverables', this.contractService.contract.id)
      .subscribe(contractDeliverables => {
        this.contractDeliverables = contractDeliverables;

        this.dataService.getData('performances', this.contractService.contract.id).subscribe(progresses => {
          progresses.forEach(p => {
            p.deliverable.contractPhase = this.contractDeliverables.find(d => d.id == p.deliverable.id).contractPhase;
          });

          this.dataService.getData('programs', this.contractService.contract.id)
            .subscribe(programs => {

              programs.forEach(p => {
                p.deliverable.contractPhase = contractDeliverables.find(d => d.id == p.deliverable.id).contractPhase;
              });

              this.progressesLoadedSubject.next({ progresses: progresses, programs: programs });
            });
        });
      });

    return this.progressesLoadedSubject;
  }

  validateProgress(deliverable: Deliverable, progress: SaveConsultantsPerformance, progresses: ConsultantsPerformance[]): TaskResult {

    //validate basics like value ranges
    if (!progress) {
      return { isSuccessful: true, message: '' };
    }

    if (progress.percentComplete < 0 || progress.percentComplete > 100) {
      return { isSuccessful: false, message: 'Progress must be a value between 0 and 100' };
    }

    if (progress.monetaryValue < 0) {
      return { isSuccessful: false, message: 'Progress cannot be negative' };
    }

    // are date ranges within phase bounds
    const dateRangeValidation = this.validatePhaseDateRanges(deliverable, progress);

    if (!dateRangeValidation.isSuccessful) {
      return dateRangeValidation;
    }

    // are dates in ascending order
    const dateChronologyValidation = this.validateDateChronology(progress, progresses);
    if (!dateChronologyValidation.isSuccessful) {
      return dateChronologyValidation;
    }

    // are percentages cumulative 
    const cumulativeValidation = this.percentCumulativeValidation(progress, progresses);
    if (!cumulativeValidation.isSuccessful) {
      return cumulativeValidation;
    }

    return { isSuccessful: true, message: '' };
  }

  validatePhaseDateRanges(deliverable, progress): TaskResult {

    const phase = this.contractService.contract.phases.find(p => p.id === deliverable.contractPhaseId);

    if (_moment(progress.assessmentDate).isSameOrBefore(phase.startDate) ||
      _moment(progress.assessmentDate).isSameOrAfter(phase.endDate)) {

      return { isSuccessful: false, message: 'Progress date must be with in the bounds of the contract phase start and end dates' };
    }

    return { isSuccessful: true, message: '' };
  }

  validateDateChronology(progress, progresses): TaskResult {
    if (progress.id) {
      // existing progress

      // temporary add the edited progress to collection
      const clonedProgresses = this.cloneProgramsWithProgressChanges(progress, progresses);

      const index = progresses.findIndex(p => p.id === progress.id);

      // is the previous progress percent complete 
      // greater than the current progress percent complete
      let previousProgress = this.getAdjacentProgress(clonedProgresses[index], clonedProgresses)

      if (previousProgress && _moment(previousProgress.assessmentDate).isAfter(progress.assessmentDate)) {
        return { isSuccessful: false, message: 'Progress dates must be ascending and chronological. Earlier Progress\'s date comes after the current.' };
      }

      let nextProgress = this.getAdjacentProgress(clonedProgresses[index], clonedProgresses, false)

      if (nextProgress && _moment(nextProgress.assessmentDate).isBefore(progress.assessmentDate)) {
        return { isSuccessful: false, message: 'Progress dates must be ascending and chronological. Future progress\'s date comes before the current.' };
      }

    } else {
      // new progress
      let lastDeliverableProgress = this.getLastProgress(progress.deliverableId, progresses);

      if (lastDeliverableProgress && _moment(lastDeliverableProgress.assessmentDate).isAfter(progress.assessmentDate)) {
        return { isSuccessful: false, message: 'Progress date for the same deliverable must be in ascending chronological order' };
      }
    }

    return { isSuccessful: true, message: '' };
  }

  percentCumulativeValidation(progress, progresses) {
    /*
         Is cumulative check. (must be checked before adding new progress for cumulative check!)
         If existing progress is being edited, only programs that come immediately 
         before or after it belonging to the same phase must be compared.
       */
    if (progress.id) {

      // temporary add the edited progress to collection
      const clonedProgresses = this.cloneProgramsWithProgressChanges(progress, progresses);

      const index = progresses.findIndex(p => p.id === progress.id);

      // is the previous progress percent complete 
      // greater than the current progress percent complete
      let previousProgress = this.getAdjacentProgress(clonedProgresses[index], clonedProgresses)

      if (previousProgress && previousProgress.percentComplete >= progress.percentComplete) {
        return { isSuccessful: false, message: 'Progress percentages must be cumulative. Earlier progress has higher percentage.' };
      }

      let nextProgress = this.getAdjacentProgress(clonedProgresses[index], clonedProgresses, false)

      if (nextProgress && nextProgress.percentComplete <= progress.percentComplete) {
        return { isSuccessful: false, message: 'Progress percentages must be cumulative. Future progress has lower percentage.' };
      }

    } else {

      let lastDeliverableProgram = this.getLastProgress(progress.deliverableId, progresses);

      if (lastDeliverableProgram && lastDeliverableProgram.percentComplete >= progress.percentComplete) {
        return { isSuccessful: false, message: 'Progress percentages must be cumulative' };
      }

    }

    return { isSuccessful: true, message: '' };
  }

  cloneProgramsWithProgressChanges(progress, progresses): ConsultantsPerformance[] {
    const clonedProgresses = new Array<ConsultantsPerformance>();

    Object.assign(clonedProgresses, progresses);

    const index = progresses.findIndex(p => p.id === progress.id);

    clonedProgresses[index] = {
      id: progress.id,
      assessmentDate: progress.assessmentDate,
      deliverable: this.contractDeliverables.find(d => d.id === progress.deliverableId),
      percentComplete: progress.percentComplete,
      monetaryValue: progress.monetaryValue,
      reason: progress.reason,
      detailedReason: progress.detailedReason
    };

    return clonedProgresses;
  }


  getDeliverableFor(progress): Deliverable {

    if (progress.deliverableId) {
      return this.contractDeliverables
        .find(c => c.id === progress.deliverableId);

    } else {
      return this.contractDeliverables
        .find(c => c.id === progress.deliverable.id);
    }
  }

  computeProgressesCumulatives(progresses: ConsultantsPerformance[]): ConsultantsPerformance[] {

    const sortedProgresses = this.sortProgresses(progresses);

    sortedProgresses.forEach(progress => {
      Object.defineProperties(progress,
        {

          phaseCumulativePercentage: {
            value: 0,
            writable: true
          },
          phaseCumulativeMoney: {
            value: 0,
            writable: true
          },

          contractCumulativePercentage: {
            value: 0,
            writable: true
          },
          contractCumulativeMoney: {
            value: 0,
            writable: true
          }
        });

      this.computeProgressCumulatives(progress.deliverable, progress, sortedProgresses);

    });

    return sortedProgresses;
  }

  computeProgressCumulatives(deliverable: Deliverable, progress: ConsultantsPerformance, progresses: ConsultantsPerformance[]) {

    if (!deliverable) {
      return;
    }

    const sortedProgresses = this.sortProgresses(progresses);

    this.computePhaseCumulativePercentage(progress, sortedProgresses);

    this.computePhaseCumulativeMoney(progress, sortedProgresses);

    this.computeContractCumulativePercentage(progress, sortedProgresses);

    this.computeContractCumulativeMoney(progress, sortedProgresses)
  }

  computeDeliverableMoneyFromPercentage(deliverable: Deliverable, progress: SaveConsultantsPerformance) {
    if (!deliverable || !progress) {
      return;
    }

    const phase = deliverable.contractPhase;

    progress.monetaryValue = deliverable.monetaryValue * (progress.percentComplete / 100);
  }

  //*************Phase computations*****************//

  computePhaseCumulativePercentage(progress, progresses: ConsultantsPerformance[]) {

    const phasePrograms =
      progresses.filter(p => p.deliverable.contractPhase.id === progress.deliverable.contractPhaseId &&
        _moment(p.assessmentDate).isSameOrBefore(progress.assessmentDate));

    let totalMoney = 0;
    phasePrograms.forEach(progress => {
      const previousProgress = this.getAdjacentProgress(progress, progresses);

      const previousMonetaryValue = previousProgress ? previousProgress.monetaryValue : 0;

      totalMoney += progress.monetaryValue - previousMonetaryValue;
    });

    try {
      const phase = this.contractService.contract.phases.find(p => p.id === progress.deliverable.contractPhaseId);

      progress.phaseCumulativePercentage = (totalMoney / phase.lumpSum) * 100;
    } catch (error) {
      progress.phaseCumulativePercentage = null;
    }
  }

  computePhaseCumulativeMoney(progress, progresses) {

    const phaseProgresses =
      progresses.filter(p => p.deliverable.contractPhase.id === progress.deliverable.contractPhaseId &&
        _moment(p.assessmentDate).isSameOrBefore(progress.assessmentDate));

    let total = 0;
    phaseProgresses.forEach(progress => {
      const previousProgress = this.getAdjacentProgress(progress, progresses);

      const previousMonetaryValue = previousProgress ? previousProgress.monetaryValue : 0;

      total += progress.monetaryValue - previousMonetaryValue;
    });
    progress.phaseCumulativeMoney = total;
  }

  //*************Contract (sum of phases) computations*****************//

  computeContractCumulativePercentage(progress, progresses) {

    const phases = this.contractService.contract.phases
    progress.contractCumulativePercentage = 0;

    const phaseTotal = phases
      .map(p => p.lumpSum)
      .reduce((acc, n) => acc + n);

    phases.forEach(phase => {

      const phaseProgresses =
        progresses.filter(p => p.deliverable.contractPhase.id === phase.id &&
          _moment(p.assessmentDate).isSameOrBefore(progress.assessmentDate));

      let total = 0;
      phaseProgresses.forEach(progress => {
        const previousProgress = this.getAdjacentProgress(progress, progresses);

        const previousMonetaryValue = previousProgress ? previousProgress.monetaryValue : 0;

        total += progress.monetaryValue - previousMonetaryValue;
      });
      progress.contractCumulativePercentage += ((total / phaseTotal) * 100);
    });

  }

  computeContractCumulativeMoney(progress, progresses) {

    const phases = this.contractService.contract.phases
    progress.contractCumulativeMoney = 0;
    phases.forEach(phase => {

      const phaseProgresses =
        progresses.filter(p => p.deliverable.contractPhase.id === phase.id &&
          _moment(p.assessmentDate).isSameOrBefore(progress.assessmentDate));

      let total = 0;
      phaseProgresses.forEach(progress => {
        const previousProgress = this.getAdjacentProgress(progress, progresses);

        const previousMonetaryValue = previousProgress ? previousProgress.monetaryValue : 0;

        total += progress.monetaryValue - previousMonetaryValue;
      });
      progress.contractCumulativeMoney += total;
    });

  }
  //#region Helper methods

  sortProgresses(consultantsProgresses: ConsultantsPerformance[]): ConsultantsPerformance[] {
    return consultantsProgresses.sort(this.comparer)
  }

  private comparer(s1, s2) {
    if (_moment(s1.assessmentDate).isBefore(s2.assessmentDate)) {
      return -1;
    } else if (_moment(s1.assessmentDate).isAfter(s2.assessmentDate)) {
      return 1;
    } else if (_moment(s1.assessmentDate).isSame(s2.assessmentDate)) {
      if (s1.monetaryValue < s2.monetaryValue) {
        return 1;
      } else if (s1.monetaryValue > s2.monetaryValue) {
        return -1;
      }
      return 0;
    }

    return 0;
  }

  getAdjacentProgress(progress: ConsultantsPerformance, progresses: ConsultantsPerformance[], previous = true): ConsultantsPerformance {

    const deliverableProgresses = progresses.filter(p => p.deliverable.id === progress.deliverable.id)
    const sortedProgresses = this.sortProgresses(deliverableProgresses);
    const index = sortedProgresses.findIndex(p => p.id === progress.id);
    const requestedIndex = previous ? index - 1 : index + 1;
    const adjacentProgress = sortedProgresses[requestedIndex];
    return adjacentProgress;
  }

  getLastProgress(deliverableId, progresses): ConsultantsPerformance {

    try {
      const sortedProgresses = this.sortProgresses(progresses);
      const deliverableProgresses = sortedProgresses.filter(p => p.deliverable.id === deliverableId);
      return deliverableProgresses.pop();
    } catch (error) {
      return null;
    }

  }
  //#endregion
}
