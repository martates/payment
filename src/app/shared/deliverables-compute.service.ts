import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/from';
import { groupBy, mergeMap, toArray } from 'rxjs/operators';

import { AppNotificationService } from '../app-services/app-notification.service';
import { ContractService } from '../contract/contract.service';
import { Contract } from '../models/contract';
import { Deliverable } from '../models/deliverable';
import { SaveDeliverable } from '../models/save-deliverable';
import { TaskResult } from '../models/task-result';
import { DeliverablesService } from './deliverables.service';

@Injectable()
export class DeliverablesComputeService {

  private deliverablesLookupSubject: Subject<any> = new Subject();
  private phaseSubject: Subject<any> = new Subject();
  private deliverablesSubject: Subject<any> = new Subject();
  private saveDeliverablesSubject: Subject<any> = new Subject();
  private deleteDeliverablesSubject: Subject<any> = new Subject();

  constructor(
    private contractService: ContractService,
    private dataService: DeliverablesService,
    private notificationService: AppNotificationService) {
  }

  getDeliverablesLookup(): Subject<any> {
    this.dataService.getData('deliverableLookups', null)
      .subscribe(lookups =>
        this.deliverablesLookupSubject.next(lookups)
      );

    return this.deliverablesLookupSubject;
  }

  getDeliverables(): Subject<any> {
    this.dataService.getData('deliverables', this.contractService.contract.id)
      .subscribe(data => {
        this.deliverablesSubject.next(data)
      });

    return this.deliverablesSubject;
  }

  save(contract, deliverable, deliverables): Subject<any> {

    this.calculateMoneyFromPercentage(contract, deliverable);

    // Add new
    if (!deliverable.id) {
      this.dataService.postData('', deliverable, contract.id).subscribe(
        data => {
          this.notificationService.messageNotification.next("Deliverable added");
          deliverables = deliverables ? deliverables.concat(data) : new Array<Deliverable>(data);
          this.saveDeliverablesSubject.next(deliverables);
        }
      );

    } else {
      const index = this.getIndex(deliverable, deliverables);
      Object.assign(deliverables[index], deliverable);

      this.dataService.putData('', deliverable, contract.id).subscribe(data => {
        this.notificationService.messageNotification.next("Deliverable updated");
        this.saveDeliverablesSubject.next(deliverables);
      });
    }

    return this.saveDeliverablesSubject;
  }

  delete(deliverable, deliverables: Deliverable[]): Subject<any> {

    this.dataService.deleteData('', deliverable, this.contractService.contract.id).subscribe(data => {
      if (deliverables) {
        const index = this.getIndex(deliverable, deliverables);
        deliverables.splice(index, 1);
      }

      this.notificationService.messageNotification.next("Deliverable deleted");
      this.deleteDeliverablesSubject.next(deliverables);
    });

    return this.deleteDeliverablesSubject;
  }

  validateDeliverable(deliverable: SaveDeliverable, deliverables: Deliverable[]): TaskResult {

    if (!deliverable) {
      return { isSuccessful: false, message: 'Deliverable is missing' };
    }

    // is deliverable duplicate?
    if (deliverables && deliverables.length > 0) {
      const found =
        deliverables.filter(d => d.id !== deliverable.id &&
          d.contractPhase.id === deliverable.contractPhaseId &&
          d.deliverableLookup.id === deliverable.deliverableLookupId);

      if (found.length > 0) {
        const message = `Error: Deliverable is already part of the current contract phase`;
        this.notificationService.errorNotification.next(message);
        return { isSuccessful: false, message: message };
      }
    }

    if (!deliverables || deliverables.length === 0) {
      if (deliverable.phasePercentage > 100) {
        const message = `Error: Deliverables percentage exceeded 100%. Computed ${deliverable.phasePercentage}% so far`;
        this.notificationService.errorNotification.next(message);
        return { isSuccessful: false, message: message };
      }
    }

    return { isSuccessful: true, message: '' };
  }

  validateDeliverablePercentage(deliverable: SaveDeliverable, deliverables: Deliverable[]): TaskResult {

    // group deliverables by phase to check percentage 
    // does not exceed 100%
    const deliverables$ = Observable.from(deliverables);

    const groupedDeliverables = deliverables$.pipe(
      groupBy(d => d.contractPhase.id),
      mergeMap(group => group.pipe(toArray()))
    );

    let relevantGroup = null;
    groupedDeliverables.forEach(arr => {
      if (arr && arr.length > 0 && arr[0].contractPhase.id === deliverable.contractPhaseId) {
        relevantGroup = arr;
      }
    });

    if (relevantGroup) {
      let totalPercentage: number = relevantGroup.map(d => {

        // if updating existing deliverable
        // which may have modified percentage
        if (d.id === deliverable.id) {
          return +deliverable.phasePercentage;
        }

        return +d.phasePercentage

      }).reduce((acc, n) => acc + n);

      if (!deliverable.id) {
        totalPercentage = totalPercentage + (+deliverable.phasePercentage);
      }

      const deliverablePhase = this.contractService.contract.phases.find(p => p.id == deliverable.contractPhaseId);

      const phasePercentLessAdvancePercent = deliverablePhase.isAdvanceNotTaken ? 100 : 100 - deliverablePhase.advancePercent;

      if (totalPercentage > phasePercentLessAdvancePercent) {
        const message = `Error: Deliverables percentage exceeded 100% (accounting for advance of ${deliverablePhase.advancePercent}%). Computed ${totalPercentage}% so far`;
        
        this.notificationService.errorNotification.next(message);
        
        return { isSuccessful: false, message: message };
      } else {
        return { isSuccessful: true, message: '' };
      }
    }

    return { isSuccessful: true, message: '' };
  }

  selectDeliverable(formGroup, deliverable) {
    formGroup.setValue({
      deliverableIdControl: deliverable.id,
      phaseControl: deliverable.contractPhaseId,
      deliverableControl: deliverable.deliverableLookup.id,
      percentageControl: deliverable.phasePercentage,
      monetaryValueControl: deliverable.monetaryValue
    });
  }

  extractDeliverable(formModel): SaveDeliverable {

    const d: SaveDeliverable = {
      id: formModel.deliverableIdControl.value || 0,
      contractPhaseId: formModel.phaseControl.value,
      deliverableLookupId: formModel.deliverableControl.value,
      phasePercentage: formModel.percentageControl.value,
      monetaryValue: formModel.monetaryValueControl.value
    };

    return d;
  }

  private getIndex(deliverable, deliverables): number {
    return deliverables.findIndex(d => d.id === deliverable.id);
  }

  calculateMoneyFromPercentage(contract: Contract, deliverable: any) {
    if (!deliverable) {
      return;
    }

    const deliverablePhase = contract.phases.find(p => p.id == deliverable.contractPhaseId);

    if (!deliverablePhase) {
      return;
    }

    let phaseTotal = deliverablePhase.lumpSum;

    let scaler;
    if (!deliverablePhase.isAdvanceNotTaken) {
      scaler = (deliverable.phasePercentage / 100) / (1 - (deliverablePhase.advancePercent / 100));
    } else {
      scaler = (deliverable.phasePercentage / 100);
    }

    deliverable.monetaryValue = phaseTotal * scaler;
  }
}
