import { Injectable } from '@angular/core';
import * as _moment from 'moment';
import { Subject } from 'rxjs/Subject';

import { ContractService } from '../contract/contract.service';
import { ActionType } from '../models/action-type';
import { ContractPhase } from '../models/contract-phase';
import { Supplement } from '../models/supplement';
import { TaskResult } from '../models/task-result';
import { SupplementService } from './supplement.service';

@Injectable()
export class SupplementComputeService {

  supplements: Supplement[];
  supplementsLoaded: Subject<Supplement[]> = new Subject();

  constructor(
    private contractService: ContractService,
    private dataService: SupplementService) { }

  getSupplements(contract): Subject<any> {
    this.dataService.getData('supplements', contract.id)
      .subscribe(supplements => {

        this.supplements = supplements.sort(this.comparer);

        this.dataService.getData('phases', contract.id).subscribe((phases: ContractPhase[]) => {
          if (phases) {
            supplements.forEach((supplement: Supplement) => {
              supplement.phases = phases.filter(p => p.supplementId === supplement.id);
            });
          }

          this.supplementsLoaded.next(this.supplements);
        });
      });

    return this.supplementsLoaded;
  }

  getSupplement(id: number | string): Supplement {
    return this.supplements.find(s => s.id == +id);
  }

  validateSupplement(supplement: Supplement): TaskResult {
    if (!supplement) {
      return { isSuccessful: false, message: 'No supplement provided' };
    }

    const contract = this.contractService.contract;
    if (!contract || !contract.id) {
      return { isSuccessful: false, message: 'Contract is not opened' };
    }

    // validate supplement date
    if (!supplement.agreementDate) {
      return { isSuccessful: false, message: 'Supplement date not provided' };
    }

    if (_moment(contract.contractSignedOn).isAfter(supplement.agreementDate)) {
      return { isSuccessful: false, message: 'Supplement date cannot be before the main contract signing date' };
    }

    return { isSuccessful: true, message: '' };
  }

  handleSupplementEdit(event) {

    switch (event.actionType) {

      case ActionType.Add:
        this.supplements.push(event.supplement);
        this.supplements = this.supplements.sort(this.comparer);
        break;

      case ActionType.Cancel:

        break;

      case ActionType.Clear:

        break;

      case ActionType.Delete:
        const index = this.supplements.findIndex(s => s.id === event.supplement.id);

        this.supplements.splice(index, 1);

        this.supplements = this.supplements.sort(this.comparer);
        break;

      case ActionType.Update:
        const editedSupplement = this.supplements.find((s) => {
          return s.id === event.supplement.id
        });

        Object.assign(editedSupplement, event.supplement);

        this.supplements = this.supplements.sort(this.comparer);
        break;
    }
  }

  // descending comparer
  private comparer(s1, s2) {
    if (_moment(s1.agreementDate).isBefore(s2.agreementDate)) {
      return 1;
    } else if (_moment(s1.agreementDate).isAfter(s2.agreementDate)) {
      return -1;
    }

    return 0;
  }
}
