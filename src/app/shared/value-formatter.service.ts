import { CurrencyPipe } from '@angular/common';
import { Inject, Injectable } from '@angular/core';

@Injectable()
export class ValueFormatterService {

  constructor(@Inject(CurrencyPipe) private currencyPipe: CurrencyPipe) { }

  convertToCurrency(value: number, formatString: string): string {
    // TODO: check value is a number
    return this.currencyPipe.transform(value, formatString);
  }

  convertFromCurrency(value: string): number {
    if (!value || typeof(value) !== 'string') {
      return null;
    }

    let spaceRiddenArray = value.split(' ').concat();

    if (spaceRiddenArray[0].startsWith('-')) { // don't splice negative sign
      spaceRiddenArray[0] = '-';
    } else {
      spaceRiddenArray = spaceRiddenArray.slice(1, spaceRiddenArray.length);
    }

    const spaceRiddenString = spaceRiddenArray.toString();
    const commaRiddenArray = spaceRiddenString.split(',');

    let commaRiddenString = '';
    commaRiddenArray.forEach(element => {
      if (element !== ',') {
        commaRiddenString += element;
      }
    });
    const result = Number.parseFloat(commaRiddenString);

    return result;
  }
}
