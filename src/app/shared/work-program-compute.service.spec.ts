import { TestBed, inject } from '@angular/core/testing';

import { WorkProgramComputeService } from './work-program-compute.service';

describe('WorkProgramComputeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorkProgramComputeService]
    });
  });

  it('should be created', inject([WorkProgramComputeService], (service: WorkProgramComputeService) => {
    expect(service).toBeTruthy();
  }));
});
