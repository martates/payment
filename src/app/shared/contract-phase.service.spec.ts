import { TestBed, inject } from '@angular/core/testing';

import { ContractPhaseService } from './contract-phase.service';

describe('ContractInfoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContractPhaseService]
    });
  });

  it('should be created', inject([ContractPhaseService], (service: ContractPhaseService) => {
    expect(service).toBeTruthy();
  }));
});
