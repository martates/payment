import { TestBed, inject } from '@angular/core/testing';

import { ContractProgressService } from './contract-progress.service';

describe('ContractProgressService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContractProgressService]
    });
  });

  it('should be created', inject([ContractProgressService], (service: ContractProgressService) => {
    expect(service).toBeTruthy();
  }));
});
