import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { ContractService } from '../contract/contract.service';
import { ContractPhase } from '../models/contract-phase';
import { EffectiveContractState } from '../models/effective-contract-state';
import { Supplement } from '../models/supplement';
import { ContractPhaseComputeService } from './contract-phase-compute.service';
import { SupplementComputeService } from './supplement-compute.service';

@Injectable()
export class EffectiveContractStateService {

  effectiveContractStateSubject: Subject<EffectiveContractState> = new Subject();

  constructor(
    private phaseComputeService: ContractPhaseComputeService,
    private supplementalService: SupplementComputeService,
    private contractService: ContractService) { }

  computeEffectiveStateAsync(): Subject<EffectiveContractState> {
    this.phaseComputeService.getPhases(this.contractService.contract).subscribe(_ => {
      this.supplementalService.getSupplements(this.contractService.contract).subscribe(supplements => {
        this.computeEffectiveState(this.contractService.contract.phases, supplements);
      });
    });

    return this.effectiveContractStateSubject;
  }

  private computeEffectiveState(phases: ContractPhase[], supplements: Supplement[]) {
    
    let effectiveContractState = new EffectiveContractState(this.contractService);

    effectiveContractState.computeEffectiveState(phases);

    this.effectiveContractStateSubject.next(effectiveContractState);
  }

}
