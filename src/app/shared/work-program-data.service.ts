import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { AsyncDataFacade } from './async-data-facade';
import { SaveConsultantsProgram } from '../models/save-consultants-program';

@Injectable()
export class WorkProgramDataService {

  constructor(private asyncDataFacade: AsyncDataFacade) { }

  getData(query: string, param: any): Observable<any> {
    const contractId = <number>param;
    switch (query) {
      case 'contract-deliverables':       
        return this.asyncDataFacade.get(`/contracts/${contractId}/deliverables`);
      case 'programs':    
        return this.asyncDataFacade.get(`/contracts/${contractId}/consultantsPrograms`);
      default:
        return Observable.of(null);
    }
  }

  postData(query: string, program: SaveConsultantsProgram, param: any): Observable<any> {
    const contractId = <number>param;
    return this.asyncDataFacade.post(`/contracts/${contractId}/consultantsPrograms`, program);
  }

  putData(query: string, program: SaveConsultantsProgram, param: any): Observable<any> {
    const contractId = <number>param;
    return this.asyncDataFacade.put(`/contracts/${contractId}/consultantsPrograms`, program);
  }

  deleteData(query: string, program: SaveConsultantsProgram, param: any): Observable<any> {
    const contractId = <number>param;
    return this.asyncDataFacade.delete(`/contracts/${contractId}/consultantsPrograms/${program.id}`);
  }
}
