import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { AsyncDataFacade } from '../async-data-facade';
import { PhaseCurrencyBreakdown } from '../../models/phase-currency-breakdown';
import { PhaseCurrencyBreakdownResource } from '../../models/phase-currency-breakdown-resource';

@Injectable()
export class CurrencyDataService {

  constructor(private asyncDataFacade: AsyncDataFacade) { }

  getData(query: string, param: any): Observable<any> {
    const phaseId: number = <number>param;

    switch (query) {

      case 'currencyBreakdowns':
        return this.asyncDataFacade.get('/phases/' + phaseId + '/currencyBreakdowns');

      default:
        return Observable.of(null);
    }
  }
  postData(query: string, data: any, param: any): Observable<any> {
    const phaseId: number = <number>param;

    const breakdowns = this.createDressedBreakdowns(<PhaseCurrencyBreakdown[]>data);

    return this.asyncDataFacade.post('/phases/' + phaseId + '/currencyBreakdowns/breakdowns', breakdowns);

  }
  putData(query: string, data: any, param: any): Observable<any> {
    const phaseId: number = <number>param;

    const breakdowns = this.createDressedBreakdowns(<PhaseCurrencyBreakdown[]>data);

    return this.asyncDataFacade.put('/phases/' + phaseId + '/currencyBreakdowns/', breakdowns);
  }
  deleteData(query: string, data: any, param: any): Observable<any> {
    throw new Error("Method not implemented.");
  }

  createDressedBreakdowns(breakdowns): any {

    const currencyResources = new Array<PhaseCurrencyBreakdownResource>();

    breakdowns.forEach(breakdown => {
      const breakdownRes = new PhaseCurrencyBreakdownResource();
      Object.assign(breakdownRes, breakdown)
      breakdownRes.amount = breakdown.amount;
      breakdownRes.currency = breakdown.currency;
      breakdownRes.exchangeRate = breakdown.exchangeRate;

      currencyResources.push(breakdownRes);
    });

    return currencyResources;
  }
}
