import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs/Subject';

import { ContractPhase } from '../../models/contract-phase';
import { CurrencyBreakdownComponent } from './currency-breakdown.component';

@Injectable()
export class CurrencyService {

  private amountStream: Subject<{}> = null;

  constructor(public dialog: MatDialog) { 
    this.amountStream = new Subject();
  }

  openCurrencyDialog(amountType: string, exchangeRates: any, phase: ContractPhase): Subject<{}> {
    setTimeout(() => {
      const dialogRef = this.dialog.open(CurrencyBreakdownComponent, {
        width: '550px',
        data: { amountType: amountType, exchangeRates: exchangeRates, phase: phase },
        autoFocus: true,
        disableClose: true
      });

      dialogRef.afterClosed().subscribe((currencyInfos) => {
        this.amountStream.next(currencyInfos);
      });
    }, 200);
    return this.amountStream;
  }
}
