import { Component, Inject, OnInit } from '@angular/core';
//import { MAT_DIALOG_DATA, MatDialogRef, MatTableDataSource } from '@angular/material';
import{MAT_DIALOG_DATA,MatDialogRef} from '@angular/material/dialog';
import{MatTableDataSource}from '@angular/material/table';
import { AppNotificationService } from '../../app-services/app-notification.service';
import { ContractPhase } from '../../models/contract-phase';
import { PhaseCurrencyBreakdown } from '../../models/phase-currency-breakdown';
import { CurrencyDataService } from './currency-data.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-currency-breakdown',
  templateUrl: './currency-breakdown.component.html',
  styleUrls: ['./currency-breakdown.component.css']
})
export class CurrencyBreakdownComponent implements OnInit {

  addAmendLabel: string;
  deleteResetLabel: string;
  currencyBreakdown: PhaseCurrencyBreakdown = null;
  currencyBreakdowns = new Array<PhaseCurrencyBreakdown>();
  exchangeRates: any;
  phase: ContractPhase;

  displayedColumns: string[];
  dataSource: any;

  constructor(private dialogRef: MatDialogRef<CurrencyBreakdownComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private notificationService: AppNotificationService,
    private currencyDataService: CurrencyDataService) {

    this.exchangeRates = data.exchangeRates;
    this.currencyBreakdown = new PhaseCurrencyBreakdown(this.exchangeRates);
    this.currencyBreakdown.type = data.amountType;

    this.phase = data.phase;
  }

  ngOnInit() {
    this.addAmendLabel = 'Save';
    this.deleteResetLabel = 'Clear';
    this.displayedColumns = ['amount', 'currency', 'exchangeRate', 'etbAmount'];

    if (this.phase && this.phase.id) {
      this.currencyDataService.getData('currencyBreakdowns', this.phase.id).subscribe(
        breakdowns => {
          const filteredBreakdowns = breakdowns.filter(b => b.type === this.data.amountType)
          this.setUpCurrencyBreakdowns(filteredBreakdowns);
        });
    }
  }


  save() {
    if (!this.currencyBreakdown.getIsValid()) {
      this.notificationService.messageNotification.next("Please complete the currency breakdown or cancel");
      return;
    }

    try {
      const breakdowns = this.currencyBreakdowns.filter(c => c.id === this.currencyBreakdown.id);

      if (breakdowns.length === 0) {
        this.currencyBreakdowns.push(this.currencyBreakdown);
      }
    } catch (error) { }

    this.dialogRef.close(this.currencyBreakdowns);
  }

  delete() {
    this.addAmendLabel = 'Save';
    this.deleteResetLabel = 'Clear';
  }

  cancel() {
    this.dialogRef.close(null);
  }

  selectBreakdown(breakdown) {
    this.currencyBreakdown = this.currencyBreakdowns.find(c => c.id === breakdown.id);

    this.addAmendLabel = 'Amend';
    this.deleteResetLabel = 'Delete';
  }

  setUpCurrencyBreakdowns(breakdowns): any {

    breakdowns.forEach(breakdown => {
      const dressedBreakdown = this.dressCurrencyBreakdown(breakdown);
      this.currencyBreakdowns.push(dressedBreakdown);
    });

    this.dataSource = new MatTableDataSource<PhaseCurrencyBreakdown>(this.currencyBreakdowns);
  }

  dressCurrencyBreakdown(plainBreakdown): PhaseCurrencyBreakdown {
    // make class methods available for newly fetched data
    if (!plainBreakdown) {
      return null;
    }

    const dressedBreakdown = new PhaseCurrencyBreakdown(this.exchangeRates);
    Object.assign(dressedBreakdown, plainBreakdown);
    dressedBreakdown.computeEtbAmount();
    return dressedBreakdown;
  }
}
