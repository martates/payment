import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrencyBreakdownComponent } from './currency-breakdown.component';

describe('CurrencyBreakdownComponent', () => {
  let component: CurrencyBreakdownComponent;
  let fixture: ComponentFixture<CurrencyBreakdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrencyBreakdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrencyBreakdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
