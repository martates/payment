import { Injectable } from '@angular/core';
import * as _moment from 'moment';
import { Subject } from 'rxjs';

import { ContractService } from '../contract/contract.service';
import { ConsultantsProgram } from '../models/consultants-program';
import { ContractPhase } from '../models/contract-phase';
import { Deliverable } from '../models/deliverable';
import { SaveConsultantsProgram } from '../models/save-consultants-program';
import { TaskResult } from '../models/task-result';
import { WorkProgramDataService } from './work-program-data.service';

@Injectable()
export class WorkProgramComputeService {

  contractDeliverables: Deliverable[];
  programExistsSubject: Subject<boolean>;
  programsLoadedSubject: Subject<ConsultantsProgram[]> = new Subject();

  constructor(private dataService: WorkProgramDataService,
    private contractService: ContractService) { }

  getDressedPrograms(): Subject<ConsultantsProgram[]> {
    this.dataService.getData('contract-deliverables', this.contractService.contract.id)
      .subscribe(contractDeliverables => {
        this.contractDeliverables = contractDeliverables;

        this.dataService.getData('programs', this.contractService.contract.id)
          .subscribe(programs => {
            
            programs.forEach(p => {
              p.deliverable.contractPhase = contractDeliverables.find(d => d.id == p.deliverable.id).contractPhase;
            });
            
            this.programsLoadedSubject.next(programs);
          })
      });

      return this.programsLoadedSubject;
  }

  validateProgramExistence(): Subject<boolean> {

    this.programExistsSubject = new Subject();

    this.dataService.getData('programs', this.contractService.contract.id)
      .subscribe(programs => {
        const programState = (programs && programs.length) ? true : false;

        this.programExistsSubject.next(programState);
      });

    return this.programExistsSubject;
  }

  validateProgram(deliverable: Deliverable, program: SaveConsultantsProgram, programs: ConsultantsProgram[]): TaskResult {

    //validate basics like value ranges
    if (!program) {
      return { isSuccessful: true, message: '' };
    }

    if (program.percentComplete < 0 || program.percentComplete > 100) {
      return { isSuccessful: false, message: 'Planned work must be a value between 0 and 100' };
    }

    if (program.monetaryValue < 0) {
      return { isSuccessful: false, message: 'Planned work cannot be negative' };
    }

    // are date ranges within phase bounds
    const dateRangeValidation = this.validatePhaseDateRanges(deliverable, program);

    if (!dateRangeValidation.isSuccessful) {
      return dateRangeValidation;
    }

    // are dates in ascending order
    const dateChronologyValidation = this.validateDateChronology(program, programs);
    if (!dateChronologyValidation.isSuccessful) {
      return dateChronologyValidation;
    }

    // are percentages cumulative 
    const cumulativeValidation = this.percentCumulativeValidation(program, programs);
    if (!cumulativeValidation.isSuccessful) {
      return cumulativeValidation;
    }

    return { isSuccessful: true, message: '' };
  }

  validatePhaseDateRanges(deliverable, program): TaskResult {

    const phase = this.contractService.contract.phases.find(p => p.id === deliverable.contractPhaseId);

    if (_moment(program.date).isSameOrBefore(phase.startDate) ||
      _moment(program.date).isSameOrAfter(phase.endDate)) {

      return { isSuccessful: false, message: 'Program date must be with in the bounds of the contract phase start and end dates' };
    }

    return { isSuccessful: true, message: '' };
  }

  validateDateChronology(program, programs): TaskResult {
    if (program.id) {
      // existing program

      // temporary add the edited program to collection
      const clonedPrograms = this.cloneProgramsWithProgramChanges(program, programs);

      const index = programs.findIndex(p => p.id === program.id);

      // is the previous program percent complete 
      // greater than the current program percent complete
      let previousProgram = this.getAdjacentProgram(clonedPrograms[index], clonedPrograms)

      if (previousProgram && _moment(previousProgram.date).isAfter(program.date)) {
        return { isSuccessful: false, message: 'Planned work dates must be ascending and chronological. Earlier program\'s date comes after the current.' };
      }

      let nextProgram = this.getAdjacentProgram(clonedPrograms[index], clonedPrograms, false)

      if (nextProgram && _moment(nextProgram.date).isBefore(program.date)) {
        return { isSuccessful: false, message: 'Planned work dates must be ascending and chronological. Future program\'s date comes before the current.' };
      }

    } else {
      // new program
      let lastDeliverableProgram = this.getLastProgram(program.deliverableId, programs);

      if (lastDeliverableProgram && _moment(lastDeliverableProgram.date).isAfter(program.date)) {
        return { isSuccessful: false, message: 'Program date for the same deliverable must be in ascending chronological order' };
      }
    }

    return { isSuccessful: true, message: '' };
  }

  percentCumulativeValidation(program, programs) {
    /*
         Is cumulative check. (must be checked before adding new program for cumulative check!)
         If existing program is being edited, only programs that come immediately 
         before or after it belonging to the same phase must be compared.
       */
    if (program.id) {

      // temporary add the edited program to collection
      const clonedPrograms = this.cloneProgramsWithProgramChanges(program, programs);

      const index = programs.findIndex(p => p.id === program.id);

      // is the previous program percent complete 
      // greater than the current program percent complete
      let previousProgram = this.getAdjacentProgram(clonedPrograms[index], clonedPrograms)

      if (previousProgram && previousProgram.percentComplete >= program.percentComplete) {
        return { isSuccessful: false, message: 'Planned work percentages must be cumulative. Earlier program has higher percentage.' };
      }

      let nextProgram = this.getAdjacentProgram(clonedPrograms[index], clonedPrograms, false)

      if (nextProgram && nextProgram.percentComplete <= program.percentComplete) {
        return { isSuccessful: false, message: 'Planned work percentages must be cumulative. Future program has lower percentage.' };
      }

    } else {

      let lastDeliverableProgram = this.getLastProgram(program.deliverableId, programs);

      if (lastDeliverableProgram && lastDeliverableProgram.percentComplete >= program.percentComplete) {
        return { isSuccessful: false, message: 'Planned work percentages must be cumulative' };
      }

    }

    return { isSuccessful: true, message: '' };
  }

  cloneProgramsWithProgramChanges(program, programs): ConsultantsProgram[] {
    const clonedPrograms = new Array<ConsultantsProgram>();

    Object.assign(clonedPrograms, programs);

    const index = programs.findIndex(p => p.id === program.id);

    clonedPrograms[index] = {
      id: program.id,
      date: program.date,
      deliverable: this.contractDeliverables.find(d => d.id === program.deliverableId),
      percentComplete: program.percentComplete,
      monetaryValue: program.monetaryValue
    };

    return clonedPrograms;
  }

  getDeliverableFor(program): Deliverable {

    if (program.deliverableId) {
      return this.contractDeliverables
        .find(c => c.id === program.deliverableId);

    } else {
      return this.contractDeliverables
        .find(c => c.id === program.deliverable.id);
    }
  }

  computeProgramsCumulatives(programs: ConsultantsProgram[]): ConsultantsProgram[] {
    const sortedPrograms = this.sortPrograms(programs);

    sortedPrograms.forEach(program => {
      Object.defineProperties(program,
        {

          phaseCumulativePercentage: {
            value: 0,
            writable: true
          },
          phaseCumulativeMoney: {
            value: 0,
            writable: true
          },

          contractCumulativePercentage: {
            value: 0,
            writable: true
          },
          contractCumulativeMoney: {
            value: 0,
            writable: true
          }
        });

      this.computeProgramCumulatives(program.deliverable, program, sortedPrograms);
    });

    return sortedPrograms;
  }

  computeProgramCumulatives(deliverable: Deliverable, program: ConsultantsProgram, programs: ConsultantsProgram[]) {

    if (!deliverable) {
      return;
    }

    const sortedPrograms = this.sortPrograms(programs);

    this.computePhaseCumulativePercentage(program, sortedPrograms);

    this.computePhaseCumulativeMoney(program, sortedPrograms);

    this.computeContractCumulativePercentage(program, sortedPrograms);

    this.computeContractCumulativeMoney(program, sortedPrograms)
  }

  //#region computeCumulatives helper methods

  /*
    Money value and percentages for 
    1. deliverable
    2. Phase 
    3. Contract (collection of phases)
 
    are computed here. Each of the following methods are 
    helpers to the computeCumulatives method
 
    Deliverable percentage is input from user and validated for 
    being cumulative in validateProgram method and therefore not
    included in the following computations
  */
  //***************************Deliverable computations************************************************//
  computeDeliverableMoneyFromPercentage(deliverable: Deliverable, program: SaveConsultantsProgram) {
    if (!deliverable || !program) {
      return;
    }

    const phase = deliverable.contractPhase;

    program.monetaryValue = deliverable.monetaryValue * (program.percentComplete / 100);
  }

  //*************Phase computations*****************//

  computePhaseCumulativePercentage(program, programs: ConsultantsProgram[]) {

    const phasePrograms =
      programs.filter(p => p.deliverable.contractPhase.id === program.deliverable.contractPhaseId &&
        _moment(p.date).isSameOrBefore(program.date));

    let totalMoney = 0;
    phasePrograms.forEach(program => {
      const previousProgram = this.getAdjacentProgram(program, programs);

      const previousMonetaryValue = previousProgram ? previousProgram.monetaryValue : 0;

      totalMoney += program.monetaryValue - previousMonetaryValue;
    });

    try {
      const phase = this.contractService.contract.phases.find(p => p.id === program.deliverable.contractPhaseId);

      program.phaseCumulativePercentage = (totalMoney / phase.lumpSum) * 100;
    } catch (error) {
      program.phaseCumulativePercentage = null;
    }
  }

  computePhaseCumulativeMoney(program, programs) {

    const phasePrograms =
      programs.filter(p => p.deliverable.contractPhase.id === program.deliverable.contractPhaseId &&
        _moment(p.date).isSameOrBefore(program.date));

    let total = 0;
    phasePrograms.forEach(program => {
      const previousProgram = this.getAdjacentProgram(program, programs);

      const previousMonetaryValue = previousProgram ? previousProgram.monetaryValue : 0;

      total += program.monetaryValue - previousMonetaryValue;
    });
    program.phaseCumulativeMoney = total;
  }

  //*************Contract (sum of phases) computations*****************//

  computeContractCumulativePercentage(program, programs) {

    const phases = this.contractService.contract.phases
    program.contractCumulativePercentage = 0;

    const phaseTotal = phases
      .map(p => p.lumpSum)
      .reduce((acc, n) => acc + n);

    phases.forEach(phase => {

      const phasePrograms =
        programs.filter(p => p.deliverable.contractPhase.id === phase.id &&
          _moment(p.date).isSameOrBefore(program.date));

      let total = 0;
      phasePrograms.forEach(program => {
        const previousProgram = this.getAdjacentProgram(program, programs);

        const previousMonetaryValue = previousProgram ? previousProgram.monetaryValue : 0;

        total += program.monetaryValue - previousMonetaryValue;
      });
      program.contractCumulativePercentage += ((total / phaseTotal) * 100);
    });

  }

  computeContractCumulativeMoney(program, programs) {

    const phases = this.contractService.contract.phases
    program.contractCumulativeMoney = 0;
    phases.forEach(phase => {

      const phasePrograms =
        programs.filter(p => p.deliverable.contractPhase.id === phase.id &&
          _moment(p.date).isSameOrBefore(program.date));

      let total = 0;
      phasePrograms.forEach(program => {
        const previousProgram = this.getAdjacentProgram(program, programs);

        const previousMonetaryValue = previousProgram ? previousProgram.monetaryValue : 0;

        total += program.monetaryValue - previousMonetaryValue;
      });
      program.contractCumulativeMoney += total;
    });

  }
  //#endregion

  //#region Helper methods

  sortPrograms(consultantsPrograms: ConsultantsProgram[]): ConsultantsProgram[] {
    return consultantsPrograms.sort(this.comparer)
  }

  private comparer(s1, s2) {
    if (_moment(s1.date).isBefore(s2.date)) {
      return -1;
    } else if (_moment(s1.date).isAfter(s2.date)) {
      return 1;
    } else if (_moment(s1.date).isSame(s2.date)) {
      if (s1.monetaryValue < s2.monetaryValue) {
        return 1;
      } else if (s1.monetaryValue > s2.monetaryValue) {
        return -1;
      }
      return 0;
    }

    return 0;
  }

  getPhase(program, deliverables): ContractPhase {
    return deliverables.find(d => d.id === program.deliverable.id).contractPhase;
  }

  getAdjacentProgram(program: ConsultantsProgram, programs: ConsultantsProgram[], previous = true): ConsultantsProgram {

    // const phase = this.getPhase(program, deliverables);
    const deliverablePrograms = programs.filter(p => p.deliverable.id === program.deliverable.id)
    const sortedPrograms = this.sortPrograms(deliverablePrograms);
    const index = sortedPrograms.findIndex(p => p.id === program.id);
    const requestedIndex = previous ? index - 1 : index + 1;
    const adjacentProgram = sortedPrograms[requestedIndex];
    return adjacentProgram;
  }

  getLastProgram(deliverableId, programs): ConsultantsProgram {

    try {
      const sortedPrograms = this.sortPrograms(programs);
      const deliverablePrograms = sortedPrograms.filter(p => p.deliverable.id === deliverableId);
      return deliverablePrograms.pop();
    } catch (error) {
      return null;
    }

  }
  //#endregion
}
