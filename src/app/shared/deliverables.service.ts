import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { AsyncDataFacade } from './async-data-facade';
import { SaveDeliverable } from '../models/save-deliverable';

@Injectable()
export class DeliverablesService {

  constructor(private asyncDataFacade: AsyncDataFacade) {
    }

  getData(query: string, param: any): Observable<any> { 
    switch (query) {
      case 'deliverableLookups':
        return this.asyncDataFacade.get('/deliverableLookups');
      case 'deliverables':
        const contractId = <number>param;
        const url = `/contracts/${contractId}/deliverables`;
        return this.asyncDataFacade.get(url);
      default:
        return Observable.of(null);
    }
  }

  postData(query: string, payLoad: any, param: any): Observable<any> {
    switch (query) {
      case 'deliverableLookups':
        return this.asyncDataFacade.post('/deliverableLookups', payLoad);

      default:
        const contractId = <number>param;
        return this.asyncDataFacade.post(`/contracts/${contractId}/deliverables`, payLoad);
    }
  }

  putData(query: string, deliverable: SaveDeliverable, param: any): Observable<any> {
    const contractId = <number>param;
    return this.asyncDataFacade.put(`/contracts/${contractId}/deliverables`, deliverable);
  }

  deleteData(query: string, deliverable: SaveDeliverable, param: any): Observable<any> {
    const contractId = <number>param;
    return this.asyncDataFacade.delete(`/contracts/${contractId}/deliverables/${deliverable.id}`);
  }
}
