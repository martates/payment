import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { AppNotificationService } from '../app-services/app-notification.service';
import { ContractService } from '../contract/contract.service';
import { Contract } from '../models/contract';
import { ContractPhase } from '../models/contract-phase';
import { Deliverable } from '../models/deliverable';
import { TaskResult } from '../models/task-result';
import { ContractPhaseService } from './contract-phase.service';
import { ValueFormatterService } from './value-formatter.service';


@Injectable()
export class ContractPhaseComputeService {

  public phasesTaskSubject: Subject<any> = new Subject();
  public removePhaseTaskSubject: Subject<any> = new Subject();
  public savePhaseTaskSubject: Subject<any> = new Subject();

  constructor(public contractService: ContractService,
    private dataService: ContractPhaseService,
    private notificationService: AppNotificationService,
    private valueFormatter: ValueFormatterService) {

  }


  getPhases(contract: Contract): Subject<any> {
    this.dataService.getData('contract-phase', contract.id)
      .subscribe((contractPhases: Array<ContractPhase>) => {
        this.dataService.getData('deliverables', contract.id)
          .subscribe((deliverables: Array<Deliverable>) => {
            contractPhases.map(phase => {
              phase.deliverables = deliverables.filter(d => d.contractPhase.id === phase.id);
            });

            contract.phases = contractPhases;
            this.calculatePhasesTotal(null);

            this.phasesTaskSubject.next();
          },
            error => {
              this.notificationService.errorNotification.next(error.message);
              this.phasesTaskSubject.next();
            }
          );
      });

    return this.phasesTaskSubject;
  }

  deletePhase(phase): Subject<any> {

    this.dataService.deleteData('', phase, null).subscribe(data => {
      this.notificationService.messageNotification.next('Contract Phase deleted');

      if (this.contractService.contract.phases) {
        const index = this.getIndex(phase);
        this.contractService.contract.phases.splice(index, 1);
        this.removePhaseTaskSubject.next();
      }

    },
      e => {
        this.notificationService.errorNotification.next(e.error);
        this.removePhaseTaskSubject.next();
      });

    return this.removePhaseTaskSubject;
  }

  save(phase: ContractPhase): Subject<any> {

    if (!phase.id) {
      phase.contractId = this.contractService.contract.id;
      this.dataService.postData('', phase, null)
        .subscribe(
          phaseRes => {
            this.notificationService.messageNotification.next('Contract Phase saved');

            this.contractService.contract.phases =
              this.contractService.contract.phases ?
                this.contractService.contract.phases.concat(phaseRes) : new Array<ContractPhase>(phaseRes);

            this.savePhaseTaskSubject.next(phaseRes);
          },
          e => {
            this.notificationService.errorNotification.next(e.error)
            this.savePhaseTaskSubject.next();
          });
    } else {

      const index = this.getIndex(phase);

      Object.assign(this.contractService.contract.phases[index], phase);

      this.dataService.putData('', phase, null).subscribe(
        phaseRes => this.savePhaseTaskSubject.next(phaseRes),
        e => {
          this.notificationService.errorNotification.next(e.error);
          this.savePhaseTaskSubject.next();
        }
      );
    }

    return this.savePhaseTaskSubject;
  }

  getIndex(phase: ContractPhase): number {
    return this.contractService.contract.phases.findIndex(p => p.id === phase.id);
  }

  calculatePhasesTotal(editedPhase: ContractPhase) {

    const contract = this.contractService.contract;

    if (!contract) {
      return;
    }

    if ((!contract.phases || contract.phases.length === 0) && editedPhase) {

      contract.phasesTotal = +editedPhase.provisionalSum + (+editedPhase.lumpSum) + (+editedPhase.reimbursable);

      this.computeContractFigures();

      contract.advanceAmount = editedPhase.advanceAmount;

      return;
    }

    const contractPhases = new Array<ContractPhase>();
    Object.assign(contractPhases, contract.phases);


    if (editedPhase && editedPhase.id) {// existing phase is being edited; replace one in array
      const index = contract.phases.findIndex(p => p.id === editedPhase.id);
      contractPhases[index] = editedPhase;
    }

    contract.phasesTotal =
      contractPhases
        .filter(phase => phase.supplementId === null)
        .map(phase => +phase.provisionalSum + (+phase.lumpSum) + (+phase.reimbursable))
        .reduce((acc, n) => acc + n, 0);

    // new phase on the making; show interim values
    if (editedPhase && !editedPhase.id) {
      contract.phasesTotal += (+editedPhase.provisionalSum) + (+editedPhase.lumpSum) + (+editedPhase.reimbursable);
    }

    this.computeContractFigures();
  }

  private computeContractFigures() {

    const contract = this.contractService.contract;

    // TODO: replace vat rate from DB value
    contract.vat = contract.phasesTotal * 0.15; 

    contract.grandTotal = contract.phasesTotal + contract.vat;

    if (contract.phases && contract.phases.length > 0) {
      contract.advanceAmount = contract.phases
        .map(phase => {
          const advancePercent = phase.advancePercent;
          // advance computed from deliverables (LS amount only)
          phase.advanceAmount = phase.lumpSum * (advancePercent / 100);
          return phase.advanceAmount;
        })
        .reduce((acc, n) => acc + n);
    }
  }

  calculateMoneyFromPercentage(contract, phase) {
    const advancePercent = phase.advancePercent ? phase.advancePercent : contract.advancePercent;
    phase.advanceAmount = phase.lumpSum * (advancePercent / 100);
  }

  validatePhase(newContractPhase: ContractPhase): TaskResult {

    const contract = this.contractService.contract;

    if (!contract) {
      return { isSuccessful: false, message: 'Contract is required for validation' };
    }

    // check contract amount is not exceeded
    // compute existing phase totals
    this.calculatePhasesTotal(null);

    if (this.phasesTotalExceedsContract()) {
      return { isSuccessful: false, message: 'Error: Phase totals exceed the contract amount' };
    }


    if (contract.phases.length === 0) {
      return { isSuccessful: true, message: '' }
    }

    if (newContractPhase.endDate < newContractPhase.startDate) {
      return { isSuccessful: false, message: 'Error: End date comes before start date' };
    }

    return { isSuccessful: true, message: '' }
  }

  validatePhasesTotal(): boolean {
    this.calculatePhasesTotal(null);

    try {
      return Math.round(this.contractService.contract.grandTotal) === Math.round(this.contractService.contract.contractAmount);
    }
    catch (error) {
      return false;
    }
  }

  private phasesTotalExceedsContract(): boolean {
    try {
      return Math.round(this.contractService.contract.grandTotal) > Math.round(this.contractService.contract.contractAmount);
    }
    catch (error) {
      return false;
    }
  }

  getSumDifference(): number {
    return this.contractService.contract.contractAmount - this.contractService.contract.grandTotal;
  }

  extractContractPhase(formControls): ContractPhase {
    const phases = this.contractService.contract.phases;
    const phaseId = formControls.idControl.value || 0;
    let supplementId = null; let contractPhaseId = null; let replacedSupplementId = null;

    try {
      supplementId = phases.find(p => p.id === phaseId).supplementId;
      contractPhaseId = phases.find(p => p.id === phaseId).contractPhaseId;
      replacedSupplementId = phases.find(p => p.id === phaseId).replacedSupplementId;
    } catch (e) { }

    return {
      id: formControls.idControl.value || 0,
      contractId: this.contractService.contract.id,
      name: formControls.nameControl.value,
      description: formControls.descriptionControl.value,
      startDate: formControls.startDateControl.value,
      endDate: formControls.endDateControl.value,
      lumpSum: this.valueFormatter.convertFromCurrency(formControls.lumpSumControl.value),
      provisionalSum: this.valueFormatter.convertFromCurrency(formControls.provisionalSumControl.value),
      reimbursable: this.valueFormatter.convertFromCurrency(formControls.reimbursableControl.value),
      advancePercent: formControls.advancePercentControl.value,
      advanceAmount: this.valueFormatter.convertFromCurrency(formControls.advanceAmountControl.value),
      isAdvanceNotTaken: formControls.advanceNotTakenControl.value || false,
      supplementId: supplementId,
      contractPhaseId: contractPhaseId,
      replacedSupplementId: replacedSupplementId
    };
  }

  selectContractPhase(form, contractPhase, editingOriginal: boolean = false) {
    if (editingOriginal && contractPhase.supplementId !== null) {
      this.notificationService.warningNotification.next(`${contractPhase.name} does not belong to the original contract and therefore not editable.`);
      return;
    }

    form.setValue({
      idControl: contractPhase.id,
      nameControl: contractPhase.name,
      descriptionControl: contractPhase.description,
      startDateControl: contractPhase.startDate,
      endDateControl: contractPhase.endDate,
      lumpSumControl: this.valueFormatter.convertToCurrency(contractPhase.lumpSum, 'ETB '),
      provisionalSumControl: this.valueFormatter.convertToCurrency(contractPhase.provisionalSum, 'ETB '),
      reimbursableControl: this.valueFormatter.convertToCurrency(contractPhase.reimbursable, 'ETB '),
      advancePercentControl: contractPhase.advancePercent || 0,
      advanceAmountControl: this.valueFormatter.convertToCurrency(contractPhase.advanceAmount || 0, 'ETB '),
      advanceNotTakenControl: contractPhase.isAdvanceNotTaken
    });
    form.markAsDirty();
  }
}
