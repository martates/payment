import { TestBed, inject } from '@angular/core/testing';

import { WorkProgramDataService } from './work-program-data.service';

describe('WorkProgramService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorkProgramDataService]
    });
  });

  it('should be created', inject([WorkProgramDataService], (service: WorkProgramDataService) => {
    expect(service).toBeTruthy();
  }));

  it('should fetch deliverable descriptions', inject([WorkProgramDataService], (service: WorkProgramDataService) => {
  }));

});
