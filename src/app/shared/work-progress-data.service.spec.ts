import { TestBed, inject } from '@angular/core/testing';

import { WorkProgressDataService } from './work-progress-data.service';

describe('WorkProgramService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorkProgressDataService]
    });
  });

  it('should be created', inject([WorkProgressDataService], (service: WorkProgressDataService) => {
    expect(service).toBeTruthy();
  }));

  it('should fetch deliverable descriptions', inject([WorkProgressDataService], (service: WorkProgressDataService) => {
  }));

});
