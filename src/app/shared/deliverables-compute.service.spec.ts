import { TestBed, inject } from '@angular/core/testing';

import { DeliverablesComputeService } from './deliverables-compute.service';

describe('DeliverableComputeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeliverablesComputeService]
    });
  });

  it('should be created', inject([DeliverablesComputeService], (service: DeliverablesComputeService) => {
    expect(service).toBeTruthy();
  }));
});
