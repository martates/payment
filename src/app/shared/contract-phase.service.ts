import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { AsyncDataFacade } from './async-data-facade';
import { ContractPhase } from '../models/contract-phase';

@Injectable()
export class ContractPhaseService {

  constructor(private asyncDataFacade: AsyncDataFacade) {
  }

  getData(query: string, param: any): Observable<any> {
    const contractId: number = <number>param;

    switch (query) {
      case 'contract-names':
        return this.asyncDataFacade.get('/contracts');

      case 'contract':
        return this.asyncDataFacade.get('/contracts/' + contractId);

      case 'contract-phase':

        return this.asyncDataFacade.get('/contracts/' + contractId + '/phases');

      case 'deliverables':
        return this.asyncDataFacade.get('/contracts/' + contractId + '/deliverables');

      default:
        return Observable.of(null);
    }
  }
  postData(query: string, contractPhase: ContractPhase, param: any): Observable<any> {
    return this.asyncDataFacade.post(`/contracts/${contractPhase.contractId}/phases`, contractPhase);
  }
  putData(query: string, contractPhase: ContractPhase, param: any): Observable<any> {
    const url = `/contracts/${contractPhase.contractId}/phases/${contractPhase.id}`;
    return this.asyncDataFacade.put(url, contractPhase);
  }
  deleteData(query: string, contractPhase: ContractPhase, param: any): Observable<any> {
    return this.asyncDataFacade.delete(`/contracts/${contractPhase.contractId}/phases/${contractPhase.id}`, {});

  }

}
