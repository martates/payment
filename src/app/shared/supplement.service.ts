import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { AsyncDataFacade } from './async-data-facade';

@Injectable()
export class SupplementService {

  constructor(private asyncDataFacade: AsyncDataFacade) { }

  getData(query: string, param: any): Observable<any> {
    const contractId = <number>param;
    switch (query) {
      case 'supplements':
        return this.asyncDataFacade.get(`/contracts/${contractId}/supplements`);

      case 'phases': {
        const options = { params: new HttpParams().set('loadSupplementPhases', 'true') };

        return this.asyncDataFacade.get('/contracts/' + contractId + '/phases', options);
      }
      default:
        return Observable.of(null);
    }
  }

  postData(query: string, data: any, param: any): Observable<any> {
    const contractId = <number>param;
    switch (query) {
      case 'supplement':
        return this.asyncDataFacade.post(`/contracts/${contractId}/supplements`, data);

      default:
        return Observable.of(null);
    }
  }

  putData(query: string, data: any, param: any): Observable<any> {
    const contractId = <number>param;
    switch (query) {
      case 'supplement':
        return this.asyncDataFacade.put(`/contracts/${contractId}/supplements`, data);

      default:
        return Observable.of(null);
    }
  }

  deleteData(query: string, supplement: any, param: any): Observable<any> {
    const contractId = <number>param;
    return this.asyncDataFacade.delete(`/contracts/${contractId}/supplements/${supplement.id}`);
  }
}
