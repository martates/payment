import { TestBed, inject } from '@angular/core/testing';

import { ContractPhaseComputeService } from './contract-phase-compute.service';

describe('ContractComputeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContractPhaseComputeService]
    });
  });

  it('should be created', inject([ContractPhaseComputeService], (service: ContractPhaseComputeService) => {
    expect(service).toBeTruthy();
  }));
});
