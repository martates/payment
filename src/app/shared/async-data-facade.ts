import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { environment } from '../../environments/environment';
import { AppNotificationService } from '../app-services/app-notification.service';
import { BusyIndicatorService } from '../busy-indicator/busy-indicator.service';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class AsyncDataFacade {

    private httpOptions: any;

    constructor(
        private authService: AuthService,
        private busyIndicatorService: BusyIndicatorService,
        private notificationService: AppNotificationService,
        private httpClient: HttpClient) {
        this.httpOptions = {
            headers: new HttpHeaders({
                'Authorization': 'Bearer ' + this.authService.token
            })
        };
    }

    get(url: string, options?): Observable<any> {

        this.busyIndicatorService.showBusySignal();

        return this.httpClient.get(this.getFullUrl(url), this.httpOptions)
            .catch(this.onCatch)
            .do((res: Response) => {

            }, (error: any) => {
                this.onError(error);
            })
            .finally(() => {
                this.onEnd();
            });
    }

    post(url: string, data: any): Observable<any> {

        this.busyIndicatorService.showBusySignal();

        return this.httpClient.post(this.getFullUrl(url), data, this.httpOptions)
            .catch(this.onCatch)
            .do((res: Response) => {

            }, (error: any) => {
                this.onError(error);
            })
            .finally(() => {
                this.onEnd();
            });
    }

    put(url: string, data: any): Observable<any> {

        this.busyIndicatorService.showBusySignal();

        return this.httpClient.put(this.getFullUrl(url), data, this.httpOptions)
            .catch(this.onCatch)
            .do((res: Response) => {

            }, (error: any) => {
                this.onError(error);
            })
            .finally(() => {
                this.onEnd();
            });
    }

    delete(url: string, data?: any): Observable<any> {

        this.busyIndicatorService.showBusySignal();

        return this.httpClient.delete(this.getFullUrl(url), this.httpOptions)
            .catch(this.onCatch)
            .do((res: Response) => {

            }, (error: any) => {
                this.onError(error);
            })
            .finally(() => {
                this.onEnd();
            });
    }

    private getFullUrl(url: string): string {
        return environment.apiBaseUrl + url;
    }

    private onCatch(error: any, caught: Observable<any>): Observable<any> {
        return Observable.throw(error);
    }

    private onError(res: Response): void {
        this.notificationService.errorNotification.next('Error, status code: ' + res.status);
    }

    private onEnd(): void {
        this.busyIndicatorService.closeBusySignal();
    }
}
