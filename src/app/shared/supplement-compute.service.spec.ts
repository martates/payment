import { TestBed, inject } from '@angular/core/testing';

import { SupplementComputeService } from './supplement-compute.service';

describe('SupplementComputeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SupplementComputeService]
    });
  });

  it('should be created', inject([SupplementComputeService], (service: SupplementComputeService) => {
    expect(service).toBeTruthy();
  }));
});
