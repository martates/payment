import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './auth/auth.guard';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './auth/login.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    //canActivate: [AuthGuard],
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'contract-phase',
    'loadChildren': () => import('../app/contract-phase/contract-phase.module').then(m => m.ContractPhaseModule),
    canLoad: [AuthGuard],
  },
  {
    path: 'deliverables',
    'loadChildren': () => import('../app/cms/deliverables/deliverables.module').then(m => m.DeliverablesModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'work-program',
    'loadChildren': () => import('../app/cms/work-program/work-program.module').then(m => m.WorkProgramModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'work-progress',
    'loadChildren': () => import('../app/cms/work-progress/work-progress.module').then(m => m.WorkProgressModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'supplements',
    'loadChildren': () => import('../app/cms/supplements/supplements.module').then(m => m.SupplementModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'pap',
    'loadChildren': () => import('../app/cms/pap/pap.module').then(m => m.PapModule),
    canLoad: [AuthGuard]
  },
  //Right of Management System
  {
    path: 'row-management',
    'loadChildren': () => import('../app/row/row-menu.module').then(m => m.RowMenuModule),
    canLoad: [AuthGuard]
  },

  // {
  //   path: 'estimateCommitte',
  //   'loadChildren': () => import('../app/row/estimate-committe/estimate-committee.module').then(m => m.EstimateCommitteModule),
  //   canLoad: [AuthGuard]
  // },
  // {
  //   path: 'fileupload',
  //   'loadChildren': () => import('../app/row/file-upload/file-upload.module').then(m => m.FileUploadModule),
  //   canLoad: [AuthGuard]
  // },
  // {
  //   path: 'ownership',
  //   'loadChildren': () => import('../app/row/ownersip/ownersip.module').then(m => m.OwnersipModule),
  //   canLoad: [AuthGuard]
  // },

  // {
  //   path: 'projectPerKm',
  //   'loadChildren': () => import('../app/row/project-per-km/project-per-km.module').then(m => m.ProjectPerKmModule),
  //   canLoad: [AuthGuard]
  // },
  // {
  //   path: 'property',
  //   'loadChildren': () => import('../app/row/property/property.module').then(m => m.PropertyModule),
  //   canLoad: [AuthGuard]
  // },
  // {
  //   path: 'report',
  //   'loadChildren': () => import('../app/row/report/report.module').then(m => m.ReportModule),
  //   canLoad: [AuthGuard]
  // },

  {
    path: 'engineers-workload',
    'loadChildren': () => import('../app/workload/engineers-workload/engineers-workload.module').then(m => m.EngineersWorkloadModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'staffs-contract',
    'loadChildren': () => import('../app/workload/staffs-contract/staffs-contract.module').then(m => m.StaffsContractModule),
    canLoad: [AuthGuard]
  },
  // {
  //   path: 'road-character',
  //   'loadChildren': () => import('../app/road/road-characterstics/road-character.module').then(m => m.RoadCharacterModule),
  //   canLoad: [AuthGuard]
  // },

  // {
  //   path: 'Report-Type',
  //   'loadChildren': () => import('../app/road/road-characterstics-report/reportroadcharacterstics.module').then(m => m.ReportroadcharactersticsModule),
  //   canLoad: [AuthGuard]
  // },
  ////////////////////////////////  Purchase Order routing
  {
    path: 'Purchase-Menu',
    'loadChildren': () => import('../app/purchase_Order/purchase-menu.module').then(m => m.PurchaseMenuModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'Purchase-Order',
    'loadChildren': () => import('../app/purchase_Order/purchase-order/purchase-order.module').then(m => m.PurchaseOrderModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'Requstion-Item',
    'loadChildren': () => import('../app/purchase_Order/request-item/request-item.module').then(m => m.RequestItemModule),
    canActivate: [AuthGuard]
  },
  ////////////////////////////////  Payment
  {
    path: 'Payment',
    'loadChildren': () => import('../app/Payment/payment-menu.module').then(m => m.PaymentMenuModule),
    // canLoad: [AuthGuard]
    canActivate: [AuthGuard],
  },
  {
    path: 'Payment-data',
    'loadChildren': () => import('../app/Payment/payment-data/payment-data.module').then(m => m.PaymentDataModule),
    // canLoad: [AuthGuard]
    canActivate: [AuthGuard]
  },
  ////////////////////////////////  Account
  {
    path: 'User-Issue',
    'loadChildren': () => import('./Account/user-menu.module').then(m => m.UserMenuModule),
    // canLoad: [AuthGuard]
    canActivate: [AuthGuard],
  },

  {
    path: 'User-Create',
    'loadChildren': () => import('./Account/user-create/user-create.module').then(m => m.UserCreateModule),
    // canLoad: [AuthGuard]
    canActivate: [AuthGuard],
  },
  {
    path: 'Role',
    'loadChildren': () => import('./Account/role/roleAllow/roleAllow.module').then(m => m.RoleAllowModule),
    // canLoad: [AuthGuard]
    canActivate: [AuthGuard],
  },
  //////////////////////////////////

  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '/home',
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],

})
export class AppRoutingModule { }
