import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StaffsContractComponent } from './staffs-contract.component';

const routes: Routes = [
{
  path: '',
  component: StaffsContractComponent
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StaffsContractRoutingModule { }



