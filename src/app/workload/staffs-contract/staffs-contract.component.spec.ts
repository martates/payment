import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffsContractComponent } from './staffs-contract.component';

describe('EngineersWorkloadComponent', () => {
  let component: StaffsContractComponent;
  let fixture: ComponentFixture<StaffsContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffsContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffsContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
