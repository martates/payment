import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StaffsContractComponent } from '../staffs-contract/staffs-contract.component';
import { StaffsContractRoutingModule } from './staffs-contract-routing.module';
// import { MatSelectModule, MatInputModule, MatButtonModule, MatTableModule, MatDialogModule,  } from '@angular/material';
import { SideInfoModule } from '../../shared/side-info/side-info.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContractProgressService } from '../../shared/contract-progress.service';

import { MaterialModule } from '../../material.module';

@NgModule({
  
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule, ReactiveFormsModule,
    MaterialModule,
    // AppMaterialModule,
    // MatSelectModule,
    // MatInputModule,
    // MatButtonModule, 
    // MatDialogModule,
    // MatTableModule,
    SideInfoModule,    
    StaffsContractRoutingModule
  ],
  declarations: [StaffsContractComponent],
  exports: [StaffsContractComponent],
  providers: [ContractProgressService]
})
export class StaffsContractModule { }
