import { Component, OnInit } from '@angular/core';
import { KeyStaffName } from '../../models/keystaff-name';
import { ContractProgressService } from '../../shared/contract-progress.service';
import { ContractProgress } from '../../models/contract-progress';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-staffs-contract',
  templateUrl: './staffs-contract.component.html',
  styleUrls: ['./staffs-contract.component.css']
})
export class StaffsContractComponent implements OnInit {
  ContractProgressExists: boolean;
  contractprogress: any;
  staffDataSource: any;
  staffDisplayedColumns: string[];
  keyStaffNames: KeyStaffName[];
  selectedKeyStaff: KeyStaffName;

  constructor(public contractProgressService: ContractProgressService,) { }

  ngOnInit() {
    this.staffDisplayedColumns = [ 'contractNames', 'consultantNames', 'contractTypes', 'projectComplitionPersentages'];
    this.contractProgressService.getData("keystaffs", null)
    .subscribe(
      keyStaffNames =>
        this.keyStaffNames = keyStaffNames
      );
  }

  
  init() {
    this.staffDataSource = [];
  }
  public fetchContractPerKeyStaff() {
    this.contractProgressService.getData('keystaff', this.selectedKeyStaff.key)
      .subscribe(data => {

       // debugger;
        if (data && data.length > 0) {
          this.ContractProgressExists = true;
        }

        this.contractprogress = data;

        this.staffDataSource = new MatTableDataSource<ContractProgress>(this.contractprogress);

      });
  }
}
