import { AppNotificationService } from './../../app-services/app-notification.service';
import { Component, OnInit, Input } from '@angular/core';
import { AppMenuService } from '../../app-services/app-menu.service';
import { ContractService } from '../../contract/contract.service';
import { ContractProgressService } from '../../shared/contract-progress.service';
import { MatTableDataSource } from '@angular/material/table';
import { ContractProgress } from '../../models/contract-progress';
import { KeyStaff } from '../../models/contract-Key-staff';

@Component({
  selector: 'app-workload',
  templateUrl: './workload.component.html',
  styleUrls: ['./workload.component.css']
})
export class WorkloadComponent implements OnInit {
  private _triggerOpen: number;
  @Input()
  set triggerOpen(value) {
    this._triggerOpen = value;
    // if page is loading openContract is called
    // from ngOnInit
    if (value >= 1)
      this.openContract(true);
  }
  get triggerOpen(): number {
    return this.triggerOpen;
  }

  //workloadFormGroup: FormGroup;
  dataSource: any;
  staffDataSource: any;

  contractprogress: any;
  displayedColumns: string[];
  staffDisplayedColumns: string[];
  private isInitialized: boolean = false;
  ContractProgressExists: boolean;
  keyStaffNames: KeyStaff[];
  selectedKeyStaff: KeyStaff;

  constructor(protected menuSvc: AppMenuService,
    public contractService: ContractService,
    public contractProgressService: ContractProgressService,
    protected notificationService: AppNotificationService) { }

  ngOnInit() {
    this.menuSvc.getMenuEntries('engineers-workload');
    this.openContract(false);
    this.displayedColumns = ['contractName', 'consultantName', 'keyStaffNames', 'staffPositions', 'contractType', 'staffAssignmentPersentages', 'projectComplitionPersentage'];
    this.staffDisplayedColumns = ['contractNames', 'consultantNames', 'contractTypes', 'projectComplitionPersentages'];
  }

  init() {
    this.isInitialized = true;
    this.dataSource = [];
    this.staffDataSource = [];


    this.fetchContractProgress();
  }

  // @param force to force open another 
  // even if contract is still open
  openContract(force): void {

    const subscription = this.contractService.contractStream.subscribe(contract => {
      if (this.contractService.contract.id) {
        this.init();
      }
      // contract loaded or canceled
      // unsubscribe
      subscription.unsubscribe();
    });

    if (!this.contractService.contract.id || force) {
      this.contractService.openContract();
    }

    if (this.contractService.contract.id && !this.isInitialized) {
      this.init();
    }

  }
  protected fetchContractProgress() {
    this.contractProgressService.getData('progress', this.contractService.contract.id)
      .subscribe(data => {
        if (data && data.length > 0) {
          this.ContractProgressExists = true;
        }

        this.contractprogress = data;


        this.dataSource = new MatTableDataSource<ContractProgress>([this.contractprogress]);
        this.contractprogress = [data]
        this.keyStaffNames = this.contractprogress[0].staff;
      });
  }
  // protected refreshFormAndTable(): void {


  //   this.dataSource = new MatTableDataSource<ContractProgress>(this.contractProgressService.ContractProgress);
  // }
  public fetchContractPerKeyStaff() {
    this.contractProgressService.getData('keystaff', this.selectedKeyStaff)
      .subscribe(data => {

        if (data && data.length > 0) {
          this.ContractProgressExists = true;
        }

        this.contractprogress = data;
console.log(this.contractprogress);
        this.staffDataSource = new MatTableDataSource<ContractProgress>(this.contractprogress);

      });
  }
  //   protected refreshFormAndTable(): void {


  //     this.dataSource = new MatTableDataSource<ContractProgress>(this.contractProgressService.ContractProgress);
  //   }
}
