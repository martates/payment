import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkloadContainerComponent } from './workload-container.component';

describe('WorkloadContainerComponent', () => {
  let component: WorkloadContainerComponent;
  let fixture: ComponentFixture<WorkloadContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkloadContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkloadContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
