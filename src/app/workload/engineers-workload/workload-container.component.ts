import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-workload-container',
  templateUrl: './workload-container.component.html',
  styleUrls: ['./workload-container.component.css']
})
export class WorkloadContainerComponent implements OnInit {

  triggerOpen: number = 0;

  constructor() { }

  ngOnInit() {
  }

  openContract() {
    this.triggerOpen += 1;
  }

}
