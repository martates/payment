import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WorkloadContainerComponent } from './workload-container.component';




const routes: Routes = [
  {
    path: '',
    component: WorkloadContainerComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EngineersWorkLoadRoutingModule { }
