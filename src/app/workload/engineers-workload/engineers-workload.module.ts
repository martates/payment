import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EngineersWorkLoadRoutingModule } from './engineers-workload-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { MatSelectModule, MatDatepickerModule, MatInputModule, MatButtonModule, MatDialogModule, MatTableModule } from '@angular/material';
import { WorkloadContainerComponent } from './workload-container.component';
import { SideInfoModule } from '../../shared/side-info/side-info.module';
import { WorkloadComponent } from './workload.component';
import { ContractProgressService } from '../../shared/contract-progress.service';

import { MaterialModule } from '../../material.module';




@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule, ReactiveFormsModule,    
    SideInfoModule,
    MaterialModule,
    // AppMaterialModule,
    // MatDialogModule,
    // MatTableModule,
    // MatSelectModule,
    // MatInputModule,
    // MatButtonModule,
    EngineersWorkLoadRoutingModule
  ],
  declarations: [WorkloadComponent, WorkloadContainerComponent],
  exports: [WorkloadComponent],
  providers: [ContractProgressService]
})
export class EngineersWorkloadModule { }
