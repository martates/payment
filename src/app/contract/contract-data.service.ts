import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { AsyncDataFacade } from '../shared/async-data-facade';

@Injectable()
export class ContractDataService {

  constructor(private asyncDataFacade: AsyncDataFacade) { }

  getData(query: string, param: any): Observable<any> {

    switch (query) {

      case 'contractNames':

        return this.asyncDataFacade.get('/contracts');

      case 'workContractNames':

        return this.asyncDataFacade.get('/workcontracts');

      case 'contract':
        const contractId = <number>param;
        return this.asyncDataFacade.get('/contracts/' + contractId);

      default:
        return Observable.of(null);
    }
  }
}
