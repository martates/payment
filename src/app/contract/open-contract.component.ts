import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

import { AppNotificationService } from '../app-services/app-notification.service';
import { ContractName } from '../models/contract-name';
import { ContractDataService } from './contract-data.service';


@Component({
  templateUrl: './open-contract.component.html',
  styleUrls: ['./open-contract.component.css']
})
export class OpenContractComponent implements OnInit {

  contractNames: ContractName[];
  selectedContract: ContractName;

  constructor(private dialogRef: MatDialogRef<OpenContractComponent>,
    private contractDataService: ContractDataService,
    private messageNotificationService: AppNotificationService) { }

  ngOnInit() {
    // this.contractDataService.getData('contractNames', null)
    //   .subscribe(
    //   contractNames =>
    //     this.contractNames = contractNames
    //   );


      if(location.pathname == "/engineers-workload"|| location.pathname=="/staffs-contract" )
      {
       
        this.contractDataService.getData('workContractNames', null)
        .subscribe(
        contractNames =>
          this.contractNames = contractNames
        ); 
      }
      else
      {
      this.contractDataService.getData('contractNames', null)
        .subscribe(
        contractNames =>
          this.contractNames = contractNames
        );
      }
  }

  close() {
    if (!this.selectedContract) {
      this.dialogRef.close();
      return;
    }

    this.contractDataService.getData('contract', this.selectedContract.key)
      .subscribe(
        contract => {
          this.dialogRef.close(contract),
        e => this.messageNotificationService.errorNotification.next(e)
      });
  }
}
