import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';

import { Contract } from '../models/contract';
import { ContractPhase } from '../models/contract-phase';
import { OpenContractComponent } from './open-contract.component';

@Injectable()
export class ContractService {

  public contract: Contract;

  public contractStream: Subject<Contract> = null;

  constructor(public dialog: MatDialog,
    private router: Router) {
    this.contract = this.initContract();
    this.contractStream = new Subject();
  }

  openContract() {
    setTimeout(() => {
      const dialogRef = this.dialog.open(OpenContractComponent, {
        width: '450px',
        data: { message: 'Open Contract' },
        autoFocus: true,
        disableClose: true
      });

      dialogRef.afterClosed().subscribe(contract => {
        this.contract = contract ? contract : this.initContract();

        this.contractStream.next(this.contract);

        if(!this.contract.id) {
          this.router.navigate(['/home']);
        }
        
      });
    }, 100);
  }

  initContract(): Contract {
    return {
      id: 0,
      name: '',
      number: '',
      consultantName: '',
      contractAmount: 0,
      phasesTotal: 0,
      vat: 0,
      grandTotal: 0,
      advanceAmount: 0,
      contractSignedOn: null,
      phases: new Array<ContractPhase>()
    };
  }
}
