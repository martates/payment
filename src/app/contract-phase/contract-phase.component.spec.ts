import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractPhaseComponent } from './contract-phase.component';

describe('ContractInfoComponent', () => {
  let component: ContractPhaseComponent;
  let fixture: ComponentFixture<ContractPhaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractPhaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractPhaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
