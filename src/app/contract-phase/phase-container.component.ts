import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-phase-container',
  templateUrl: './phase-container.component.html',
  styleUrls: ['./phase-container.component.css']
})
export class PhaseContainerComponent implements OnInit {

  triggerOpen: number = 0;

  constructor() { }

  ngOnInit() {
  }

  openContract() {
    this.triggerOpen += 1;
  }
}
