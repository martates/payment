import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import {
//   MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatDialogModule,
//   MatInputModule, MatNativeDateModule, MatSelectModule, MatTableModule
// } from '@angular/material';

import { SideInfoModule } from '../shared/side-info/side-info.module';
import { ContractPhaseRoutingModule } from './contract-phase-routing.module';
import { ContractPhaseComponent } from './contract-phase.component';
import { PhaseContainerComponent } from './phase-container.component';
import { AppMaterialModule } from '../app-material.module';
import { MaterialModule } from '../material.module';



@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule, ReactiveFormsModule,
    MaterialModule,
    // AppMaterialModule,
    // MatSelectModule, MatDatepickerModule,
    // MatNativeDateModule, MatInputModule,
    // MatButtonModule, MatTableModule,
    // MatDialogModule, MatCheckboxModule,
    SideInfoModule,
    ContractPhaseRoutingModule
  ],
  declarations: [PhaseContainerComponent, ContractPhaseComponent],
  exports: [ContractPhaseComponent]
})
export class ContractPhaseModule { }
