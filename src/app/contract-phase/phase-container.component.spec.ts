import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhaseContainerComponent } from './phase-container.component';

describe('PhaseContainerComponent', () => {
  let component: PhaseContainerComponent;
  let fixture: ComponentFixture<PhaseContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhaseContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhaseContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
