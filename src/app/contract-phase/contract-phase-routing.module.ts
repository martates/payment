import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PhaseContainerComponent } from './phase-container.component';

const routes: Routes = [
  {
    path: '',
    component: PhaseContainerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContractPhaseRoutingModule { }
