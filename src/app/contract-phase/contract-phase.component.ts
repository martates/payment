import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';

import { AppMenuService } from '../app-services/app-menu.service';
import { AppNotificationService } from '../app-services/app-notification.service';
import { ContractService } from '../contract/contract.service';
import { ContractPhase } from '../models/contract-phase';
import { PhaseCurrencyBreakdown } from '../models/phase-currency-breakdown';
import { ContractPhaseComputeService } from '../shared/contract-phase-compute.service';
import { CurrencyDataService } from '../shared/currency-breakdown/currency-data.service';
import { CurrencyService } from '../shared/currency-breakdown/currency.service';
import { DeliverablesService } from '../shared/deliverables.service';
import { ValueFormatterService } from '../shared/value-formatter.service';

const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-contract-phase',
  templateUrl: './contract-phase.component.html',
  styleUrls: ['./contract-phase.component.css'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})
export class ContractPhaseComponent implements OnInit {

  private _triggerOpen: number;
  @Input()
  set triggerOpen(value) {
    this._triggerOpen = value;
    // if page is loading openContract is called
    // from ngOnInit
    if (value >= 1)
      this.openContract(true);
  }
  get triggerOpen(): number {
    return this.triggerOpen;
  }

  contractInfoFormGroup: FormGroup;
  dataSource: any;
  displayedColumns: string[];
  addAmendLabel: string;
  deleteResetLabel: string;
  currencyBreakdowns = new Array<PhaseCurrencyBreakdown>();
  private isInitialized: boolean = false;
  deliverablesExist: boolean;

  constructor(
    protected menuSvc: AppMenuService,
    private builder: FormBuilder,
    protected computeService: ContractPhaseComputeService,
    public contractService: ContractService,
    protected currencyService: CurrencyService,
    protected currencyDataService: CurrencyDataService,
    protected valueFormatter: ValueFormatterService,
    protected notificationService: AppNotificationService,
    private deliverableDataService: DeliverablesService) {
  }

  ngOnInit() {
    this.addAmendLabel = 'Save';
    this.deleteResetLabel = 'Clear';
    this.menuSvc.getMenuEntries('contract-phase');
    this.createForm();
    this.displayedColumns = ['phaseName', 'phaseDescription', 'startDate', 'endDate', 'lumpSum', 'provisionalSum', 'reimbursable', 'advance'];
    this.openContract(false);
  }

  init() {
    this.isInitialized = true;
    this.fetchPhases();
  }

  protected fetchPhases() {
    this.deliverableDataService.getData('deliverables', this.contractService.contract.id)
      .subscribe(data => {
        if (data && data.length > 0) {
          this.deliverablesExist = true;
        }

        this.computeService.getPhases(this.contractService.contract)
          .subscribe(_ => {
            this.refreshFormAndTable()
          });
      });
  }

  protected createForm(): void {
    this.contractInfoFormGroup = this.builder.group({
      idControl: [0],
      nameControl: [null, Validators.required],
      descriptionControl: [null, Validators.required],
      startDateControl: [null, Validators.required],
      endDateControl: [null, Validators.required],
      lumpSumControl: [{ value: null, disabled: true }, Validators.required],
      provisionalSumControl: [{ value: null, disabled: true }, Validators.required],
      reimbursableControl: [{ value: null, disabled: true }, Validators.required],
      advancePercentControl: [null, Validators.required],
      advanceAmountControl: [{ value: null, disabled: true }],
      advanceNotTakenControl: [false],
    });

    this.onFormValuesChange();
  }

  onFormValuesChange(): void {

    this.contractInfoFormGroup.get('advancePercentControl').valueChanges.subscribe(val => {
      const advanceAmount = this.computeAdvance();

      this.patchPhaseAdvance(advanceAmount);

      this.UpdatePhaseAndContractTotals();

    });

    this.contractInfoFormGroup.get('lumpSumControl').valueChanges.subscribe(val => {

      const advanceAmount = this.computeAdvance();

      this.patchPhaseAdvance(advanceAmount);

      this.UpdatePhaseAndContractTotals();

    });

    this.contractInfoFormGroup.get('provisionalSumControl').valueChanges.subscribe(val => {

      this.UpdatePhaseAndContractTotals();

    });

    this.contractInfoFormGroup.get('reimbursableControl').valueChanges.subscribe(val => {

      this.UpdatePhaseAndContractTotals();

    });
  }

  computeAdvance(): number {

    const phase = this.extractPhase();

    this.computeService.calculateMoneyFromPercentage(this.contractService.contract, phase);

    return phase.advanceAmount;
  }

  // @param force to force open another 
  // even if contract is still open
  openContract(force): void {

    const subscription = this.contractService.contractStream.subscribe(contract => {
      if (this.contractService.contract.id) {
        this.init();
      }
      // contract loaded or canceled
      // unsubscribe
      subscription.unsubscribe();
    });

    if (!this.contractService.contract.id || force) {
      this.contractService.openContract();
    }

    if (this.contractService.contract.id && !this.isInitialized) {
      this.init();
    }
  }

  save(): void {
    const phase = this.extractPhase();

    const result = this.computeService.validatePhase(phase);

    if (!result.isSuccessful) {
      this.notificationService.messageNotification.next(result.message);
      return;
    }

    // send contract if advance changes as a result of phase 
    // changes
    phase.contractAdvanceAmount = this.contractService.contract.advanceAmount;

    this.computeService.save(phase).subscribe(
      _ => this.saveCurrencyBreakdown(phase),
      error => this.onCurrencySaved());
  }

  delete(): void {
    const p = this.extractPhase();

    this.computeService.deletePhase(p)
      .subscribe(_ => {
        this.currencyBreakdowns = new Array<PhaseCurrencyBreakdown>();

        this.refreshFormAndTable();

        this.addAmendLabel = 'Save';
        this.deleteResetLabel = 'Clear';
      });
  }

  protected refreshFormAndTable(): void {
    this.contractInfoFormGroup.reset();

    this.dataSource = new MatTableDataSource<ContractPhase>(this.contractService.contract.phases);
  }

  public getCanDelete(): boolean {
    // if the form is marked dirty, enable clear
    if (!this.contractInfoFormGroup.untouched &&
      !this.contractInfoFormGroup.pristine) {
      return true;
    }

    // if the form is selected with existing phase, enable delete
    const phase = this.extractPhase();
    if (phase.id) {
      return true;
    }

    return false;
  }

  protected selectContractPhase(contractPhase: ContractPhase, editingOriginal = true): void {

    // if contractPhase is supplemented, selection should be prohibited 
    // because it no longer owns the phase
    const phases = this.contractService.contract.phases;

    const replaced = phases.filter(phase => phase.contractPhaseId === contractPhase.id);

    if (replaced.length > 0) {
      this.notificationService.warningNotification.next('This phase is replaced by a supplemental agreement. You may edit the phase inside the supplement.');
      return;
    }

    this.computeService.selectContractPhase(this.contractInfoFormGroup, contractPhase, editingOriginal);

    this.currencyBreakdowns = new Array<PhaseCurrencyBreakdown>();
    this.addAmendLabel = 'Amend';
    this.deleteResetLabel = 'Delete';
  }

  //# Handle currency
  openCurrencyDialog(amountType: string) {

    const currencies = this.contractService.contract.currencyBreakdowns;

    const phase = this.extractPhase();

    const subscription = this.currencyService.openCurrencyDialog(amountType, currencies, phase)
      .subscribe((currencyBreakdowns: PhaseCurrencyBreakdown[]) => {

        if (!currencyBreakdowns || currencyBreakdowns.length === 0) {
          return;
        }

        this.mergeCurrencyBreakdowns(currencyBreakdowns);

        let totalInEtb = this.currencyBreakdowns
          .filter(breakdown => breakdown.type === amountType)
          .map(breakdown => +breakdown.etbAmount)
          .reduce((acc, n) => acc + n);

        const formattedEtbAmount = this.valueFormatter.convertToCurrency(totalInEtb, 'ETB ');

        switch (amountType) {
          case 'Lump Sum':
            this.contractInfoFormGroup.patchValue({
              lumpSumControl: formattedEtbAmount
            });

            break;

          case 'Provisional Sum':
            this.contractInfoFormGroup.patchValue({
              provisionalSumControl: formattedEtbAmount
            });

            break;
          case 'Reimbursable':
            this.contractInfoFormGroup.patchValue({
              reimbursableControl: formattedEtbAmount
            });

            break;

          default:
            break;
        }
        subscription.unsubscribe();
      });
  }

  protected saveCurrencyBreakdown(phase): void {

    if (this.currencyBreakdowns.length === 0) {
      this.onCurrencySaved();
      return;
    }

    // post (for new breakdowns)
    const newCurrencyEntries = this.currencyBreakdowns.filter(c => !c.id);

    newCurrencyEntries.forEach(breakdown => {
      breakdown.contractPhaseId = phase.id;
    });

    // put (for existing breakdowns)
    const existingCurrencyEntries = this.currencyBreakdowns.filter(c => c.id);

    if (existingCurrencyEntries.length > 0 && newCurrencyEntries.length > 0) {
      this.currencyDataService.putData('', existingCurrencyEntries, phase.id)
        .subscribe(_ =>
          this.currencyDataService.postData('currencyBreakdowns', newCurrencyEntries, phase.id).subscribe(_ => {
            this.onCurrencySaved();
          }),
          error => this.onCurrencySaved()
        );
    } else if (existingCurrencyEntries.length > 0 && newCurrencyEntries.length === 0) {
      this.currencyDataService.putData('', existingCurrencyEntries, phase.id)
        .subscribe(_ => this.onCurrencySaved());
    } else if (existingCurrencyEntries.length === 0 && newCurrencyEntries.length > 0) {
      this.currencyDataService.postData('currencyBreakdowns', newCurrencyEntries, phase.id)
        .subscribe(_ => this.onCurrencySaved());
    }
  }

  onCurrencySaved() {
    this.refreshFormAndTable();
    this.currencyBreakdowns = new Array<PhaseCurrencyBreakdown>();
    this.addAmendLabel = 'Save';
    this.deleteResetLabel = 'Clear';
  }

  mergeCurrencyBreakdowns(currencyBreakdowns) {

    currencyBreakdowns.forEach(currencyBreakdown => {
      // same amount type (eg. LS...)
      // could be send multiple times- merge them

      if (this.currencyBreakdowns.length === 0) {
        this.currencyBreakdowns.push(currencyBreakdown);
      } else {

        let found: boolean;

        this.currencyBreakdowns.forEach(breakdown => {
          if (breakdown.type === currencyBreakdown.type &&
            breakdown.currency === currencyBreakdown.currency) {

            /* 
              Do not merge breakdown with itself. The only viable case is
              the currencyBreakdown is new (breakdown.id === null) 
            */
            if (!currencyBreakdown.id) {

              breakdown.amount = (+breakdown.amount) + (+currencyBreakdown.amount);

              breakdown.computeEtbAmount();

              found = true;
            }
          }
        });

        if (!found) { // if found,  entry merged; no need to push
          this.currencyBreakdowns.push(currencyBreakdown);
        }
      }
    });
  }

  //#end region

  patchPhaseAdvance(advanceAmount) {
    this.contractInfoFormGroup.patchValue({
      advanceAmountControl: this.valueFormatter.convertToCurrency(advanceAmount, 'ETB ')
    });
  }

  UpdatePhaseAndContractTotals() {
    // update phase and contract totals
    const phase = this.extractPhase();
    this.computeService.calculatePhasesTotal(phase);
  }

  protected extractPhase(): ContractPhase {
    return this.computeService.extractContractPhase(this.contractInfoFormGroup.controls);
  }

  setRowStyles(contractPhase) {
    if (contractPhase.supplementId !== null) {
      return {
        'background-color': 'rgba(187, 187, 187, 0.555)'
      };
    }
  }
}
