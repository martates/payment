import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import {
//   MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatDividerModule, MatExpansionModule,
//   MatIconModule, MatInputModule, MatListModule, MatNativeDateModule, MatOptionModule, MatRadioModule,
//   MatSelectModule, MatTableModule, MatTabsModule
// } from '@angular/material';
import { ContractPhaseModule } from '../../contract-phase/contract-phase.module';
import { SharedModule } from '../../shared/shared.module';
import { SideInfoModule } from '../../shared/side-info/side-info.module';
import { EditSupplementComponent } from './edit-supplement/edit-supplement.component';
import { ExistingPhaseComponent } from './existing-phases/existing-phase.component';
import { SupplementDeliverableComponent } from './deliverables/deliverables-list/supplement-deliverable.component';
import { NewPhaseComponent } from './new-phases/new-phase.component';
import { SupplementDetailComponent } from './supplement-detail/supplement-detail.component';
import { SupplementListComponent } from './supplement-list/supplement-list.component';
import { SupplementComponent } from './supplement.component';
import { SupplementRoutingModule } from './supplement.routing.module';
import { EditDeliverableComponent } from './deliverables/edit-deliverable/edit-deliverable.component';
import { SupplementDeliverablesContainerComponent } from './deliverables/supplement-deliverables-container.component';
import { EffectiveContractStateComponent } from './effective-contract-state/effective-contract-state.component';

import { MaterialModule } from '../../material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    // MatTabsModule, MatInputModule,
    // MatDatepickerModule, MatNativeDateModule,
    // MatExpansionModule, MatIconModule,
    // MatButtonModule, MatTableModule,
    // MatRadioModule, MatOptionModule,
    // MatSelectModule, MatListModule,
    // MatDividerModule, MatCheckboxModule,
    // AppMaterialModule,
    MaterialModule,
    SideInfoModule,
    SharedModule,
    SupplementRoutingModule,
    ContractPhaseModule
  ],
  declarations: [SupplementComponent, NewPhaseComponent, SupplementDeliverableComponent, ExistingPhaseComponent, SupplementListComponent, EditSupplementComponent, SupplementDetailComponent, EditDeliverableComponent, SupplementDeliverablesContainerComponent, EffectiveContractStateComponent],
  providers: []
})
export class SupplementModule { }
