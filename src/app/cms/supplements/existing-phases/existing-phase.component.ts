
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';

import { AppMenuService } from '../../../app-services/app-menu.service';
import { AppNotificationService } from '../../../app-services/app-notification.service';
import { ContractPhaseComponent } from '../../../contract-phase/contract-phase.component';
import { ContractService } from '../../../contract/contract.service';
import { ContractPhase } from '../../../models/contract-phase';
import { Deliverable } from '../../../models/deliverable';
import { Supplement } from '../../../models/supplement';
import { ContractPhaseComputeService } from '../../../shared/contract-phase-compute.service';
import { CurrencyDataService } from '../../../shared/currency-breakdown/currency-data.service';
import { CurrencyService } from '../../../shared/currency-breakdown/currency.service';
import { DeliverablesService } from '../../../shared/deliverables.service';
import { ValueFormatterService } from '../../../shared/value-formatter.service';
import { SupplementComputeService } from '../../../shared/supplement-compute.service';


@Component({
  templateUrl: './existing-phase.component.html',
  styleUrls: ['./existing-phase.component.css']
})
export class ExistingPhaseComponent extends ContractPhaseComponent implements OnInit {

  supplement: Supplement;
  phases: ContractPhase[];

  constructor(menuSvc: AppMenuService,
    builder: FormBuilder,
    phaseComputeService: ContractPhaseComputeService,
    contractService: ContractService,
    currencyService: CurrencyService,
    currencyDataService: CurrencyDataService,
    valueFormatter: ValueFormatterService,
    notificationService: AppNotificationService,
    private supplementComputeService: SupplementComputeService,
    deliverableDataService: DeliverablesService,
    private router: Router,
    private route: ActivatedRoute) {

    super(menuSvc, builder, phaseComputeService,
      contractService, currencyService, currencyDataService,
      valueFormatter, notificationService, deliverableDataService);
  }

  ngOnInit() {

    // grab supplement before calling super.ngOnInit
    const id = this.route.snapshot.paramMap.get('id');

    if (+id) {
      this.supplement = this.supplementComputeService.getSupplement(id);
    }

    super.ngOnInit();

    this.displayedColumns = ['belongsTo', 'phaseName', 'phaseDescription', 'startDate', 'endDate', 'lumpSum', 'provisionalSum', 'reimbursable', 'advance'];

    this.menuSvc.getMenuEntries('Supplemental Agreement');
  }

  protected refreshFormAndTable(): void {
    this.contractInfoFormGroup.reset();

    const phases = this.contractService.contract.phases;

    phases.forEach(phase => {
      if (phase.supplementId == null) {
        phase['belongsTo'] = 'Original Contract';
      } else {
        phase['belongsTo'] = `Supplement ${this.supplement.number}`;
      }
    });

    // update supplements (in supplement component side info) with any changes to phases
    this.supplementComputeService.supplements.forEach((supplement: Supplement) => {
      supplement.phases = phases.filter(p => p.supplementId === supplement.id);
    });

    /* See in project repository: /design-docs/Modify contract phases in supplements
         
         filter phases that can be overridden. These are 
           a) if no previous supplements exist, all 
              the original contract phases
           b) if supplements exist
               b-1 all original contract phases that 
                   aren't overridden by subsequent supplements
               b-2 all supplement phases that aren't overridden 
                   by subsequent supplements           
       */
    this.phases = this.filterLatestPhases(phases);

    this.dataSource = new MatTableDataSource<ContractPhase>(this.phases);
  }

  filterLatestPhases(phases: ContractPhase[]): ContractPhase[] {
    return phases.filter((phase, index, arr) => {
      // not replaced
      const replaced = arr.filter(p => p.contractPhaseId === phase.id);

      return (replaced.length === 0);
    })
  }

  setRowStyles(contractPhase) {
    if (contractPhase.belongsTo === 'Original Contract') {
      return {
        'background-color': 'rgba(187, 187, 187, 0.555)'
      };
    }
  }

  save(): void {
    // when update is called, the phase could be associated with 
    // 1. The original contract (a new supplement)
    // 2. An existing supplement
    // and the action could be
    // a. Change the value of the existing phase
    // b. Delete the phase
    const phase = this.computeService.extractContractPhase(this.contractInfoFormGroup.controls);

    if (!phase.id) {
      this.notificationService.warningNotification.next('You may only edit existing phases. Try \'Create phase\' link to create new phase');
      return;
    }
    // copy the phase
    const clonedPhase: ContractPhase = Object.assign({}, phase);

    // reset the cloned id
    clonedPhase.id = 0;

    // 1-a Original contract (new supplement), changed figures (Lump Sum)
    if (!phase.supplementId) {

      // associate it with the new supplement by updating supplement id field with the new supplement
      clonedPhase.supplementId = this.supplement.id;

      // update the replaced phase field with the current phase id
      clonedPhase.contractPhaseId = phase.id;

      this.computeService.save(clonedPhase).subscribe(phaseRes => {
        this.saveCurrencyBreakdown(phaseRes);
        this.notificationService.messageNotification.next('Contract Phase saved');
      });
    } else {
      // 2-a An existing supplement, changed figures (Lump Sum)
      // - if current supplement is not the same as the existing supplement then the supplement 
      //   is being supplemented
      if (phase.supplementId === this.supplement.id) {
        //  send put
        this.computeService.save(phase).subscribe(phaseRes => {
          this.saveCurrencyBreakdown(phaseRes);
          this.notificationService.messageNotification.next('Contract Phase saved');
        });
      } else {
        // associate it with the new supplement by updating supplement id field with the new supplement
        clonedPhase.supplementId = this.supplement.id;

        // update replaced supplement id with the old supplement id
        clonedPhase.replacedSupplementId = phase.supplementId;

        // update the replaced phase field with the current phase id
        clonedPhase.contractPhaseId = phase.id;

        this.computeService.save(clonedPhase).subscribe(phaseRes => {
          this.saveCurrencyBreakdown(phaseRes);
          this.notificationService.messageNotification.next('Contract Phase saved');
        });
      }
    }

  }

  protected saveCurrencyBreakdown(phase): void {

    if (this.currencyBreakdowns.length === 0) {
      this.onCurrencySaved();
      return;
    }

    // separate what needs to be posted and updated
    this.currencyBreakdowns.forEach(breakdown => {
      
      if(breakdown.contractPhaseId !== phase.id) {
        breakdown.id = null; // reset id to prompt post 
      }

      breakdown.contractPhaseId = phase.id;
    });

    // post (for new breakdowns)
    const newCurrencyEntries = this.currencyBreakdowns.filter(c => !c.id);

    // put (for existing breakdowns)
    const existingCurrencyEntries = this.currencyBreakdowns.filter(c => c.id);

    if (existingCurrencyEntries.length > 0 && newCurrencyEntries.length > 0) {
      this.currencyDataService.putData('', existingCurrencyEntries, phase.id)
        .subscribe(_ =>
          this.currencyDataService.postData('currencyBreakdowns', newCurrencyEntries, phase.id).subscribe(_ => {
            this.onCurrencySaved();
          }),
          error => this.onCurrencySaved()
        );
    } else if (existingCurrencyEntries.length > 0 && newCurrencyEntries.length === 0) {
      this.currencyDataService.putData('', existingCurrencyEntries, phase.id)
        .subscribe(_ => this.onCurrencySaved());
    } else if (existingCurrencyEntries.length === 0 && newCurrencyEntries.length > 0) {
      this.currencyDataService.postData('currencyBreakdowns', newCurrencyEntries, phase.id)
        .subscribe(_ => this.onCurrencySaved());
    }
  }

  delete(): void {
    // when update is called, the phase could be associated with 
    // 1. The original contract (a new supplement)
    // 2. An existing supplement
    // and the action could be
    // a. Change the value of the existing phase
    // b. Delete the phase
    const phase = this.computeService.extractContractPhase(this.contractInfoFormGroup.controls);

    if (!phase.id) {
      this.contractInfoFormGroup.reset();
      return;
    }

    // copy the phase
    const clonedPhase: ContractPhase = Object.assign({}, phase);

    // reset the cloned id
    clonedPhase.id = 0;

    if (!phase.supplementId) {
      // 1-b Original contract, delete phase
      // copy the phase
      // associate it with the new supplement by updating supplement id field with the new supplement
      clonedPhase.supplementId = this.supplement.id;

      // update the replaced phase field with the current phase id
      clonedPhase.contractPhaseId = phase.id;

      // zero-out all phase values
      clonedPhase.lumpSum = 0;
      clonedPhase.reimbursable = 0;

      // send post (note this is a post and not delete b/c a new zero valued 
      // phase is created and the original is kept without deletion)
      this.computeService.save(clonedPhase).subscribe(phaseRes => {
        this.refreshFormAndTable();
      });
    } else {

      // 2-b An existing supplement, delete phase
      // associate it with the new supplement by updating supplement id field with the new supplement
      clonedPhase.supplementId = this.supplement.id;

      // - update replaced supplement id with the old supplement id
      clonedPhase.replacedSupplementId = phase.supplementId;

      // - update the replaced phase field with the current phase id
      clonedPhase.contractPhaseId = phase.id;

      // - zero-out all phase values
      clonedPhase.lumpSum = 0;
      clonedPhase.reimbursable = 0;

      this.computeService.save(clonedPhase).subscribe(phaseRes => {
        this.refreshFormAndTable();
      });
    }
  }

  protected selectContractPhase(contractPhase: ContractPhase, editingOriginal = false): void {

    this.computeService.selectContractPhase(this.contractInfoFormGroup, contractPhase, editingOriginal);

    this.addAmendLabel = 'Amend';
    this.deleteResetLabel = 'Delete';
  }

  protected extractPhase(): ContractPhase {
    const phase = this.computeService.extractContractPhase(this.contractInfoFormGroup.controls);

    phase.supplementId = this.supplement.id;

    return phase;
  }
}
