import { ComponentFixture, TestBed, async } from '@angular/core/testing';

import { ExistingPhaseComponent } from './existing-phase.component';

describe('ExistingPhasesComponent', () => {
  let component: ExistingPhaseComponent;
  let fixture: ComponentFixture<ExistingPhaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExistingPhaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExistingPhaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
