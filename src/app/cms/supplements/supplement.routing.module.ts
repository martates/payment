import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SupplementDeliverableComponent } from './deliverables/deliverables-list/supplement-deliverable.component';
import { EditDeliverableComponent } from './deliverables/edit-deliverable/edit-deliverable.component';
import { SupplementDeliverablesContainerComponent } from './deliverables/supplement-deliverables-container.component';
import { EditSupplementComponent } from './edit-supplement/edit-supplement.component';
import { EffectiveContractStateComponent } from './effective-contract-state/effective-contract-state.component';
import { ExistingPhaseComponent } from './existing-phases/existing-phase.component';
import { NewPhaseComponent } from './new-phases/new-phase.component';
import { SupplementDetailComponent } from './supplement-detail/supplement-detail.component';
import { SupplementListComponent } from './supplement-list/supplement-list.component';
import { SupplementComponent } from './supplement.component';

const routes: Routes = [
  {
    path: '',
    component: SupplementComponent,
    children: [
      {
        path: '',
        component: SupplementListComponent
      },
      {
        path: 'edit-supplement/:id',
        component: EditSupplementComponent
      },
      {
        path: 'effective-contract-state',
        component: EffectiveContractStateComponent
      },
      {
        path: 'supplement-detail/:id',
        component: SupplementDetailComponent,
        children: [
          {
            path: 'new-phase',
            component: NewPhaseComponent
          },
          {
            path: 'modify-phase',
            component: ExistingPhaseComponent
          },
          {
            path: 'deliverables',
            component: SupplementDeliverablesContainerComponent,
            children: [
              {
                path: '',
                component: SupplementDeliverableComponent
              },
              {
                path: ':id',
                component: EditDeliverableComponent
              }
            ]
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupplementRoutingModule { }
