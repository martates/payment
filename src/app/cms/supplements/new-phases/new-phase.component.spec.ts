import { ComponentFixture, TestBed, async } from '@angular/core/testing';

import { NewPhaseComponent } from './new-phase.component';

describe('PhaseComponent', () => {
  let component: NewPhaseComponent;
  let fixture: ComponentFixture<NewPhaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPhaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPhaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
