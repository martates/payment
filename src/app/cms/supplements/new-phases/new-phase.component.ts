import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';

import { AppMenuService } from '../../../app-services/app-menu.service';
import { AppNotificationService } from '../../../app-services/app-notification.service';
import { ContractPhaseComponent } from '../../../contract-phase/contract-phase.component';
import { ContractService } from '../../../contract/contract.service';
import { ContractPhase } from '../../../models/contract-phase';
import { Supplement } from '../../../models/supplement';
import { ContractPhaseComputeService } from '../../../shared/contract-phase-compute.service';
import { CurrencyDataService } from '../../../shared/currency-breakdown/currency-data.service';
import { CurrencyService } from '../../../shared/currency-breakdown/currency.service';
import { DeliverablesService } from '../../../shared/deliverables.service';
import { ValueFormatterService } from '../../../shared/value-formatter.service';
import { SupplementComputeService } from '../../../shared/supplement-compute.service';



@Component({
  templateUrl: './new-phase.component.html',
  styleUrls: ['./new-phase.component.css']
})
export class NewPhaseComponent extends ContractPhaseComponent implements OnInit {

  supplement: Supplement;

  constructor(menuSvc: AppMenuService,
    builder: FormBuilder,
    phaseComputeService: ContractPhaseComputeService,
    contractService: ContractService,
    currencyService: CurrencyService,
    currencyDataService: CurrencyDataService,
    valueFormatter: ValueFormatterService,
    messageNotificationService: AppNotificationService,
    private supplementComputeService: SupplementComputeService,
    deliverableDataService: DeliverablesService,
    private router: Router,
    private route: ActivatedRoute) {

    super(menuSvc, builder, phaseComputeService,
      contractService, currencyService, currencyDataService,
      valueFormatter, messageNotificationService, deliverableDataService);
  }

  ngOnInit() {

    // grab supplement before calling super.ngOnInit
    const id = this.route.snapshot.paramMap.get('id');

    if (+id) {
      this.supplement = this.supplementComputeService.getSupplement(id);
    }

    super.ngOnInit();

    this.displayedColumns = ['belongsTo', 'phaseName', 'phaseDescription', 'startDate', 'endDate', 'lumpSum', 'provisionalSum', 'reimbursable', 'advance'];
    
    this.menuSvc.getMenuEntries('Supplemental Agreement');
  }

  protected refreshFormAndTable(): void {
    this.contractInfoFormGroup.reset();

    const phases = this.contractService.contract.phases;

    phases.forEach(phase => {
      if (phase.supplementId === null) {
        phase['belongsTo'] = 'Original Contract';
      } else {
        phase['belongsTo'] = `Supplement ${this.supplement.number}`;
      }
    });

    // update supplements (in supplement component side info) with any changes to phases
    this.supplementComputeService.supplements.forEach((supplement: Supplement) => {
      supplement.phases = phases.filter(p => p.supplementId === supplement.id);
    });

    this.dataSource = new MatTableDataSource<ContractPhase>(this.contractService.contract.phases);
  }

  protected extractPhase(): ContractPhase {
    const phase = this.computeService.extractContractPhase(this.contractInfoFormGroup.controls);

    phase.supplementId = this.supplement.id;

    return phase;
  }

  protected selectContractPhase(contractPhase: ContractPhase): void {
    if (contractPhase.supplementId !== this.supplement.id) {
      this.notificationService.errorNotification.next(`${contractPhase.name} does not belong to this supplement and therefore not editable.`);
    } else {
      super.selectContractPhase(contractPhase, false);
    }
  }

  setRowStyles(contractPhase) {
    if (contractPhase.belongsTo === 'Original Contract') {
      return {
        'background-color': 'rgba(187, 187, 187, 0.555)'
      };
    }
  }

  showDeliverableEditor(phase) {
    phase.editDeliverable = true;
  }

  setEditDeliverable(phase) {
    phase.editDeliverable = false;
  }
}
