import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/operator/switchMap';


@Component({
  templateUrl: './supplement-detail.component.html',
  styleUrls: ['./supplement-detail.component.css']
})
export class SupplementDetailComponent implements OnInit {

  supplementIndex: string;

  editPhase: boolean = false;

  constructor(private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.supplementIndex = this.route.snapshot.paramMap.get('id');
  }

  showPhaseEditor() {
    this.editPhase = true;
  }

  setEditPhase(state) {
    this.editPhase = state;
  }

  createPhase() {
    this.router.navigate(['new-phase', { id: this.supplementIndex }], { relativeTo: this.route });
  }

  modifyPhase() {
    this.router.navigate(['modify-phase', { id: this.supplementIndex }], { relativeTo: this.route });
  }

  editDeliverable() {
    this.router.navigate(['deliverables', { id: this.supplementIndex }], { relativeTo: this.route });
  }
  
  back() {
    this.router.navigate(['/supplements'], { relativeTo: this.route });
  }
}
