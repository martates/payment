import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/operator/switchMap';

import { AppNotificationService } from '../../../app-services/app-notification.service';
import { ContractService } from '../../../contract/contract.service';
import { ActionType } from '../../../models/action-type';
import { Supplement } from '../../../models/supplement';
import { SupplementComputeService } from '../../../shared/supplement-compute.service';
import { SupplementService } from '../../../shared/supplement.service';


@Component({
  selector: 'app-edit-supplement',
  templateUrl: './edit-supplement.component.html',
  styleUrls: ['./edit-supplement.component.css']
})
export class EditSupplementComponent implements OnInit {

  supplement: Supplement;

  supplementFormGroup: FormGroup;
  addAmendLabel: string;
  deleteResetLabel: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private builder: FormBuilder,
    private dataService: SupplementService,
    private computeService: SupplementComputeService,
    public contractService: ContractService,
    private notificationService: AppNotificationService) { }

  ngOnInit() {
    this.createForm();
    this.addAmendLabel = 'Save';
    this.deleteResetLabel = 'Clear';
 
    const index = this.route.snapshot.paramMap.get('id');

    if (+index) {
      this.supplement = this.computeService.getSupplement(index);

      setTimeout(() => {
        this.selectSupplement(this.supplement);
      }, 500);
    }
  }

  private createForm(): void {
    this.supplementFormGroup = this.builder.group({
      idControl: [0],
      numberControl: ['', Validators.required],
      nameControl: [{}, Validators.required],
      agreementDateControl: [null, Validators.required],
      purposeControl: [0, Validators.required]
    });
  }

  public getCanDelete(): boolean {
    // if the form is marked dirty, enable clear
    if (!this.supplementFormGroup.untouched &&
      !this.supplementFormGroup.pristine) {
      return true;
    }

    // if the form is selected with existing supplement, enable delete
    const supplement = this.getSupplementFromFormModel();
    if (supplement.id) {
      return true;
    }

    return false;
  }

  private getSupplementFromFormModel(): Supplement {
    const formModel = this.supplementFormGroup.value;

    const supplement: Supplement = {
      id: formModel.idControl || 0,
      contractId: this.contractService.contract ? this.contractService.contract.id : 0,
      number: formModel.numberControl,
      name: formModel.nameControl,
      agreementDate: formModel.agreementDateControl,
      purpose: formModel.purposeControl,
      supplementAmount: 0
    };

    return supplement;
  }

  private selectSupplement(supplement: Supplement): void {
    this.supplementFormGroup.setValue({
      idControl: supplement.id,
      numberControl: supplement.number,
      nameControl: supplement.name,
      agreementDateControl: supplement.agreementDate,
      purposeControl: supplement.purpose
    });

    this.addAmendLabel = 'Amend';
    this.deleteResetLabel = 'Delete';
  }

  saveSupplement() {
    const supplement: Supplement = this.getSupplementFromFormModel();

    const result = this.computeService.validateSupplement(supplement);

    if (!result.isSuccessful) {
      this.notificationService.errorNotification.next(result.message);
      return;
    }

    // Add new
    if (!supplement.id) {
      this.dataService.postData('supplement', supplement, this.contractService.contract.id).subscribe(
        data => {
          // Fire save complete event
          this.handleResponse(ActionType.Add, data, 'saved');
        }
      );
    } else {
      this.dataService.putData('supplement', supplement, this.contractService.contract.id).subscribe(
        data => {
          this.handleResponse(ActionType.Update, data, 'updated');
        });
    }
  }

  handleResponse(actionType: ActionType, payload, message) {

    this.supplementFormGroup.reset();

    if (message) {
      this.notificationService.messageNotification.next(`Supplement ${message}`);
    }

    this.addAmendLabel = 'Save';
    this.deleteResetLabel = 'Clear';

    this.computeService.handleSupplementEdit({ actionType: actionType, supplement: payload });

    this.router.navigate(['/supplements']);
  }

  deleteSupplement() {
    const supplement: Supplement = this.getSupplementFromFormModel();

    if (!supplement.id) {
      this.handleResponse(ActionType.Clear, null, '');
    } else {
      this.dataService.deleteData('', supplement, this.contractService.contract.id).subscribe(
        data => {
          this.handleResponse(ActionType.Delete, supplement, 'deleted');
        });
    }
  }

  back() {
    // this.computeService.handleSupplementEdit({ actionType: ActionType.Cancel, supplement: null });
    this.router.navigate(['/supplements']);
  }
}
