import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { ContractService } from '../../../../contract/contract.service';
import { DeliverablesComputeService } from '../../../../shared/deliverables-compute.service';
import { DeliverablesComponent } from '../../../deliverables/deliverables.component';
import { ContractPhase } from '../../../../models/contract-phase';
import { AppMenuService } from '../../../../app-services/app-menu.service';
import { ContractPhaseComputeService } from '../../../../shared/contract-phase-compute.service';
import { WorkProgramComputeService } from '../../../../shared/work-program-compute.service';
import { NewDeliverableNameService } from '../../../../shared/new-deliverable-name/new-deliverable-name.service';

@Component({
  selector: 'app-edit-deliverable',
  templateUrl: './edit-deliverable.component.html',
  styleUrls: ['./edit-deliverable.component.css']
})
export class EditDeliverableComponent extends DeliverablesComponent implements OnInit {

  phases: ContractPhase[];
  phase: ContractPhase;

  constructor(
    menuSvc: AppMenuService,
    builder: FormBuilder,
    contractService: ContractService,
    phaseComputeService: ContractPhaseComputeService,
    programComputeService: WorkProgramComputeService,
    deliverableNameService: NewDeliverableNameService,
    computeService: DeliverablesComputeService,
    private route: ActivatedRoute) {

    super(menuSvc, builder, contractService, phaseComputeService,
      programComputeService, deliverableNameService, computeService);
  }

  ngOnInit() {

    super.ngOnInit();

    this.isInitialized = true;

    this.deliverableNameService.nameStream.subscribe(deliverableLookup => {
      if (deliverableLookup)
        this.deliverableLookups.push(deliverableLookup.description);
    });

    this.computeService.getDeliverablesLookup().subscribe(lookups => {
      this.deliverableLookups = lookups;
    });

    const index = this.route.snapshot.paramMap.get('id');

    this.phases = this.contractService.contract.phases.filter(p => p.id === (+index));

    this.computeService.getDeliverables().subscribe(data => {

      this.deliverables = data; 

      this.phase = this.contractService.contract.phases.find(p => p.id === (+index));

      const deliverables = this.deliverables.filter(d => d.contractPhase.id === (+index));

      this.refreshFormAndTable(deliverables);
    });

  }

  init() {
    // override parent's init method, b/c we don't expect to open new contract on the fly
  }

  public save(): void {

    // overridden. Contract is now supplemented.

    // if(!this.phasesValid || this.programExists) {
    //   return;
    // }

    const deliverable = this.computeService.extractDeliverable(this.deliverablesFormGroup.controls);

    const result = this.computeService.validateDeliverable(deliverable, this.deliverables);

    if (!result.isSuccessful) {
      return;
    }

    if (this.deliverables && this.deliverables.length > 0) {
      const result = this.computeService.validateDeliverablePercentage(deliverable, this.deliverables);

      if (result.isSuccessful) {
        this.doSave(deliverable);
      }

    } else {
      this.doSave(deliverable);
    }
  }

  doSave(deliverable) {
    const subscription = this.computeService.save(this.contractService.contract, deliverable, this.deliverables).subscribe(updatedDeliverables => {
     
      this.deliverables = updatedDeliverables;

      const index = this.route.snapshot.paramMap.get('id');

      const deliverables = this.deliverables.filter(d => d.contractPhase.id === (+index));

      this.refreshFormAndTable(deliverables);

      this.addAmendLabel = 'Save';

      this.deleteResetLabel = 'Clear';

      subscription.unsubscribe();
    });
  }

}
