import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ContractService } from '../../../../contract/contract.service';
import { ContractPhase } from '../../../../models/contract-phase';
import { Supplement } from '../../../../models/supplement';
import { DeliverablesComputeService } from '../../../../shared/deliverables-compute.service';
import { SupplementComputeService } from '../../../../shared/supplement-compute.service';

@Component({
  templateUrl: './supplement-deliverable.component.html',
  styleUrls: ['./supplement-deliverable.component.css']
})
export class SupplementDeliverableComponent implements OnInit {

  phases: ContractPhase[];
  supplement: Supplement;

  constructor(
    private computeService: DeliverablesComputeService,
    public contractService: ContractService,
    private router: Router,
    private route: ActivatedRoute,
    private supplementComputeService: SupplementComputeService) {
  }

  ngOnInit() {

    const id = this.route.snapshot.paramMap.get('id');

    if (+id) {
      this.supplement = this.supplementComputeService.getSupplement(id);
    }

    this.computeService.getDeliverables().subscribe(deliverables => {

      const phases = this.contractService.contract.phases;

      this.phases = new Array<ContractPhase>();

      phases.forEach(phase => {
        if (phase.supplementId === null) {
          phase['belongsTo'] = 'Original Contract';
        } else {
          phase['belongsTo'] = `Supplement ${this.supplement.number}`;
        }
      });

      phases.forEach(phase => {
        const filtered = phases.filter(p => phase.id === p.contractPhaseId);
        if (filtered.length === 0) {
          phase.deliverables = deliverables.filter(d => d.contractPhase.id === phase.id);
          this.phases.push(phase);
        }
      });
    });

  }

  editDeliverable(phase) {
    this.router.navigate([`${phase.id}`], { relativeTo: this.route });
  }
}
