import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplementDeliverableComponent } from './supplement-deliverable.component';

describe('DeliverableComponent', () => {
  let component: SupplementDeliverableComponent;
  let fixture: ComponentFixture<SupplementDeliverableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplementDeliverableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplementDeliverableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
