import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplementDeliverablesContainerComponent } from './supplement-deliverables-container.component';

describe('SupplementDeliverablesContainerComponent', () => {
  let component: SupplementDeliverablesContainerComponent;
  let fixture: ComponentFixture<SupplementDeliverablesContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplementDeliverablesContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplementDeliverablesContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
