import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AppMenuService } from '../../app-services/app-menu.service';
import { ContractService } from '../../contract/contract.service';
import { SupplementComputeService } from '../../shared/supplement-compute.service';

@Component({
  templateUrl: './supplement.component.html',
  styleUrls: ['./supplement.component.css']
})
export class SupplementComponent implements OnInit {

  constructor(private menuSvc: AppMenuService,
    private router: Router,
    private route: ActivatedRoute,
    public contractService: ContractService,
    public computeService: SupplementComputeService) {

  }

  ngOnInit() {
    this.menuSvc.getMenuEntries('supplement');
  }

  // @param force to force open another 
  // even if contract is still open
  openContract(force): void {

    // run only when the open another contract is fired.
    // initial load occurs in supplement-list component
    const subscription = this.contractService.contractStream.subscribe(contract => {
      if (this.contractService.contract.id) {
        const subscription = this.computeService.getSupplements(this.contractService.contract).subscribe(supplements => {
          subscription.unsubscribe();
          
        });
      }
      // contract loaded or canceled
      // unsubscribe
      subscription.unsubscribe();
    });

    this.contractService.openContract();
  }
}
