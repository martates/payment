import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ContractService } from '../../../contract/contract.service';
import { SupplementComputeService } from '../../../shared/supplement-compute.service';

@Component({
  templateUrl: './supplement-list.component.html',
  styleUrls: ['./supplement-list.component.css']
})
export class SupplementListComponent implements OnInit {

  noSupplements: boolean = false;
  isInitialized: boolean = false;

  constructor(private router: Router,
    private route: ActivatedRoute,
    public contractService: ContractService,
    public computeService: SupplementComputeService) { }

  ngOnInit() {
    this.setSupplementsExistState();

    this.openContract(false);
  }

  // @param force to force open another 
  // even if contract is still open
  openContract(force): void {

    const subscription = this.contractService.contractStream.subscribe(contract => {
      if (this.contractService.contract.id) {
        this.init();
      }
      // contract loaded or canceled
      // unsubscribe
      subscription.unsubscribe();
    });

    if (!this.contractService.contract.id || force) {
      this.contractService.openContract();
    }

    if (this.contractService.contract.id && !this.isInitialized) {
      this.init();
    }
  }

  init() {

    this.isInitialized = true;

    const subscription = this.computeService.getSupplements(this.contractService.contract).subscribe(supplements => {
      subscription.unsubscribe();
      this.setSupplementsExistState();
    });

  }

  setSupplementsExistState() {
    this.noSupplements =
      (this.computeService.supplements && this.computeService.supplements.length > 0) ? false : true;
  }

  edit(supplement) {
    this.router.navigate(['edit-supplement', supplement.id], { relativeTo: this.route });
  }

  createSupplement() {
    this.router.navigate(['edit-supplement', 0], { relativeTo: this.route });
  }

  effectiveContractState() {
    this.router.navigate(['effective-contract-state'], { relativeTo: this.route });
  }

  editDetail(supplement) {
    this.router.navigate(['supplement-detail', supplement.id], { relativeTo: this.route });
  }
}
