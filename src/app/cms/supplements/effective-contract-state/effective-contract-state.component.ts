import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { EffectiveContractState } from '../../../models/effective-contract-state';
import { EffectiveContractStateService } from '../../../shared/effective-contract-state.service';


@Component({
  templateUrl: './effective-contract-state.component.html',
  styleUrls: ['./effective-contract-state.component.css']
})
export class EffectiveContractStateComponent implements OnInit {

  effectiveContractState: EffectiveContractState

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private effectiveContractStateService: EffectiveContractStateService) { }

  ngOnInit() {
    this.effectiveContractStateService.computeEffectiveStateAsync()
      .subscribe(effectiveContractState =>
        this.effectiveContractState = effectiveContractState);
  }

  back() {
    this.router.navigate(['/supplements'], { relativeTo: this.route });
  }

}
