import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EffectiveContractStateComponent } from './effective-contract-state.component';

describe('EffectiveContractStateComponent', () => {
  let component: EffectiveContractStateComponent;
  let fixture: ComponentFixture<EffectiveContractStateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EffectiveContractStateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EffectiveContractStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
