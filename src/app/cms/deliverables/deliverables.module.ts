import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import {
//   MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatDialogModule,
//   MatIconModule, MatInputModule, MatNativeDateModule, MatSelectModule,
//   MatTableModule, MatToolbarModule
// } from '@angular/material';

import { SideInfoModule } from '../../shared/side-info/side-info.module';
import { DeliverablesComponent } from './deliverables.component';
import { DeliverablesRoutingModule } from './deliverables.routing.module';

import { MaterialModule } from '../../material.module';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    // AppMaterialModule,
    // MatSelectModule, MatDatepickerModule,
    // MatNativeDateModule, MatInputModule,
    // MatButtonModule, MatTableModule,
    // MatToolbarModule, MatIconModule,
    // MatDialogModule, MatCheckboxModule,
   SideInfoModule,
    DeliverablesRoutingModule
  ],
  declarations: [DeliverablesComponent],
  exports: [DeliverablesComponent]
})
export class DeliverablesModule { }
