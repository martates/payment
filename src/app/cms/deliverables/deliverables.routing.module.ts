import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DeliverablesComponent } from './deliverables.component';


const routes: Routes = [
  {
    path: '',
    component: DeliverablesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeliverablesRoutingModule { }
