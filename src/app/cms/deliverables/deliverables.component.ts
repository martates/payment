import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { AppMenuService } from '../../app-services/app-menu.service';

import { ContractService } from '../../contract/contract.service';
import { ContractPhase } from '../../models/contract-phase';
import { Deliverable } from '../../models/deliverable';
import { ContractPhaseComputeService } from '../../shared/contract-phase-compute.service';
import { DeliverablesComputeService } from '../../shared/deliverables-compute.service';
import { NewDeliverableNameService } from '../../shared/new-deliverable-name/new-deliverable-name.service';
import { WorkProgramComputeService } from '../../shared/work-program-compute.service';


@Component({
  templateUrl: './deliverables.component.html',
  styleUrls: ['./deliverables.component.css']
})
export class DeliverablesComponent implements OnInit {

  //#region fields
  phasesValid: boolean = false;
  programExists: boolean = true;
  currentPhase: ContractPhase;
  addAmendLabel: string;
  deleteResetLabel: string;
  deliverablesFormGroup: FormGroup;
  deliverableLookups: any[];
  deliverables: Deliverable[];
  displayedColumns: string[];
  dataSource: any;
  protected isInitialized: boolean = false;
  //#endregion

  constructor(
    protected menuSvc: AppMenuService,
    protected builder: FormBuilder,
    public contractService: ContractService,
    protected phaseComputeService: ContractPhaseComputeService,
    protected programComputeService: WorkProgramComputeService,
    protected deliverableNameService: NewDeliverableNameService,
    protected computeService: DeliverablesComputeService) {
  }

  ngOnInit(): void {

    this.menuSvc.getMenuEntries('deliverables');
    this.createForm();
    this.addAmendLabel = 'Save';
    this.deleteResetLabel = 'Clear';
    this.displayedColumns = ['deliverable', 'phase', 'percentage', 'monetaryValue'];
    this.openContract(false);
  }

  init() {
    this.isInitialized = true;
    // validate phases before commencing 
    this.phasesValid = this.phaseComputeService.validatePhasesTotal();

    this.programComputeService.validateProgramExistence().subscribe(result =>
      this.programExists = result
    );

    // subscribe to deliverable name service
    this.deliverableNameService.nameStream.subscribe(deliverableLookup => {
      if (deliverableLookup)
        this.deliverableLookups.push(deliverableLookup.description);
    });

    this.computeService.getDeliverablesLookup().subscribe(lookups => {
      this.deliverableLookups = lookups;
    });

    this.computeService.getDeliverables().subscribe(data => {
      this.deliverables = data;
      this.refreshFormAndTable(this.deliverables);
    });

  }

  // @param force to force open another 
  // even if contract is still open
  openContract(force): void {

    const subscription = this.contractService.contractStream.subscribe(contract => {
      if (this.contractService.contract.id) {
        this.init();
      }
      // contract loaded or canceled
      // unsubscribe
      subscription.unsubscribe();
    });

    if (!this.contractService.contract.id || force) {
      this.contractService.openContract();
    }

    if (this.contractService.contract.id && !this.isInitialized) {
      this.init();
    }
  }

  //#region Commands
  public save(): void {

    if(!this.phasesValid || this.programExists) {
      return;
    }

    const deliverable = this.computeService.extractDeliverable(this.deliverablesFormGroup.controls);

    const result = this.computeService.validateDeliverable(deliverable, this.deliverables);

    if (!result.isSuccessful) {
      return;
    }

    if (this.deliverables && this.deliverables.length > 0) {
      const result = this.computeService.validateDeliverablePercentage(deliverable, this.deliverables);

      if (result.isSuccessful) {
        this.doSave(deliverable);
      }

    } else {
      this.doSave(deliverable);
    }
  }

  doSave(deliverable) {
    const subscription = this.computeService.save(this.contractService.contract, deliverable, this.deliverables).subscribe(updatedDeliverables => {
      this.deliverables = updatedDeliverables;
      this.refreshFormAndTable(this.deliverables);
      this.addAmendLabel = 'Save';
      this.deleteResetLabel = 'Clear';
      subscription.unsubscribe();
    });
  }

  public delete(): void {
    
    if(!this.phasesValid || this.programExists) {
      return;
    }

    const deliverable = this.computeService.extractDeliverable(this.deliverablesFormGroup.controls);

    if (!deliverable.id) {
      this.deliverablesFormGroup.reset();
      return;
    }

    const subscription = this.computeService.delete(deliverable, this.deliverables).subscribe(updatedDeliverables => {
      this.deliverables = updatedDeliverables;
      this.refreshFormAndTable(this.deliverables);
      this.addAmendLabel = 'Save';
      this.deleteResetLabel = 'Clear';
      subscription.unsubscribe();
    });
  }

  private selectDeliverable(deliverable: Deliverable): void {
    this.computeService.selectDeliverable(this.deliverablesFormGroup, deliverable);

    this.addAmendLabel = 'Amend';
    this.deleteResetLabel = 'Delete';
  }
  //#endregion

  //#region helper methods

  private createForm(): void {
    this.deliverablesFormGroup = this.builder.group({
      deliverableIdControl: [0],
      deliverableControl: ['', Validators.required],
      phaseControl: [{}, Validators.required],
      percentageControl: [0, Validators.required],
      monetaryValueControl: [0.00, Validators.required]
    });

    this.onFormValuesChange();
  }

  onFormValuesChange(): void {

    this.deliverablesFormGroup.get('percentageControl').valueChanges.subscribe(val => {

      const deliverable = this.computeService.extractDeliverable(this.deliverablesFormGroup.controls)

      this.computeService.calculateMoneyFromPercentage(this.contractService.contract, deliverable);

      this.deliverablesFormGroup.patchValue({
        monetaryValueControl: deliverable.monetaryValue
      });
    });

    this.deliverablesFormGroup.get('phaseControl').valueChanges.subscribe(phaseID => {

      this.currentPhase = this.contractService.contract.phases.find(p => p.id === phaseID);
      
    });
  }


  protected refreshFormAndTable(deliverables): void {
    this.currentPhase = null;
    this.deliverablesFormGroup.reset();
    this.dataSource = new MatTableDataSource<Deliverable>(deliverables);
  }

  public getCanDelete(): boolean {
    // if the form is marked dirty, enable clear
    if (!this.deliverablesFormGroup.untouched &&
      !this.deliverablesFormGroup.pristine) {
      return true;
    }

    // if the form is selected with existing deliverable, enable delete
    const deliverable = this.computeService.extractDeliverable(this.deliverablesFormGroup.controls);
    if (deliverable.id) {
      return true;
    }

    return false;
  }
  //#endregion

  createDeliverableLookup() {
    this.deliverableNameService.openDialog();
  }
}
