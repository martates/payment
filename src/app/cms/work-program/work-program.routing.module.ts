import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WorkProgramComponent } from './work-program.component';

const routes: Routes = [
  {
    path: '',
    component: WorkProgramComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkProgramRoutingModule { }
