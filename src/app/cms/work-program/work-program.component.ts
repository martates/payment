import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';

import { AppMenuService } from '../../app-services/app-menu.service';
import { AppNotificationService } from '../../app-services/app-notification.service';
import { ContractService } from '../../contract/contract.service';
import { ConsultantsProgram } from '../../models/consultants-program';
import { ContractPhase } from '../../models/contract-phase';
import { SaveConsultantsProgram } from '../../models/save-consultants-program';
import { WorkProgramComputeService } from '../../shared/work-program-compute.service';
import { WorkProgramDataService } from '../../shared/work-program-data.service';

@Component({
  templateUrl: './work-program.component.html',
  styleUrls: ['./work-program.component.css']
})
export class WorkProgramComponent implements OnInit {

  //#region fields
  addAmendLabel: string;
  deleteResetLabel: string;
  workProgramFormGroup: FormGroup;
  consultantsPrograms: ConsultantsProgram[];
  displayedColumns: string[];
  dataSource: any;
  currentPhase: ContractPhase;
  private isInitialized: boolean = false;
  //#endregion

  constructor(
    private menuSvc: AppMenuService,
    private builder: FormBuilder,
    private dataService: WorkProgramDataService,
    public computeService: WorkProgramComputeService,
    private notificationService: AppNotificationService,
    public contractService: ContractService) {
  }

  ngOnInit(): void {

    this.menuSvc.getMenuEntries('work-program');
    this.createForm();
    this.addAmendLabel = 'Save';
    this.deleteResetLabel = 'Clear';
    this.displayedColumns = ['date', 'deliverable', 'phase',
      'percentage', 'deliverableMonetaryValue',
      'cumPhasePercentage', 'cumPhaseMonetaryValue',
      'cumContractPercentage', 'cumContractMonetaryValue'];

    this.openContract(false);
  }

  init() {
    this.isInitialized = true;

    const subscription = this.computeService.getDressedPrograms().subscribe( programs => {
      this.consultantsPrograms = programs;
      this.refreshFormAndTable();
      subscription.unsubscribe();
    });

  }

  // @param force to force open another 
  // even if contract is still open
  openContract(force): void {

    const subscription = this.contractService.contractStream.subscribe(contract => {
      if (this.contractService.contract.id) {
        this.init();
      }
      // contract loaded or canceled
      // unsubscribe
      subscription.unsubscribe();
    });

    if (!this.contractService.contract.id || force) {
      this.contractService.openContract();
    }

    if (this.contractService.contract.id && !this.isInitialized) {
      this.init();
    }
  }

  //#region Commands
  save(): void {

    const program: SaveConsultantsProgram = this.getProgramFromFormModel();
    const deliverable = this.computeService.getDeliverableFor(program);
    const result = this.computeService.validateProgram(deliverable, program, this.consultantsPrograms);
    if (!result.isSuccessful) {
      this.notificationService.messageNotification.next(result.message);
      return;
    }

    // Add new
    if (!program.id) {
      this.dataService.postData('', program, this.contractService.contract.id).subscribe(
        program => {
          this.consultantsPrograms = this.consultantsPrograms ? this.consultantsPrograms.concat(program) : new Array<ConsultantsProgram>(program);
          this.refreshFormAndTable();
          this.notificationService.messageNotification.next("Programme added");

          this.addAmendLabel = 'Save';
          this.deleteResetLabel = 'Clear';
        }
      );

    } else {
      const index = this.getIndex(program);
      Object.assign(this.consultantsPrograms[index], program);
      // object assign is shallow copy. If program.deliverable changed
      // that wouldn't be captured by object.assign
      
      this.consultantsPrograms[index].deliverable = this.computeService.getDeliverableFor(program);

      this.dataService.putData('', program, this.contractService.contract.id).subscribe(
        program => {
          ///////////////////////////////////////////////////////////////////////
          // TODO: This is to force refresh after existing program is edited
          // This is a hack and needs to be replace with proper code using
          // the angular framework change detection mechanism

          this.consultantsPrograms = this.consultantsPrograms.concat();

          //////////////////////////////////////////////////////////////////////

          this.refreshFormAndTable();
          this.notificationService.messageNotification.next("Programme updated");
          this.addAmendLabel = 'Save';
          this.deleteResetLabel = 'Clear';
        });
    }
  }

  delete(): void {
    const program: SaveConsultantsProgram = this.getProgramFromFormModel();

    if (!program.id) {
      this.refreshFormAndTable();
      return;
    }

    this.dataService.deleteData('', program, this.contractService.contract.id).subscribe(data => {
      if (this.consultantsPrograms) {
        const index = this.getIndex(program);
        this.consultantsPrograms.splice(index, 1);

        ///////////////////////////////////////////////////////////////////////
        // TODO: This is to force refresh after existing program is edited
        // This is a hack and needs to be replace with proper code using
        // the angular framework change detection mechanism
        this.consultantsPrograms = this.consultantsPrograms.concat();
      }
      this.notificationService.messageNotification.next("Programme deleted");

      this.refreshFormAndTable();
      this.addAmendLabel = 'Save';
      this.deleteResetLabel = 'Clear'
    });
  }

  private selectProgram(program: ConsultantsProgram): void {
    this.workProgramFormGroup.setValue({
      deliverableIdControl: program.id,
      deliverableControl: program.deliverable.id,
      dateControl: program.date,
      percentageControl: program.percentComplete,
      monetaryValueControl: program.monetaryValue
    });

    this.addAmendLabel = 'Amend';
    this.deleteResetLabel = 'Delete';
  }
  //#endregion

  //#region helper methods

  private createForm(): void {
    this.workProgramFormGroup = this.builder.group({
      deliverableIdControl: [0],
      deliverableControl: ['', Validators.required],
      dateControl: [null, Validators.required],
      percentageControl: [0, Validators.required],
      monetaryValueControl: [{ value: 0.00, disabled: true }, Validators.required],
    });

    this.onFormValuesChange();
  }

  onFormValuesChange(): void {

    this.workProgramFormGroup.get('percentageControl').valueChanges.subscribe(val => {
      const program = this.getProgramFromFormModel();

      const deliverable = this.computeService.getDeliverableFor(program);

      this.computeService.computeDeliverableMoneyFromPercentage(deliverable, program);

      this.workProgramFormGroup.patchValue({
        monetaryValueControl: program.monetaryValue
      });

    });

    this.workProgramFormGroup.get('deliverableControl').valueChanges.subscribe(deliverableId => {

      const deliverable = this.computeService.contractDeliverables.find(d => d.id === deliverableId)
      
      if (deliverable) {
        this.currentPhase = deliverable.contractPhase;
      }
    });
  }

  private getIndex(program): number {
    return this.consultantsPrograms.findIndex(d => d.id === program.id);
  }

  private getProgramFromFormModel(): SaveConsultantsProgram {
    const formModel = this.workProgramFormGroup.controls;

    const program: SaveConsultantsProgram = {
      id: formModel.deliverableIdControl.value || 0,
      deliverableId: formModel.deliverableControl.value,
      date: formModel.dateControl.value,
      percentComplete: formModel.percentageControl.value,
      monetaryValue: formModel.monetaryValueControl.value
    };

    return program;
  }

  private refreshFormAndTable(): void {
    this.createForm();
    this.currentPhase = null;
    const programs = this.computeService.computeProgramsCumulatives(this.consultantsPrograms);
    this.dataSource = new MatTableDataSource<ConsultantsProgram>(programs);
  }

  public getCanDelete(): boolean {
    // if the form is marked dirty, enable clear
    if (!this.workProgramFormGroup.untouched &&
      !this.workProgramFormGroup.pristine) {
      return true;
    }

    // if the form is selected with existing deliverable, enable delete
    const program = this.getProgramFromFormModel();
    if (program.id) {
      return true;
    }

    return false;
  }



  //#endregion
}
