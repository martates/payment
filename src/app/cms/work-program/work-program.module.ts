import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
// import {
//   MatButtonModule, MatDatepickerModule, MatDialogModule, MatIconModule, MatInputModule,
//   MatNativeDateModule, MatSelectModule, MatTableModule, MatToolbarModule
// } from '@angular/material';
import { ChartModule } from '@progress/kendo-angular-charts';
import 'hammerjs';

import { SideInfoModule } from '../../shared/side-info/side-info.module';
import { WorkProgramChartComponent } from './work-program-chart/work-program-chart.component';
import { WorkProgramComponent } from './work-program.component';
import { WorkProgramRoutingModule } from './work-program.routing.module';

import { MaterialModule } from '../../material.module';



@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
    // AppMaterialModule,
    // MatSelectModule, MatDatepickerModule,
    // MatNativeDateModule, MatInputModule,
    // MatButtonModule, MatTableModule,
    // MatToolbarModule, MatIconModule,
    // MatDialogModule,
    ChartModule,
    SideInfoModule,
    WorkProgramRoutingModule
  ],
  declarations: [WorkProgramComponent, WorkProgramChartComponent],
})
export class WorkProgramModule { }
