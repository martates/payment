import { ComponentFixture, TestBed, async } from '@angular/core/testing';

import { TestExportsModule } from '../../test/test-exports';
import { WorkProgramComponent } from './work-program.component';

describe('WorkProgramComponent', () => {
  let component: WorkProgramComponent;
  let fixture: ComponentFixture<WorkProgramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkProgramComponent ],
      imports: [TestExportsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
