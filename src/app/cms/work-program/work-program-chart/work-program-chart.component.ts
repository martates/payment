import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import * as _moment from 'moment';

import { ContractService } from '../../../contract/contract.service';
import { ConsultantsProgram } from '../../../models/consultants-program';

@Component({
  selector: 'app-work-program-chart',
  templateUrl: './work-program-chart.component.html',
  styleUrls: ['./work-program-chart.component.css']
})
export class WorkProgramChartComponent implements OnChanges {

  @Input() consultantsPrograms: ConsultantsProgram[];

  data: any[];

  constructor(private contractService: ContractService) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.consultantsPrograms) {
      let programsClones = new Array<any>();
      Object.assign(programsClones, this.consultantsPrograms);

      try {
        const firstValue = programsClones[0];

        if (firstValue.contractCumulativeMoney !== 0) {

          const firstPhase = this.contractService.contract.phases.sort(this.comparer)[0];
          const date = _moment(firstPhase.startDate).subtract(1, 'day');

          programsClones.splice(0, 0, {
            id: 0, date: date.toDate(), deliverable: null,
            percentComplete: 0, monetaryValue: 0,
            phaseCumulativePercentage: 0, phaseCumulativeMoney: 0,
            contractCumulativePercentage: 0, contractCumulativeMoney: 0
          });
        }
      }
      catch (error) { }

      // scale to a thousandth 
      programsClones.forEach(program => {
        Object.defineProperty(program, 'contractAmountToDate', {
          value: 0,
          writable: true
        });
        program.contractAmountToDate = program.contractCumulativeMoney / 1000;
      });

      this.data = programsClones;
    }
  }

  private comparer(s1, s2) {
    if (_moment(s1.startDate).isBefore(s2.startDate)) {
      return -1;
    } else if (_moment(s1.startDate).isAfter(s2.startDate)) {
      return 1;
    }

    return 0;
  }

}
