import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkProgramChartComponent } from './work-program-chart.component';

describe('WorkProgramChartComponent', () => {
  let component: WorkProgramChartComponent;
  let fixture: ComponentFixture<WorkProgramChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkProgramChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkProgramChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
