import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';

import { AppMenuService } from '../../app-services/app-menu.service';
import { AppNotificationService } from '../../app-services/app-notification.service';
import { ContractService } from '../../contract/contract.service';
import { ConsultantsPerformance } from '../../models/consultants-performance';
import { ConsultantsProgram } from '../../models/consultants-program';
import { ContractPhase } from '../../models/contract-phase';
import { SaveConsultantsPerformance } from '../../models/save-consultants-performance';
import { WorkProgramComputeService } from '../../shared/work-program-compute.service';
import { WorkProgressComputeService } from '../../shared/work-progress-compute.service';
import { WorkProgressDataService } from '../../shared/work-progress-data.service';

@Component({
  templateUrl: './work-progress.component.html',
  styleUrls: ['./work-progress.component.css']
})
export class WorkProgressComponent implements OnInit {

  //#region fields
  addAmendLabel: string;
  deleteResetLabel: string;
  workProgressFormGroup: FormGroup;
  consultantsPerformances: ConsultantsPerformance[];
  consultantsPrograms: ConsultantsProgram[];
  displayedColumns: string[];
  dataSource: any;
  currentPhase: ContractPhase;
  private isInitialized: boolean = false;
  //#endregion

  constructor(
    private menuSvc: AppMenuService,
    private builder: FormBuilder,
    private dataService: WorkProgressDataService,
    public computeService: WorkProgressComputeService,
    private workProgramComputeService: WorkProgramComputeService,
    public contractService: ContractService,
    private notificationService: AppNotificationService) {
  }

  ngOnInit(): void {

    this.menuSvc.getMenuEntries('work-progress');
    this.createForm();
    this.addAmendLabel = 'Save';
    this.deleteResetLabel = 'Clear';
    this.displayedColumns = ['deliverable', 'assessmentDate',
      'percentage', 'monetaryValue',
      'cumPhasePercentage', 'cumPhaseMonetaryValue',
      'cumContractPercentage', 'cumContractMonetaryValue'];

    this.openContract(false);
  }

  init() {

    this.isInitialized = true;

    const subscription = this.computeService.getDressedProgresses().subscribe(programsAndProgresses => {

      this.consultantsPerformances = programsAndProgresses.progresses;

      this.consultantsPrograms = programsAndProgresses.programs;

      this.consultantsPrograms = this.workProgramComputeService.sortPrograms(this.consultantsPrograms);

      this.consultantsPrograms.forEach(program => {
        this.workProgramComputeService.computeContractCumulativeMoney(program, this.consultantsPrograms)
      });

      this.consultantsPrograms.forEach(program => {
        program.deliverable.contractPhase = this.computeService.contractDeliverables.find(d => d.id == program.deliverable.id).contractPhase;
      });

      this.refreshFormAndTable();

      subscription.unsubscribe();
    });
  }

  // @param force to force open another 
  // even if contract is still open
  openContract(force): void {

    const subscription = this.contractService.contractStream.subscribe(contract => {
      if (this.contractService.contract.id) {
        this.init();
      }
      // contract loaded or canceled
      // unsubscribe
      subscription.unsubscribe();
    });

    if (!this.contractService.contract.id || force) {
      this.contractService.openContract();
    }

    if (this.contractService.contract.id && !this.isInitialized) {
      this.init();
    }
  }

  //#region Commands
  public save(): void {
    const progress: SaveConsultantsPerformance = this.getProgressFromFormModel();
    const deliverable = this.computeService.getDeliverableFor(progress);
    const result = this.computeService.validateProgress(deliverable, progress, this.consultantsPerformances);
    if (!result.isSuccessful) {
      this.notificationService.messageNotification.next(result.message);
      return;
    }
    // Add new
    if (!progress.id) {
      this.dataService.postData('', progress, this.contractService.contract.id).subscribe(
        progress => {
          this.consultantsPerformances = this.consultantsPerformances ? this.consultantsPerformances.concat(progress) : new Array<ConsultantsPerformance>(progress);
          this.refreshFormAndTable();
          this.notificationService.messageNotification.next("Performance assessment added");
        }
      );

    } else {
      const index = this.getIndex(progress);
      Object.assign(this.consultantsPerformances[index], progress);
      // object assign is shallow copy. If program.deliverable changed
      // that wouldn't be captured by object.assign
      this.consultantsPerformances[index].deliverable = this.computeService.contractDeliverables.find(d => d.id === progress.deliverableId);

      this.dataService.putData('', progress, this.contractService.contract.id).subscribe(
        data => {
          this.refreshFormAndTable();
          ///////////////////////////////////////////////////////////////////////
          // TODO: This is to force refresh after existing program is edited
          // This is a hack and needs to be replace with proper code using
          // the angular framework change detection mechanism

          this.consultantsPerformances = this.consultantsPerformances.concat();

          //////////////////////////////////////////////////////////////////////
        });
    }

    this.addAmendLabel = 'Save';
    this.deleteResetLabel = 'Clear';
  }

  public delete(): void {
    const performance: SaveConsultantsPerformance = this.getProgressFromFormModel();

    this.dataService.deleteData('', performance, this.contractService.contract.id).subscribe(data => {
      if (this.consultantsPerformances) {
        const index = this.getIndex(performance);
        this.consultantsPerformances.splice(index, 1);

        ///////////////////////////////////////////////////////////////////////
        // TODO: This is to force refresh after existing program is edited
        // This is a hack and needs to be replace with proper code using
        // the angular framework change detection mechanism
        this.consultantsPerformances = this.consultantsPerformances.concat();
      }

      this.notificationService.messageNotification.next("Performance assessment deleted");

      this.refreshFormAndTable();
      this.addAmendLabel = 'Save';
      this.deleteResetLabel = 'Clear'
    });
  }

  private selectPerformance(performance: ConsultantsPerformance): void {
    this.workProgressFormGroup.setValue({
      deliverableIdControl: performance.id,
      deliverableControl: performance.deliverable.id,
      assessmentDateControl: performance.assessmentDate,
      submissionDateControl: performance.submissionDate,
      percentageControl: performance.percentComplete,
      monetaryValueControl: performance.monetaryValue,
      reasonControl: performance.reason,
      detailedReasonControl: performance.detailedReason
    });

    this.addAmendLabel = 'Amend';
    this.deleteResetLabel = 'Delete';
  }
  //#endregion

  //#region helper methods

  private createForm(): void {
    this.workProgressFormGroup = this.builder.group({
      deliverableIdControl: [0],
      deliverableControl: ['', Validators.required],
      assessmentDateControl: [null, Validators.required],
      submissionDateControl: [null],
      percentageControl: [0, Validators.required],
      monetaryValueControl: [{ value: 0.00, disabled: true }, Validators.required],
      reasonControl: [null, Validators.required],
      detailedReasonControl: [null, Validators.required]
    });

    this.onFormValuesChange();
  }

  onFormValuesChange(): void {

    this.workProgressFormGroup.get('percentageControl').valueChanges.subscribe(val => {

      const progress = this.getProgressFromFormModel();

      if (!progress.deliverableId) {
        return;
      }

      const deliverable = this.computeService.getDeliverableFor(progress);

      this.computeService.computeDeliverableMoneyFromPercentage(deliverable, progress);

      this.workProgressFormGroup.patchValue({
        monetaryValueControl: progress.monetaryValue
      });
    });

    this.workProgressFormGroup.get('deliverableControl').valueChanges.subscribe(deliverableId => {
      if (!deliverableId) {
        return;
      }

      const deliverable = this.computeService.contractDeliverables.find(d => d.id === deliverableId)

      this.currentPhase = deliverable.contractPhase;
    });
  }

  private getIndex(program): number {
    return this.consultantsPerformances.findIndex(d => d.id === program.id);
  }

  private getProgressFromFormModel(): SaveConsultantsPerformance {
    const formModel = this.workProgressFormGroup.controls;

    const performance: SaveConsultantsPerformance = {
      id: formModel.deliverableIdControl.value || 0,
      deliverableId: formModel.deliverableControl.value,
      assessmentDate: formModel.assessmentDateControl.value,
      submissionDate: formModel.submissionDateControl.value,
      percentComplete: formModel.percentageControl.value,
      monetaryValue: formModel.monetaryValueControl.value,
      reason: formModel.reasonControl.value,
      detailedReason: formModel.detailedReasonControl.value
    };

    return performance;
  }

  private refreshFormAndTable(): void {
    this.createForm();
    this.currentPhase = null;
    const progresses = this.computeService.computeProgressesCumulatives(this.consultantsPerformances);
    this.dataSource = new MatTableDataSource<ConsultantsPerformance>(progresses);
  }

  public getCanDelete(): boolean {
    // if the form is marked dirty, enable clear
    if (!this.workProgressFormGroup.untouched &&
      !this.workProgressFormGroup.pristine) {
      return true;
    }

    // if the form is selected with existing deliverable, enable delete
    const performance = this.getProgressFromFormModel();
    if (performance.id) {
      return true;
    }

    return false;
  }

  //#endregion
}
