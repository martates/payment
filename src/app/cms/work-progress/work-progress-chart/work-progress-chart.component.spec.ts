import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkProgressChartComponent } from './work-progress-chart.component';

describe('WorkProgramChartComponent', () => {
  let component: WorkProgressChartComponent;
  let fixture: ComponentFixture<WorkProgressChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkProgressChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkProgressChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
