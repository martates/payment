import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
// import {
//   MatButtonModule, MatDatepickerModule, MatIconModule, MatInputModule, MatNativeDateModule,
//   MatSelectModule, MatTableModule, MatToolbarModule
// } from '@angular/material';
import { ChartModule } from '@progress/kendo-angular-charts';
import 'hammerjs';

import { SideInfoModule } from '../../shared/side-info/side-info.module';
import { WorkProgressChartComponent } from './work-progress-chart/work-progress-chart.component';
import { WorkProgressRoutingModule } from './work-progress-routing.module';
import { WorkProgressComponent } from './work-progress.component';

import { MaterialModule } from '../../material.module';



@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
    // AppMaterialModule,
    // MatSelectModule, MatDatepickerModule,
    // MatNativeDateModule, MatInputModule,
    // MatButtonModule, MatTableModule,
    // MatToolbarModule, MatIconModule,
    ChartModule,
    SideInfoModule,
    WorkProgressRoutingModule
  ],
  declarations: [WorkProgressComponent, WorkProgressChartComponent],
})
export class WorkProgressModule { }
