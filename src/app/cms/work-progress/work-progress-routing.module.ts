import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WorkProgressComponent } from './work-progress.component';

const routes: Routes = [
  {
    path: '',
    component: WorkProgressComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkProgressRoutingModule { }
