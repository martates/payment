import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ChartModule } from '@progress/kendo-angular-charts';
import 'hammerjs';

import { PapComputeService } from './pap-compute.service';
import { PapDataService } from './pap-data.service';
import { PapComponent } from './pap.component';
import { PapRoutingModule } from './pap.routing.module';


@NgModule({
  imports: [
    CommonModule,
    ChartModule,
    PapRoutingModule
  ],
  declarations: [PapComponent],
  providers: [PapComputeService, PapDataService]
})
export class PapModule { }
