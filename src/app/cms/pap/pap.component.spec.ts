import { ComponentFixture, TestBed, async } from '@angular/core/testing';

import { PapComponent } from './pap.component';


describe('PapComponent', () => {
  let component: PapComponent;
  let fixture: ComponentFixture<PapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
