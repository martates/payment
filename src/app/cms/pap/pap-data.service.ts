import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { AsyncDataFacade } from '../../shared/async-data-facade';


@Injectable()
export class PapDataService {

  constructor(private asyncDataFacade: AsyncDataFacade) { }

  getData(query: string, param: any): Observable<any> {
    const contractId = <number>param;
    switch (query) {
      case 'performances':
        return this.asyncDataFacade.get(`/contracts/${contractId}/consultantsPerformances`);
      default:
        return Observable.of(null);
    }
  }
}
