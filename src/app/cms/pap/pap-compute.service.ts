import { Injectable } from '@angular/core';
import * as moment from 'moment';

import { ConsultantsPerformance } from '../../models/consultants-performance';
import { ConsultantsProgram } from '../../models/consultants-program';
import { EffectiveContractStateService } from '../../shared/effective-contract-state.service';
import { AppNotificationService } from '../../app-services/app-notification.service';
import { Subject } from 'rxjs';
import { EffectiveContractState } from '../../models/effective-contract-state';

@Injectable()
export class PapComputeService {

  programs: ConsultantsProgram[];
  assessments: ConsultantsPerformance[];
  assessment: ConsultantsPerformance;
  effectiveContractStateService: EffectiveContractStateService;
  papSubject: Subject<{ score: number, message: string, computeDate: Date }> = new Subject();

  constructor(private notificationService: AppNotificationService) {
  }

  computePapAsync(programs: ConsultantsProgram[],
    assessments: ConsultantsPerformance[],
    assessment: ConsultantsPerformance,
    effectiveContractStateService: EffectiveContractStateService): Subject<{ score: number, message: string, computeDate: Date }> {

    this.programs = programs;
    this.assessments = assessments;
    this.assessment = assessment;
    this.effectiveContractStateService = effectiveContractStateService;

    this.effectiveContractStateService.computeEffectiveStateAsync().subscribe(effectiveState => {

      // happy path
      this.computePap(effectiveState),

        error => this.notificationService.errorNotification.next(error);
    });

    return this.papSubject;
  }

  computePap(effectiveState: EffectiveContractState) {
    if (!this.assessment) {
      return this.papSubject.next({ score: NaN, message: 'Consultant progress is not supplied', computeDate: null });
    }

    if (!this.programs) {
      return this.papSubject.next({ score: NaN, message: 'Consultant has no activity schedule', computeDate: null });
    }

    // get approved predicted cost

    // Supplemented amount is the sum of original contract value + all supplements' LS component
    // all values are exclusive of VAT
    const supplementedAmount = effectiveState.effectiveTotal;

    // if measurement is > predicted cost
    if (this.assessment.contractCumulativeMoney > supplementedAmount) {
      return this.papSubject.next({ score: NaN, message: 'Not calculated (assessed amount is greater than predicted amount)', computeDate: null });
    }

    // required data points
    let scaledAssessment: number = null;

    // the last original contractor program
    // used through out our calculation
    let program: ConsultantsProgram = this.programs.sort(this.comparer)[0];

    //#region measurement date is > predicted completion
    const predictedCompletionDate = this.getContractCompletion(effectiveState);

    const contractCommencement = effectiveState.originalStartDate;

    let assessmentAtCompletion = this.getAssessedValueForCompletionDate(predictedCompletionDate);

    if (moment(this.assessment.assessmentDate).isAfter(moment(predictedCompletionDate))) {

      scaledAssessment = (program.contractCumulativeMoney / supplementedAmount) * assessmentAtCompletion;

      try {
        const pap = Math.round((scaledAssessment / program.contractCumulativeMoney) * 100);

        const daysFromStartToAssessment = moment(this.assessment.assessmentDate).diff(moment(contractCommencement));

        const daysFromStartToCompletion = moment(predictedCompletionDate).diff(moment(contractCommencement));

        const percentageBeyondCompletion = Math.round((daysFromStartToAssessment / daysFromStartToCompletion)) - 1;

        return this.papSubject.next({ score: pap, message: `${percentageBeyondCompletion} beyond contract completion`, computeDate: this.assessment.assessmentDate });

      } catch (error) {
      }
    }
    //#endregion

    //#region measurement date is <= predicted completion
    if (moment(this.assessment.assessmentDate).isSameOrBefore(moment(predictedCompletionDate))) {

      scaledAssessment = (this.assessment.contractCumulativeMoney) * (program.contractCumulativeMoney / assessmentAtCompletion);

      // find matching program for progress
      // Find the consultant program (approximately) 
      // corresponding to the last assessment date

      const originalCompletion = effectiveState.originalEndDate;

      const eot = moment(predictedCompletionDate).diff(moment(originalCompletion)) * -1;
      const date = moment(this.assessment.assessmentDate).add(eot);

      const programmedValue = this.getProgramForAssessmentDate(date);
      const pap = (this.assessment.contractCumulativeMoney / programmedValue) * 100;
      return this.papSubject.next({ score: pap, message: '', computeDate: this.assessment.assessmentDate });
    }

    //#endregion

  }

  getContractCompletion(effectiveState: EffectiveContractState): Date {
    try {
      return moment(effectiveState.supplementsEndDate)
        .isAfter(moment(effectiveState.originalEndDate)) ?
        effectiveState.supplementsEndDate : effectiveState.originalEndDate;
    } catch (error) {
      return effectiveState.originalEndDate;
    }
  }

  getAssessedValueForCompletionDate(date): any {

    const sortedAssessments = this.assessments.sort(this.comparer);

    var filteredAssessments = sortedAssessments.filter(p => moment(p.assessmentDate).isSame(moment(date)));

    if (filteredAssessments.length !== 0)
      return filteredAssessments[0].contractCumulativeMoney;

    for (let i = 0; i < sortedAssessments.length; i++)
    {
      if (moment(sortedAssessments[i].assessmentDate).isBefore(date)) {
        let lowerBoundaryProgram = sortedAssessments[i];

        let upperBoundaryProgram = null;

        if ((i - 1) >= 0)
          upperBoundaryProgram = sortedAssessments[i - 1];

        if (lowerBoundaryProgram === null && upperBoundaryProgram === null)
          return null;
        else if (lowerBoundaryProgram === null && upperBoundaryProgram !== null)
          return upperBoundaryProgram.contractCumulativeMoney;
        else if (lowerBoundaryProgram !== null && upperBoundaryProgram === null)
          return lowerBoundaryProgram.contractCumulativeMoney;
        else
          return (lowerBoundaryProgram.contractCumulativeMoney + upperBoundaryProgram.contractCumulativeMoney) / 2;
      }
    }

    return null;
  }

  private getProgramForAssessmentDate(date: moment.Moment): number {

    const sortedPrograms = this.programs.sort(this.comparer);

    var filteredPrograms = sortedPrograms.filter(p => moment(p.date).isSame(moment(date)));

    if (filteredPrograms.length !== 0)
      return filteredPrograms[0].contractCumulativeMoney;

    for (let i = 0; i < sortedPrograms.length; i++) {
      if (moment(sortedPrograms[i].date).isBefore(date)) {
        let lowerBoundaryProgram = sortedPrograms[i];

        let upperBoundaryProgram = null;

        if ((i - 1) >= 0)
          upperBoundaryProgram = sortedPrograms[i - 1];

        if (lowerBoundaryProgram === null && upperBoundaryProgram === null)
          return null;
        else if (lowerBoundaryProgram === null && upperBoundaryProgram !== null)
          return upperBoundaryProgram.contractCumulativeMoney;
        else if (lowerBoundaryProgram !== null && upperBoundaryProgram === null)
          return lowerBoundaryProgram.contractCumulativeMoney;
        else
          return (lowerBoundaryProgram.contractCumulativeMoney + upperBoundaryProgram.contractCumulativeMoney) / 2;
      }
    }
  }

  // descending comparer
  private comparer(s1, s2) {
    if (moment(s1.date).isBefore(s2.date)) {
      return 1;
    } else if (moment(s1.date).isAfter(s2.date)) {
      return -1;
    }

    return 0;
  }
}
