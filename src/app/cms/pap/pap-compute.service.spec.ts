import { TestBed, inject } from '@angular/core/testing';

import { PapComputeService } from './pap-compute.service';


describe('PapComputeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PapComputeService]
    });
  });

  it('should be created', inject([PapComputeService], (service: PapComputeService) => {
    expect(service).toBeTruthy();
  }));
});
