import { Component, OnInit } from '@angular/core';
import * as _moment from 'moment';

import { AppMenuService } from '../../app-services/app-menu.service';
import { ContractService } from '../../contract/contract.service';
import { ConsultantsPerformance } from '../../models/consultants-performance';
import { ConsultantsProgram } from '../../models/consultants-program';
import { WorkProgramComputeService } from '../../shared/work-program-compute.service';
import { WorkProgressComputeService } from '../../shared/work-progress-compute.service';
import { PapComputeService } from './pap-compute.service';
import { PapDataService } from './pap-data.service';
import { EffectiveContractStateService } from '../../shared/effective-contract-state.service';


@Component({
  selector: 'app-pap',
  templateUrl: './pap.component.html',
  styleUrls: ['./pap.component.css']
})
export class PapComponent implements OnInit {

  consultantsPrograms: ConsultantsProgram[];
  consultantsPerformances: ConsultantsPerformance[];

  programData: any[];
  progressData: any[];

  pap: { score: number, message: string, computeDate: Date } = { score: 0, message: '', computeDate: null };

  constructor(private menuSvc: AppMenuService,
    private dataService: PapDataService,
    private papComputeService: PapComputeService,
    private programComputeService: WorkProgramComputeService,
    private progressComputeService: WorkProgressComputeService,
    private contractService: ContractService,
    private effectiveContractStateService: EffectiveContractStateService) { }

  ngOnInit(): void {
    this.menuSvc.getMenuEntries('pap');

    const subscription = this.progressComputeService.getDressedProgresses().subscribe(programsAndProgresses => {

      this.consultantsPerformances = programsAndProgresses.progresses;
      this.consultantsPrograms = programsAndProgresses.programs;

      this.computeTotals();

      this.graph();

      this.computePap();
    });
  }

  computeTotals() {
    this.programComputeService.computeProgramsCumulatives(this.consultantsPrograms);
    this.progressComputeService.computeProgressesCumulatives(this.consultantsPerformances);
  }

  graph() {

    let programsClones = new Array<any>();
    if (this.consultantsPrograms) {

      Object.assign(programsClones, this.consultantsPrograms);

      try {
        const firstValue = programsClones[0];

        if (firstValue.contractCumulativeMoney !== 0) {

          const firstPhase = this.contractService.contract.phases.sort(this.comparer)[0];
          const date = _moment(firstPhase.startDate).subtract(1, 'day');

          programsClones.splice(0, 0, {
            id: 0, date: date.toDate(), deliverable: null,
            percentComplete: 0, monetaryValue: 0,
            phaseCumulativePercentage: 0, phaseCumulativeMoney: 0,
            contractCumulativePercentage: 0, contractCumulativeMoney: 0
          });
        }
      } catch (error) { }

      // scale to a thousandth 
      programsClones.forEach(program => {
        Object.defineProperty(program, 'contractAmountToDate', {
          value: 0,
          writable: true
        });
        program.contractAmountToDate = program.contractCumulativeMoney / 1000;
      });

      this.programData = programsClones;
    }

    // ***************************Progress********************************* //

    let progressClones = new Array<any>();
    Object.assign(progressClones, this.consultantsPerformances);

    try {
      const firstValue = progressClones[0];

      if (firstValue.contractCumulativeMoney !== 0) {
        const firstPhase = this.contractService.contract.phases.sort(this.comparer)[0];
        const date = _moment(firstPhase.startDate).subtract(1, 'day');

        progressClones.splice(0, 0, {
          id: 0, assessmentDate: date.toDate(), deliverable: null,
          percentComplete: 0, monetaryValue: 0,
          phaseCumulativePercentage: 0, phaseCumulativeMoney: 0,
          contractCumulativePercentage: 0, contractCumulativeMoney: 0,
          reason: '', detailedReason: ''
        });
      }
    } catch (error) { }

    // scale to a thousandth 
    progressClones.forEach(progress => {
      Object.defineProperty(progress, 'contractAmountToDate', {
        value: 0,
        writable: true
      });
      progress.contractAmountToDate = progress.contractCumulativeMoney / 1000;

    });

    this.progressData = progressClones;
  }

  computePap() {
    if (this.consultantsPerformances) {
      const lastAssessmentIndex = this.consultantsPerformances.length - 1;
      const lastAssessment = this.consultantsPerformances[lastAssessmentIndex];
      this.papComputeService.computePapAsync(this.consultantsPrograms, this.consultantsPerformances, lastAssessment, this.effectiveContractStateService)
        .subscribe(pap => {
          this.pap = pap;
          console.log(pap);
        });
    } else {
      this.pap.message = 'Progress data is not available';
    }
  }

  private comparer(s1, s2) {
    if (_moment(s1.startDate).isBefore(s2.startDate)) {
      return -1;
    } else if (_moment(s1.startDate).isAfter(s2.startDate)) {
      return 1;
    }

    return 0;
  }
}
