import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PapComponent } from './pap.component';

const routes: Routes = [
  {
    path: '',
    component: PapComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PapRoutingModule { }
