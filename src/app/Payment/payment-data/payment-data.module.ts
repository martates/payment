import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import {
//   MatSnackBarModule, MatSelectModule,
//   MatOptionModule, MatNativeDateModule,
//   MatFormFieldModule, MatDatepickerModule,
//   MatInputModule, MatDialogModule,
//   MatIconModule, MatCommonModule,
//   MatTableModule, MatTabsModule,
//   MatSidenavModule, MatRadioModule,
//   MatButtonModule,
//   MatPaginatorModule,
//   MatCheckboxModule
// } from '@angular/material';
import { PaymentDataRoutingModule } from './payment-data-routing.module';
import { PaymentDataComponent } from './payment-data.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PaymentService } from '../Services/payment.service';
import { NewFinancedbyComponent } from '../new-financedby/new-financedby.component';
import { FinancedByService } from '../Services/financed-by.service';
import { NewRemarkComponent } from '../new-remark/new-remark.component';
import { RemarkService } from '../Services/remark.service';
import { MaterialModule } from '../../material.module';
//import { AuthService } from 'src/app/auth/auth.service';
//import{AuthService} from '../../auth/auth.service';

@NgModule({
  imports: [
    CommonModule,
    CommonModule,
    HttpClientModule, HttpModule,
    ReactiveFormsModule, FormsModule,
    MaterialModule,
    // AppMaterialModule,
    //  MatCommonModule,
    // MatIconModule, MatDialogModule,
    // MatInputModule, MatDatepickerModule,
    // MatFormFieldModule, MatNativeDateModule,
    // MatSnackBarModule, MatSelectModule,
    // MatOptionModule, MatSelectModule,
    // MatSnackBarModule, MatTableModule,
    // MatTabsModule, MatSidenavModule,
    // MatRadioModule, MatButtonModule,
    // MatPaginatorModule,MatCheckboxModule,   
    PaymentDataRoutingModule,
  ],
  declarations: [PaymentDataComponent,NewFinancedbyComponent,NewRemarkComponent],
  entryComponents: [NewFinancedbyComponent,NewRemarkComponent],
  providers: [PaymentService,FinancedByService,RemarkService]
})
export class PaymentDataModule { }
