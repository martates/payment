import { Component, OnInit, ViewChild } from '@angular/core';
import { Project, ProjectByKm, ProjectName } from '../../models/row-models';
import { AppMenuService } from '../../app-services/app-menu.service';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { ProjectSharedService } from '../../row/Services/project-shared.service';
import { AppNotificationService } from '../../app-services/app-notification.service';
import { Payment, ArrfinancedBy, Remark } from '../../models/payment';
import { PaymentService } from '../Services/payment.service';
import { Subscription } from 'rxjs/Subscription';
import { DatePipe } from '@angular/common';
import { NewFinancedbyComponent } from '../new-financedby/new-financedby.component';
import { NewRemarkComponent } from '../new-remark/new-remark.component';
import { AuthService } from '../../auth/auth.service';
import { User } from '../../models/user';

// export interface financedBy {
//   key: Number;
//   value: string;
// }

@Component({
  selector: 'app-payment-data',
  templateUrl: './payment-data.component.html',
  styleUrls: ['./payment-data.component.css']
})
export class PaymentDataComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  public team = ['Team 1', 'Team 2', 'Road Fund'];
  public region = ['Adigrat RNMD', 'Alemgena RNMD', 'Asset Management',
    'Central', 'Debremarkos RNMD', 'Design & Build',
    'DG and DDG', 'Diredawa RNMD', 'East', 'Express',
    'Gondar RNMD', 'ICT Service', 'Jimma RNMD',
    'Kombolcha RNMD', 'Nekempt RNMD', 'North',
    'Planning and Budget', 'Procurement', 'QARISM',
    'Shashemene RNMD', 'Sodo ', 'South', 'West',
  ];
  public status = ['Voucher Prepared', 'Returned to Origin', 'on progress', 'Encoded', 'Voiucher |Prepared', 'Initiated', 'Approved', 'Force Approved']
  public type = ['Civil Works', 'Supervision Consulting', 'supervision'];

  pipe = new DatePipe('en-US');
  private subscription: Subscription;
  paymentform: FormGroup;
  addAmendLabel: string;
  deleteResetLabel: string;
  dataSource: any;
  projectNames: ProjectName[];
  user: User;
  displayedColumns: string[];
  public length: number = 5;

  arrfinancedBy: ArrfinancedBy = {
    financedById: 0,
    financedByname: null,
    noofDateExpected: 0
  };

  remark: Remark = {
    remarkId: 0,
    remarkName: null,
  };

  payment: Payment = {
    id: 0,
    ProjectID: 0,
    ProjectName: null,
    dateRecived: null,
    payeTo: null,
    financedBy: null,
    region: null,
    ipcinv: null,
    type: null,
    dateToprocess: null,
    payDate: null,
    status: null,
    counterAccount: null,
    team: null,
    teamLeader: null,
    dateReturn: null,
    paidstatus: null,
    remark: null,
    lastEditedBy:null
  };

  project: Project = {
    projectID: 0,
    name: null,
    number: null,
    roadLength: null,
    projectByKm: Array<ProjectByKm>(),
    payement: Array<Payment>(),
  };
  datePipe: any;

  constructor(private menuSvc: AppMenuService,
    public dialog: MatDialog,
    public dialog1: MatDialog,
    private formmodel: FormBuilder,
    public projectSharedService: ProjectSharedService,
    private paymentService: PaymentService,
    public authService: AuthService,
    private messageNotificationService: AppNotificationService, ) {   
  }

  ngOnInit() {
    // console.log(this.financedBy)
    this.addAmendLabel = 'Save';
    this.deleteResetLabel = 'Clear';
    this.createForm();
    this.financedByList();
    this.remarkList();
    this.subscription = this.projectSharedService.projectStream.subscribe(project => {
      this.init();
    });
    this.init();
    this.displayedColumns = ['id', 'ProjectName', 'dateRecived', 'financedBy', 'region', 'ipcinv', 'type',
      'dateToprocess', 'payDate', 'status', 'counterAccount', 'team', 'teamLeader', 'dateReturn', 'remark'];
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  init() {
    if (this.projectSharedService.project.projectID === 0) {
      this.openProject();
      return;
    }
    this.paymentList();
  }
  openProject(): void {
    this.projectSharedService.openProject();
  }

  private refreshFormAndTable(): void {
    this.addAmendLabel = 'Save';
    this.paymentform.reset();
    this.createForm();
    this.dataSource = new MatTableDataSource<Payment>(this.project.payement);
  }

  paymentList() {
    this.payment.ProjectID = this.projectSharedService.project.projectID;
    this.subscription = this.paymentService.getData('payment-per-project', this.payment.ProjectID)
      .subscribe(paymentperpro => {
        this.project.payement = paymentperpro
        this.dataSource = new MatTableDataSource<Payment>(paymentperpro);
        this.dataSource.paginator = this.paginator;
      });
  }

  private createForm(): void {
    this.paymentform = this.formmodel.group({
      idControl: [0, Validators.required],
      dateRecivedcontrol: [null, Validators.required],
      payeeControl: [null, Validators.required],
      financedByControl: [null, Validators.required],
      regionControl: [null, Validators.required],
      IPCINVControl: [null, Validators.required],
      typeControl: [null, Validators.required],
      expectedDateToProcessControl: [null, Validators.required],
      expectedDateToPayeControl: [null, Validators.required],
      statusControl: [null, Validators.required],
      counterAcountantControl: [null, Validators.required],
      teamControl: [null, Validators.required],
      teamLeaderControl: [null, Validators.required],
      dateReturnControl: [null, Validators.required],
      paidControl: [null, Validators.required],
      remarkControl: [null, Validators.required],
      lastEditedByControl:[null,Validators.required]
    });
  }

  private getPaymentFromFormModel(): Payment {
    this.authService.userSubject.subscribe(user => {
      this.user = user;
    });
    const formModel = this.paymentform.value;
    const r: Payment = {
      id: formModel.idControl,
      ProjectID: this.projectSharedService.project.projectID,
      ProjectName: this.projectSharedService.project.name,
      dateRecived: formModel.dateRecivedcontrol,
      payeTo: formModel.payeeControl,
      financedBy: formModel.financedByControl,
      region: formModel.regionControl,
      ipcinv: formModel.IPCINVControl,
      type: formModel.typeControl,
      dateToprocess: formModel.expectedDateToProcessControl,
      payDate: formModel.expectedDateToPayeControl,
      status: formModel.statusControl,
      counterAccount: formModel.counterAcountantControl,
      team: formModel.teamControl,
      teamLeader: formModel.teamLeaderControl,
      dateReturn: formModel.dateReturnControl,
      paidstatus: formModel.paidControl,
      remark: formModel.remarkControl,
      lastEditedBy:this.user.userName
    };
    return r;
  }

  save(): void {
    const r: Payment = this.getPaymentFromFormModel();
    if (this.addAmendLabel === 'Save') {
      this.subscription = this.paymentService.postData(r).subscribe(
        paymnet => {
          this.paymentList();
          this.refreshFormAndTable();
          this.messageNotificationService.messageNotification.next('Paymenet List saved');
        },
        e => this.messageNotificationService.errorNotification.next(e.error())
      );

    }
    else if (this.addAmendLabel === 'Amend') {
      // if(this.paymentform.controls['paidControl'].value==='0'){
      const index = this.getIndex(r);
      Object.assign(this.project.payement[index], r);
      this.subscription = this.paymentService.putData(r, null).subscribe(
        payment => {
          this.paymentList();
          this.refreshFormAndTable();
          this.paymentform.reset();
        },
        e => this.messageNotificationService.messageNotification.next('Payment Data Updated Succesfully')

      );
    }
    this.addAmendLabel = 'Save';
    this.deleteResetLabel = 'Clear';
  }

  removePaymentFromList(): void {
    if (this.deleteResetLabel === 'Delete') {
      const r: Payment = this.getPaymentFromFormModel();
      this.subscription = this.paymentService.deleteData(r, null).subscribe(data => {
        this.paymentList();
        this.refreshFormAndTable();
        this.messageNotificationService.messageNotification.next('Payment Remove from the list');
        this.addAmendLabel = 'Save';
        this.deleteResetLabel = 'Clear';
      },
        e => this.messageNotificationService.errorNotification.next(e.error));
    }
    else {
      this.refreshFormAndTable();
    }
  }
  private getIndex(del: Payment): number {
    return this.project.payement.findIndex(d => d.id === del.id);
  }
  public getCanDelete(): boolean {
    return !this.paymentform.pristine;
  }
  public selectPayment(payment: Payment): void {
    this.paymentform.setValue({
      idControl: payment.id,
      dateRecivedcontrol: payment.dateRecived,
      payeeControl: payment.payeTo,
      financedByControl: payment.financedBy,
      regionControl: payment.region,
      IPCINVControl: payment.ipcinv,
      typeControl: payment.type,
      expectedDateToProcessControl: payment.dateToprocess,
      expectedDateToPayeControl: payment.payDate,
      statusControl: payment.status,
      counterAcountantControl: payment.counterAccount,
      teamControl: payment.team,
      teamLeaderControl: payment.teamLeader,
      dateReturnControl: payment.dateReturn,
      paidControl: payment.paidstatus,
      remarkControl: payment.remark,
      lastEditedByControl:payment.lastEditedBy
    });
    this.paymentform.markAsDirty();
    this.addAmendLabel = 'Amend';
    this.deleteResetLabel = 'Delete';
  }

  financedByList() {
    this.subscription = this.paymentService.getData('FinancedBy-Names', null)
      .subscribe(financedByList => {
        this.arrfinancedBy = financedByList;
      });
  }

  remarkList() {
    this.subscription = this.paymentService.getData('remark-Names', null)
      .subscribe(remarkList => {
        this.remark = remarkList;
      });
  }

  dateProccess(financedByControl: any) {
    var newArray = [];
    const r: Payment = this.getPaymentFromFormModel();
    this.subscription = this.paymentService.getData('FinancedBy-Names', null)
      .subscribe(financedByList => {
        for (var i = 0; i < financedByList.length; i++) {
          if (this.paymentform.controls['financedByControl'].value == this.arrfinancedBy[i].financedByname) {
            this.addDays(r.dateRecived, this.arrfinancedBy[i].noofDateExpected);
          }
        }
      });
  }

  addDays(date: Date, days: number) {
    date.setDate(date.getDate() + days);
    this.paymentform.controls.expectedDateToProcessControl.setValue(this.pipe.transform(date, 'MM/dd/yyyy'));
    this.paymentform.controls.expectedDateToPayeControl.setValue(this.pipe.transform(date, 'MM/dd/yyyy'));
  }

  addfinancedbyDialog() {
    setTimeout(() => {
      const dialogRef = this.dialog.open(NewFinancedbyComponent, {
        width: '450px',
        data: { message: 'New Financed By' },
        autoFocus: true,
        disableClose: true
      });
    }, 500);
  }

  addremarkDialog() {
    setTimeout(() => {
      const dialogRef1 = this.dialog1.open(NewRemarkComponent, {
        width: '450px',
        data: { message: 'New Remark By' },
        autoFocus: true,
        disableClose: true
      });
    }, 500);
  }
}