import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaymentDataComponent } from './payment-data.component';

const routes: Routes = [
 { path: '',
  component: PaymentDataComponent
}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaymentDataRoutingModule { }
