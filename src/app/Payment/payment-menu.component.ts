import { Component, OnInit, ViewChild } from '@angular/core';
import { AppMenuService } from '../app-services/app-menu.service';
import { MatTableDataSource} from '@angular/material/table';
import{MatPaginator} from '@angular/material/paginator';
import { Subscription } from 'rxjs/Subscription';
import { PaymentService } from './Services/payment.service';
import { Payment } from '../models/payment';

@Component({
  selector: 'app-payment-menu',
  templateUrl: './payment-menu.component.html',
  styleUrls: ['./payment-menu.component.css']
})
export class PaymentMenuComponent implements OnInit {
  panelOpenState = false;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  private subscription: Subscription;
  dataSource: any;
  dataSourcepaid: any;
  public length: number = 5;
  constructor(private menuSvc: AppMenuService,
    private paymentService: PaymentService, ) { }
  displayedColumns: string[] =  ['id', 'ProjectName', 'dateRecived','financedBy', 'region','ipcinv','type',
  'dateToprocess','payDate','status','counterAccount','team','teamLeader','dateReturn'];
 
  displayedColumnsPaid: string[] =  ['id', 'ProjectName', 'dateRecived','financedBy', 'region','ipcinv','type',
  'dateToprocess','payDate','status','counterAccount','team','teamLeader','dateReturn'];

  ngOnInit() {
    this.menuSvc.getMenuEntries('Payment');
    this.paymentListNotPaid();
    this.paymentListPaid();
  }
  paymentListNotPaid() {
    this.subscription = this.paymentService.getData('paymentList', null)
      .subscribe(paymentperpro => {
        this.dataSource = new MatTableDataSource<Payment>(paymentperpro);
        this.dataSource.paginator = this.paginator;
      });
  } 

  paymentListPaid() {
    this.subscription = this.paymentService.getData('paymentListPaid', null)
      .subscribe(paymentperpro => {
        this.dataSourcepaid = new MatTableDataSource<Payment>(paymentperpro);
        this.dataSourcepaid.paginator = this.paginator;
      });
  } 

}
