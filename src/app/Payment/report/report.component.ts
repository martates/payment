import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { PaymentService } from '../Services/payment.service';
import { MatTableDataSource} from '@angular/material/table';
import{MatPaginator} from '@angular/material/paginator';
import { Payment } from '../../models/payment';
import { from } from 'rxjs';

// const ELEMENT_DATA: PeriodicElement[] = [
//   {id: 1, projectName: 'Beles Mekane Birhan', financedBy: 'World Bank Development (IDA)', region: 'North', dateRecived: new Date(Date.now()), type: 'Civil Works', status: 'Voucher Prepared'},
//   {id: 2, projectName: 'Beles Mekane Birhan', financedBy: 'World Bank Development (IDA)', region: 'North', dateRecived: new Date(Date.now()), type: 'Supervision Consulting', status: 'Returned to Origin'},
// ];
@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})


export class ReportComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  private subscription: Subscription;
  dataSource: any;
  public length: number = 10;

  constructor( private paymentService: PaymentService,) { }
  displayedColumns: string[] = ['id', 'ProjectName', 'dateRecived','financedBy', 'region','ipcinv','type',
   'dateToprocess','payDate','status','counterAccount','team','teamLeader','dateReturn'];
  ngOnInit() {
    this.paymentList();
  }

  paymentList() {
    this.subscription = this.paymentService.getData('paymentList',null)
      .subscribe(paymentperpro => {
        this.dataSource = new MatTableDataSource<Payment>(paymentperpro);
        this.dataSource.paginator = this.paginator;
      });
  }
}
export interface PeriodicElement {
  id:number;
  projectName: string;
  financedBy: string;
  region: string;
  dateRecived: Date;
  type:string;
  status:string;
}
