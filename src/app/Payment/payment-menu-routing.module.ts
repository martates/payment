import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaymentMenuComponent } from './payment-menu.component';

const routes: Routes = [
  {
    path: '',
    component: PaymentMenuComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaymentMenuRoutingModule { }
