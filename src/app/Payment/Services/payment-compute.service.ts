import { Injectable } from '@angular/core';
import { PaymentService } from './payment.service';
import { Subject } from 'rxjs/Subject';
import { AppNotificationService } from '../../app-services/app-notification.service';
import { Payment } from '../../models/payment';
import { ProjectService } from '../../row/Services/project.service';
import { ProjectSharedService } from '../../row/Services/project-shared.service';

@Injectable()
export class PaymentComputeService {
  public removePhaseTaskSubject: Subject<any> = new Subject();
  constructor(private dataService: PaymentService,
    private notificationService: AppNotificationService,
    public projectSharedService: ProjectSharedService,
  ) { }

  deletePayment(phase): Subject<any> {

    this.dataService.deleteData(phase, null).subscribe(data => {
      this.notificationService.messageNotification.next('Payment deleted');

      if (this.projectSharedService.project) {
        const index = this.getIndex(phase);
        this.projectSharedService.project.payement.splice(index, 1);
        this.removePhaseTaskSubject.next();
      }

    },
      e => {
        this.notificationService.errorNotification.next(e.error);
        this.removePhaseTaskSubject.next();
      });

    return this.removePhaseTaskSubject;
  }
  getIndex(payment: Payment): number {
    return this.projectSharedService.project.payement.findIndex(p => p.id === payment.id);
  }

  extractContractPhase(formControls): Payment {
    // const phases = this.contractService.contract.phases;
    // const phaseId = formControls.idControl.value || 0;
    // let supplementId = null; let contractPhaseId = null; let replacedSupplementId = null;

    // try {
    //   supplementId = phases.find(p => p.id === phaseId).supplementId;
    //   contractPhaseId = phases.find(p => p.id === phaseId).contractPhaseId;
    //   replacedSupplementId = phases.find(p => p.id === phaseId).replacedSupplementId;
    // } catch (e) { }

    return {
      id: formControls.idControl,
      ProjectID: this.projectSharedService.project.projectID,
      ProjectName: this.projectSharedService.project.name,
      dateRecived: formControls.dateRecivedcontrol,
      payeTo: formControls.payeeControl,
      financedBy: formControls.financedByControl,
      region: formControls.regionControl,
      ipcinv: formControls.IPCINVControl,
      type: formControls.typeControl,
      dateToprocess: formControls.expectedDateToProcessControl,
      payDate: formControls.expectedDateToPayeControl,
      status: formControls.statusControl,
      counterAccount: formControls.counterAcountantControl,
      team: formControls.teamControl,
      teamLeader: formControls.teamLeaderControl,
      dateReturn: formControls.dateReturnControl,
      paidstatus: formControls.paidControl,
      remark:formControls.remarkControl,
      lastEditedBy:formControls.lastEditedByControl
    };
  }

  // addDays(date: Date, days: number): Date {
  //   console.log('adding ' + days + ' days');
  //   console.log(date);
  //   date.setDate(date.getDate() + days);
  //   console.log(date);
  //   return date;
  // }
}

