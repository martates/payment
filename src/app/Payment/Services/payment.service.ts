import { Injectable } from '@angular/core';
import { AsyncDataFacade } from '../../shared/async-data-facade';
import { Observable } from 'rxjs/Observable';
import { Payment } from '../../models/payment';

@Injectable()
export class PaymentService {

  constructor(private asyncDataFacade: AsyncDataFacade, ) { }
  postData(payment: any): Observable<any> {
    return this.asyncDataFacade.post(`/paymenetadd`, payment);
  }

  putData(payment: any, param: any): Observable<any> {
    // api/projectperkm/{projectperkmId}/ownerShip/{ownerShip
    const url = `/project/${payment.id}/payment/${payment.id}`;
    return this.asyncDataFacade.put(url, payment);
  }
  deleteData(paymnet: Payment, parm: any): Observable<any> {
    const url = `/payment/${paymnet.id}`;
    return this.asyncDataFacade.delete(url, {});
  }
  getData(query: string, param: any): Observable<any> {
    const projectId = <number>param;
    switch (query) {
      case 'payment-per-project':
        const url = '/projects/' + projectId + '/payment';
        return this.asyncDataFacade.get(url);

      case 'paymentList':
        return this.asyncDataFacade.get('/paymentList/');
        
      case 'paymentListPaid':
        return this.asyncDataFacade.get('/paymentListPaid/');

      case 'FinancedBy-Names':
        return this.asyncDataFacade.get('/allfinancedByList/');

      case 'remark-Names':
        return this.asyncDataFacade.get('/allRemarkList/');

      default:
        return Observable.of(null);
    }
  }
}
