import { Injectable } from '@angular/core';
import { AsyncDataFacade } from '../../shared/async-data-facade';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FinancedByService {

  constructor(private asyncDataFacade: AsyncDataFacade) { }
  getData(query: string, param: any, param2: any): Observable<any> {
    switch (query) {
      case 'FinancedBy-Names':
    //  return this.asyncDataFacade.get('/financedByNames/');     
     return this.asyncDataFacade.get('/allfinancedByList/');     

      default:
        return Observable.of(null);
    }
  }
  postData(financedBy: any): Observable<any> {
    return this.asyncDataFacade.post(`/financedByadd`, financedBy);
  }
}
