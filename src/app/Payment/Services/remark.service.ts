import { Injectable } from '@angular/core';
import { AsyncDataFacade } from '../../shared/async-data-facade';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RemarkService {

  constructor(private asyncDataFacade: AsyncDataFacade) { }
  getData(query: string, param: any, param2: any): Observable<any> {
    switch (query) {
      case 'remark-Names':
    //  return this.asyncDataFacade.get('/financedByNames/');     
     return this.asyncDataFacade.get('/allRemarkList/');     

      default:
        return Observable.of(null);
    }
  }
  postData(remark: any): Observable<any> {
    return this.asyncDataFacade.post(`/remarkadd`, remark);
  }
}
