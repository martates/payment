import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { AppNotificationService } from '../../app-services/app-notification.service';
import { FinancedByService } from '../Services/financed-by.service';
import { Subscription } from 'rxjs/Subscription';
import { ArrfinancedBy } from '../../models/payment';

@Component({
  selector: 'app-new-financedby',
  templateUrl: './new-financedby.component.html',
  styleUrls: ['./new-financedby.component.css']
})
export class NewFinancedbyComponent implements OnInit {
  private subscription: Subscription;
  finacedByName: string;
  addAmendLabel: string;
  newFinancedbyForm: FormGroup;
  constructor(private formmodel: FormBuilder,
    private dialogRef: MatDialogRef<NewFinancedbyComponent>,
    private financedByService: FinancedByService,
    private messageNotificationService: AppNotificationService,
 //   private paymentDataComponent: PaymentDataComponent
  ) { }
  arrfinancedBy: ArrfinancedBy = {
    financedById: 0,
    financedByname: null,
    noofDateExpected: 0
  };
  ngOnInit() {
    this.createForm();
    this.addAmendLabel = 'Save';
  }
  close() {
    if (!this.finacedByName) {
      this.dialogRef.close();
      return;
    }
  }

  private createForm(): void {
    this.newFinancedbyForm = this.formmodel.group({
      financedByIdControl: [0, Validators.required],
      financedBynameControl: [null, Validators.required],
      noexpectedDateToProcessControl: [null, Validators.required],
    });
  }

  save(): void {
    const r: ArrfinancedBy = this.getPaymentFromFormModel();
    if (this.addAmendLabel === 'Save') {
      this.subscription = this.financedByService.postData(r).subscribe(
        paymnet => {
          this.messageNotificationService.messageNotification.next('Financed By List saved');

        },
        e => this.messageNotificationService.errorNotification.next(e.error())
      );
      this.newFinancedbyForm.reset;
      this.close();
     // this.paymentDataComponent.paymentform.reset;
    }
    //  else if (this.addAmendLabel === 'Amend') {

    //   const index = this.getIndex(r);

    //   Object.assign(this.project.payement[index], r);

    //   this.subscription = this.paymentService.putData(r, null).subscribe(
    //     e => this.messageNotificationService.messageNotification.next('Payment Data Updated Succesfully')
    //   );
    //   this.paymentList();
    //   this.refreshFormAndTable();
    //   this.paymentform.reset();
    // }
    // this.addAmendLabel = 'Save';
    // this.deleteResetLabel = 'Clear';
  }

  private getPaymentFromFormModel(): ArrfinancedBy {
    const formModel = this.newFinancedbyForm.value;
    const r: ArrfinancedBy = {
      financedById: formModel.financedByIdControl,
      financedByname: formModel.financedBynameControl,
      noofDateExpected: formModel.noexpectedDateToProcessControl,
    };
    return r;
  }
}
