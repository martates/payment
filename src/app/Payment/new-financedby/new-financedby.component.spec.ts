import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewFinancedbyComponent } from './new-financedby.component';

describe('NewFinancedbyComponent', () => {
  let component: NewFinancedbyComponent;
  let fixture: ComponentFixture<NewFinancedbyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewFinancedbyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewFinancedbyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
