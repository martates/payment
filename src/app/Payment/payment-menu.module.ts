import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentMenuComponent } from './payment-menu.component';
import { PaymentMenuRoutingModule } from './payment-menu-routing.module';
import { PaymentService } from './Services/payment.service';
import { PaymentComputeService } from './Services/payment-compute.service';
import { MaterialModule } from '../material.module';
import { AuthService } from '../auth/auth.service';

@NgModule({
  imports: [
    CommonModule,
    PaymentMenuRoutingModule,
    MaterialModule,
  ],
  declarations: [PaymentMenuComponent],
  providers: [PaymentService,PaymentComputeService,AuthService]
})
export class PaymentMenuModule { }
