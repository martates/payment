import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewRemarkComponent } from './new-remark.component';

describe('NewRemarkComponent', () => {
  let component: NewRemarkComponent;
  let fixture: ComponentFixture<NewRemarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewRemarkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewRemarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
