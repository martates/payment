import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { AppNotificationService } from '../../app-services/app-notification.service';
import { RemarkService } from '../Services/remark.service';
import { Remark } from '../../models/payment';

@Component({
  selector: 'app-new-remark',
  templateUrl: './new-remark.component.html',
  styleUrls: ['./new-remark.component.css']
})
export class NewRemarkComponent implements OnInit {
  private subscription: Subscription;
  remarkName: string;
  addAmendLabel: string;
  newRemarkForm: FormGroup;

  constructor(private formmodel: FormBuilder,
    private dialogRef1: MatDialogRef<NewRemarkComponent>,
    private remarkService: RemarkService,
    private messageNotificationService: AppNotificationService) { }

  ngOnInit() {
    this.createForm();
    this.addAmendLabel = 'Save';
  }
  close() {
    if (!this.remarkName) {
      this.dialogRef1.close();
      return;
    }
  }

  private createForm(): void {
    this.newRemarkForm = this.formmodel.group({
      remarkIdControl: [0, Validators.required],
      remarkNameControl: [null, Validators.required],
    });
  }
  save(): void {
    const r: Remark = this.getPaymentFromFormModel();
    if (this.addAmendLabel === 'Save') {
      this.subscription = this.remarkService.postData(r).subscribe(
        remark => {
          this.messageNotificationService.messageNotification.next('Financed By List saved');

        },
        e => this.messageNotificationService.errorNotification.next(e.error())
      );
      this.newRemarkForm.reset;
      this.close();
      // this.paymentDataComponent.paymentform.reset;
    }
  }

  private getPaymentFromFormModel(): Remark {
    const formModel = this.newRemarkForm.value;
    const r: Remark = {
      remarkId: formModel.remarkIdControl,
      remarkName: formModel.remarkNameControl,
    };
    return r;
  }
}
