import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ReplaySubject, BehaviorSubject, Observable } from 'rxjs';

import { environment } from '../../environments/environment';
import { AppNotificationService } from '../app-services/app-notification.service';
import { User } from '../models/user';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable()
export class AuthService {

  userSubject: ReplaySubject<User> = new ReplaySubject();
  private currentUserSubject: BehaviorSubject<AuthService>;
  public currentUser: Observable<AuthService>;
  user: User;
  token: string;
  roles: any;
  isAuthenticated: boolean = false;
  returnUrl: string;

  constructor( 
    // private route: ActivatedRoute,
    private notificationService: AppNotificationService,
    private http: HttpClient,
    private router: Router) { }
  public get currentUserValue(): AuthService {
    return this.currentUserSubject.value;
  }
  login(name, password): ReplaySubject<User> {
    // const userName = sessionStorage.getItem("u");

    // const cred = sessionStorage.getItem("c");

    this.http.post<JWT>(environment.apiBaseUrl + '/auth', { name, password })
      .subscribe(jwt => {

        if (jwt.user) {
          this.isAuthenticated = true;
          localStorage.setItem('currentUser', JSON.stringify(jwt.user));
        }

        this.user = jwt.user;
        this.token = jwt.token;
        this.roles = jwt.roles;

        this.userSubject.next(this.user);
      },
        error => this.notificationService.errorNotification.next('There was a problem with your credentials. System cannot log you in.'));

    return this.userSubject;
  }

  logout() {
    this.currentUser = null;
    localStorage.removeItem('currentUser');
    this.isAuthenticated = false;
    this.userSubject = null;
    this.token = null;
    this.roles = null;
    this.userSubject = null;
    this.router.navigate(['']);
  }
}

class JWT {
  user: User;
  roles: string[];
  token: string;
  userName: string;
  cred: string;
}
