import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Router, RouterStateSnapshot, Route } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { AuthService } from './auth.service';
import { AppNotificationService } from '../app-services/app-notification.service';

@Injectable()
export class AuthGuard implements CanActivate, CanLoad {

  constructor(
    private authService: AuthService,
    private router: Router,
    private notificationService: AppNotificationService) {

  }

  canLoad(route: Route): boolean | Observable<boolean> | Promise<boolean> {
    return this.canNavigate(route, null);
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    return this.canNavigate(route, state);
  }

  private canNavigate(route, state) {
    if (this.authService.isAuthenticated) {
      return true;
    }

    if (state)
      this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    else
      this.router.navigate(['/login']);

    // this.notificationService.warningNotification.next(`Please login to access ${route.path}`);
    
    return false;
  }
}
