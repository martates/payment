import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthService } from './auth.service';
import { AppNotificationService } from '../app-services/app-notification.service';
import { AppMenuService } from '../app-services/app-menu.service';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  model: any = {};
  loading = false;
  returnUrl: string;

  constructor(
    public dialogRef1: MatDialogRef<LoginComponent>,
    private menuSvc: AppMenuService,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthService,
    private notificationService: AppNotificationService,
  ) { }

  ngOnInit() {
    //  this.authenticationService.logout();
    this.menuSvc.getMenuEntries('login');
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  login() {
    this.loading = true;
    this.authenticationService.login(this.model.username, this.model.password)
      .subscribe(
        data => {
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.notificationService.errorNotification.next("Login error")
          this.loading = false;
        });
   this.close();
  }

  close() {
    //if (!this.authenticationService.user){
      this.dialogRef1.close();
    return;
   // }
  }

}
