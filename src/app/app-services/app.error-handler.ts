import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler, Inject, Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
import { AppNotificationService } from './app-notification.service';


@Injectable()
export class AppErrorHandler implements ErrorHandler {

  constructor(@Inject(AppNotificationService) private errorNotificationService: AppNotificationService) {
  }

  handleError(issue: any): void {
    if (!environment.production) {
      console.log(issue);
    }

    // TODO: DEBUG; Remove
    if (issue.message.includes('ExpressionChangedAfterItHasBeenCheckedError')) {
      return;
    }

    if(issue.error instanceof ProgressEvent) {
      this.errorNotificationService.errorNotification.next('Network issues. Check if you are connected to a network.');
    } else if (issue instanceof HttpErrorResponse) {
      this.errorNotificationService.errorNotification.next(issue.error);
    } else {
      this.errorNotificationService.errorNotification.next(issue.message);
    }
  }
}
