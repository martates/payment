// export class Menu {
//   constructor(
//     public label: string,
//     public hint: string,
//     public routeLink: string) {
//   }
// }
export class Menu {
  constructor(
    // public ApplicationId: string,
    // public RoleId: string,
    public RoleName: string,
    public LoweredRoleName: string,
    public Description: string) { }
}