import { EventEmitter, Injectable } from '@angular/core';

import { Menu } from './app-menu';
import { Subscription } from 'rxjs';
import { UserCreateService } from '../Account/Service/user-create.service';
import { AuthService } from '../auth/auth.service';
import { User } from '../models/user';

@Injectable()
export class AppMenuService {
  private subscription: Subscription;
  menuChanged = new EventEmitter<Array<Menu>>();
  menuEntries: Array<Menu>;
  newArray: Array<[]>;
 user: User;
  constructor(private userCreateService: UserCreateService,
    public authService: AuthService,) {
      this.authService.userSubject.subscribe(user => {
        this.user = user;
      });
  }
 
  getMenuEntries(module: string): void {
    switch (module) {
      case 'login':
        this.menuEntries = new Array<Menu>();
        this.menuEntries.push(
          new Menu('', '', ''))
          ;
        this.menuChanged.emit(this.menuEntries);
        break;
      case 'home':
        this.menuEntries = new Array<Menu>();
        this.subscription = this.userCreateService.getData('roleListbasedonUserId', this.user.userId).subscribe(
          allRoleList => {
            for (var i = 0; i < allRoleList.length; i++) {
              this.menuEntries.push(new Menu(allRoleList[i].roleName, allRoleList[i].loweredRoleName, allRoleList[i].description));
            }
          });
        // this.menuEntries = new Array<Menu>();
        // this.menuEntries.push(
        //   // new Menu('Contract Phases Info', 'contract phases and values', 'contract-phase'),
        //   // new Menu('Consultancy Monitoring', 'programme, variations & performance', 'deliverables'),
        //   // new Menu('RoW Management', 'right of way management', 'row-management'),

        //   // new Menu('Engineers workload', 'workload and Project Progress Performance', 'engineers-workload'),
        //   // new Menu('Staff\'s Contracts', 'staff\'s contracts', 'staffs-contract'),

        //   // new Menu('road-character', 'road-character', 'road-character'),
        //   // new Menu('Purchase-Menu', 'Purchase-Menu', 'Purchase-Menu'),

        //   // new Menu('Payment', 'Payment', 'Payment'),
        //   // new Menu('User-Issue', 'User-Issue', 'User-Issue'),
        //   //  console.log(this.menuEntries)
        //  // new Menu(this.menuEntries[0][0], this.menuEntries[0][1], this.menuEntries[0][2])
        // );

        this.menuChanged.emit(this.menuEntries);
        break;

      case 'contract-phase':
        this.menuEntries = new Array<Menu>();
        this.menuEntries.push(
          new Menu('Consultancy Monitoring', 'programme, variations & performance', 'deliverables'),
        );

        this.menuChanged.emit(this.menuEntries);
        break;

      case 'deliverables':
      case 'work-program':
      case 'work-progress':
      case 'supplements':
      case 'pap':
        this.menuEntries = new Array<Menu>();
        this.menuEntries.push(
          new Menu('Contract Deliverables', '', 'deliverables'),
          new Menu('Planned Service Programme', '', 'work-program'),
          new Menu('Progress', '', 'work-progress'),
          new Menu('Supplemental Agreement', '', 'supplements'),
          new Menu('PAP', '', 'pap'),
          new Menu('Contract Phases Info', 'contract phases and values', 'contract-phase')
        );

        this.menuChanged.emit(this.menuEntries);
        break;

      case 'row-management':
      case 'projectPerKm':
      case 'ownership':
      case 'property':
      case 'estimateCommitte':
      case 'fileupload':
      case 'report':
        this.menuEntries = new Array<Menu>();
        this.menuEntries.push(
          new Menu('Project Per KM', '', 'projectPerKm'),
          new Menu('Ownership', '', 'ownership'),
          new Menu('Property', '', 'property'),
          new Menu('Estimate Committe', '', 'estimateCommitte'),
          new Menu('File-Upload', '', 'fileupload'),
          new Menu('Report', '', 'report'),
          new Menu('Back To ROW Management Home', '', 'row-management')
        );
        this.menuChanged.emit(this.menuEntries);
        break;


      case 'road-character':
        this.menuEntries = new Array<Menu>();
        this.menuEntries.push(
          new Menu('Report Type', 'Report Type', 'Report-Type'),
          new Menu('Back To Road character', 'road-character', 'road-character'),
        );
        this.menuChanged.emit(this.menuEntries)
        break;

      case 'Purchase-Menu':
        this.menuEntries = new Array<Menu>();
        this.menuEntries.push(
          new Menu('Requstion Item', 'Requstion-Item', 'Requstion-Item'),
          new Menu('Purchase Order', 'Purchase-Order', 'Purchase-Order'),
        );
        this.menuChanged.emit(this.menuEntries)
        break;

      case 'Payment':
        this.menuEntries = new Array<Menu>();
        this.menuEntries.push(
          new Menu('Payment-data', 'Payment-data', 'Payment-data'),
        );
        this.menuChanged.emit(this.menuEntries)
        break;

      case 'User-Issue':
        this.menuEntries = new Array<Menu>();
        this.menuEntries.push(
          new Menu('User-Create', 'User-Create', 'User-Create'),
          new Menu('', 'Manage-Password', ''),
          new Menu('Role', 'Role', 'Role'),
          new Menu('', 'Unlock User', ''),
          new Menu('', 'Reset Password', ''),
          new Menu('', 'Accounts Statics', ''),
          new Menu('', 'Reports', '',)
        );
        this.menuChanged.emit(this.menuEntries)
        break;
    }
  }
}
