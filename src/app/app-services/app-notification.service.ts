import { Subject } from 'rxjs/Subject';
import { Injectable } from "@angular/core";

@Injectable()
export class AppNotificationService {
  public errorNotification: Subject<string> = new Subject();
  public warningNotification: Subject<string> = new Subject();
  public messageNotification: Subject<string> = new Subject();
}
