export interface User {
    userId: string;
    userName: string;
    firstName: string;
    lastName: string;
    position: string;
    directorate: string;
}

export interface NewUser {
    ApplicationId: string;
    //  UserId:string;
    UserName: string;
    FirstName: string;
    LastName: string;
    Position: string;
    Directorate: string;
    BadgeNumber: string;
    Telephone: string;
    LoweredUserName: string;
    MobileAlias: string;
    IsAnonymous: number;
    LastActivityDate: Date;
    LastEditedBy: string
}
export interface MemberShip {
    ApplicationId: string,
    UserId: string,
    Password: string,
    PasswordFormat: number,
    PasswordSalt: string,
    MobilePIN: string,
    Email: string,
    LoweredEmail: string,
    PasswordQuestion: string,
    PasswordAnswer: string,
    IsApproved: number,
    IsLockedOut: number,
    CreateDate: Date,
    LastLoginDate: Date,
    LastPasswordChangedDate: Date,
    LastLockoutDate: Date,
    FailedPasswordAttemptCount: number,
    FailedPasswordAttemptWindowStart: Date,
    FailedPasswordAnswerAttemptCount: number,
    FailedPasswordAnswerAttemptWindowStart: Date,
    Comment: string
}

export interface Position {
    Position: string
}
export interface Directorate {
    Directorate: string
}
export interface Role {
    ApplicationId: string,
    RoleId: string,
    RoleName: string,
    LoweredRoleName: string,
    Description: string
}

export interface UserInRole {
    RoleId: string,
    UserId: string,
}