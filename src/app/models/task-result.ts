export interface TaskResult {
    isSuccessful: boolean;
    message: string;
}