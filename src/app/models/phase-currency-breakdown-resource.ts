// PhaseCurrencyBreakdown class uses getters for properties
// This is an exact copy except avoiding the getters when sending the 
// resource to API
export class PhaseCurrencyBreakdownResource {
    amount: number;
    currency: string;
    exchangeRate: number;
}