export interface SaveConsultantsPerformance {
    id: number;
    assessmentDate: Date;
    submissionDate: Date;
    percentComplete: number;
    monetaryValue: number;
    deliverableId: number;
    reason: string;
    detailedReason: string;
}