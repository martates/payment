import { ContractPhase } from './contract-phase';
import { DeliverableLookup } from './deliverable-lookup';

export interface Deliverable {
  id: number;
  contractPhase: ContractPhase;
  contractPhaseId?: number;
  deliverableLookup: DeliverableLookup;
  phasePercentage: number;
  monetaryValue: number;
}
