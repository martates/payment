import { Deliverable } from "./deliverable";

export interface ContractPhase {
  id: number;
  contractId: number;
  name: string;
  description: string;
  startDate: Date;
  endDate: Date;
  lumpSum: number;
  reimbursable: number;
  provisionalSum: number;
  advancePercent: number;
  advanceAmount: number;
  supplementId?: number;
  // reference to previous phase the
  // current phase replaces
  contractPhaseId?: number;
  // if this belongs to the first supplement, the previous
  // this supercedes is the contract itself identified by contractId. 
  // If this belongs to a supplement, the replacedSupplementId represents
  // the replaced supplement
  replacedSupplementId?: number;
  deliverables?: Array<Deliverable>;
  // Change in phase advance amount/percent
  // triggers change in the contract
  // advance. This makes the change 
  // available to back-end api
  contractAdvanceAmount?: number;
  contractAdvancePercent?: number;

  isAdvanceNotTaken?: boolean;
}
