export interface roadCharacter {
  rnmdId: number;
  rnmd: string;
  section: string;
  roadClass: string;
  roadNo: string;
  segmentNo: string;
  segmentName: string;
  asphaltlength: number;
  gravellength: number;
  totallength: number;
  region: string;
  ds_Standard: string;
  roadstatus: string;
  registraion_year: Date;
  roadAdmin: string;
  gtpPlan: string;
}

export interface RnmdsList {
  Id: number;
  rnmd: string;
}

export interface SectionList {
  section: string;
}

export interface RoadClassList {
  roadClass: string;
}

export interface RnmdsListDrop {
  key: number;
  value: string;
}

export interface SectionListDrop {
  key: number;
  value: string;
}

export interface RoadClassListDrop {
  key: number;
  value: string;
}