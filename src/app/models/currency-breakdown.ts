export class CurrencyBreakdown {
    id: number;
    contractId: number;
    currency: string;
    exchangeRate: number;
}