export interface SaveConsultantsProgram {
    id: number;
    date: Date;
    percentComplete: number;
    monetaryValue: number;
    deliverableId: number;
}