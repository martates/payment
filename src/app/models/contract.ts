import { ConsultantsPerformance } from './consultants-performance';
import { ConsultantsProgram } from './consultants-program';
import { ContractPhase } from './contract-phase';
import { CurrencyBreakdown } from './currency-breakdown';

export interface Contract {
  id: number;
  name: string;
  number: string;
  consultantName: string;
  contractAmount: number;
  // computed field (not sent to our
  // api endpoints)
  phasesTotal: number;
  // computed field (not sent to our
  // api endpoints)
  vat: number;
  // computed field (not sent to our
  // api endpoints)
  grandTotal: number;
  advanceAmount: number;
  contractSignedOn: Date;
  phases: Array<ContractPhase>;
  schedule?: Array<ConsultantsProgram>;
  progress?: Array<ConsultantsPerformance>;
  currencyBreakdowns?: Array<CurrencyBreakdown>;
  contractCommencement?: Date;
  contractPeriod?: number;
}
