
import * as moment from 'moment';

import { ContractService } from '../contract/contract.service';
import { ContractPhase } from "./contract-phase";


export class EffectiveContractState {
    originalPhases: ContractPhase[];
    supplementPhases: ContractPhase[];
    originalPs: number; originalLs: number; originalReimbursable: number;
    originalStartDate: Date; originalEndDate: Date;
    supplementsPs: number; supplementsLs: number; supplementsReimbursable: number;
    supplementsStartDate: Date; supplementsEndDate: Date;
    effectiveLs: number; effectivePs: number; effectiveReimbursable: number;
    effectiveVat: number; effectiveTotal: number;

    constructor(private contractService: ContractService) {
    }

    computeEffectiveState(phases: ContractPhase[]): any {
        this.originalPhases = phases.filter(p => p.supplementId === null);

        this.supplementPhases = phases.filter(p => p.supplementId !== null);

        const contract = this.contractService.contract;

        this.originalStartDate = contract.contractCommencement;

        this.originalEndDate =
            moment(contract.contractCommencement)
                .add(contract.contractPeriod - 1, 'days')
                .toDate();

        this.originalLs =
            this.originalPhases
                .map(phase => phase.lumpSum)
                .reduce((acc, n) => acc + n, 0);

        this.originalPs =
            this.originalPhases
                .map(phase => phase.provisionalSum)
                .reduce((acc, n) => acc + n, 0);

        this.originalReimbursable =
            this.originalPhases
                .map(phase => phase.reimbursable)
                .reduce((acc, n) => acc + n, 0);

        this.supplementsLs =
            this.supplementPhases
                .map(phase => phase.lumpSum)
                .reduce((acc, n) => acc + n, 0);

        this.supplementsPs =
            this.supplementPhases
                .map(phase => phase.provisionalSum)
                .reduce((acc, n) => acc + n, 0);

        this.supplementsReimbursable =
            this.supplementPhases
                .map(phase => phase.reimbursable)
                .reduce((acc, n) => acc + n, 0);

        //////////////////////////////////////////////////////////////////////

        const intactPhases = this.originalPhases.filter(p => this.supplementPhases.filter(sp => sp.contractPhaseId === p.id).length === 0);

        const intactPhasesLs =
            intactPhases
                .map(phase => phase.lumpSum)
                .reduce((acc, n) => acc + n, 0);

        const intactPhasesPs =
            intactPhases
                .map(phase => phase.provisionalSum)
                .reduce((acc, n) => acc + n, 0);

        const intactPhasesReimbursable =
            intactPhases
                .map(phase => phase.reimbursable)
                .reduce((acc, n) => acc + n, 0);

        this.effectiveLs = this.supplementsLs + intactPhasesLs;
        this.effectivePs = this.supplementsPs + intactPhasesPs;
        this.effectiveReimbursable = this.supplementsReimbursable + intactPhasesReimbursable;

        const subTotal = this.effectiveLs + this.effectivePs + this.effectiveReimbursable;

        // TODO: Fetch VAT from DB
        this.effectiveVat = subTotal * 0.15;

        this.effectiveTotal = subTotal + this.effectiveVat;

        ///////////////////////////////////////////////////////////////////////

        this.supplementsStartDate = contract.contractCommencement;

        const sortedPhases = this.supplementPhases
            .sort(this.comparer);

        if (sortedPhases && sortedPhases.length > 0) {
            const lastCompletedPhase = sortedPhases[0];
            this.supplementsEndDate = lastCompletedPhase.endDate;
        } else {
            this.supplementsEndDate = this.originalEndDate;
        }
    }

    // descending comparer
    private comparer(d1, d2) {
        if (moment(d1.endDate).isBefore(d2.endDate)) {
            return 1;
        } else if (moment(d1.endDate).isAfter(d2.endDate)) {
            return -1;
        }

        return 0;
    }

}