export interface ContractName {
  key: number;
  value: string;
}
