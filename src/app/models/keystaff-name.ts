export interface KeyStaffName {
  key: number;
  value: string;
}