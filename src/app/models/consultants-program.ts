import { Deliverable } from "./deliverable";

export interface ConsultantsProgram {
    id: number;

    date: Date;

    deliverable: Deliverable;

    // cumulative of deliverable by nature
    percentComplete: number;
    // cumulative, this deliverable only
    monetaryValue: number;

    phaseCumulativePercentage?: number;

    phaseCumulativeMoney?: number;

   
    contractCumulativePercentage?: number;

    contractCumulativeMoney?: number; 
}