import { CurrencyBreakdown } from "./currency-breakdown";

// Currency breakdown by LS, PS and reimbursable
export class PhaseCurrencyBreakdown {

    currencies: string[];

    contractExchangeRates: CurrencyBreakdown[];

    id: number;

    contractPhaseId: number;

    etbAmount: number;

    // LS, PS, Reimbursable
    type: string;

    private _amount: number;
    set amount(amount: number) {
        this._amount = amount;
        this.computeEtbAmount();
    }
    get amount(): number {
        return this._amount;
    }

    private _currency: string;
    set currency(currency: string) {
        this._currency = currency;

        try {
            this.exchangeRate = this.contractExchangeRates.filter(ex => ex.currency === currency)[0].exchangeRate;
            this.computeEtbAmount();
        } catch (err) {
            this.exchangeRate = null;
        }

    }
    get currency(): string {
        return this._currency;
    }

    private _exchangeRate: number;
    set exchangeRate(exchange: number) {
        this._exchangeRate = exchange;
        this.computeEtbAmount();
    }
    get exchangeRate(): number {
        return this._exchangeRate;
    }


    constructor(exchangeRates: CurrencyBreakdown[]) {
        this.contractExchangeRates = exchangeRates;
        this.currencies = ['ETB', 'USD', 'GBP', 'EUR'];
    }

    getIsValid(): boolean {
        return (this.type && this._amount && this._currency && this._exchangeRate) ? true : false;
    }

    computeEtbAmount() {
        try {
            this.etbAmount = this._amount * this._exchangeRate;
        }
        catch (error) { }
    }
}