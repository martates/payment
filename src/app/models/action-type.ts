export enum ActionType {
    Add,
    Cancel,
    Clear,
    Delete,
    Update
}