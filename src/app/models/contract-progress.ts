import { KeyStaff } from "./contract-Key-staff";

/**
 * interface ContractProgress 
 */
export interface ContractProgress  {
    
    ContractName: string;
    Firm:string;
    ContractType:string;
    Staff: Array<KeyStaff>;
    ProgressPerformance:number;
   
    
}