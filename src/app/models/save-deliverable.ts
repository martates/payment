export interface SaveDeliverable {
  id: number;
  contractPhaseId: number;
  deliverableLookupId: number;
  phasePercentage: number;
  monetaryValue: number;
}
