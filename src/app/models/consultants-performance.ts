import { Deliverable } from "./deliverable";

export interface ConsultantsPerformance {
    id: number;
    assessmentDate: Date;
    submissionDate?: Date;
    reason: string;
    detailedReason: string;
    percentComplete: number;
    monetaryValue: number;
    deliverable: Deliverable;
    phaseCumulativePercentage?: number;
    phaseCumulativeMoney?: number;
    contractCumulativePercentage?: number;
    contractCumulativeMoney?: number; 
}