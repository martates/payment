export interface Payment {
      id: number;
      ProjectID: number;
      ProjectName: string;
      dateRecived: Date;
      payeTo: string;
      financedBy: string;
      region: string;
      ipcinv: string
      type: string;
      dateToprocess: Date;
      payDate: Date;
      status: string;
      counterAccount: string;
      team: string;
      teamLeader: string;
      dateReturn: Date;
      paidstatus: string;
      remark:string;
      lastEditedBy:string;
}
export interface ArrfinancedBy {
      financedById: number;
      financedByname: string;
      noofDateExpected: number;
}
export interface Remark {
      remarkId: number;
      remarkName: string;
  }