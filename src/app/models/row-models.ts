//import { ProjectByKm, Ownership } from './row-models';
import { Payment } from './payment';
export interface Project {
    projectID: number;
    name: string;
    number: string;
    roadLength: string;
    projectByKm: Array<ProjectByKm>;
    payement: Array<Payment>;
}
export interface ProjectName {
    key: number;
    value: string;
}
export interface ProjectByKm {
    projectPerKmId: number;
    projectId: number;
    kmStart: number;
    kmEnd: number;
    state: string;
    zone: string;
    woreda: string;
    ownership: Array<Ownership>;
    estimateCommitte: Array<EstimateCommitte>;
}
export interface ProjectPerKmName {
    key: number;
    value: number;
}
export interface Ownership {
    ownershipId: number;
    ownershipName: string;
    projectPerKmId: number;
    property: Array<Property>;
}
export interface OwnershipName {
    key: number;
    value: string;
}
export interface Property {
    propertyId: number;
    ownershipId: number;
    type: string;
    category: string;
    propertyType: string;
    description: string;
    measurement: string;
    quantity: number;
    singlePrice: number;
    totalPrice: number;
    longtiude: number;
    latitude: number;
    outsideConsultant: string;
    eraConsultant: string;
    paymentDate: Date;
    isPermanent: String;
    passionDate: Date;
    startDate: Date;
    endDate: Date;
}
export interface EstimateCommitte {
    id: number;
    projectPerKmId: number;
    name: string;
    position: string;
    address: string;
}
export interface FileUpload {
    id: number;
    ownershipId: number;
    fileName: any;
}
export interface PropertyType {
    id: number;
    name: string;
    units: Unit[];
}
export interface Unit {
    id: number;
    name: string;
}
export class Percent {
    constructor(
        perc_number: number,
        ptype: string) {
    }
}
export class SumGravelength {
    constructor(
        region: string,
        gravelength: number,
        asphaltlengt:number) {
    }
}