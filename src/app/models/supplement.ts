import { ContractPhase } from "./contract-phase";

export interface Supplement {   
    id: number;
    contractId: number;
    number: number;
    name: string;
    agreementDate: Date;
    purpose: string;
    supplementAmount: number;
    phases?: ContractPhase[];
}