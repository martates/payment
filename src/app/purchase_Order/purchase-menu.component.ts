import { Component, OnInit } from '@angular/core';
import { AppMenuService } from '../app-services/app-menu.service';

@Component({
  selector: 'app-purchase-menu',
  templateUrl: './purchase-menu.component.html',
  styleUrls: ['./purchase-menu.component.css']
})
export class PurchaseMenuComponent implements OnInit {

  constructor(private menuSvc: AppMenuService) { }

  ngOnInit() {
    this.menuSvc.getMenuEntries('Purchase-Menu');
  }

}
