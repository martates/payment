import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PurchaseMenuComponent } from './purchase-menu.component';

const routes: Routes = [
  {
    path: '',
    component: PurchaseMenuComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PurchaseMenuRoutingModule { }
