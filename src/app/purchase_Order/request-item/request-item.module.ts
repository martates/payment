import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestItemRoutingModule } from './request-item-routing.module';
import { RequestItemComponent } from './request-item.component';

@NgModule({
  imports: [
    CommonModule,
    RequestItemRoutingModule
  ],
  declarations: [RequestItemComponent]
})
export class RequestItemModule { }
