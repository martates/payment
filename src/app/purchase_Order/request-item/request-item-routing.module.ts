import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RequestItemComponent } from './request-item.component';

const routes: Routes = [
  {
    path: '',
    component: RequestItemComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],  
})
export class RequestItemRoutingModule { }
