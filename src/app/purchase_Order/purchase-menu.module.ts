import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PurchaseMenuComponent } from './purchase-menu.component';
import { PurchaseMenuRoutingModule } from './purchase-menu-routing.module';

@NgModule({
  imports: [
    CommonModule,
    PurchaseMenuRoutingModule,
  ],
  declarations: [PurchaseMenuComponent]
})
export class PurchaseMenuModule { }
