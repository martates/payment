import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PurchaseOrderComponent } from './purchase-order.component';
import { PurchaseOrderRoutingModule } from './purchase-order-routing.module';

@NgModule({
  imports: [
    CommonModule,
    PurchaseOrderRoutingModule
  ],
  declarations: [PurchaseOrderComponent]
})
export class PurchaseOrderModule { }
