import { Component, OnInit } from '@angular/core';
import { AppMenuService } from '../../app-services/app-menu.service';

@Component({
  selector: 'app-purchase-order',
  templateUrl: './purchase-order.component.html',
  styleUrls: ['./purchase-order.component.css']
})
export class PurchaseOrderComponent implements OnInit {

  constructor(private menuSvc: AppMenuService) { }

  ngOnInit() {
    this.menuSvc.getMenuEntries('Purchase-Order');
  }

}
