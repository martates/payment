﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class Previous_Contract_Phase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContractPhases_Supplements_SupplementId",
                table: "ContractPhases");

            migrationBuilder.AlterColumn<int>(
                name: "SupplementId",
                table: "ContractPhases",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "ContractPhaseId",
                table: "ContractPhases",
                nullable: true,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ContractPhases_ContractPhaseId",
                table: "ContractPhases",
                column: "ContractPhaseId");

            migrationBuilder.AddForeignKey(
                name: "FK_ContractPhases_ContractPhases_ContractPhaseId",
                table: "ContractPhases",
                column: "ContractPhaseId",
                principalTable: "ContractPhases",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_ContractPhases_Supplements_SupplementId",
                table: "ContractPhases",
                column: "SupplementId",
                principalTable: "Supplements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContractPhases_ContractPhases_ContractPhaseId",
                table: "ContractPhases");

            migrationBuilder.DropForeignKey(
                name: "FK_ContractPhases_Supplements_SupplementId",
                table: "ContractPhases");

            migrationBuilder.DropIndex(
                name: "IX_ContractPhases_ContractPhaseId",
                table: "ContractPhases");

            migrationBuilder.DropColumn(
                name: "ContractPhaseId",
                table: "ContractPhases");

            migrationBuilder.AlterColumn<int>(
                name: "SupplementId",
                table: "ContractPhases",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ContractPhases_Supplements_SupplementId",
                table: "ContractPhases",
                column: "SupplementId",
                principalTable: "Supplements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
