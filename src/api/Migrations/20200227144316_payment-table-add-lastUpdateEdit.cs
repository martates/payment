﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Asgard.Web.Migrations
{
    public partial class paymenttableaddlastUpdateEdit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LastEditedBy",
                table: "Payments",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastEditedBy",
                table: "Payments");
        }
    }
}
