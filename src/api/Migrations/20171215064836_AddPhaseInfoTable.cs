﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class AddPhaseInfoTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PhaseInfos",
                columns: table => new
                {
                    PhaseInfoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContractID = table.Column<int>(nullable: true),
                    PhaseIEndDate = table.Column<DateTime>(nullable: false),
                    PhaseIIEndDate = table.Column<DateTime>(nullable: false),
                    PhaseIIStartDate = table.Column<DateTime>(nullable: false),
                    PhaseIITotalAmount = table.Column<decimal>(nullable: false),
                    PhaseIStartDate = table.Column<DateTime>(nullable: false),
                    PhaseITotalAmount = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhaseInfos", x => x.PhaseInfoId);
                    table.ForeignKey(
                        name: "FK_PhaseInfos_Contracts_ContractID",
                        column: x => x.ContractID,
                        principalTable: "Contracts",
                        principalColumn: "ContractID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PhaseInfos_ContractID",
                table: "PhaseInfos",
                column: "ContractID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PhaseInfos");
        }
    }
}
