﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class Linkdeliverablestodeliverableslookup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "Deliverables");

            migrationBuilder.AddColumn<int>(
                name: "DeliverableLookupId",
                table: "Deliverables",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Deliverables_DeliverableLookupId",
                table: "Deliverables",
                column: "DeliverableLookupId");

            migrationBuilder.AddForeignKey(
                name: "FK_Deliverables_DeliverableLookups_DeliverableLookupId",
                table: "Deliverables",
                column: "DeliverableLookupId",
                principalTable: "DeliverableLookups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deliverables_DeliverableLookups_DeliverableLookupId",
                table: "Deliverables");

            migrationBuilder.DropIndex(
                name: "IX_Deliverables_DeliverableLookupId",
                table: "Deliverables");

            migrationBuilder.DropColumn(
                name: "DeliverableLookupId",
                table: "Deliverables");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Deliverables",
                maxLength: 1000,
                nullable: false,
                defaultValue: "");
        }
    }
}
