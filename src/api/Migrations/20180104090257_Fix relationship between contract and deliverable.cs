﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class Fixrelationshipbetweencontractanddeliverable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deliverables_Contracts_ContractID",
                table: "Deliverables");

            migrationBuilder.RenameColumn(
                name: "ContractID",
                table: "Deliverables",
                newName: "ContractId");

            migrationBuilder.RenameColumn(
                name: "Amount",
                table: "Deliverables",
                newName: "PhasePercentage");

            migrationBuilder.RenameIndex(
                name: "IX_Deliverables_ContractID",
                table: "Deliverables",
                newName: "IX_Deliverables_ContractId");

            migrationBuilder.RenameColumn(
                name: "ContractID",
                table: "Contracts",
                newName: "ContractId");

            migrationBuilder.AlterColumn<int>(
                name: "ContractId",
                table: "Deliverables",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "MonetaryValue",
                table: "Deliverables",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "PhaseId",
                table: "Deliverables",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Deliverables_PhaseId",
                table: "Deliverables",
                column: "PhaseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Deliverables_Contracts_ContractId",
                table: "Deliverables",
                column: "ContractId",
                principalTable: "Contracts",
                principalColumn: "ContractId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Deliverables_ContractPhases_PhaseId",
                table: "Deliverables",
                column: "PhaseId",
                principalTable: "ContractPhases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deliverables_Contracts_ContractId",
                table: "Deliverables");

            migrationBuilder.DropForeignKey(
                name: "FK_Deliverables_ContractPhases_PhaseId",
                table: "Deliverables");

            migrationBuilder.DropIndex(
                name: "IX_Deliverables_PhaseId",
                table: "Deliverables");

            migrationBuilder.DropColumn(
                name: "MonetaryValue",
                table: "Deliverables");

            migrationBuilder.DropColumn(
                name: "PhaseId",
                table: "Deliverables");

            migrationBuilder.RenameColumn(
                name: "ContractId",
                table: "Deliverables",
                newName: "ContractID");

            migrationBuilder.RenameColumn(
                name: "PhasePercentage",
                table: "Deliverables",
                newName: "Amount");

            migrationBuilder.RenameIndex(
                name: "IX_Deliverables_ContractId",
                table: "Deliverables",
                newName: "IX_Deliverables_ContractID");

            migrationBuilder.RenameColumn(
                name: "ContractId",
                table: "Contracts",
                newName: "ContractID");

            migrationBuilder.AlterColumn<int>(
                name: "ContractID",
                table: "Deliverables",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Deliverables_Contracts_ContractID",
                table: "Deliverables",
                column: "ContractID",
                principalTable: "Contracts",
                principalColumn: "ContractID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
