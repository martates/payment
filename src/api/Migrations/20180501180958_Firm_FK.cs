﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Asgard.Web.Migrations {
    public partial class Firm_FK : Migration {
        protected override void Up (MigrationBuilder migrationBuilder) {
            migrationBuilder.DropForeignKey (
                name: "FK_ConsultantsPerformances_Contracts_ContractId",
                table: "ConsultantsPerformances");

            migrationBuilder.DropForeignKey (
                name: "FK_ConsultantsPrograms_Contracts_ContractId",
                table: "ConsultantsPrograms");

            migrationBuilder.DropForeignKey (
                name: "FK_ContractPhases_Contracts_ContractId",
                table: "ContractPhases");

            migrationBuilder.DropForeignKey (
                name: "FK_Contracts_Firm_FirmId",
                table: "Contracts");

            migrationBuilder.DropForeignKey (
                name: "FK_CurrencyBreakdown_Contracts_ContractId",
                table: "CurrencyBreakdown");

            migrationBuilder.DropForeignKey (
                name: "FK_Deliverables_Contracts_ContractId",
                table: "Deliverables");

            migrationBuilder.DropForeignKey (
                name: "FK_Supplements_Contracts_ContractId",
                table: "Supplements");

            migrationBuilder.DropPrimaryKey (
                name: "PK_Contracts",
                table: "Contracts");

            migrationBuilder.RenameTable (
                name: "Contracts",
                newName: "Contract");

            migrationBuilder.RenameColumn (
                name: "FirmId",
                table: "Contract",
                newName: "Contractor");

            migrationBuilder.CreateIndex (
                name: "IX_Contract_Contractor",
                table: "Contract",
                column: "Contractor");

            migrationBuilder.AlterColumn<int> (
                name: "Contractor",
                table: "Contract",
                nullable : false,
                oldClrType : typeof (int),
                oldNullable : true);

            migrationBuilder.AddPrimaryKey (
                name: "PK_Contract",
                table: "Contract",
                column: "ContractId");

            migrationBuilder.AddForeignKey (
                name: "FK_ConsultantsPerformances_Contract_ContractId",
                table: "ConsultantsPerformances",
                column: "ContractId",
                principalTable: "Contract",
                principalColumn: "ContractId",
                onDelete : ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey (
                name: "FK_ConsultantsPrograms_Contract_ContractId",
                table: "ConsultantsPrograms",
                column: "ContractId",
                principalTable: "Contract",
                principalColumn: "ContractId",
                onDelete : ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey (
                name: "FK_Contract_Firm_Contractor",
                table: "Contract",
                column: "Contractor",
                principalTable: "Firm",
                principalColumn: "FirmID",
                onDelete : ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey (
                name: "FK_ContractPhases_Contract_ContractId",
                table: "ContractPhases",
                column: "ContractId",
                principalTable: "Contract",
                principalColumn: "ContractId",
                onDelete : ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey (
                name: "FK_CurrencyBreakdown_Contract_ContractId",
                table: "CurrencyBreakdown",
                column: "ContractId",
                principalTable: "Contract",
                principalColumn: "ContractId",
                onDelete : ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey (
                name: "FK_Deliverables_Contract_ContractId",
                table: "Deliverables",
                column: "ContractId",
                principalTable: "Contract",
                principalColumn: "ContractId",
                onDelete : ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey (
                name: "FK_Supplements_Contract_ContractId",
                table: "Supplements",
                column: "ContractId",
                principalTable: "Contract",
                principalColumn: "ContractId",
                onDelete : ReferentialAction.Cascade);
        }

        protected override void Down (MigrationBuilder migrationBuilder) {
            migrationBuilder.DropForeignKey (
                name: "FK_ConsultantsPerformances_Contract_ContractId",
                table: "ConsultantsPerformances");

            migrationBuilder.DropForeignKey (
                name: "FK_ConsultantsPrograms_Contract_ContractId",
                table: "ConsultantsPrograms");

            migrationBuilder.DropForeignKey (
                name: "FK_Contract_Firm_Contractor",
                table: "Contract");

            migrationBuilder.DropForeignKey (
                name: "FK_ContractPhases_Contract_ContractId",
                table: "ContractPhases");

            migrationBuilder.DropForeignKey (
                name: "FK_CurrencyBreakdown_Contract_ContractId",
                table: "CurrencyBreakdown");

            migrationBuilder.DropForeignKey (
                name: "FK_Deliverables_Contract_ContractId",
                table: "Deliverables");

            migrationBuilder.DropForeignKey (
                name: "FK_Supplements_Contract_ContractId",
                table: "Supplements");

            migrationBuilder.DropPrimaryKey (
                name: "PK_Contract",
                table: "Contract");

            migrationBuilder.RenameTable (
                name: "Contract",
                newName: "Contracts");

            migrationBuilder.RenameColumn (
                name: "Contractor",
                table: "Contracts",
                newName: "FirmId");

            migrationBuilder.RenameIndex (
                name: "IX_Contract_Contractor",
                table: "Contracts",
                newName: "IX_Contracts_FirmId");

            migrationBuilder.AlterColumn<int> (
                name: "FirmId",
                table: "Contracts",
                nullable : true,
                oldClrType : typeof (int));

            migrationBuilder.AddPrimaryKey (
                name: "PK_Contracts",
                table: "Contracts",
                column: "ContractId");

            migrationBuilder.AddForeignKey (
                name: "FK_ConsultantsPerformances_Contracts_ContractId",
                table: "ConsultantsPerformances",
                column: "ContractId",
                principalTable: "Contracts",
                principalColumn: "ContractId",
                onDelete : ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey (
                name: "FK_ConsultantsPrograms_Contracts_ContractId",
                table: "ConsultantsPrograms",
                column: "ContractId",
                principalTable: "Contracts",
                principalColumn: "ContractId",
                onDelete : ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey (
                name: "FK_ContractPhases_Contracts_ContractId",
                table: "ContractPhases",
                column: "ContractId",
                principalTable: "Contracts",
                principalColumn: "ContractId",
                onDelete : ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey (
                name: "FK_Contracts_Firm_FirmId",
                table: "Contracts",
                column: "FirmId",
                principalTable: "Firm",
                principalColumn: "FirmID",
                onDelete : ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey (
                name: "FK_CurrencyBreakdown_Contracts_ContractId",
                table: "CurrencyBreakdown",
                column: "ContractId",
                principalTable: "Contracts",
                principalColumn: "ContractId",
                onDelete : ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey (
                name: "FK_Deliverables_Contracts_ContractId",
                table: "Deliverables",
                column: "ContractId",
                principalTable: "Contracts",
                principalColumn: "ContractId",
                onDelete : ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey (
                name: "FK_Supplements_Contracts_ContractId",
                table: "Supplements",
                column: "ContractId",
                principalTable: "Contracts",
                principalColumn: "ContractId",
                onDelete : ReferentialAction.Cascade);
        }
    }
}