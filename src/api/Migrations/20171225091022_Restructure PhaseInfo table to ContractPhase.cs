﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class RestructurePhaseInfotabletoContractPhase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PhaseInfos");

            migrationBuilder.CreateTable(
                name: "ContractPhases",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContractId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    EndDate = table.Column<DateTime>(nullable: false),
                    LumpSum = table.Column<decimal>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ProvisionalSum = table.Column<decimal>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractPhases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContractPhases_Contracts_ContractId",
                        column: x => x.ContractId,
                        principalTable: "Contracts",
                        principalColumn: "ContractID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ContractPhases_ContractId",
                table: "ContractPhases",
                column: "ContractId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ContractPhases");

            migrationBuilder.CreateTable(
                name: "PhaseInfos",
                columns: table => new
                {
                    PhaseInfoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContractId = table.Column<int>(nullable: false),
                    PhaseEndDate = table.Column<DateTime>(nullable: false),
                    PhaseStartDate = table.Column<DateTime>(nullable: false),
                    phaseLumpSum = table.Column<decimal>(nullable: false),
                    phaseName = table.Column<string>(nullable: false),
                    phaseProvisionSum = table.Column<decimal>(nullable: false),
                    phaseTotal = table.Column<decimal>(nullable: false),
                    phaseVat = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhaseInfos", x => x.PhaseInfoId);
                    table.ForeignKey(
                        name: "FK_PhaseInfos_Contracts_ContractId",
                        column: x => x.ContractId,
                        principalTable: "Contracts",
                        principalColumn: "ContractID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PhaseInfos_ContractId",
                table: "PhaseInfos",
                column: "ContractId",
                unique: true);
        }
    }
}
