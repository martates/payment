﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class initialdatabasemartaroadchracter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "SupplementAmount",
                table: "Supplements",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "ExchangeRate",
                table: "PhaseCurrencyBreakdowns",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "Amount",
                table: "PhaseCurrencyBreakdowns",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "PhasePercentage",
                table: "Deliverables",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "MonetaryValue",
                table: "Deliverables",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<DateTime>(
                name: "LastModifiedOn",
                table: "CurrencyBreakdown",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<decimal>(
                name: "ExchangeRate",
                table: "CurrencyBreakdown",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedOn",
                table: "CurrencyBreakdown",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<decimal>(
                name: "Reimbursable",
                table: "ContractPhases",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "ProvisionalSum",
                table: "ContractPhases",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "LumpSum",
                table: "ContractPhases",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "AdvancePercent",
                table: "ContractPhases",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "AdvanceAmount",
                table: "ContractPhases",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "OriginalContractAmount",
                table: "Contract",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "AdvanceAmount",
                table: "Contract",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AddColumn<int>(
                name: "ProjectID",
                table: "Contract",
                nullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "PercentComplete",
                table: "ConsultantsPrograms",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "MonetaryValue",
                table: "ConsultantsPrograms",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "PercentComplete",
                table: "ConsultantsPerformances",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "MonetaryValue",
                table: "ConsultantsPerformances",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.CreateTable(
                name: "KeyStaffRegister",
                columns: table => new
                {
                    StaffID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    IdNumber = table.Column<string>(nullable: true),
                    IdType = table.Column<string>(nullable: true),
                    IsLocalStaff = table.Column<bool>(nullable: true),
                    Landline = table.Column<string>(nullable: true),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastModifiedOn = table.Column<DateTime>(nullable: true),
                    Mobile = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KeyStaffRegister", x => x.StaffID);
                });

            migrationBuilder.CreateTable(
                name: "Measurement",
                columns: table => new
                {
                    MeasurementID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AdvanceRepayment = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    CertifiedClaims = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    ContractId = table.Column<int>(nullable: true),
                    CorrectionOnCertificate = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    CriticalActivities = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: true),
                    Escalation = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    ExpenditureVsBudget = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    InterestOnLatePayments = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastModifiedOn = table.Column<DateTime>(nullable: true),
                    MeasurementAndVOs = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    OtherPayments = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    Taxes = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    TotalRetention = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    VatThisInvoice = table.Column<decimal>(type: "decimal(18,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Measurement", x => x.MeasurementID);
                    table.ForeignKey(
                        name: "FK_Measurement_Contract_ContractId",
                        column: x => x.ContractId,
                        principalTable: "Contract",
                        principalColumn: "ContractId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Project",
                columns: table => new
                {
                    ProjectID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Number = table.Column<string>(nullable: true),
                    RoadLength = table.Column<decimal>(type: "decimal(18,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project", x => x.ProjectID);
                });

            migrationBuilder.CreateTable(
                name: "PropertyType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "rnmdLists",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    rnmd = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_rnmdLists", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "roadClassesLists",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    roadClass = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_roadClassesLists", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RoadSegmentLists",
                columns: table => new
                {
                    rnmdId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoadAdmin = table.Column<string>(nullable: true),
                    asphaltlength = table.Column<int>(nullable: false),
                    ds_Standard = table.Column<string>(nullable: true),
                    gravellength = table.Column<int>(nullable: false),
                    gtpPlan = table.Column<string>(nullable: true),
                    region = table.Column<string>(nullable: true),
                    registraion_year = table.Column<DateTime>(nullable: false),
                    rnmd = table.Column<string>(nullable: true),
                    roadClass = table.Column<string>(nullable: true),
                    roadNo = table.Column<string>(nullable: true),
                    roadstatus = table.Column<string>(nullable: true),
                    section = table.Column<string>(nullable: true),
                    segmentName = table.Column<string>(nullable: true),
                    segmentNo = table.Column<string>(nullable: true),
                    totallength = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoadSegmentLists", x => x.rnmdId);
                });

            migrationBuilder.CreateTable(
                name: "sectionLists",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    section = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sectionLists", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ContactDetail",
                columns: table => new
                {
                    ContactDetailID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AssignmentDate = table.Column<DateTime>(nullable: true),
                    ContactType = table.Column<string>(nullable: true),
                    ContractId = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Landline = table.Column<string>(nullable: true),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastModifiedOn = table.Column<DateTime>(nullable: true),
                    LeavingDate = table.Column<DateTime>(nullable: true),
                    Mobile = table.Column<string>(nullable: true),
                    Position = table.Column<string>(nullable: true),
                    KeyStaffID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactDetail", x => x.ContactDetailID);
                    table.ForeignKey(
                        name: "FK_ContactDetail_KeyStaffRegister_KeyStaffID",
                        column: x => x.KeyStaffID,
                        principalTable: "KeyStaffRegister",
                        principalColumn: "StaffID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContractProgressPerformanceScore",
                columns: table => new
                {
                    PPid = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContractId = table.Column<int>(nullable: false),
                    GreenFlag = table.Column<bool>(nullable: true),
                    MeasurementID = table.Column<int>(nullable: false),
                    ProgressPerformance = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    ProgressPerformanceMessage = table.Column<string>(nullable: true),
                    RedFlag = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractProgressPerformanceScore", x => x.PPid);
                    table.ForeignKey(
                        name: "FK_ContractProgressPerformanceScore_Measurement_MeasurementID",
                        column: x => x.MeasurementID,
                        principalTable: "Measurement",
                        principalColumn: "MeasurementID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProjectPerKm",
                columns: table => new
                {
                    ProjectPerKmId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    KMStart = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    KmEnd = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    ProjectID = table.Column<int>(nullable: false),
                    State = table.Column<string>(nullable: true),
                    Woreda = table.Column<string>(nullable: true),
                    Zone = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectPerKm", x => x.ProjectPerKmId);
                    table.ForeignKey(
                        name: "FK_ProjectPerKm_Project_ProjectID",
                        column: x => x.ProjectID,
                        principalTable: "Project",
                        principalColumn: "ProjectID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Unit",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    PropertyTypeId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Unit", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Unit_PropertyType_PropertyTypeId",
                        column: x => x.PropertyTypeId,
                        principalTable: "PropertyType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EstimateCommittes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Address = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Position = table.Column<string>(nullable: true),
                    ProjectPerKmId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EstimateCommittes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EstimateCommittes_ProjectPerKm_ProjectPerKmId",
                        column: x => x.ProjectPerKmId,
                        principalTable: "ProjectPerKm",
                        principalColumn: "ProjectPerKmId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Ownership",
                columns: table => new
                {
                    OwnershipId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    OwnershipName = table.Column<string>(nullable: true),
                    ProjectPerKmId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ownership", x => x.OwnershipId);
                    table.ForeignKey(
                        name: "FK_Ownership_ProjectPerKm_ProjectPerKmId",
                        column: x => x.ProjectPerKmId,
                        principalTable: "ProjectPerKm",
                        principalColumn: "ProjectPerKmId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FileUploads",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FileName = table.Column<string>(maxLength: 255, nullable: false),
                    OwnershipId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FileUploads", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FileUploads_Ownership_OwnershipId",
                        column: x => x.OwnershipId,
                        principalTable: "Ownership",
                        principalColumn: "OwnershipId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Property",
                columns: table => new
                {
                    PropertyId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Category = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    EndDate = table.Column<DateTime>(nullable: false),
                    EraConsultant = table.Column<string>(nullable: true),
                    IsPermanent = table.Column<string>(nullable: true),
                    Latitude = table.Column<int>(nullable: false),
                    Longtiude = table.Column<int>(nullable: false),
                    Measurement = table.Column<string>(nullable: true),
                    OutsideConsultant = table.Column<string>(nullable: true),
                    OwnershipId = table.Column<int>(nullable: false),
                    PassionDate = table.Column<string>(nullable: true),
                    PaymentDate = table.Column<DateTime>(nullable: false),
                    PropertyType = table.Column<string>(nullable: true),
                    Quantity = table.Column<string>(nullable: true),
                    SinglePrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    TotalPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Property", x => x.PropertyId);
                    table.ForeignKey(
                        name: "FK_Property_Ownership_OwnershipId",
                        column: x => x.OwnershipId,
                        principalTable: "Ownership",
                        principalColumn: "OwnershipId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Contract_ProjectID",
                table: "Contract",
                column: "ProjectID");

            migrationBuilder.CreateIndex(
                name: "IX_ContactDetail_KeyStaffID",
                table: "ContactDetail",
                column: "KeyStaffID");

            migrationBuilder.CreateIndex(
                name: "IX_ContractProgressPerformanceScore_MeasurementID",
                table: "ContractProgressPerformanceScore",
                column: "MeasurementID");

            migrationBuilder.CreateIndex(
                name: "IX_EstimateCommittes_ProjectPerKmId",
                table: "EstimateCommittes",
                column: "ProjectPerKmId");

            migrationBuilder.CreateIndex(
                name: "IX_FileUploads_OwnershipId",
                table: "FileUploads",
                column: "OwnershipId");

            migrationBuilder.CreateIndex(
                name: "IX_Measurement_ContractId",
                table: "Measurement",
                column: "ContractId");

            migrationBuilder.CreateIndex(
                name: "IX_Ownership_ProjectPerKmId",
                table: "Ownership",
                column: "ProjectPerKmId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectPerKm_ProjectID",
                table: "ProjectPerKm",
                column: "ProjectID");

            migrationBuilder.CreateIndex(
                name: "IX_Property_OwnershipId",
                table: "Property",
                column: "OwnershipId");

            migrationBuilder.CreateIndex(
                name: "IX_Unit_PropertyTypeId",
                table: "Unit",
                column: "PropertyTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contract_Project_ProjectID",
                table: "Contract",
                column: "ProjectID",
                principalTable: "Project",
                principalColumn: "ProjectID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contract_Project_ProjectID",
                table: "Contract");

            migrationBuilder.DropTable(
                name: "ContactDetail");

            migrationBuilder.DropTable(
                name: "ContractProgressPerformanceScore");

            migrationBuilder.DropTable(
                name: "EstimateCommittes");

            migrationBuilder.DropTable(
                name: "FileUploads");

            migrationBuilder.DropTable(
                name: "Property");

            migrationBuilder.DropTable(
                name: "rnmdLists");

            migrationBuilder.DropTable(
                name: "roadClassesLists");

            migrationBuilder.DropTable(
                name: "RoadSegmentLists");

            migrationBuilder.DropTable(
                name: "sectionLists");

            migrationBuilder.DropTable(
                name: "Unit");

            migrationBuilder.DropTable(
                name: "KeyStaffRegister");

            migrationBuilder.DropTable(
                name: "Measurement");

            migrationBuilder.DropTable(
                name: "Ownership");

            migrationBuilder.DropTable(
                name: "PropertyType");

            migrationBuilder.DropTable(
                name: "ProjectPerKm");

            migrationBuilder.DropTable(
                name: "Project");

            migrationBuilder.DropIndex(
                name: "IX_Contract_ProjectID",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "ProjectID",
                table: "Contract");

            migrationBuilder.AlterColumn<decimal>(
                name: "SupplementAmount",
                table: "Supplements",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "ExchangeRate",
                table: "PhaseCurrencyBreakdowns",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Amount",
                table: "PhaseCurrencyBreakdowns",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "PhasePercentage",
                table: "Deliverables",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "MonetaryValue",
                table: "Deliverables",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<DateTime>(
                name: "LastModifiedOn",
                table: "CurrencyBreakdown",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "ExchangeRate",
                table: "CurrencyBreakdown",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedOn",
                table: "CurrencyBreakdown",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Reimbursable",
                table: "ContractPhases",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "ProvisionalSum",
                table: "ContractPhases",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "LumpSum",
                table: "ContractPhases",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "AdvancePercent",
                table: "ContractPhases",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "AdvanceAmount",
                table: "ContractPhases",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "OriginalContractAmount",
                table: "Contract",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "AdvanceAmount",
                table: "Contract",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "PercentComplete",
                table: "ConsultantsPrograms",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "MonetaryValue",
                table: "ConsultantsPrograms",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "PercentComplete",
                table: "ConsultantsPerformances",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "MonetaryValue",
                table: "ConsultantsPerformances",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");
        }
    }
}
