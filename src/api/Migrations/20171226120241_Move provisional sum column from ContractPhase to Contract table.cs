﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class MoveprovisionalsumcolumnfromContractPhasetoContracttable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ProvisionalSum",
                table: "ContractPhases",
                newName: "Reimbursable");

            migrationBuilder.AddColumn<decimal>(
                name: "ProvisionalSum",
                table: "Contracts",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ContractPhases",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "ContractPhases",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProvisionalSum",
                table: "Contracts");

            migrationBuilder.RenameColumn(
                name: "Reimbursable",
                table: "ContractPhases",
                newName: "ProvisionalSum");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ContractPhases",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "ContractPhases",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
