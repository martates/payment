﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class Reverse_Link_Contract__ContractType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contract_ContractType_ContractType11",
                table: "Contract");

            migrationBuilder.DropIndex(
                name: "IX_Contract_ContractType11",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "ContractType11",
                table: "Contract");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ContractType11",
                table: "Contract",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Contract_ContractType11",
                table: "Contract",
                column: "ContractType11");

            migrationBuilder.AddForeignKey(
                name: "FK_Contract_ContractType_ContractType11",
                table: "Contract",
                column: "ContractType11",
                principalTable: "ContractType",
                principalColumn: "ContractType1",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
