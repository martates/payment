﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class Update_Currency : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "CurrencyBreakdown");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "CurrencyBreakdown",
                newName: "CurrencyBreakDownID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CurrencyBreakDownID",
                table: "CurrencyBreakdown",
                newName: "Id");

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "CurrencyBreakdown",
                nullable: true);
        }
    }
}
