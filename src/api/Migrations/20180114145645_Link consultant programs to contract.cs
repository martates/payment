﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class Linkconsultantprogramstocontract : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ContractId",
                table: "ConsultantsPrograms",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ConsultantsPrograms_ContractId",
                table: "ConsultantsPrograms",
                column: "ContractId");

            migrationBuilder.AddForeignKey(
                name: "FK_ConsultantsPrograms_Contracts_ContractId",
                table: "ConsultantsPrograms",
                column: "ContractId",
                principalTable: "Contracts",
                principalColumn: "ContractId",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ConsultantsPrograms_Contracts_ContractId",
                table: "ConsultantsPrograms");

            migrationBuilder.DropIndex(
                name: "IX_ConsultantsPrograms_ContractId",
                table: "ConsultantsPrograms");

            migrationBuilder.DropColumn(
                name: "ContractId",
                table: "ConsultantsPrograms");
        }
    }
}
