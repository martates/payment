﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Asgard.Web.Migrations
{
    public partial class initialpaymentcreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Payments",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProjectID = table.Column<int>(nullable: false),
                    dateRecived = table.Column<DateTime>(nullable: false),
                    payeTo = table.Column<int>(nullable: false),
                    financedBy = table.Column<string>(nullable: true),
                    region = table.Column<string>(nullable: true),
                    ipcinv = table.Column<string>(nullable: true),
                    type = table.Column<string>(nullable: true),
                    dateToprocess = table.Column<DateTime>(nullable: false),
                    payDate = table.Column<DateTime>(nullable: false),
                    status = table.Column<string>(nullable: true),
                    counterAccount = table.Column<string>(nullable: true),
                    team = table.Column<string>(nullable: true),
                    teamLeader = table.Column<string>(nullable: true),
                    dateReturn = table.Column<DateTime>(nullable: false),
                    paidstatus = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payments", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Payments");
        }
    }
}
