﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class Createrelationshipbetweencontractanddeliverable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ContractID",
                table: "Deliverables",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Deliverables_ContractID",
                table: "Deliverables",
                column: "ContractID");

            migrationBuilder.AddForeignKey(
                name: "FK_Deliverables_Contracts_ContractID",
                table: "Deliverables",
                column: "ContractID",
                principalTable: "Contracts",
                principalColumn: "ContractID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deliverables_Contracts_ContractID",
                table: "Deliverables");

            migrationBuilder.DropIndex(
                name: "IX_Deliverables_ContractID",
                table: "Deliverables");

            migrationBuilder.DropColumn(
                name: "ContractID",
                table: "Deliverables");
        }
    }
}
