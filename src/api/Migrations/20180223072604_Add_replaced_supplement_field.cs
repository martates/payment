﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class Add_replaced_supplement_field : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContractPhases_ContractPhases_ContractPhaseId",
                table: "ContractPhases");

            migrationBuilder.AlterColumn<int>(
                name: "ContractPhaseId",
                table: "ContractPhases",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "ReplacedSupplementId",
                table: "ContractPhases",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ContractPhases_ReplacedSupplementId",
                table: "ContractPhases",
                column: "ReplacedSupplementId");

            migrationBuilder.AddForeignKey(
                name: "FK_ContractPhases_ContractPhases_ContractPhaseId",
                table: "ContractPhases",
                column: "ContractPhaseId",
                principalTable: "ContractPhases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ContractPhases_Supplements_ReplacedSupplementId",
                table: "ContractPhases",
                column: "ReplacedSupplementId",
                principalTable: "Supplements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContractPhases_ContractPhases_ContractPhaseId",
                table: "ContractPhases");

            migrationBuilder.DropForeignKey(
                name: "FK_ContractPhases_Supplements_ReplacedSupplementId",
                table: "ContractPhases");

            migrationBuilder.DropIndex(
                name: "IX_ContractPhases_ReplacedSupplementId",
                table: "ContractPhases");

            migrationBuilder.DropColumn(
                name: "ReplacedSupplementId",
                table: "ContractPhases");

            migrationBuilder.AlterColumn<int>(
                name: "ContractPhaseId",
                table: "ContractPhases",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ContractPhases_ContractPhases_ContractPhaseId",
                table: "ContractPhases",
                column: "ContractPhaseId",
                principalTable: "ContractPhases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
