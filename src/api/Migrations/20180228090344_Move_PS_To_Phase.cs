﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class Move_PS_To_Phase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProvisionalSum",
                table: "Contracts");

            migrationBuilder.AddColumn<decimal>(
                name: "ProvisionalSum",
                table: "ContractPhases",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProvisionalSum",
                table: "ContractPhases");

            migrationBuilder.AddColumn<decimal>(
                name: "ProvisionalSum",
                table: "Contracts",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
