﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class Addaudittrialcolumnstomodels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "Deliverables",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOn",
                table: "Deliverables",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "LastEditedBy",
                table: "Deliverables",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModifiedOn",
                table: "Deliverables",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "ContractPhases",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOn",
                table: "ContractPhases",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "LastEditedBy",
                table: "ContractPhases",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModifiedOn",
                table: "ContractPhases",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Deliverables");

            migrationBuilder.DropColumn(
                name: "CreatedOn",
                table: "Deliverables");

            migrationBuilder.DropColumn(
                name: "LastEditedBy",
                table: "Deliverables");

            migrationBuilder.DropColumn(
                name: "LastModifiedOn",
                table: "Deliverables");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "ContractPhases");

            migrationBuilder.DropColumn(
                name: "CreatedOn",
                table: "ContractPhases");

            migrationBuilder.DropColumn(
                name: "LastEditedBy",
                table: "ContractPhases");

            migrationBuilder.DropColumn(
                name: "LastModifiedOn",
                table: "ContractPhases");
        }
    }
}
