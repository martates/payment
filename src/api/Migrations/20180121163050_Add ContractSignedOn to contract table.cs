﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class AddContractSignedOntocontracttable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contracts_Contracts_SupplementsId",
                table: "Contracts");

            migrationBuilder.AlterColumn<int>(
                name: "SupplementsId",
                table: "Contracts",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<DateTime>(
                name: "ContractSignedOn",
                table: "Contracts",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Contracts_Contracts_SupplementsId",
                table: "Contracts",
                column: "SupplementsId",
                principalTable: "Contracts",
                principalColumn: "ContractId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contracts_Contracts_SupplementsId",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "ContractSignedOn",
                table: "Contracts");

            migrationBuilder.AlterColumn<int>(
                name: "SupplementsId",
                table: "Contracts",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Contracts_Contracts_SupplementsId",
                table: "Contracts",
                column: "SupplementsId",
                principalTable: "Contracts",
                principalColumn: "ContractId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
