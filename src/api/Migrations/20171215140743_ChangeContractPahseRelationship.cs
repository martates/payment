﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class ChangeContractPahseRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_PhaseInfos_ContractId",
                table: "PhaseInfos");

            migrationBuilder.CreateIndex(
                name: "IX_PhaseInfos_ContractId",
                table: "PhaseInfos",
                column: "ContractId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_PhaseInfos_ContractId",
                table: "PhaseInfos");

            migrationBuilder.CreateIndex(
                name: "IX_PhaseInfos_ContractId",
                table: "PhaseInfos",
                column: "ContractId");
        }
    }
}
