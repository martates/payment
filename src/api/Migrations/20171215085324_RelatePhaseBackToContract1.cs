﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class RelatePhaseBackToContract1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PhaseInfos_Contracts_ContractID",
                table: "PhaseInfos");

            migrationBuilder.RenameColumn(
                name: "ContractID",
                table: "PhaseInfos",
                newName: "ContractId");

            migrationBuilder.RenameIndex(
                name: "IX_PhaseInfos_ContractID",
                table: "PhaseInfos",
                newName: "IX_PhaseInfos_ContractId");

            migrationBuilder.AlterColumn<int>(
                name: "ContractId",
                table: "PhaseInfos",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PhaseInfos_Contracts_ContractId",
                table: "PhaseInfos",
                column: "ContractId",
                principalTable: "Contracts",
                principalColumn: "ContractID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PhaseInfos_Contracts_ContractId",
                table: "PhaseInfos");

            migrationBuilder.RenameColumn(
                name: "ContractId",
                table: "PhaseInfos",
                newName: "ContractID");

            migrationBuilder.RenameIndex(
                name: "IX_PhaseInfos_ContractId",
                table: "PhaseInfos",
                newName: "IX_PhaseInfos_ContractID");

            migrationBuilder.AlterColumn<int>(
                name: "ContractID",
                table: "PhaseInfos",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_PhaseInfos_Contracts_ContractID",
                table: "PhaseInfos",
                column: "ContractID",
                principalTable: "Contracts",
                principalColumn: "ContractID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
