﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class AddReasonFieldsToPerformanceTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SubmissionDate",
                table: "ConsultantsPerformances",
                newName: "Date");

            migrationBuilder.AddColumn<string>(
                name: "DetailedReason",
                table: "ConsultantsPerformances",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Reason",
                table: "ConsultantsPerformances",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DetailedReason",
                table: "ConsultantsPerformances");

            migrationBuilder.DropColumn(
                name: "Reason",
                table: "ConsultantsPerformances");

            migrationBuilder.RenameColumn(
                name: "Date",
                table: "ConsultantsPerformances",
                newName: "SubmissionDate");
        }
    }
}
