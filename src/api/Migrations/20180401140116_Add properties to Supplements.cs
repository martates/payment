﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class AddpropertiestoSupplements : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SupplementAmount",
                table: "Supplements",
                newName: "Reimbursable");

            migrationBuilder.AddColumn<decimal>(
                name: "LumpSum",
                table: "Supplements",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ProvisionalSum",
                table: "Supplements",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LumpSum",
                table: "Supplements");

            migrationBuilder.DropColumn(
                name: "ProvisionalSum",
                table: "Supplements");

            migrationBuilder.RenameColumn(
                name: "Reimbursable",
                table: "Supplements",
                newName: "SupplementAmount");
        }
    }
}
