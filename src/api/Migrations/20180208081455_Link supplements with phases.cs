﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class Linksupplementswithphases : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SupplementId",
                table: "ContractPhases",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ContractPhases_SupplementId",
                table: "ContractPhases",
                column: "SupplementId");

            migrationBuilder.AddForeignKey(
                name: "FK_ContractPhases_Supplements_SupplementId",
                table: "ContractPhases",
                column: "SupplementId",
                principalTable: "Supplements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContractPhases_Supplements_SupplementId",
                table: "ContractPhases");

            migrationBuilder.DropIndex(
                name: "IX_ContractPhases_SupplementId",
                table: "ContractPhases");

            migrationBuilder.DropColumn(
                name: "SupplementId",
                table: "ContractPhases");
        }
    }
}
