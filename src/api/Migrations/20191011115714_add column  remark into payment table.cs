﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Asgard.Web.Migrations
{
    public partial class addcolumnremarkintopaymenttable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "payeTo",
                table: "Payments",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "ProjectName",
                table: "Payments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "remark",
                table: "Payments",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "financedBys",
                columns: table => new
                {
                    financedById = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    financedByname = table.Column<string>(nullable: true),
                    noofDateExpected = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_financedBys", x => x.financedById);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Payments_ProjectID",
                table: "Payments",
                column: "ProjectID");

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_Project_ProjectID",
                table: "Payments",
                column: "ProjectID",
                principalTable: "Project",
                principalColumn: "ProjectID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payments_Project_ProjectID",
                table: "Payments");

            migrationBuilder.DropTable(
                name: "financedBys");

            migrationBuilder.DropIndex(
                name: "IX_Payments_ProjectID",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "ProjectName",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "remark",
                table: "Payments");

            migrationBuilder.AlterColumn<int>(
                name: "payeTo",
                table: "Payments",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
