﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class Add_FirmType_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contracts_Firms_FirmId",
                table: "Contracts");

            migrationBuilder.DropForeignKey(
                name: "FK_CurrencyBreakdowns_Contracts_ContractId",
                table: "CurrencyBreakdowns");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Firms",
                table: "Firms");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CurrencyBreakdowns",
                table: "CurrencyBreakdowns");

            migrationBuilder.RenameTable(
                name: "Firms",
                newName: "Firm");

            migrationBuilder.RenameTable(
                name: "CurrencyBreakdowns",
                newName: "CurrencyBreakdown");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Firm",
                newName: "FirmName");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Firm",
                newName: "FirmID");

            migrationBuilder.AddColumn<string>(
                name: "FirmType",
                table: "Firm",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Firm",
                table: "Firm",
                column: "FirmID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CurrencyBreakdown",
                table: "CurrencyBreakdown",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Contracts_Firm_FirmId",
                table: "Contracts",
                column: "FirmId",
                principalTable: "Firm",
                principalColumn: "FirmID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CurrencyBreakdown_Contracts_ContractId",
                table: "CurrencyBreakdown",
                column: "ContractId",
                principalTable: "Contracts",
                principalColumn: "ContractId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contracts_Firm_FirmId",
                table: "Contracts");

            migrationBuilder.DropForeignKey(
                name: "FK_CurrencyBreakdown_Contracts_ContractId",
                table: "CurrencyBreakdown");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Firm",
                table: "Firm");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CurrencyBreakdown",
                table: "CurrencyBreakdown");

            migrationBuilder.DropColumn(
                name: "FirmType",
                table: "Firm");

            migrationBuilder.RenameTable(
                name: "Firm",
                newName: "Firms");

            migrationBuilder.RenameTable(
                name: "CurrencyBreakdown",
                newName: "CurrencyBreakdowns");

            migrationBuilder.RenameColumn(
                name: "FirmName",
                table: "Firms",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "FirmID",
                table: "Firms",
                newName: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Firms",
                table: "Firms",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CurrencyBreakdowns",
                table: "CurrencyBreakdowns",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Contracts_Firms_FirmId",
                table: "Contracts",
                column: "FirmId",
                principalTable: "Firms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CurrencyBreakdowns_Contracts_ContractId",
                table: "CurrencyBreakdowns",
                column: "ContractId",
                principalTable: "Contracts",
                principalColumn: "ContractId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
