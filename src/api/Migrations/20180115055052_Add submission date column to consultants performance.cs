﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class Addsubmissiondatecolumntoconsultantsperformance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Date",
                table: "ConsultantsPerformances",
                newName: "SubmissionDate");

            migrationBuilder.AddColumn<DateTime>(
                name: "AssessmentDate",
                table: "ConsultantsPerformances",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AssessmentDate",
                table: "ConsultantsPerformances");

            migrationBuilder.RenameColumn(
                name: "SubmissionDate",
                table: "ConsultantsPerformances",
                newName: "Date");
        }
    }
}
