﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class Formrealtionfromdeliverablestocotractphase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deliverables_ContractPhases_PhaseId",
                table: "Deliverables");

            migrationBuilder.DropIndex(
                name: "IX_Deliverables_PhaseId",
                table: "Deliverables");

            migrationBuilder.DropColumn(
                name: "PhaseId",
                table: "Deliverables");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "ContractPhases",
                newName: "ContractPhaseId");

            migrationBuilder.AddColumn<int>(
                name: "ContractPhaseId",
                table: "Deliverables",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Deliverables_ContractPhaseId",
                table: "Deliverables",
                column: "ContractPhaseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Deliverables_ContractPhases_ContractPhaseId",
                table: "Deliverables",
                column: "ContractPhaseId",
                principalTable: "ContractPhases",
                principalColumn: "ContractPhaseId",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deliverables_ContractPhases_ContractPhaseId",
                table: "Deliverables");

            migrationBuilder.DropIndex(
                name: "IX_Deliverables_ContractPhaseId",
                table: "Deliverables");

            migrationBuilder.DropColumn(
                name: "ContractPhaseId",
                table: "Deliverables");

            migrationBuilder.RenameColumn(
                name: "ContractPhaseId",
                table: "ContractPhases",
                newName: "Id");

            migrationBuilder.AddColumn<int>(
                name: "PhaseId",
                table: "Deliverables",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Deliverables_PhaseId",
                table: "Deliverables",
                column: "PhaseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Deliverables_ContractPhases_PhaseId",
                table: "Deliverables",
                column: "PhaseId",
                principalTable: "ContractPhases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
