﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class alter_table_phaseinfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PhaseIEndDate",
                table: "PhaseInfos");

            migrationBuilder.DropColumn(
                name: "PhaseIIEndDate",
                table: "PhaseInfos");

            migrationBuilder.RenameColumn(
                name: "PhaseITotalAmount",
                table: "PhaseInfos",
                newName: "phaseVat");

            migrationBuilder.RenameColumn(
                name: "PhaseIStartDate",
                table: "PhaseInfos",
                newName: "PhaseStartDate");

            migrationBuilder.RenameColumn(
                name: "PhaseIITotalAmount",
                table: "PhaseInfos",
                newName: "phaseTotal");

            migrationBuilder.RenameColumn(
                name: "PhaseIIStartDate",
                table: "PhaseInfos",
                newName: "PhaseEndDate");

            migrationBuilder.AddColumn<decimal>(
                name: "phaseLumpSum",
                table: "PhaseInfos",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "phaseProvisionSum",
                table: "PhaseInfos",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "phaseLumpSum",
                table: "PhaseInfos");

            migrationBuilder.DropColumn(
                name: "phaseProvisionSum",
                table: "PhaseInfos");

            migrationBuilder.RenameColumn(
                name: "phaseVat",
                table: "PhaseInfos",
                newName: "PhaseITotalAmount");

            migrationBuilder.RenameColumn(
                name: "phaseTotal",
                table: "PhaseInfos",
                newName: "PhaseIITotalAmount");

            migrationBuilder.RenameColumn(
                name: "PhaseStartDate",
                table: "PhaseInfos",
                newName: "PhaseIStartDate");

            migrationBuilder.RenameColumn(
                name: "PhaseEndDate",
                table: "PhaseInfos",
                newName: "PhaseIIStartDate");

            migrationBuilder.AddColumn<DateTime>(
                name: "PhaseIEndDate",
                table: "PhaseInfos",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "PhaseIIEndDate",
                table: "PhaseInfos",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
