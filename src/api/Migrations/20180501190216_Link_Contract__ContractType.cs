﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class Link_Contract__ContractType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ContractType11",
                table: "Contract",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ContractType",
                columns: table => new
                {
                    ContractType1 = table.Column<string>(nullable: false),
                    Category = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractType", x => x.ContractType1);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Contract_ContractType11",
                table: "Contract",
                column: "ContractType11");

            migrationBuilder.AddForeignKey(
                name: "FK_Contract_ContractType_ContractType11",
                table: "Contract",
                column: "ContractType11",
                principalTable: "ContractType",
                principalColumn: "ContractType1",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contract_ContractType_ContractType11",
                table: "Contract");

            migrationBuilder.DropTable(
                name: "ContractType");

            migrationBuilder.DropIndex(
                name: "IX_Contract_ContractType11",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "ContractType11",
                table: "Contract");
        }
    }
}
