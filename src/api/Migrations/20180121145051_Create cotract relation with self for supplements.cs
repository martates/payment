﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class Createcotractrelationwithselfforsupplements : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SupplementsId",
                table: "Contracts",
                nullable: true,
                defaultValue: null);

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_SupplementsId",
                table: "Contracts",
                column: "SupplementsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contracts_Contracts_SupplementsId",
                table: "Contracts",
                column: "SupplementsId",
                principalTable: "Contracts",
                principalColumn: "ContractId",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contracts_Contracts_SupplementsId",
                table: "Contracts");

            migrationBuilder.DropIndex(
                name: "IX_Contracts_SupplementsId",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "SupplementsId",
                table: "Contracts");
        }
    }
}
