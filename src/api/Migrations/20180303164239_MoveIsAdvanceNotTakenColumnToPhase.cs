﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class MoveIsAdvanceNotTakenColumnToPhase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsAdvanceNotTaken",
                table: "Deliverables");

            migrationBuilder.AddColumn<bool>(
                name: "IsAdvanceNotTaken",
                table: "ContractPhases",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsAdvanceNotTaken",
                table: "ContractPhases");

            migrationBuilder.AddColumn<bool>(
                name: "IsAdvanceNotTaken",
                table: "Deliverables",
                nullable: false,
                defaultValue: false);
        }
    }
}
