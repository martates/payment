﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class Reversecontractfirmrelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Firms_Contracts_ContractID",
                table: "Firms");

            migrationBuilder.DropIndex(
                name: "IX_Firms_ContractID",
                table: "Firms");

            migrationBuilder.DropColumn(
                name: "ContractID",
                table: "Firms");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Firms",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FirmId",
                table: "Contracts",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_FirmId",
                table: "Contracts",
                column: "FirmId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contracts_Firms_FirmId",
                table: "Contracts",
                column: "FirmId",
                principalTable: "Firms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contracts_Firms_FirmId",
                table: "Contracts");

            migrationBuilder.DropIndex(
                name: "IX_Contracts_FirmId",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "FirmId",
                table: "Contracts");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Firms",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<int>(
                name: "ContractID",
                table: "Firms",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Firms_ContractID",
                table: "Firms",
                column: "ContractID");

            migrationBuilder.AddForeignKey(
                name: "FK_Firms_Contracts_ContractID",
                table: "Firms",
                column: "ContractID",
                principalTable: "Contracts",
                principalColumn: "ContractID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
