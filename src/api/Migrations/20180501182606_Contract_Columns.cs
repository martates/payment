﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class Contract_Columns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "DeductRetentionBeforeVatCalc",
                table: "Contract",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsCommencementDateModified",
                table: "Contract",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsContractInactive",
                table: "Contract",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeductRetentionBeforeVatCalc",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "IsCommencementDateModified",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "IsContractInactive",
                table: "Contract");
        }
    }
}
