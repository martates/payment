﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class RenameprimarykeyfieldstoId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DeliverableId",
                table: "Deliverables",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "DeliverableLookupId",
                table: "DeliverableLookups",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "ContractPhaseId",
                table: "ContractPhases",
                newName: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Deliverables",
                newName: "DeliverableId");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "DeliverableLookups",
                newName: "DeliverableLookupId");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "ContractPhases",
                newName: "ContractPhaseId");
        }
    }
}
