﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Asgard.Web.Migrations
{
    public partial class hook_pahse_to_supplement : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContractPhases_Supplements_SupplementId",
                table: "ContractPhases");

            migrationBuilder.AlterColumn<int>(
                name: "SupplementId",
                table: "ContractPhases",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ContractPhases_Supplements_SupplementId",
                table: "ContractPhases",
                column: "SupplementId",
                principalTable: "Supplements",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContractPhases_Supplements_SupplementId",
                table: "ContractPhases");

            migrationBuilder.AlterColumn<int>(
                name: "SupplementId",
                table: "ContractPhases",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_ContractPhases_Supplements_SupplementId",
                table: "ContractPhases",
                column: "SupplementId",
                principalTable: "Supplements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
