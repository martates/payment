using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core.Models;
using Asgard.Web.Core.Models.Users;
using AutoMapper;

namespace Asgard.Web {
    public class MappingProfile : AutoMapper.Profile {
        public MappingProfile () {
            // DeliverableLookup DeliverableLookupResource
            CreateMap<DeliverableLookup, DeliverableLookupResource> ();

            // Deliverable & SaveDeliverableResource
            CreateMap<SaveDeliverableResource, Deliverable> ();
            CreateMap<Deliverable, SaveDeliverableResource> ();

            // Deliverable & DeliverableResource
            CreateMap<Deliverable, DeliverableResource> ();

            // PhaseInfo & PhaseInfoResource
            CreateMap<ContractPhaseResource, ContractPhase> ()
                .ForMember (c => c.Deliverables, opt => opt.Ignore ());

            CreateMap<ContractPhase, ContractPhaseResource> ()
                .ForMember (c => c.ContractAdvancePercent, opt => opt.Ignore ())
                .ForMember (c => c.ContractAdvanceAmount, opt => opt.Ignore ());

            // ConsultantsProgram & SaveConsultantsProgramResource
            CreateMap<SaveConsultantsProgramResource, ConsultantsProgram> ()
                .ForMember (cr => cr.ContractId, opt => opt.Ignore ());
            CreateMap<ConsultantsProgram, SaveConsultantsProgramResource> ();

            // ConsultantsPerformance & SaveConsultantsPerformanceResource
            CreateMap<SaveConsultantsPerformanceResource, ConsultantsPerformance> ()
                .ForMember (cr => cr.ContractId, opt => opt.Ignore ());
            CreateMap<ConsultantsPerformance, SaveConsultantsPerformanceResource> ();

            // ConsultantsProgram & ConsultantsProgramResource
            CreateMap<ConsultantsProgram, ConsultantsProgramResource> ();

            // ConsultantsPerformance & ConsultantsPerformanceResource
            CreateMap<ConsultantsPerformance, ConsultantsPerformanceResource> ();

            // Contract & ContractResource
            CreateMap<Contract, ContractResource> ()
                .ForMember (cr => cr.Id, opt => opt.MapFrom (c => c.ContractId))
                .ForMember (cr => cr.Number, opt => opt.MapFrom (c => c.ContractNumber))
                .ForMember (cr => cr.Name, opt => opt.MapFrom (c => c.ContractName))
                .ForMember (cr => cr.ConsultantName, opt => opt.MapFrom (c => c.Firm.Name))
                .ForMember (cr => cr.ContractAmount, opt => opt.MapFrom (c => c.OriginalContractAmount))
                .ForMember (cr => cr.Phases, opt => opt.MapFrom (c => c.ContractPhases));

            CreateMap<Supplement, SupplementResource> ();
            CreateMap<SupplementResource, Supplement> ();

            CreateMap<CurrencyBreakdown, CurrencyBreakdownResource> ();

            CreateMap<PhaseCurrencyBreakdown, PhaseCurrencyBreakdownResource> ();
            CreateMap<PhaseCurrencyBreakdownResource, PhaseCurrencyBreakdown> ();

            CreateMap<User, UserResource> ();

            CreateMap<UserInRole, UserInRoleResource> ()
                .ForMember (ur => ur.RoleName, opt => opt.MapFrom (u => u.Role.RoleName))
                .ForMember (ur => ur.LoweredRoleName, opt => opt.MapFrom (u => u.Role.LoweredRoleName))
                .ForMember (ur => ur.Description, opt => opt.MapFrom (u => u.Role.Description));
            //right of way project per km
            CreateMap<ProjectPerKm, ProjectPerKmResource> ();
            CreateMap<ProjectPerKmResource, ProjectPerKm> ();

            // right of way ownership resouce
            CreateMap<OwnerShipResource, Ownership> ();
            CreateMap<Ownership, OwnerShipResource> ();

            //Right of way Information & Resource
            CreateMap<PropertyResource, Property> ();
            CreateMap<Property, PropertyResource> ();

            CreateMap<PropertyType, PropertyTypeResource> ();
            CreateMap<Unit, UnitResource> ();

            CreateMap<EstimateCommitte, EstimateCommitteResource> ();
            CreateMap<EstimateCommitteResource, EstimateCommitte> ();

            CreateMap<FileUpload, FileUploadResource> ();
            CreateMap<FileUploadResource, FileUpload> ();

            CreateMap<KeyStaffRegister, KeyStaffRegisterResource> ();

            CreateMap<ContactDetail, KeyStaffRegisterResource> ();

            // Road Character Mapping
            CreateMap<RoadSegmentList, RoadSegmentListResource> ();
            CreateMap<RoadSegmentListResource, RoadSegmentList> ();

            CreateMap<rnmdList, rnmdsListResource> ();
            CreateMap<rnmdsListResource, rnmdList> ();

            CreateMap<roadClassesList, roadClassListResource> ();
            CreateMap<roadClassListResource, roadClassesList> ();

            CreateMap<sectionList, sectionListResource> ();
            CreateMap<sectionListResource, sectionList> ();

            CreateMap<Payment, PaymentResource> ();
            CreateMap<PaymentResource, Payment> ();

            CreateMap<FinancedBy, FinancedByResource> ();
            CreateMap<FinancedByResource, FinancedBy> ();

            CreateMap<Remark, RemarkResource> ();
            CreateMap<RemarkResource, Remark> ();
        }
    }
}