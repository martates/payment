using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Helpers;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core {
    public interface IDeliverableRepository {
        IEnumerable<Deliverable> GetDeliverables(int contractId);
        void Add(Deliverable deliverable, int contractId);
        Task<Deliverable> GetDeliverable(int deliverableId, bool includeRelated);
        Task<TaskResult> DeleteDeliverable(int id);
        void CopyDeliverables(ContractPhase contractPhase);
        void UpdateDeliverableMonetaryValues(ContractPhase contractPhase);
    }
}