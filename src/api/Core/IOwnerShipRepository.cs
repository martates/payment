using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core
{
    public interface IOwnerShipRepository
    {
        void Add(Ownership ownership);
        void Insert(int ProjectPerKmId, Ownership Ownership);
        // Affiliate GetAffiliate(int affiliateId, bool includeRelated);
        Ownership GetOwnership(int ownershipId, bool includeRelated);
        IEnumerable<Ownership> GetOwnerList();
        IEnumerable<Ownership> GetOwnerships(int ownershipId);
        IEnumerable<KeyValuePair<int, string>> GetOwnershipNames();
        IEnumerable<KeyValuePair<int, string>> GetOwnershipswithPerKm(int ProjectPerKmId);
        Task Deleteownership(int ownershipId);
        Ownership UpdateOwnership(int ownershipId);

    }

}
