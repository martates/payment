using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Asgard.Web.Persistance
{
    public interface IFileUploadStorage
    {
        Task<string> StorePhoto(string uploadsFolderPath, IFormFile file);
    }
}