using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core {
    public interface IpaymentReposiotry {
        void Add (Payment payment);
        IEnumerable<Payment> GetPaymentList ();
         IEnumerable<Payment> GetPaymentListPaid ();
        IEnumerable<Payment> GetPayment (int projectId);
        Payment UpdatePayment (int id);
        Task DeletePayment (int id);
    }
}