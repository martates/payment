using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core
{
    public interface IfinancedByReposiotry
    {
        void Add(FinancedBy financedBy);
        IEnumerable<FinancedBy> GetfinancedByList();
        IEnumerable<FinancedBy> GetfinancedBy();
        FinancedBy UpdatefinancedBy(int id);
        Task DeletefinancedBy(int id);
        IEnumerable<KeyValuePair<int, string>> GetFinancedByNames();

    }
}