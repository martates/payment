using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core {
    public interface IremarkReposiotry {
        void Add (Remark remark);
        IEnumerable<Remark> GetRemarkList ();
        Remark UpdateRemark (int id);
        Task DeleteRemark (int id);
    }
}