using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core {
    public interface IRequesionItemRepository {
       void Add (Requestion_Item requestion_Item);
        IEnumerable<Requestion_Item> GetRequestionItem ();
        Requestion_Item UpdateRequestionItem (int projectPerKmId);
        // IEnumerable<KeyValuePair<int, string>> GetProjectPerKmsdrop (int projectId);
        IEnumerable<Requestion_Item> GetRequestionItems (int projectId);
        Task DeleteRequestionItem (int projectPerKmId);    
    }
}