using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core {
    public interface IPurchaseOrderRepository {
         void Add (Purchase_Order purchase_Order);
        IEnumerable<Purchase_Order> GetPurchaseOrder ();
        Purchase_Order UpdatePurchaseOrder (int projectPerKmId);
        // IEnumerable<KeyValuePair<int, string>> GetProjectPerKmsdrop (int projectId);
        IEnumerable<Purchase_Order> GetPurchaseOrders (int projectId);
        Task DeletePurchaseOrder (int projectPerKmId); 
    }
}