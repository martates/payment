using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Core.Models
{
    public class FinancedBy
    {
         [Key]
        public int financedById { get; set; }
        public string financedByname { get; set; }
        public int noofDateExpected { get; set; }
    }
}