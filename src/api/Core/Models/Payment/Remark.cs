using System.ComponentModel.DataAnnotations;
namespace Asgard.Web.Core.Models {
    public class Remark {
        [Key]
        public int remarkId { get; set; }
        public string remarkName { get; set; }
    }
}