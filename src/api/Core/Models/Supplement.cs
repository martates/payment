using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Asgard.Web.Core.Models
{
    public class Supplement
    {
        public int Id { get; set; }
        public int ContractId { get; set; }
        public Contract Contract { get; set; }
        public int Number { get; set; }
        public string Name { get; set; }
        public DateTime AgreementDate { get; set; }
        public string Purpose { get; set; }
         [Column(TypeName = "decimal(18,2)")]
        public decimal SupplementAmount { get; set; }
        public IEnumerable<ContractPhase> SupplementPhases { get; set; }
        public IEnumerable<ContractPhase> ReplacedSupplementPhases { get; set; }
        public string CreatedBy { get; set; }
        public string LastEditedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastModifiedOn { get; set; }
    }
}