using System.Collections.Generic;

namespace Asgard.Web.Core.Models
{
    public class PropertyType {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Unit> Units { get; set; }
    }
}