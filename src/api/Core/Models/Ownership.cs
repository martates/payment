using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Core.Models
{
    public class Ownership
    {
        [Key]
        public int OwnershipId { get; set; }
        public string OwnershipName { get; set; }
        public int ProjectPerKmId { get; set; }
        public IEnumerable<Property> Property { get; set; }
        public Collection<FileUpload> FileUploads { get; set; }
        public ProjectPerKm projectPerKm { get; set; }
        public Ownership()
        {
            FileUploads = new Collection<FileUpload>();
        }
    }
}