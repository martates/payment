namespace Asgard.Web.Core.Models
{
    public class Unit
    {
        public int  Id { get; set; }
        public string Name { get; set; } 
        public PropertyType PropertyType { get; set; }
             
    }
}