using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Asgard.Web.Core.Models
{
    public class ConsultantsPerformance
    {
        public int Id { get; set; }
        public DateTime AssessmentDate { get; set; }
        public DateTime? SubmissionDate { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal PercentComplete { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal MonetaryValue { get; set; }
        public string Reason { get; set; }
        public string DetailedReason { get; set; }
        public Deliverable Deliverable { get; set; }
        public int DeliverableId { get; set; }
        public Contract Contract { get; set; }
        public int ContractId { get; set; }
        public string CreatedBy { get; set; }
        public string LastEditedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastModifiedOn { get; set; }
    }
}