using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Asgard.Web.Core.Models
{
    public class Deliverable
    {
        public int Id { get; set; }
        public Contract Contract { get; set; }
        public DeliverableLookup DeliverableLookup { get; set; }

        [Required]
        public int DeliverableLookupId { get; set; }
        public int ContractId { get; set; }
        public ContractPhase ContractPhase { get; set; }
        public int ContractPhaseId { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal PhasePercentage { get; set; }

        [Required]
        [Column(TypeName = "decimal(18,2)")]
        public decimal MonetaryValue { get; set; }
        public ICollection<ConsultantsProgram> ConsultantsPrograms { get; set; }
        public ICollection<ConsultantsPerformance> ConsultantsPerformances { get; set; }
        public string CreatedBy { get; set; }
        public string LastEditedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastModifiedOn { get; set; }

        public Deliverable Clone()
        {
            return (Deliverable)MemberwiseClone();
        }
    }
}