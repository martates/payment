using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Core.Models
{
    public class roadClassesList
    {
        [Key]
        public int Id { get; set; }
        public string roadClass { get; set; }
    }
}
