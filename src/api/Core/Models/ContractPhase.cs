using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Asgard.Web.Core.Models
{
    public class ContractPhase
    {
        public int Id { get; set; }
        public Contract Contract { get; set; }
        public int ContractId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        [Required]
        [Column(TypeName = "decimal(18,2)")]
        public decimal LumpSum { get; set; }

        [Required]
        [Column(TypeName = "decimal(18,2)")]
        public decimal Reimbursable { get; set; }

        [Required]
        [Column(TypeName = "decimal(18,2)")]
        public decimal ProvisionalSum { get; set; }

        [Required]
        [Column(TypeName = "decimal(18,2)")]
        public decimal AdvancePercent { get; set; }

        [Required]
        [Column(TypeName = "decimal(18,2)")]
        public decimal AdvanceAmount { get; set; }
        public Supplement Supplement { get; set; }

        public int? SupplementId { get; set; }
        // reference to the replaced phase's supplemental agreement, if the current phase is a new supplement
        // to an existing supplement
        public Supplement ReplacedSupplement { get; set; }
        public int? ReplacedSupplementId { get; set; }
        // reference to the replaced phase, if the current phase is a supplement
        // to an existing phase
        public ContractPhase PreviousContractPhase { get; set; }
        public int? ContractPhaseId { get; set; }
        public Boolean IsAdvanceNotTaken { get; set; }
        public ICollection<Deliverable> Deliverables { get; set; }
        public string CreatedBy { get; set; }
        public string LastEditedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastModifiedOn { get; set; }
    }
}