using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Asgard.Web.Core.Models {
    public class Requestion_Item {
        [Key]
        public int RequestItemID { get; set; }
        public int Requestion_No { get; set; }
        public DateTime Requestion_Date { get; set; }
        public string Supplier_To { get; set; }
        public DateTime Supplier_Date { get; set; }
    }
}