using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Asgard.Web.Core.Models
{
    public class Project
    {
        [Key]
        public int ProjectID { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
            [Column(TypeName = "decimal(18,2)")]
        public decimal? RoadLength { get; set; }
        public IEnumerable<ProjectPerKm> ProjectPerKm { get; set; }
         public IEnumerable<Payment> payment { get; set; }
        public IEnumerable<Contract> Contract { get; set; }

    }
}