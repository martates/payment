using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Asgard.Web.Core.Models {
    public class Property {
        [Key]
        public int PropertyId { get; set; }
        public int OwnershipId { get; set; }
        public string Category { get; set; }
        public string Type { get; set; }
        public string PropertyType { get; set; }
        public string Description { get; set; }
        public string Measurement { get; set; }
        public string Quantity { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal SinglePrice { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal TotalPrice { get; set; }
        public int Longtiude { get; set; }
        public int Latitude { get; set; }
        public string OutsideConsultant { get; set; }
        public string EraConsultant { get; set; }
        public DateTime PaymentDate { get; set; }
        public string IsPermanent { get; set; }
        public string PassionDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Ownership Ownership { get; set; }


    }
}