using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Asgard.Web.Core.Models
{
    public class Contract
    {
        public int ContractId { get; set; }
        public string ContractNumber { get; set; }
        public string ContractName { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? OriginalContractAmount { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal AdvanceAmount { get; set; }
        public DateTime? ContractSignedOn { get; set; }
        public IEnumerable<ContractPhase> ContractPhases { get; set; }
        public IEnumerable<Deliverable> Deliverables { get; set; }
        public IEnumerable<Supplement> Supplements { get; set; }
        public IEnumerable<CurrencyBreakdown> CurrencyBreakdowns { get; set; }
        public Firm Firm { get; set; }
        public int FirmId { get; set; }
        public bool DeductRetentionBeforeVatCalc { get; set; }
        public bool IsContractInactive { get; set; }
        public bool IsCommencementDateModified { get; set; }
        public DateTime? ContractCommencement { get; set; }
        public int? ContractPeriod { get; set; }
        public bool? IsAwarded { get; set; }
        public string ContractType { get; set; }
    }
}