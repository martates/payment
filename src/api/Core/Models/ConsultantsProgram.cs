using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Asgard.Web.Core.Models
{
    public class ConsultantsProgram
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal PercentComplete { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal MonetaryValue { get; set; }
        public Deliverable Deliverable { get; set; }
        public int DeliverableId { get; set; }
        public Contract Contract { get; set; }
        public int ContractId { get; set; }
        public string CreatedBy { get; set; }
        public string LastEditedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastModifiedOn { get; set; }
    }
}