using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Core.Models
{
    public class Firm
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public string FirmType { get; set; }
        public ICollection<Contract> Contracts { get; set; }
    }
}