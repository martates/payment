using System;
using System.ComponentModel.DataAnnotations.Schema;

// by LS, PS and reimbursable
namespace Asgard.Web.Core.Models
{
    public class PhaseCurrencyBreakdown
    {
        public int Id { get; set; }
        public ContractPhase ContractPhase { get; set; }
        // LS, PS, Reimbursable
        [Column(TypeName = "decimal(18,2)")]
        public decimal Amount { get; set; }
        public string Type { get; set; }
        public int ContractPhaseId { get; set; }
        public string Currency { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal ExchangeRate { get; set; }
        public string CreatedBy { get; set; }
        public string LastEditedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastModifiedOn { get; set; }
    }
}