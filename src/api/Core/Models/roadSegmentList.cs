using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Core.Models
{
    public class RoadSegmentList
    {
        [Key]
        public int rnmdId { get; set; }
        public string rnmd { get; set; }
        public string section { get; set; }
        public string roadClass { get; set; }
        public string roadNo { get; set; }
        public string segmentNo { get; set; }
        public string segmentName { get; set; }
        public int asphaltlength { get; set; }
        public int gravellength { get; set; }
        public int totallength { get; set; }
        public string region { get; set; }
        public string ds_Standard { get; set; }
        public string roadstatus { get; set; }
        public DateTime registraion_year { get; set; }
        public string RoadAdmin { get; set; }
        public string gtpPlan { get; set; }
    }
}
