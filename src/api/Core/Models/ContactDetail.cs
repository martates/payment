using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core.Models
{
    public class ContactDetail
    {
		[Key]
    public int ContactDetailID{get; set;}
	public int ContractId { get; set; }
	public int StaffID {get; set;}
	public string Position {get; set;}
	public KeyStaffRegister KeyStaffRegister {get; set;}
	public DateTime? AssignmentDate{get; set;}
	public DateTime? LeavingDate {get; set;}
	public string Landline {get; set;}
	public string Mobile {get; set;}
	public string Email {get; set;}
	public string ContactType {get; set;}
	public string LastEditedBy {get; set;}
	public DateTime? CreatedOn {get; set;} 
	public DateTime? LastModifiedOn {get; set;} 
	
    }
}