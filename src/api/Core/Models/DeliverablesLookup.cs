using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Core.Models {
    public class DeliverableLookup {
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
    }

}
