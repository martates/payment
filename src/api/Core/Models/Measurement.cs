using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core.Models
{
    public class Measurement
    {
        public int? ContractId { get; set; }
        [Key]
        public int MeasurementID { get; set; }
        public Contract contracts { get; set; }
        public DateTime? Date { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? MeasurementAndVOs { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? CertifiedClaims { get; set; } //Currency
        [Column(TypeName = "decimal(18,2)")]
        public decimal? Escalation { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? TotalRetention { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? Taxes { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? InterestOnLatePayments { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? OtherPayments { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? AdvanceRepayment { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? CorrectionOnCertificate { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? VatThisInvoice { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? ExpenditureVsBudget { get; set; }
        public string CriticalActivities { get; set; }
        public string LastEditedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public IEnumerable<ContractProgressPerformanceScore> ContractProgressPerformanceScore { get; set; }
    }
}