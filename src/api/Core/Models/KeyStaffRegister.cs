using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core.Models
{
    public class KeyStaffRegister
    {
		[Key]
    public int StaffID {get; set;}
	public string FullName {get; set;}
	public string IdType {get; set;}
	public string IdNumber {get; set;}
	public bool? IsLocalStaff {get; set;}// [bit] 
	public string Landline {get; set;}
	public string Mobile {get; set;}
	public string Email {get; set;}
	public string LastEditedBy {get; set;}
	public DateTime? CreatedOn {get; set;}
	public DateTime? LastModifiedOn {get; set;}
    public IEnumerable<ContactDetail> ContactDetails {get; set;}
    }
}