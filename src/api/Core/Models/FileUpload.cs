using System.ComponentModel.DataAnnotations;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core.Models
{
    public class FileUpload
    {
        public int Id {get; set;}
         public int OwnershipId{get; set;}
        [Required]
        [StringLength(255)]
        public string FileName {get; set;}
       
         public Ownership ownership { get; set; }
    }
}