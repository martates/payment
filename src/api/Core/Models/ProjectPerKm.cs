using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Asgard.Web.Core.Models
{
    public class ProjectPerKm
    {
        [Key]
        public int ProjectPerKmId { get; set; }
        public int ProjectID { get; set; }
         [Column(TypeName = "decimal(18,2)")]
        public Decimal KMStart { get; set; }
         [Column(TypeName = "decimal(18,2)")]
        public Decimal KmEnd { get; set; }
        public string State { get; set; }
        public string Zone { get; set; }
        public string Woreda { get; set; }
        public Project project { get; set; }

        public IEnumerable<Ownership> ownership { get; set; }
        public IEnumerable<EstimateCommitte> estimateCommitte { get; set; }
    }
}