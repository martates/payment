﻿using System;
using System.Collections.Generic;

namespace Asgard.Web.Core.Models.Users {
    public class Role {
        public Role () {
            UsersInRoles = new HashSet<UserInRole> ();
            MandatoryRolesMatrix = new HashSet<MandatoryRolesMatrix> ();
        }

        public Guid ApplicationId { get; set; }
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public string LoweredRoleName { get; set; }
        public string Description { get; set; }

        public Application Application { get; set; }
        public ICollection<UserInRole> UsersInRoles { get; set; }
        public ICollection<MandatoryRolesMatrix> MandatoryRolesMatrix { get; set; }
    }
}