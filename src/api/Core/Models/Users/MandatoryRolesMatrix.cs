﻿using System;
using System.Collections.Generic;

namespace Asgard.Web.Core.Models.Users
{
    public partial class MandatoryRolesMatrix
    {
        public int MandatoryRoleId { get; set; }
        public string Directorate { get; set; }
        public string Position { get; set; }
        public Guid? MandatoryRole { get; set; }

        public Role MandatoryRoleNavigation { get; set; }
    }
}
