﻿using System;
using System.Collections.Generic;

namespace Asgard.Web.Core.Models.Users
{
    public partial class Profile
    {
        public Guid UserId { get; set; }
        public string PropertyNames { get; set; }
        public string PropertyValuesString { get; set; }
        public byte[] PropertyValuesBinary { get; set; }
        public DateTime LastUpdatedDate { get; set; }

        public User User { get; set; }
    }
}
