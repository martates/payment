﻿using System;
using System.Collections.Generic;

namespace Asgard.Web.Core.Models.Users
{
    public partial class User
    {
        public User()
        {
            Roles = new HashSet<UserInRole>();
        }

        public Guid ApplicationId { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string Directorate { get; set; }
        public string BadgeNumber { get; set; }
        public string Telephone { get; set; }
        public string LoweredUserName { get; set; }
        public string MobileAlias { get; set; }
        public bool IsAnonymous { get; set; }
        public DateTime LastActivityDate { get; set; }
        public string LastEditedBy { get; set; }

        public Application Application { get; set; }
        public Membership Membership { get; set; }
        public Profile AspnetProfile { get; set; }
        public ICollection<UserInRole> Roles { get; set; }
    }
}
