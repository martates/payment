﻿using System;
using System.Collections.Generic;

namespace Asgard.Web.Core.Models.Users
{
    public partial class Application
    {
        public Application()
        {
            AspnetMembership = new HashSet<Membership>();
            AspnetRoles = new HashSet<Role>();
            AspnetUsers = new HashSet<User>();
        }

        public string ApplicationName { get; set; }
        public string LoweredApplicationName { get; set; }
        public Guid ApplicationId { get; set; }
        public string Description { get; set; }

        public ICollection<Membership> AspnetMembership { get; set; }
        public ICollection<Role> AspnetRoles { get; set; }
        public ICollection<User> AspnetUsers { get; set; }
    }
}
