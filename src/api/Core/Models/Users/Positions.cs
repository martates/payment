﻿using System;
using System.Collections.Generic;

namespace Asgard.Web.Core.Models.Users
{
    public partial class Positions
    {
        public string Position { get; set; }
    }
}
