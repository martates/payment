﻿using System;
using System.Collections.Generic;

namespace Asgard.Web.Core.Models.Users
{
    public partial class UserInRole
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }

        public Role Role { get; set; }
        public User User { get; set; }
    }
}
