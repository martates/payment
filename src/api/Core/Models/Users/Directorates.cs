﻿using System;
using System.Collections.Generic;

namespace Asgard.Web.Core.Models.Users
{
    public partial class Directorates
    {
        public string Directorate { get; set; }
    }
}
