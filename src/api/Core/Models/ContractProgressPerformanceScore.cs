using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core.Models
{
    public class ContractProgressPerformanceScore
    {
		[Key]
    public int PPid {get; set;}
	public Measurement measurements{get; set;}
	public int ContractId {get; set;}
	public int MeasurementID{get; set;}
	[Column(TypeName = "decimal(18,2)")]
	public decimal? ProgressPerformance {get; set;} //[decimal](18, 2) NULL,
	public string ProgressPerformanceMessage{get; set;}
	public bool? RedFlag {get; set;} //[bit] NULL,
	public bool? GreenFlag {get; set;} //[bit] NULL,
    }
}