using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core {
    public interface IConsultantsProgramRepository {
        IEnumerable<ConsultantsProgram> GetConsultantsPrograms(int contractId);
        void Add(ConsultantsProgram consultantsProgram, int contractId);
        Task<ConsultantsProgram> GetConsultantsProgram(int programId, bool includeRelated);
        Task<bool> DeleteConsultantsProgram(int id);
    }
}