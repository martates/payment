using System.Collections.Generic;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core
{ 
    public interface IContractProgressRepository
    
    {
        ContractProgressResource GetContractProgress(int contractId);
        IEnumerable<ContractProgressResource> GetContractByStaff(int staffId);
       IEnumerable<KeyValuePair<int, string>> getkeyStaff();
    }
}