using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Helpers;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Persistance
{
    public interface IEstimateCommitteRepository
    {
         
        void Add(int PropjectPerKMId,EstimateCommitte estimateCommitte);
        IEnumerable<EstimateCommitte> GetEstimateCommittes(int projectperKm);
        Task<EstimateCommitte> GetEstimateCommitte(int Id);
        Task<TaskResult> DeleteEstimateCommitte(int Id);
    }
}