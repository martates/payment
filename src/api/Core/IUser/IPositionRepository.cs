using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Models.Users;

namespace Asgard.Web.Core {
    public interface IPositionRepository {
        IEnumerable<Positions> GetAllPostion ();
        IEnumerable<Directorates> GetAllDirectorate ();
    }
}