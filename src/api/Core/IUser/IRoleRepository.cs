using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Models.Users;

namespace Asgard.Web.Core {
    public interface IRoleRepository {
        Task<Role> CreateRole (Role role);
        Task<UserInRole> CreateUserInRole (UserInRole userInRole);
        // UserInRole GetUserInRole (Guid userId);
        IEnumerable<UserInRole> GetUserInRole (Guid userId);
        IEnumerable<Role> GetAllRole ();
    }
}