using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Models.Users;

namespace Asgard.Web.Core {
    public interface IUserCreateRepository {
        Task<User> CreateUser (User user);
        IEnumerable<User> GetUser (string username);
        IEnumerable<User> GetAllUser ();
        User GetSingleUser (System.Guid userId);
    }
}