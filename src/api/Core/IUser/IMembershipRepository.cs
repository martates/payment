using System.Threading.Tasks;
using Asgard.Web.Core.Models.Users;

namespace Asgard.Web.Core
{
    public interface IMembershipRepository {
      //  Task<Membership> CreateUser (Membership aspNetUser, string pass);
        Task<Membership> CreateUser (Membership aspNetUser);
        //void Add (Membership membership, Guid UserId);
    }

}