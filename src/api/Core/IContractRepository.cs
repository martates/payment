using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core
{
    public interface IContractRepository
    {
         Contract GetContract(int id);
         IEnumerable<KeyValuePair<int, string>> GetContractNames();
         IEnumerable<KeyValuePair<int, string>> GetWorkContractNames ();

         void UpdateContractAdvanceFigures(int id, decimal advancePercent, decimal advanceAmount);
    }
}