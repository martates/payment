using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core {
    public interface IRoadSegmentListRepository {
        void Add (RoadSegmentList roadSegmentList);
        RoadSegmentList UpdateRoadSegementList (int rnmdId);
        IEnumerable<RoadSegmentList> GetroadSegmentList ();
        List<RoadSegmentList> GroupParts ();

        // IEnumerable<IGrouping<string, RoadSegmentList>> GetroadSegmentList ();
        // int GetroadSegmentSum ();
        // Task<IQueryable<RoadSegmentList>> GetroadSegmentList ();

        // IQueryable<IGrouping<string, int>> GetroadSegmentGroupSum ();
        // IQueryable<IGrouping<string , int>> GetroadSegmentGroupSum ();
        // void GroupParts ();

    }
}