using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core {
    public interface IDeliverableLookupRepository {
        IEnumerable<DeliverableLookup> GetDeliverableLookups();

        void Add(DeliverableLookup lookup);
    }
}