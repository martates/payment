using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Helpers;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core {
    public interface ISupplementRepository {
        IEnumerable<Supplement> GetSupplements(int originalContractId);
        TaskResult Add(Supplement deliverable, int contractId);
        Task<Supplement> GetSupplement(int supplementId, bool includeRelated);
        Task<TaskResult> DeleteSupplement(int id);
        TaskResult VerifySequenceNumber (int contractId, int number);
    }
}