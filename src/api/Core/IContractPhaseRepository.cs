using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Helpers;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core
{
    public interface IContractPhaseRepository
    {
        void Add(int contractId, ContractPhase contractPhase);
        IEnumerable<ContractPhase> GetContractPhases(int contractId);
        Task<ContractPhase> GetContractPhase(int contractPhaseId);
        Task<TaskResult> DeleteContractPhase(int contractPhaseId);
    }
}