using System.Threading.Tasks;

namespace Asgard.Web.Core {
    public interface IUnitOfWork
    {
        Task Complete();
    }
}
