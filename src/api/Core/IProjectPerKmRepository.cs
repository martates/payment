using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Persistance
{
    public interface IProjectPerKmRepository
    {
        void Add (ProjectPerKm projectPerKm);
        IEnumerable<ProjectPerKm> GetProjectPerKm ();
        ProjectPerKm UpdateProjectPerKm (int projectPerKmId);
        IEnumerable<KeyValuePair<int, string>> GetProjectPerKmsdrop (int projectId);
        IEnumerable<ProjectPerKm> GetProjectPerKms (int projectId);
        Task DeleteProjectPerKm (int projectPerKmId);    }
}