namespace Asgard.Web.Core.Helpers
{
    
    public class Credentials {
        public string Name { get; set; }

        public string Password { get; set; }
    }
}