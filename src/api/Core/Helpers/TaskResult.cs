namespace Asgard.Web.Core.Helpers
{
    public class TaskResult
    {
        public TaskResult(bool result, string message)
        {
            IsSuccessful = result;
            Message = message;
        }

        public readonly bool IsSuccessful;
        public readonly string Message;
    }
}