using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Core.Models
{
    public interface IrnmdsRepository
    {
       void Add(rnmdList rnmdList);
        IEnumerable<KeyValuePair<int, string>> GetrnmdsList();
    }
}
