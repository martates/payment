using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Core.Models
{
    public interface IsectionRepository
    {
         void Add(sectionList sectionList);
          IEnumerable<KeyValuePair<int, string>> GetSectionList();
    } 
}
