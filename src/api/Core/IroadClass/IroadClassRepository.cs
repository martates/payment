using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Core.Models
{
    public interface IroadClassRepository
    {
       void Add(roadClassesList roadClassesList);
        IEnumerable<KeyValuePair<int, string>> GetroadClassList();
    }
}
