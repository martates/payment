using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Persistance {
    public interface IProjectRepository {
        Project GetProject (int id);
        IEnumerable<KeyValuePair<int, string>> GetProjectNames ();
    }
}