using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core
{
    public interface IConsultantsPerformanceRepository
    {
        IEnumerable<ConsultantsPerformance> GetConsultantsPerformances(int contractId);
        void Add(ConsultantsPerformance consultantsPerformance, int contractId);
        Task<ConsultantsPerformance> GetConsultantsPerformance(int performanceId, bool includeRelated);
        Task<bool> DeleteConsultantsPerformance(int id);
    }
}