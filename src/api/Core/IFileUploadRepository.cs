using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Models;
namespace Asgard.Web.Persistance
{
    public interface IFileUploadRepository
    {
         Task<IEnumerable<FileUpload>> GetFileUploads(int ownershipId);
    }
}