using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core
{
    public interface IPropertyRepository
    {
        void Add(Property property);
        IEnumerable<Property> Getproperty();
        Property GetPropety(int ownershipId);
        IEnumerable<Property> Getpropertys(int ownershipId);
        IEnumerable<Property> Getpropertysasfull(int projectId, int projectPerKmId, int ownershipId);
        Task Deleteproperty(int PropertyId);
        IEnumerable<Project> GetPropertyOnSpecificProject(int projectId);
    }
}