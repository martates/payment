using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Core
{
    public interface IPhaseCurrencyBreakdownRepository
    {
         IEnumerable<PhaseCurrencyBreakdown> GetCurrencybreakdowns(int phaseId);
         Task<PhaseCurrencyBreakdown> GetCurrencybreakdown(int breakdownId);
         void Add(PhaseCurrencyBreakdown breakdown);
    }
}