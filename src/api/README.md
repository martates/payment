# Asgard.web

Asgard is the code name for the next evolution of the Ethiopian Roads Authority Management System (ERAMS) - a project management, monitoring & evaluation, construction specification rate buildup and
estimate validation system.

The current iteration adds consultants management modules. This project (Asgard.Web) is a .net core web-api project serving the client angular project (asgard).

### Contact
Developer
IMC Worldwide Eth Ltd.
http://www.imcworldwide.com/
Address: Babo Building, 3rd Floor, Room 304/305 Bole, Sub City, Addis Ababa, Ethiopia
Tel: +251 416 635 616
Fax: +251 116 637 078

Support and maintainers
Ashenafi Zena (ashenafizena@gmail.com; +251 91 123 9341)
Letarik Terefe (sirletarik@gmail.com; +251 91 154 8422)
Marta Tesfaye (martates2008@gmail.com; +251 91 044 74 57) 
Adane Fekade (ada2292@gmail.com; +251 91 203 44 44 )

### To run the web-api
Open the Asgard.Web folder in Visual Studio Code (VS Code)
Run dotnet restore on the command-line
Press Debug (F5) to build and run the program or ./run in a terminal to just build and run

