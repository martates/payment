using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using Asgard.Web.Persistance;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Asgard.Web.Controllers
{
    // [Authorize]
    public class PropertyController : Controller
    {
        private readonly IPropertyRepository repository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public PropertyController(IMapper mapper, IPropertyRepository repository, IUnitOfWork unitOfWork)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
            this.repository = repository;
        }

        [Route("api/property")]
        [HttpPost]
        public async Task<IActionResult> CreaterRightofWay([FromBody] PropertyResource property)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var propertyres = mapper.Map<PropertyResource, Property>(property);
            repository.Add(propertyres);
            await unitOfWork.Complete();
            var result = mapper.Map<Property, PropertyResource>(propertyres);
            return Ok(result);
        }

        [HttpGet("api/propertys")]
        public IEnumerable<PropertyResource> GetPropetys()
        {
            var property = repository.Getproperty();
            var result = mapper.Map<IEnumerable<Property>, IEnumerable<PropertyResource>>(property);
            return result;
        }

        [HttpGet("api/ownership/{ownershipId}/propertys")]
        public IEnumerable<Property> GetrightofWayInfo(int ownershipId)
        {
            var _rightof = repository.Getpropertys(ownershipId);
            var result = mapper.Map<IEnumerable<Property>, IEnumerable<Property>>(_rightof);
            return result;
        }

        [Route("api/ownership/{id}/property/{property}")]
        [HttpPut]
        public async Task<ActionResult> UpdateContractPhase([FromBody] PropertyResource property)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var _property = repository.GetPropety(property.OwnershipId);

            mapper.Map<PropertyResource, Property>(property, _property);

            await unitOfWork.Complete();

            return Ok(mapper.Map<Property, PropertyResource>(_property));
        }

        [HttpDelete("api/property/{id}")]
        public async Task<IActionResult> DeleteProperty()
        {
            var propertyId = int.Parse(RouteData.Values["id"].ToString());
            await repository.Deleteproperty(propertyId);
            await unitOfWork.Complete();
            return NoContent();
        }

        //  [HttpDelete("api/project_uniqe/{id}")]
        // public IEnumerable<Project> GetPropertyOnSpecificProject(int projectid)
        // {
        //     var _property = int.Parse(RouteData.Values["id"].ToString());
        //     var result = mapper.Map<IEnumerable<Property>, IEnumerable<Property>>(_property);
        //      unitOfWork.Complete();
        //     return result;
        // } 


        //  public IEnumerable<Property> GetrightofWayInfo(int ownershipId)
        // {
        //     var _rightof = repository.Getpropertys(ownershipId);
        //     var result = mapper.Map<IEnumerable<Property>, IEnumerable<Property>>(_rightof);
        //     return result;
        // }
    }
}