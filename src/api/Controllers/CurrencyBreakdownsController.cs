using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Asgard.Web.Controllers {
    [Authorize]
    [Route ("api/phases/{phaseId}/currencyBreakdowns")]
    public class CurrencyBreakdownsController : Controller {
        private readonly IMapper mapper;
        private readonly IPhaseCurrencyBreakdownRepository repository;
        private readonly IUnitOfWork unitOfWork;

        public CurrencyBreakdownsController (IMapper mapper, IPhaseCurrencyBreakdownRepository repository, IUnitOfWork unitOfWork) {
            this.repository = repository;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        [HttpPost]
        [Route ("breakdown")]
        public async Task<IActionResult> CreateBreakdown (int phaseId, [FromBody] PhaseCurrencyBreakdownResource breakdownRes) {
            if (!ModelState.IsValid)
                return BadRequest (ModelState);

            var breakdownFromRes = mapper.Map<PhaseCurrencyBreakdownResource, PhaseCurrencyBreakdown> (breakdownRes);

            breakdownFromRes.CreatedBy = ""; // TODO: Add identity here
            breakdownFromRes.CreatedOn = DateTime.Now;
            breakdownFromRes.LastEditedBy = ""; // TODO: Add identity here
            breakdownFromRes.LastModifiedOn = DateTime.Now;

            repository.Add (breakdownFromRes);

            await unitOfWork.Complete ();

            var breakdown = mapper.Map<PhaseCurrencyBreakdown, PhaseCurrencyBreakdownResource> (breakdownFromRes);

            return Created (string.Format ("api/phases/{0}/currencyBreakdowns/{1}", phaseId, breakdown.Id), breakdown);
        }

        [HttpPost]
        [Route ("breakdowns")]
        public async Task<IActionResult> CreateBreakdowns (int phaseId, [FromBody] PhaseCurrencyBreakdownResource[] breakdownResources) {
            if (!ModelState.IsValid)
                return BadRequest (ModelState);

            var breakdownFromResources = mapper.Map<IEnumerable<PhaseCurrencyBreakdownResource>, IEnumerable<PhaseCurrencyBreakdown>> (breakdownResources);

            foreach (var breakdownFromResource in breakdownFromResources) {
                breakdownFromResource.CreatedBy = ""; // TODO: Add identity here
                breakdownFromResource.CreatedOn = DateTime.Now;
                breakdownFromResource.LastEditedBy = ""; // TODO: Add identity here
                breakdownFromResource.LastModifiedOn = DateTime.Now;

                repository.Add (breakdownFromResource);
            }

            await unitOfWork.Complete ();

            var breakdowns = mapper.Map<IEnumerable<PhaseCurrencyBreakdown>, IEnumerable<PhaseCurrencyBreakdownResource>> (breakdownFromResources);

            return Created (string.Format ("api/phases/{0}/currencyBreakdowns", phaseId), breakdowns);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateBreakdown (int phaseId, int breakdownId, [FromBody] PhaseCurrencyBreakdownResource[] breakdownResources) {
            if (!ModelState.IsValid)
                return BadRequest (ModelState);

            var currencies = new List<PhaseCurrencyBreakdown> ();
            
            foreach (var breakdownResource in breakdownResources) {

                var breakdown = await repository.GetCurrencybreakdown (breakdownResource.Id);

                breakdown.LastEditedBy = ""; // TODO: Add identity here
                breakdown.LastModifiedOn = DateTime.Now;

                mapper.Map<PhaseCurrencyBreakdownResource, PhaseCurrencyBreakdown> (breakdownResource, breakdown);

                currencies.Add (breakdown);
            }

            await unitOfWork.Complete ();

            return Ok (mapper.Map<IEnumerable<PhaseCurrencyBreakdown>, IEnumerable<PhaseCurrencyBreakdownResource>> (currencies));
        }

        [HttpGet]
        public IActionResult GetCCurrencyBreakdowns (int phaseId) {
            var breakdowns = repository.GetCurrencybreakdowns (phaseId);

            return Ok (mapper.Map<IEnumerable<PhaseCurrencyBreakdown>, IEnumerable<PhaseCurrencyBreakdownResource>> (breakdowns));
        }
    }
}