using System.Collections.Generic;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Asgard.Web.Controllers
{
   [Authorize]
    [Route("api/contractprogress/")]
    public class ContractProgressController : Controller
    {
        private readonly IMapper mapper;
        private readonly IContractProgressRepository repository;
        public ContractProgressController(IMapper mapper, IContractProgressRepository repository)
        {
            this.repository = repository;
            this.mapper = mapper;
        }
        [HttpGet]
        [Route("contract/{contractId}")]
        public IActionResult GetContractProgress(int contractId)
        {
            var contractProgressResource = repository.GetContractProgress(contractId);

            if (contractProgressResource == null)
            {
                return NotFound();
            }

            return Ok(contractProgressResource);
        }
        [HttpGet]
        [Route("keystaff/{staffId}")]
        public IActionResult GetContractByStaff(int StaffId)
        {
            var progressResources = repository.GetContractByStaff(StaffId);

            if (progressResources == null)
            {
                return NotFound();
            }

            return Ok(progressResources);
        }
        [HttpGet]
        [Route("keystaffs/")]
        public IEnumerable<KeyValuePair<int, string>> getkeyStaff()
        {
            return repository.getkeyStaff();

            
        }

    }
}