using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core.Models;
using Asgard.Web.Core;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Asgard.Web.Controllers
{
  // [Authorize]
    public class ContractsController : Controller
    {
        private readonly IMapper mapper;
        private readonly IContractRepository repository;
        public ContractsController(IMapper mapper, IContractRepository repository)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        [HttpGet]
        [Route("api/contracts/{id}")]
        public IActionResult GetContract(int id) 
        {
            var contract = repository.GetContract(id);

            if (contract == null)
            {
                return NotFound();
            }
            
            return Ok(mapper.Map<Contract, ContractResource>(contract));
        }

        [HttpGet]
         [Route("api/contracts")]
        public IEnumerable<KeyValuePair<int, string>> GetContractNames() 
        {
            return repository.GetContractNames();
        }

        [HttpGet]
         [Route("api/workcontracts")]
        public IEnumerable<KeyValuePair<int, string>> GetWorkContractNames() 
        {
            return repository.GetWorkContractNames();
        }
    }
}