﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Asgard.Web.Controllers {
    [Authorize]
    [Route ("api/deliverableLookups")]
    public class DeliverableLookupsController : Controller {
        private readonly IMapper mapper;
        private readonly IDeliverableLookupRepository repository;
        private readonly IUnitOfWork unitOfWork;

        public DeliverableLookupsController (IMapper mapper, IDeliverableLookupRepository repository, IUnitOfWork unitOfWork) {
            this.mapper = mapper;
            this.repository = repository;
            this.unitOfWork = unitOfWork;
        }

        [HttpGet]
        public IEnumerable<DeliverableLookupResource> GetAll () {
            var deliverableLookup = repository.GetDeliverableLookups ();

            return mapper.Map<IEnumerable<DeliverableLookup>, IEnumerable<DeliverableLookupResource>> (deliverableLookup);
        }

        [HttpPost]
        public IActionResult CreateDeliverableLookup ([FromBody] DeliverableLookupResource deliverableLookupRes) {
            var lookup = mapper.Map<DeliverableLookupResource, DeliverableLookup> (deliverableLookupRes);

            repository.Add (lookup);

            unitOfWork.Complete ();

            deliverableLookupRes.Id = lookup.Id;
            
            return Created ("api/deliverableLookups", deliverableLookupRes);
        }
    }
}