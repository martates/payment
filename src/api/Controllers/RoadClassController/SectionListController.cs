using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using Asgard.Web.Persistance;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Asgard.Web.Controllers {
    // [Authorize]
    public class SectionListController : Controller {
        private readonly IsectionRepository repository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public SectionListController (IMapper mapper, IsectionRepository repository, IUnitOfWork unitOfWork) {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
            this.repository = repository;
        }

        [Route ("api/sectionListAdd")]
        [HttpPost]
        public async Task<IActionResult> CreateRoadCharacter ([FromBody] sectionListResource section) {
            if (!ModelState.IsValid) {
                return BadRequest (ModelState);
            }

            var sections = mapper.Map<sectionListResource, sectionList> (section);
            repository.Add (sections);
            await unitOfWork.Complete ();
            var result = mapper.Map<sectionList, sectionListResource> (sections);
            return Ok (result);
        }

        [HttpGet]
        [Route ("api/sectiondrop")]
        public IEnumerable<KeyValuePair<int, string>> GetSectionList () {
            return repository.GetSectionList ();
        }

    }
}