using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using Asgard.Web.Persistance;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Asgard.Web.Controllers {
    // [Authorize]
    public class RnmdsListController : Controller {
        private readonly IrnmdsRepository repository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public RnmdsListController (IMapper mapper, IrnmdsRepository repository, IUnitOfWork unitOfWork) {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
            this.repository = repository;
        }

        [Route ("api/rnmdsListAdd")]
        [HttpPost]
        public async Task<IActionResult> CreateRoadCharacter ([FromBody] rnmdsListResource rnmds) {
            if (!ModelState.IsValid) {
                return BadRequest (ModelState);
            }

            var rnmd = mapper.Map<rnmdsListResource, rnmdList> (rnmds);
            repository.Add (rnmd);
            await unitOfWork.Complete ();
            var result = mapper.Map<rnmdList, rnmdsListResource> (rnmd);
            return Ok (result);
        }

        [HttpGet]
        [Route ("api/rnmddrop")]
        public IEnumerable<KeyValuePair<int, string>> GetRnmdList() {
            return repository.GetrnmdsList();
        }

    }
}