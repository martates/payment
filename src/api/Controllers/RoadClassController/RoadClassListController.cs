using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using Asgard.Web.Persistance;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Asgard.Web.Controllers {
    // [Authorize]
    public class RoadClassListController : Controller {
        private readonly IroadClassRepository repository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public RoadClassListController (IMapper mapper, IroadClassRepository repository, IUnitOfWork unitOfWork) {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
            this.repository = repository;
        }

        [Route ("api/roadClassListAdd")]
        [HttpPost]
        public async Task<IActionResult> CreateRoadCharacter ([FromBody] roadClassListResource roadCla) {
            if (!ModelState.IsValid) {
                return BadRequest (ModelState);
            }

            var roadClass = mapper.Map<roadClassListResource, roadClassesList> (roadCla);
            repository.Add (roadClass);
            await unitOfWork.Complete ();
            var result = mapper.Map<roadClassesList, roadClassListResource> (roadClass);
            return Ok (result);
        }

        [HttpGet]
        [Route ("api/roadClassdrop")]
        public IEnumerable<KeyValuePair<int, string>> GetroadClassList () {
            return repository.GetroadClassList ();
        }

    }
}