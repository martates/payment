using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Asgard.Web.Controllers
{
    [Authorize]
    [Route("api/contracts/{contractId}/consultantsPrograms")]
    public class ConsultantsProgramsController : Controller
    {
        private readonly IMapper mapper;
        private readonly IConsultantsProgramRepository repository;
        private readonly IUnitOfWork unitOfWork;

        public ConsultantsProgramsController(IMapper mapper, IConsultantsProgramRepository repository, IUnitOfWork unitOfWork)
        {
            this.mapper = mapper;
            this.repository = repository;
            this.unitOfWork = unitOfWork;
        }

        [HttpPost]
        public async Task<IActionResult> CreateConsultantsProgram(int contractId, [FromBody] SaveConsultantsProgramResource programRes)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var programFromRes = mapper.Map<SaveConsultantsProgramResource, ConsultantsProgram>(programRes);

            programFromRes.CreatedBy = ""; // TODO: Add identity here
            programFromRes.CreatedOn = DateTime.Now;
            programFromRes.LastEditedBy = ""; // TODO: Add identity here
            programFromRes.LastModifiedOn = DateTime.Now;

            repository.Add(programFromRes, contractId);

            await unitOfWork.Complete();

            var dressedProgram = await repository.GetConsultantsProgram(programFromRes.Id, true);

            var program = mapper.Map<ConsultantsProgram, ConsultantsProgramResource>(dressedProgram);

            return Created(string.Format("api/contracts/{0}/consultantsPrograms/{1}", contractId, program.Id), program);
        }

        [HttpGet]
        public IEnumerable<ConsultantsProgramResource> GetConsultantsPrograms(int contractId)
        {
            var programs = repository.GetConsultantsPrograms(contractId);

            var result = mapper.Map<IEnumerable<ConsultantsProgram>, IEnumerable<ConsultantsProgramResource>>(programs);

            return result;
        }

        [HttpPut]
        public async Task<IActionResult> UpdateConsultantsProgram(int contractId, [FromBody]SaveConsultantsProgramResource programRes)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var program = await repository.GetConsultantsProgram(programRes.Id, true);

            program.LastEditedBy = ""; // TODO: Add identity here
            program.LastModifiedOn = DateTime.Now;

            mapper.Map<SaveConsultantsProgramResource, ConsultantsProgram>(programRes, program);

            await unitOfWork.Complete();

            return Ok(mapper.Map<ConsultantsProgram, ConsultantsProgramResource>(program));
        }

        [HttpDelete("{programId}")]
        public async Task<IActionResult> DeleteConsultantsProgram(int contractId, int programId)
        {
            bool result = await repository.DeleteConsultantsProgram(programId);

            if (!result)
                return NotFound();

            await this.unitOfWork.Complete();

            return new NoContentResult();
        }
    }
}