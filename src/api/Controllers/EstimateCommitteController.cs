using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using Asgard.Web.Persistance;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace Asgard.Web.Controllers
{
    [Authorize]
  [Route("api/projectsbykm/{ProjectPerKmId}/estimatecommittes")]
    public class EstimateCommitteController : Controller
    {
        private readonly IEstimateCommitteRepository repository;
      //  private readonly EramsContext context;
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        public EstimateCommitteController(IMapper mapper, IEstimateCommitteRepository repository, IUnitOfWork unitOfWork)
        {
           // this.context = context;
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
            this.repository = repository;

        }
        [HttpPost]
        public async Task<ActionResult> CreateEstimateCommitte(int ProjectPerKmId, [FromBody] EstimateCommitteResource estimateCommitteRes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

           //  var id = RouteData.Values["Id"];

             var estimateCommitteResFromRes = mapper.Map<EstimateCommitteResource, EstimateCommitte>(estimateCommitteRes);

           // estimateCommitteResFromRes.Id = 0; // TODO: Add identity here
            // estimateCommitteResFromRes.Name ="";
            // estimateCommitteResFromRes.Position = ""; // TODO: Add identity here
            // estimateCommitteResFromRes.Address = "";

             try
             {
                //repository.Add(estimateCommitteResFromRes);
                 repository.Add(int.Parse(ProjectPerKmId.ToString()), estimateCommitteResFromRes);
             }
             catch (InvalidOperationException e)
             {
             ModelState.AddModelError("Estimate Committe Id", e.Message);
                 return BadRequest(ModelState);
             }
            await unitOfWork.Complete();

            //estimateCommitteRes.PropjectPerKmId = estimateCommitteResFromRes.Id;
 var result = mapper.Map<EstimateCommitte, EstimateCommitteResource> (estimateCommitteResFromRes);
return Created(string.Format("/api/projectsbykm/{0}/estimatecommittes/{1}", ProjectPerKmId, estimateCommitteRes.Id), estimateCommitteRes);
           // return Ok (result);
        }
        [HttpGet]
        public IEnumerable<EstimateCommitteResource> GetEstimateCommitte(int ProjectPerKmId)
        {
           // var PropjectPerKMId = int.Parse(RouteData.Values["id"].ToString());

            var estimateCommittes = repository.GetEstimateCommittes(ProjectPerKmId);

            var result = mapper.Map<IEnumerable<EstimateCommitte>, IEnumerable<EstimateCommitteResource>>(estimateCommittes);

            return result;
        }
        
        [HttpPut("{committeeId}")]
        public async Task<ActionResult> UpdateEstimateCommittee(int committeeId, [FromBody]EstimateCommitteResource estimateCommitteRes)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var estimateCommittee = await repository.GetEstimateCommitte(committeeId);

          //  contractPhase.LastEditedBy = ""; // TODO: Add identity here
           // contractPhase.LastModifiedOn = DateTime.Now;

            mapper.Map<EstimateCommitteResource, EstimateCommitte>(estimateCommitteRes, estimateCommittee);

            await unitOfWork.Complete();

            return Ok(mapper.Map<EstimateCommitte, EstimateCommitteResource>(estimateCommittee));
        }
        
        [HttpDelete("{committeeId}")]
        public async Task<IActionResult> DeleteEstimateCommittee(int committeeId)
        {
            var task = await repository.DeleteEstimateCommitte(committeeId);

            // if (task.Result)
            // {
            //     await unitOfWork.Complete();

            //     return NoContent();
            // }

            return new NotFoundObjectResult(task.Message);
        }
    }
} 