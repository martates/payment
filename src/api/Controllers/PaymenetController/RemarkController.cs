using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Asgard.Web.Controllers {
    public class RemarkController : Controller {
        private readonly IremarkReposiotry repository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public RemarkController (IMapper mapper, IremarkReposiotry repository, IUnitOfWork unitOfWork) {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
            this.repository = repository;
        }

        [Route ("api/remarkadd")]
        [HttpPost]
        public async Task<IActionResult> CreatefinancedBy ([FromBody] RemarkResource remarkadd) {
              if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var remark = mapper.Map<RemarkResource, Remark>(remarkadd);
            repository.Add(remark);
            await unitOfWork.Complete();
            var result = mapper.Map<Remark, RemarkResource>(remark);
            return Ok(result);
        }
             
        [HttpGet("api/allRemarkList")]
        public IEnumerable<RemarkResource> GetAllRemarkName()
        {
            // TODO: make call async
            // Map to a given contract
            var remarkList = repository.GetRemarkList();
            var result = mapper.Map<IEnumerable<Remark>, IEnumerable<RemarkResource>>(remarkList);
            return result;
        }
    }
}