using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Asgard.Web.Controllers
{
    public class FinancedByController : Controller
    {
        private readonly IfinancedByReposiotry repository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public FinancedByController(IMapper mapper, IfinancedByReposiotry repository, IUnitOfWork unitOfWork)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
            this.repository = repository;
        }

        [Route("api/financedByadd")]
        [HttpPost]
        public async Task<IActionResult> CreatefinancedBy([FromBody] FinancedByResource financedBytadd)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var financedBy = mapper.Map<FinancedByResource, FinancedBy>(financedBytadd);
            repository.Add(financedBy);
            await unitOfWork.Complete();
            var result = mapper.Map<FinancedBy, FinancedByResource>(financedBy);
            return Ok(result);
        }
        // [HttpGet]
        // [Route("api/financedByNames")]
        // public IEnumerable<KeyValuePair<int, string>> GetfinancedByNames()
        // {
        //     return repository.GetFinancedByNames();
        // }     

        
        [HttpGet("api/allfinancedByList")]
        public IEnumerable<FinancedByResource> GetAllFinancedByName()
        {
            // TODO: make call async
            // Map to a given contract
            var paymentList = repository.GetfinancedBy();
            var result = mapper.Map<IEnumerable<FinancedBy>, IEnumerable<FinancedByResource>>(paymentList);
            return result;
        }
    }
}