using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Asgard.Web.Controllers
{
    // [Authorize]
    public class PaymentController : Controller
    {
        private readonly IpaymentReposiotry repository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public PaymentController(IMapper mapper, IpaymentReposiotry repository, IUnitOfWork unitOfWork)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
            this.repository = repository;
        }

        [Route("api/paymenetadd")]
        [HttpPost]
        public async Task<IActionResult> CreatePayment([FromBody] PaymentResource paymentadd)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var paymenet = mapper.Map<PaymentResource, Payment>(paymentadd);
            repository.Add(paymenet);
            await unitOfWork.Complete();
            var result = mapper.Map<Payment, PaymentResource>(paymenet);
            return Ok(result);
        }

        [Route ("api/project/{id}/payment/{payment}")]
        [HttpPut]
        public async Task<ActionResult> UpdatePayment ([FromBody] PaymentResource payment) {
            if (!ModelState.IsValid)
                return BadRequest (ModelState);

            var _payment = repository.UpdatePayment (payment.id);

            mapper.Map<PaymentResource, Payment> (payment, _payment);

            await unitOfWork.Complete ();

            return Ok (mapper.Map<Payment, PaymentResource> (_payment));
        }


        [HttpDelete ("api/payment/{id}")]
        public async Task<IActionResult> DeleteProperty () {
            var payment = int.Parse (RouteData.Values["id"].ToString ());
            await repository.DeletePayment (payment);
            await unitOfWork.Complete ();
            return NoContent ();
        }

        [HttpGet("api/paymentList")]
        public IEnumerable<PaymentResource> GetOwner()
        {
            // TODO: make call async
            // Map to a given contract
            var paymentList = repository.GetPaymentList();
            var result = mapper.Map<IEnumerable<Payment>, IEnumerable<PaymentResource>>(paymentList);
            return result;
        }


  [HttpGet("api/paymentListpaid")]
        public IEnumerable<PaymentResource> paymentListpaid()
        {
            // TODO: make call async
            // Map to a given contract
            var paymentList = repository.GetPaymentListPaid();
            var result = mapper.Map<IEnumerable<Payment>, IEnumerable<PaymentResource>>(paymentList);
            return result;
        }

         [HttpGet ("api/projects/{projectId}/payment")]
        public IEnumerable<Payment> GetProjectPerKms (int projectId) {
            var _rightof = repository.GetPayment (projectId);
            var result = mapper.Map<IEnumerable<Payment>, IEnumerable<Payment>> (_rightof);
            return result;
        }
    }
}