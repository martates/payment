using Microsoft.AspNetCore.Mvc;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Asgard.Web.Controllers.Resources;
using System;
using Microsoft.AspNetCore.Authorization;

namespace Asgard.Web.Controllers
{
    [Authorize]
    [Route("api/contracts/{contractId}/deliverables")]
    public class DeliverablesController : Controller
    {
        private readonly IMapper mapper;
        private readonly IDeliverableRepository repository;
        private readonly IUnitOfWork unitOfWork;

        public DeliverablesController(IMapper mapper, IDeliverableRepository repository, IUnitOfWork unitOfWork)
        {
            this.mapper = mapper;
            this.repository = repository;
            this.unitOfWork = unitOfWork;
        }

        [HttpPost]
        public async Task<IActionResult> CreateDeliverable(int contractId, [FromBody] SaveDeliverableResource deliverableRes)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var deliverableFromRes = mapper.Map<SaveDeliverableResource, Deliverable>(deliverableRes);

            deliverableFromRes.CreatedBy = ""; // TODO: Add identity here
            deliverableFromRes.CreatedOn = DateTime.Now;
            deliverableFromRes.LastEditedBy = ""; // TODO: Add identity here
            deliverableFromRes.LastModifiedOn = DateTime.Now;

            repository.Add(deliverableFromRes, contractId);

            await unitOfWork.Complete();

            var dressedDeliverable = await repository.GetDeliverable(deliverableFromRes.Id, true);

            var deliverable = mapper.Map<Deliverable, DeliverableResource>(dressedDeliverable);

            return Created(string.Format("api/contracts/{0}/deliverables/{1}", contractId, deliverable.Id), deliverable);
        }

        [HttpGet]
        public IEnumerable<DeliverableResource> GetDeliverables(int contractId)
        {
            var deliverables = repository.GetDeliverables(contractId);

            var result = mapper.Map<IEnumerable<Deliverable>, IEnumerable<DeliverableResource>>(deliverables);

            return result;
        }

        [HttpPut]
        public async Task<IActionResult> UpdateDeliverable(int contractId, [FromBody]SaveDeliverableResource deliverableRes)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var deliverable = await repository.GetDeliverable(deliverableRes.Id, false);

            deliverable.LastEditedBy = ""; // TODO: Add identity here
            deliverable.LastModifiedOn = DateTime.Now;

            mapper.Map<SaveDeliverableResource, Deliverable>(deliverableRes, deliverable);

            await unitOfWork.Complete();

            return Ok(mapper.Map<Deliverable, DeliverableResource>(deliverable));
        }

        [HttpDelete("{deliverableId}")]
        public async Task<IActionResult> DeleteDeliverable(int deliverableId)
        {
            var task = await repository.DeleteDeliverable(deliverableId);

            if (task.IsSuccessful)
            {
                await unitOfWork.Complete();

                return new NoContentResult();
            }

            return new NotFoundObjectResult(task.Message);
        }
    }
}