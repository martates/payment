using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Core.Models {
    public class RoadClassGroupByResource {
        public string region { get; set; }
        public int Total { get; set; }
      //  public IEnumerable<RoadSegmentList> roadSegmentLists { get; set; }
    }
}