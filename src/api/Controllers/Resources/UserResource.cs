using System;

namespace Asgard.Web.Controllers.Resources {
    public partial class UserResource
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string Directorate { get; set; }
    }
}