using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Core.Models
{
    public class rnmdsListResource
    {
        [Key]
        public int Id { get; set; }
        public string rnmd { get; set; }
    }
}
