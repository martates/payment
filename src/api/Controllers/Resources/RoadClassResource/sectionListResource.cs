using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Core.Models
{
    public class sectionListResource
    {
        [Key]
        public int Id { get; set; }
        public string section { get; set; }
    }
}
