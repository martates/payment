using System;
using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Controllers.Resources
{
    public class ContactDetailResource
    {
        	[Key]
    public int ContactDetailID{get; set;}
	public int ContractID { get; set; }
	public int StaffID {get; set;}
	public string Position {get; set;}
	public DateTime AssignmentDate{get; set;}
	public DateTime LeavingDate {get; set;}
	public string Landline {get; set;}
	public string Mobile {get; set;}
	public string Email {get; set;}
	public string ContactType {get; set;}
	public string LastEditedBy {get; set;}
	public DateTime CreatedOn {get; set;} 
	public DateTime LastModifiedOn {get; set;} 
	
    }
}