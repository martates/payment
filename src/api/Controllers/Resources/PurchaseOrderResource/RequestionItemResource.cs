using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Asgard.Web.Core.Models {
    public class RequestionItemResource {
        public int Requestion_No { get; set; }
        public DateTime Requestion_Date { get; set; }
        public string Supplier_To { get; set; }
        public DateTime Supplier_Date { get; set; }

    }
}