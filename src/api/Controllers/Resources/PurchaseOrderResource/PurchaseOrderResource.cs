using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Asgard.Web.Core.Models {
    public class PurchaseOrderResource {
        public int Item_No { get; set; }
        public string Article { get; set; }
        public string IBM_No { get; set; }
        public int Quantity_recived { get; set; }
        public int Quantity_Order { get; set; }
        public string Measurement { get; set; }
        public int Unit_Price { get; set; }
        public int Amount { get; set; }
    }
}