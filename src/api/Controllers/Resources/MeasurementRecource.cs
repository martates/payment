using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Controllers.Resources
{
    public class MeasurementRecource
    {
        public int ContractId { get; set; }
        [Key]
        public int MeasurementID { get; set; }
        public DateTime date { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal MeasurementAndVOs { get; set; }
        public decimal CertifiedClaims { get; set; } //Currency
        public decimal Escalation { get; set; }
        public decimal TotalRetention { get; set; }
        public decimal Taxes { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal InterestOnLatePayments { get; set; }
        public decimal OtherPayments { get; set; }
        public decimal AdvanceRepayment { get; set; }
        public decimal CorrectionOnCertificate { get; set; }
        public decimal VatThisInvoice { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal ExpenditureVsBudget { get; set; }
        public string CriticalActivities { get; set; }
        public string LastEditedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastModifiedOn { get; set; }
        public IEnumerable<ContractProgressPerformanceScore> ContractProgressPerformanceScores { get; set; }
    }
}