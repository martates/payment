using System;
using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Controllers.Resources {
    public class ContractPhaseResource {
        public int Id { get; set; }

        [Required]
        public int ContractId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        [Required]
        public decimal LumpSum { get; set; }

        [Required]
        public decimal Reimbursable { get; set; }

        public decimal AdvancePercent { get; set; }

        [Required]
        public decimal AdvanceAmount { get; set; }

        // Change in phase advance/figures
        // triggers change in the contract
        // advance
        public decimal ContractAdvanceAmount { get; set; }
        public decimal ContractAdvancePercent { get; set; }

        public decimal ProvisionalSum { get; set; }
        public int? SupplementId { get; set; }
        public int? ReplacedSupplementId { get; set; }
        public int? ContractPhaseId { get; set; }
        public Boolean IsAdvanceNotTaken { get; set; }
    }
}