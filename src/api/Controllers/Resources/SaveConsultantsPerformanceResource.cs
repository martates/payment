using System;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Controllers.Resources
{
    public class SaveConsultantsPerformanceResource
    {
        public int Id { get; set; }
        public DateTime AssessmentDate { get; set; }
        public DateTime? SubmissionDate { get; set; }
        public decimal PercentComplete { get; set; }
        public decimal MonetaryValue { get; set; }
        public string Reason { get; set; }
        public string DetailedReason { get; set; }
        public int DeliverableId { get; set; }
        public int ContractId { get; set; }
    }
}