using System;
using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Controllers.Resources {
        public class SaveDeliverableResource {
        public int Id { get; set; }
        [Required]
        public int DeliverableLookupId { get; set; }
        [Required]
        public int ContractPhaseId { get; set; }
        public decimal PhasePercentage { get; set; }
        [Required]
        public decimal MonetaryValue { get; set; }
    }
}