using System;
using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Controllers.Resources {
        public class DeliverableResource {
        public int Id { get; set; }
        public decimal PhasePercentage { get; set; }
        public decimal MonetaryValue { get; set; }
        public ContractPhaseResource ContractPhase { get; set; }
        public int ContractPhaseId { get; set; }
        public DeliverableLookupResource DeliverableLookup { get; set; }
    }
}