using System;

namespace Asgard.Web.Controllers.Resources
{
    public class ConsultantsPerformanceResource
    {
        public int Id { get; set; }
        public DateTime AssessmentDate { get; set; }
        public DateTime? SubmissionDate { get; set; }
        public decimal PercentComplete { get; set; }
        public decimal MonetaryValue { get; set; }
        public string Reason { get; set; }
        public string DetailedReason { get; set; }
        public DeliverableResource Deliverable { get; set; }
    }
}