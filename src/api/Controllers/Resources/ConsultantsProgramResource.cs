using System;

namespace Asgard.Web.Controllers.Resources
{
    public class ConsultantsProgramResource
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public decimal PercentComplete { get; set; }
        public decimal MonetaryValue { get; set; }
        public DeliverableResource Deliverable { get; set; }
    }
}