using System;
using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Controllers.Resources {
    public class SupplementResource {
        public int Id { get; set; }
        public int ContractId { get; set; }

        [Required]
        public int Number { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public DateTime AgreementDate { get; set; }
        public string Purpose { get; set; }

        [Required]
        public decimal SupplementAmount { get; set; }
    }
}