using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Controllers.Resources {
    public class ContractResource {
        public int Id { get; set; }

        [Required]
        public string Number { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string ConsultantName { get; set; }
        public decimal ContractAmount { get; set; }
        public decimal AdvancePercent { get; set; }
        public decimal AdvanceAmount { get; set; }
        public DateTime? ContractSignedOn { get; set; }
        public DateTime? ContractCommencement { get; set; }
        public int? ContractPeriod { get; set; }
        public IEnumerable<ContractPhaseResource> Phases { get; set; }
        public IEnumerable<CurrencyBreakdownResource> CurrencyBreakdowns { get; set; }

    }
}