using System.ComponentModel.DataAnnotations;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Controllers.Resources
{
    public class ContractProgressPerformanceScoreResource
    {
        [Key]
    public int PPid {get; set;}
	public Measurement measurements{get; set;}
	public int ContractID {get; set;}
	public int MeasurementID{get; set;}
	public int ProgressPerformance {get; set;} //[decimal](18, 2) NULL,
	public string ProgressPerformanceMessage{get; set;}
	public int RedFlag {get; set;} //[bit] NULL,
	public int GreenFlag {get; set;} //[bit] NULL,
    }
}