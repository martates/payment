using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Controllers.Resources {
    public class FinancedByResource {
        [Key]
        public int financedById { get; set; }
        public string financedByname { get; set; }
        public int noofDateExpected { get; set; }
    }
}