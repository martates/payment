using System;
using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Controllers.Resources {
    public class PaymentResource {
        [Key]
        public int id { get; set; }
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public DateTime dateRecived { get; set; }
        public string payeTo { get; set; }
        public string financedBy { get; set; }
        public string region { get; set; }
        public string ipcinv { get; set; }
        public string type { get; set; }
        public DateTime dateToprocess { get; set; }
        public DateTime payDate { get; set; }
        public string status { get; set; }
        public string counterAccount { get; set; }
        public string team { get; set; }
        public string teamLeader { get; set; }
        public Nullable<DateTime> dateReturn { get; set; }
        public string paidstatus { get; set; }
        public string remark { get; set; }
        public string LastEditedBy { get; set; }
    }
}