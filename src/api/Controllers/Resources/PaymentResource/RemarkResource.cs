using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Controllers.Resources {
    public class RemarkResource {
        [Key]
        public int remarkId { get; set; }
        public string remarkName { get; set; }
    }
}