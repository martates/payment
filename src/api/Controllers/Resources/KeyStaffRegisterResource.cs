using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Asgard.Web.Core.Models;

namespace Asgard.Web.Controllers.Resources
{
    public class KeyStaffRegisterResource
    {
   		[Key]
   public int StaffID {get; set;}
	public string FullName {get; set;}
	public string Position {get; set;}
	// public string IdNumber {get; set;}
	// public string IsLocalStaff {get; set;}// [bit] 
	// public string Landline {get; set;}
	// public string Mobile {get; set;}
	// public string Email {get; set;}
	// public string LastEditedBy {get; set;}
	// public DateTime CreatedOn {get; set;}
	// public DateTime LastModifiedOn {get; set;}
 // public IEnumerable<ContactDetail> ContactDetails {get; set;}
        
    }
}