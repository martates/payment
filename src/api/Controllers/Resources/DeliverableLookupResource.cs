
namespace Asgard.Web.Controllers.Resources {
public class DeliverableLookupResource {
        public int Id { get; set; }
        
        public string Description { get; set; }
    }
}