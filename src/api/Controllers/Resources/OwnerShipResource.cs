using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Controllers.Resources {
    public class OwnerShipResource {
        [Key]
        public int OwnershipId { get; set; }
        public string OwnershipName { get; set; }
        public int ProjectPerKmId { get; set; }
    }
}