using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Asgard.Web.Controllers.Resources
{
    public class ProjectResource
    {
          [Key]
        public int ProjectID { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
         [Column(TypeName = "decimal(18,2)")]
        public decimal? RoadLength { get; set; }
    }        
}