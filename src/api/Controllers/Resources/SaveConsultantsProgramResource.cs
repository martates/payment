using System;
using System.ComponentModel.DataAnnotations;

namespace Asgard.Web.Controllers.Resources
{
    public class SaveConsultantsProgramResource
    {        
        public int Id { get; set; }
        public int ContractId { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public decimal PercentComplete { get; set; }
        [Required]
        public decimal MonetaryValue { get; set; }
        [Required]
        public int DeliverableId { get; set; }
    }
}