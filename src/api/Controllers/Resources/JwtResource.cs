using System.Collections.Generic;

namespace Asgard.Web.Controllers.Resources
{
    public class JwtResource
    {
        public string Token { get; set; }
        public UserResource User { get; set; }
        public ICollection<UserInRoleResource> Roles { get; set; }
    }
}