using System;
using Asgard.Web.Core.Models.Users;

namespace Asgard.Web.Controllers.Resources {
    public class UserInRoleResource {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
         public string LoweredRoleName { get; set; }
        public string Description { get; set; }
    }
}