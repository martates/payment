using System;
using System.ComponentModel.DataAnnotations;

// by LS, PS and reimbursable
namespace Asgard.Web.Controllers.Resources {
    public class PhaseCurrencyBreakdownResource {
        public int Id { get; set; }
        public int ContractPhaseId { get; set; }
        [Required]
        public decimal Amount { get; set; }
        // LS, PS, Reimbursable
        [Required]
        public string Type { get; set; }
        [Required]
        public string Currency { get; set; }
        [Required]
        public decimal ExchangeRate { get; set; }
    }
}