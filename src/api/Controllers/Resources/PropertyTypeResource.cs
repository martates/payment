using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Asgard.Web.Controllers.Resources
{
    public class PropertyTypeResource 
    {
       public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<UnitResource> Units { get; set; }
        
    }
}