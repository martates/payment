using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Asgard.Web.Controllers.Resources
{
    public class ProjectPerKmResource
    {
         [Key]
        public int ProjectPerKmId { get; set; }
        public int ProjectID { get; set; }        
        public Decimal KMStart { get; set; }
        public Decimal KmEnd { get; set; }
        public string State { get; set; }
        public string Zone { get; set; }
        public string Woreda { get; set; }
    }
}