
namespace Asgard.Web.Controllers.Resources {
    public class EstimateCommitteResource {

        public int Id { get; set; }
        public int ProjectPerKmId { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public string Address { get; set; }
    }
}