namespace Asgard.Web.Controllers.Resources {
    public class CurrencyBreakdownResource {

        public int Id { get; set; }
        public int ContractId { get; set; }
        public decimal EtbAmount { get; set; }
        public decimal Amount { get; set; }

        public string Currency { get; set; }

        public decimal ExchangeRate { get; set; }
    }
}