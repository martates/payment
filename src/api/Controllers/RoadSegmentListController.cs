using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using Asgard.Web.Persistance;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Asgard.Web.Controllers {
    // [Authorize]
    public class RoadSegmentListController : Controller {
        private readonly IRoadSegmentListRepository repository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public RoadSegmentListController (IMapper mapper, IRoadSegmentListRepository repository, IUnitOfWork unitOfWork) {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
            this.repository = repository;
        }

        [Route ("api/roadCharacterAdd")]
        [HttpPost]
        public async Task<IActionResult> CreateRoadCharacter ([FromBody] RoadSegmentListResource roadCharacterstics) {
            if (!ModelState.IsValid) {
                return BadRequest (ModelState);
            }

            var roadCharacter = mapper.Map<RoadSegmentListResource, RoadSegmentList> (roadCharacterstics);
            repository.Add (roadCharacter);
            await unitOfWork.Complete ();
            var result = mapper.Map<RoadSegmentList, RoadSegmentListResource> (roadCharacter);
            return Ok (result);
        }

         [Route("api/roadCharacter/{rnmdId}/roadCharacter/{roadcharacter}")]
        [HttpPut]
        public async Task<ActionResult> UpdateRoadSegment([FromBody] RoadSegmentListResource roadSegmentList)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var _roadSegmentList = repository.UpdateRoadSegementList(roadSegmentList.rnmdId);

            mapper.Map<RoadSegmentListResource, RoadSegmentList>(roadSegmentList, _roadSegmentList);

            await unitOfWork.Complete();

            return Ok(mapper.Map<RoadSegmentList, RoadSegmentListResource>(_roadSegmentList));
        }

        [HttpGet ("api/roadSegment")]
        public IEnumerable<RoadSegmentList> GetRoadSegmentAll () {
            return repository.GetroadSegmentList ();
        }

        [HttpGet ("api/roadSegmentGroupby")]
        public List<RoadSegmentList> GetRoadSegmentGroupby () {

            return repository.GroupParts ();
        }

    }
}