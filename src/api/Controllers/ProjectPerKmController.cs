using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using Asgard.Web.Persistance;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Asgard.Web.Controllers {
    // [Authorize]
    public class ProjectPerKmController : Controller {
        private readonly IProjectPerKmRepository repository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public ProjectPerKmController (IMapper mapper, IProjectPerKmRepository repository, IUnitOfWork unitOfWork) {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
            this.repository = repository;
        }

        [Route ("api/projectperkm")]
        [HttpPost]
        public async Task<IActionResult> CreateAffilate ([FromBody] ProjectPerKmResource projectperkm) {
            if (!ModelState.IsValid) {
                return BadRequest (ModelState);
            }
            var ownershipresres = mapper.Map<ProjectPerKmResource, ProjectPerKm> (projectperkm);
            repository.Add (ownershipresres);
            await unitOfWork.Complete ();
            var result = mapper.Map<ProjectPerKm, ProjectPerKmResource> (ownershipresres);
            return Ok (result);
        }

        [Route ("api/project/{id}/projectperkm/{projectPerKm}")]
       // [Route ("api/ownership/{id}/property/{property}")]
        [HttpPut]
        public async Task<ActionResult> UpdateContractPhase ([FromBody] ProjectPerKmResource projectPerKm) {
            if (!ModelState.IsValid)
                return BadRequest (ModelState);

            var _property = repository.UpdateProjectPerKm (projectPerKm.ProjectPerKmId);

            mapper.Map<ProjectPerKmResource, ProjectPerKm> (projectPerKm, _property);

            await unitOfWork.Complete ();

            return Ok (mapper.Map<ProjectPerKm, ProjectPerKmResource> (_property));
        }

        [HttpDelete ("api/projectPerKm/{id}")]
        public async Task<IActionResult> DeleteProperty () {
            var projectPerKm = int.Parse (RouteData.Values["id"].ToString ());
            await repository.DeleteProjectPerKm (projectPerKm);
            await unitOfWork.Complete ();
            return NoContent ();
        }

        [HttpGet ("api/projectperkm")]
        public IEnumerable<ProjectPerKmResource> GetPropetys () {
            // TODO: make call async
            // Map to a given contract
            var projectperkm = repository.GetProjectPerKm ();
            var result = mapper.Map<IEnumerable<ProjectPerKm>, IEnumerable<ProjectPerKmResource>> (projectperkm);
            return result;
        }

        [HttpGet ("api/projects/{projectId}/projectperkm")]
        public IEnumerable<ProjectPerKm> GetProjectPerKms (int projectId) {
            var _rightof = repository.GetProjectPerKms (projectId);
            var result = mapper.Map<IEnumerable<ProjectPerKm>, IEnumerable<ProjectPerKm>> (_rightof);
            return result;
        }

        [HttpGet]
        [Route ("api/projects/{projectId}/projectperkmdrop")]
        public IEnumerable<KeyValuePair<int, string>> GetProjectPerKmsdrop (int projectId) {
            return repository.GetProjectPerKmsdrop (projectId);
        }

    }
}