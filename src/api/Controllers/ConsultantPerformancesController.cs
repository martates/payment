using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Asgard.Web.Controllers
{
    [Authorize]
    [Route("api/contracts/{contractId}/consultantsPerformances")]
    public class ConsultantPerformancesController : Controller
    {
        private readonly IMapper mapper;
        private readonly IConsultantsPerformanceRepository repository;
        private readonly IUnitOfWork unitOfWork;

        public ConsultantPerformancesController(IMapper mapper, IConsultantsPerformanceRepository repository, IUnitOfWork unitOfWork)
        {
            this.mapper = mapper;
            this.repository = repository;
            this.unitOfWork = unitOfWork;
        }

        [HttpPost]
        public async Task<IActionResult> CreateConsultantsProgram(int contractId, [FromBody] SaveConsultantsPerformanceResource performanceRes)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var programFromRes = mapper.Map<SaveConsultantsPerformanceResource, ConsultantsPerformance>(performanceRes);

            programFromRes.CreatedBy = ""; // TODO: Add identity here
            programFromRes.CreatedOn = DateTime.Now;
            programFromRes.LastEditedBy = ""; // TODO: Add identity here
            programFromRes.LastModifiedOn = DateTime.Now;

            repository.Add(programFromRes, contractId);

            await unitOfWork.Complete();

            var dressedPerformance = await repository.GetConsultantsPerformance(programFromRes.Id, true);

            var performance = mapper.Map<ConsultantsPerformance, ConsultantsPerformanceResource>(dressedPerformance);

            return Created(string.Format("api/contracts/{0}/consultantsPerformances/{1}", contractId, performance.Id), performance);
        }

        [HttpGet]
        public IEnumerable<ConsultantsPerformanceResource> GetConsultantsPerformances(int contractId)
        {
            var performance = repository.GetConsultantsPerformances(contractId);

            var result = mapper.Map<IEnumerable<ConsultantsPerformance>, IEnumerable<ConsultantsPerformanceResource>>(performance);

            return result;
        }

        [HttpPut]
        public async Task<IActionResult> UpdateConsultantsPerformance(int contractId, [FromBody]SaveConsultantsPerformanceResource performanceRes)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var performance = await repository.GetConsultantsPerformance(performanceRes.Id, false);

            performance.LastEditedBy = ""; // TODO: Add identity here
            performance.LastModifiedOn = DateTime.Now;

            mapper.Map<SaveConsultantsPerformanceResource, ConsultantsPerformance>(performanceRes, performance);

            await unitOfWork.Complete();

            return Ok(mapper.Map<ConsultantsPerformance, ConsultantsPerformanceResource>(performance));
        }

        [HttpDelete("{performanceId}")]
        public async Task<IActionResult> DeleteConsultantsPerformance(int contractId, int performanceId)
        {
            bool result = await repository.DeleteConsultantsPerformance(performanceId);

            if (!result)
                return NotFound();

            await this.unitOfWork.Complete();

            return new NoContentResult();
        }
    }
}