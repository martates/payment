using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Asgard.Web.Controllers {
    [Authorize]
    [Route ("api/contracts/{contractId}/supplements")]
    public class SupplementsController : Controller {
        private readonly IMapper mapper;
        private readonly ISupplementRepository repository;
        public IUnitOfWork unitOfWork;

        public SupplementsController (IMapper mapper, ISupplementRepository repository, IUnitOfWork unitOfWork) {
            this.mapper = mapper;
            this.repository = repository;
            this.unitOfWork = unitOfWork;
        }

        [HttpGet]
        public IEnumerable<SupplementResource> GetSupplementaryAgreementsFor (int contractId) {
            var supplements = repository.GetSupplements (contractId);

            return mapper.Map<IEnumerable<Supplement>, IEnumerable<SupplementResource>> (supplements);
        }

        [HttpPost]
        public async Task<IActionResult> CreateSupplement (int contractId, [FromBody] SupplementResource supplementRes) {
            if (!ModelState.IsValid)
                return BadRequest (ModelState);

            var supplementFromRes = mapper.Map<SupplementResource, Supplement> (supplementRes);

            var result = repository.Add (supplementFromRes, contractId);

            if (!result.IsSuccessful)
                return BadRequest (result.Message);

            await unitOfWork.Complete ();

            var supplement = mapper.Map<Supplement, SupplementResource> (supplementFromRes);

            return Created (string.Format ("api/contracts/{0}/supplements/{1}", contractId, supplement.Id), supplement);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateSupplement (int contractId, [FromBody] SupplementResource supplementRes) {
            if (!ModelState.IsValid)
                return BadRequest (ModelState);

            var supplement = await repository.GetSupplement (supplementRes.Id, false);

            if (supplement.Number != supplementRes.Number) {
                var result = repository.VerifySequenceNumber (contractId, supplementRes.Number);

                if (!result.IsSuccessful) {
                    return BadRequest (result.Message);
                }
            }

            mapper.Map<SupplementResource, Supplement> (supplementRes, supplement);

            await unitOfWork.Complete ();

            return Ok (mapper.Map<Supplement, SupplementResource> (supplement));
        }

        [HttpDelete("{supplementId}")]
        public async Task<IActionResult> DeleteSupplement(int supplementId)
        {
            var task = await repository.DeleteSupplement(supplementId);

            if (task.IsSuccessful)
            {
                await unitOfWork.Complete();

                return new NoContentResult();
            }

            return new NotFoundObjectResult(task.Message);
        }
    }
}