using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core.Models;
using Asgard.Web.Persistance;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Asgard.Web.Controllers
{
    // [Authorize]
    public class ProjectController : Controller
    {
        private readonly IMapper mapper;
        private readonly IProjectRepository repository;
        public ProjectController(IMapper mapper, IProjectRepository repository)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        [HttpGet]
        [Route("api/projects/{id}")]
        public IActionResult GetProject(int id)
        {
            var project = repository.GetProject(id);

            if (project == null)
            {
                return NotFound();
            }

            return Ok(mapper.Map<Project, ProjectResource>(project));
        }

        [HttpGet]
        [Route("api/projects")]
        public IEnumerable<KeyValuePair<int, string>> GetContractNames()
        {
            return repository.GetProjectNames();
        }
    }
}