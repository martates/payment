using System;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using Asgard.Web.Persistance;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Asgard.Web.Controllers {
   // [Authorize]
    [Route ("/api/ownership/{ownershipId}/fileuploads")]
    public class FileUploadsController : Controller {
        private readonly IHostingEnvironment host;
        private readonly IOwnerShipRepository repository;
        private readonly IFileUploadRepository fileUploadRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        //  private readonly IFileUploadService fileUploadService;
        private readonly FileUploadSettings fileUploadSettings;

        public FileUploadsController (IHostingEnvironment host,
            IOwnerShipRepository repository,
            IFileUploadRepository fileUploadRepository,
            IOptionsSnapshot<FileUploadSettings> options,
            IUnitOfWork unitOfWork, IMapper mapper) 
            {
            this.fileUploadSettings = options.Value;
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
            this.repository = repository;
            this.fileUploadRepository = fileUploadRepository;
            this.host = host;
        }

        [HttpGet]
        public async Task<IEnumerable<FileUploadResource>> GetFiles (int ownershipId) {
            var photos = await fileUploadRepository.GetFileUploads (ownershipId);

            return mapper.Map<IEnumerable<FileUpload>, IEnumerable<FileUploadResource>> (photos);
        }

        [HttpPost]
        [RequestSizeLimit(100_000_000)]
        public async Task<IActionResult> Upload (int ownershipId, IFormFile file) {
            var ownership = repository.GetOwnership (ownershipId, includeRelated : false);
            if (ownership == null)
                return NotFound ();
           // if (file == null) return BadRequest ("Null file");
            // if (file.Length == 0) return BadRequest ("Empty file");
            // if (file.Length > fileUploadSettings.MaxBytes) return BadRequest ("Max file size exceeded");
           // if (!fileUploadSettings.IsSupported (file.FileName)) return BadRequest ("Invalid file type.");
            var uploadsFolderPath = Path.Combine (host.WebRootPath, "uploads");
            //var fileUpload = await FileUploadService.UploadPhoto(affiliate, file, uploadsFolderPath);
            if (!Directory.Exists (uploadsFolderPath))
                Directory.CreateDirectory (uploadsFolderPath);

            var fileName = Guid.NewGuid ().ToString () + Path.GetExtension (file.FileName);
            var filePath = Path.Combine (uploadsFolderPath, fileName);

            using (var stream = new FileStream (filePath, FileMode.Create)) {
                await file.CopyToAsync (stream);
            }
            var fileUpload = new FileUpload { FileName = fileName };
            ownership.FileUploads.Add (fileUpload);
            await unitOfWork.Complete ();
            //    return fileName;

            return Ok (mapper.Map<FileUpload, FileUploadResource> (fileUpload));

        }
    }
}