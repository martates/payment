using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using api.Utilities;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Helpers;
using Asgard.Web.Core.Models.Users;
using Asgard.Web.Persistance;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace Asgard.Web.Controllers {
    [Route ("api/auth")]
    public class AuthController : Controller {
        private readonly EramsUsersContext context;
        private readonly ILogger<AuthController> log;
        private readonly IMapper mapper;
        private readonly IMembershipRepository repository;

        public AuthController (IMapper mapper, EramsUsersContext context, ILogger<AuthController> log, IMembershipRepository repository) {
            this.mapper = mapper;
            this.context = context;
            this.log = log;
            this.repository = repository;
        }

        [HttpPost ("createMembership")]
        public async Task<ActionResult> createUser ([FromBody] Membership model) {

            byte[] buf = new byte[16];
            (new RNGCryptoServiceProvider ()).GetBytes (buf);
            var newSalt = Convert.ToBase64String (buf);

            string encodedPasswd = EncodePassword (model.Password, 1, newSalt); //

            Membership new_membership = new Membership {
                ApplicationId = model.ApplicationId,
                UserId = model.UserId,
                Password = encodedPasswd,
                PasswordFormat = model.PasswordFormat,
                PasswordSalt = newSalt,
                MobilePin = model.MobilePin,
                Email = model.Email,
                LoweredEmail = model.LoweredEmail,
                PasswordQuestion = model.PasswordQuestion,
                PasswordAnswer = model.PasswordAnswer,
                IsApproved = model.IsApproved,
                IsLockedOut = model.IsLockedOut,
                CreateDate = model.CreateDate,
                LastLoginDate = model.LastLoginDate,
                LastPasswordChangedDate = model.LastLockoutDate,
                LastLockoutDate = model.LastLockoutDate,
                FailedPasswordAttemptCount = model.FailedPasswordAttemptCount,
                FailedPasswordAttemptWindowStart = model.FailedPasswordAnswerAttemptWindowStart,
                FailedPasswordAnswerAttemptCount = model.FailedPasswordAttemptCount,
                FailedPasswordAnswerAttemptWindowStart = model.FailedPasswordAttemptWindowStart,
                Comment = model.Comment
            };
            try {
                await this.repository.CreateUser (new_membership);
            } catch (Exception) { }
            // return CreatedAtRoute ("GetMovie", new { id = new_membership.UserId }, new_membership);
            return Ok (new_membership);

        }

        [HttpPost]
        public IActionResult Login ([FromBody] Credentials credentials) {

            if (credentials == null || credentials.Name == null || credentials.Password == null) {
                log.LogError ("Auth Controller: Either name, password or both are missing");
                return BadRequest ("Invalid username or password");
            }

            var user = this.context.Users
                .Include (u => u.Roles)
                .ThenInclude (r => r.Role)
                .FirstOrDefault (u => u.UserName == credentials.Name);

            if (user == null) {
                log.LogError ("Auth Controller: User does not exist");

                return BadRequest ("Invalid username or password");
            }

            var membership = this.context.Memberships.FirstOrDefault (u => u.UserId == user.UserId);

            if (membership == null) {
                log.LogError ("Auth Controller: Membership info does not exist");

                return BadRequest ("Invalid username or password");
            }

            // hash and compare password
            // hasing algorithm copied from Microsoft Membership Library;see
            // https://github.com/Microsoft/referencesource/tree/master/System.Web/Security
            string salt = membership.PasswordSalt;
            string encodedPasswd = EncodePassword (credentials.Password, 1, salt); // 1 ishased in Membership Provider

            if (encodedPasswd == null) {
                log.LogError ("Auth Controller: Hashing failed");

            }

            var result = membership.Password.Equals (encodedPasswd);

            if (!result) {
                log.LogError ("Auth Controller: Invalid username or password");

                return BadRequest ("Invalid username or password");
            }

            var claims = new System.Security.Claims.Claim[] {
                new Claim (JwtRegisteredClaimNames.Sub, user.UserName)
            };

            var signingKey = new SymmetricSecurityKey (Encoding.UTF8.GetBytes (JwtPassPhrase.PASS_PHRASE));
            var signingCredentials = new SigningCredentials (signingKey, SecurityAlgorithms.HmacSha256);

            var jwt = new JwtSecurityToken (claims: claims, signingCredentials: signingCredentials);

            var encodedJwt = new JwtSecurityTokenHandler ()
                .WriteToken (jwt);

            var userResource = Mapper.Map<User, UserResource> (user);
            var roles = Mapper.Map<ICollection<UserInRole>, ICollection<UserInRoleResource>> (user.Roles);

            return Ok (new JwtResource () {
                Token = encodedJwt,
                    User = userResource,
                    Roles = roles
            });
        }

        // https://github.com/Microsoft/referencesource/tree/master/System.Web/Security
        private string EncodePassword (string pass, int passwordFormat, string salt) {

            byte[] bIn = Encoding.Unicode.GetBytes (pass);
            byte[] bSalt = Convert.FromBase64String (salt);
            byte[] bRet = null;

            // https://github.com/dotnet/corefx/issues/22626
            // In dotnet core 2.x, If you need to call HashAlgorithm.Create to create an instance without knowing the concrete type, it throws PNSE on .NET Core 2.
            // Workaround is to call (HashAlgorithm)CryptoConfig.CreateFromName(string), though calling CryptoConfig directly is generally discouraged.

            // This is recommended way of use
            // HashAlgorithm hm = HashAlgorithm.Create("SHA1");
            // TODO: follow up on platform update and revert back to
            //       HashAlgorithm.Create("SHA1") when made available
            var hm = (HashAlgorithm) CryptoConfig.CreateFromName ("SHA1");

            byte[] bAll = new byte[bSalt.Length + bIn.Length];
            Buffer.BlockCopy (bSalt, 0, bAll, 0, bSalt.Length);
            Buffer.BlockCopy (bIn, 0, bAll, bSalt.Length, bIn.Length);
            bRet = hm.ComputeHash (bAll);

            return Convert.ToBase64String (bRet);
        }

    }
}