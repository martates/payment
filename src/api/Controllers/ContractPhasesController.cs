using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Asgard.Web.Controllers {
    
    [Authorize]
    [Route ("api/contracts/{contractId}/phases")]
    public class ContractPhasesController : Controller {
        private readonly IContractPhaseRepository repository;
        private readonly IDeliverableRepository deliverableRepository;
        private readonly IContractRepository contractRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public ContractPhasesController (IMapper mapper,
            IContractPhaseRepository repository,
            IDeliverableRepository deliverableRepository,
            IContractRepository contractRepository,
            IUnitOfWork unitOfWork) {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
            this.repository = repository;
            this.deliverableRepository = deliverableRepository;
            this.contractRepository = contractRepository;
        }

        [HttpPost]
        public async Task<ActionResult> CreateContractPhase (int contractId, [FromBody] ContractPhaseResource contractPhaseRes) {
            if (!ModelState.IsValid) {
                return BadRequest (ModelState);
            }

            var contractPhase = mapper.Map<ContractPhaseResource, ContractPhase> (contractPhaseRes);

            contractPhase.CreatedBy = ""; // TODO: Add identity here
            contractPhase.CreatedOn = DateTime.Now;
            contractPhase.LastEditedBy = ""; // TODO: Add identity here
            contractPhase.LastModifiedOn = DateTime.Now;

            try {
                repository.Add (contractId, contractPhase);
            } catch (InvalidOperationException e) {
                ModelState.AddModelError ("Contract Id", e.Message);
                return BadRequest (ModelState);
            }

            contractRepository.UpdateContractAdvanceFigures (contractId, contractPhaseRes.ContractAdvancePercent, contractPhaseRes.ContractAdvanceAmount);

            if (contractPhase.SupplementId != null) {
                // creating new phase; with supplement id provided
                // therefore, copy the deliverables in the previous phase 
                // to this new phase
                deliverableRepository.CopyDeliverables (contractPhase);
            }

            await unitOfWork.Complete ();

            contractPhaseRes.Id = contractPhase.Id;

            return Created (string.Format ("/api/contracts/{0}/phases/{1}", contractId, contractPhaseRes.Id), contractPhaseRes);
        }

        [HttpPut ("{phaseId}")]
        public async Task<ActionResult> UpdateContractPhase (int contractId, int phaseId, [FromBody] ContractPhaseResource contractPhaseRes) {
            if (!ModelState.IsValid)
                return BadRequest (ModelState);

            var contractPhase = await repository.GetContractPhase (phaseId);

            contractPhase.LastEditedBy = ""; // TODO: Add identity here
            contractPhase.LastModifiedOn = DateTime.Now;

            mapper.Map<ContractPhaseResource, ContractPhase> (contractPhaseRes, contractPhase);

            contractRepository.UpdateContractAdvanceFigures (contractId, contractPhaseRes.ContractAdvancePercent, contractPhaseRes.ContractAdvanceAmount);

            // if phase advance not taken flag is changed,
            // need to update deliverables monetary values
            deliverableRepository.UpdateDeliverableMonetaryValues (contractPhase);

            await unitOfWork.Complete ();

            return Ok (mapper.Map<ContractPhase, ContractPhaseResource> (contractPhase));
        }

        [HttpGet]
        public ActionResult GetContractPhases (int contractId) {

            var contractPhases = repository.GetContractPhases (contractId);

            if(contractPhases == null) {
                return BadRequest("Phases could not be loaded because requested supplement is not found");
            }
            var result = mapper.Map<IEnumerable<ContractPhase>, IEnumerable<ContractPhaseResource>> (contractPhases);

            return Ok (result);
        }

        [HttpDelete ("{phaseId}")]
        public async Task<IActionResult> DeleteContratPhase (int phaseId) {
            var task = await repository.DeleteContractPhase (phaseId);

            if (task.IsSuccessful) {
                await unitOfWork.Complete ();

                return NoContent ();
            }

            return new NotFoundObjectResult (task.Message);
        }

    }
}