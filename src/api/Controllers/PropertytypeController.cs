using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core.Models;
using Asgard.Web.Persistance;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Asgard.Web.Controllers {
  //  [Authorize]
    public class PropertytypeController : Controller {
        private readonly EramsContext context;
        private readonly IMapper mapper;
        public PropertytypeController (EramsContext context, IMapper mapper) {
            this.mapper = mapper;
            this.context = context;
        }

        [HttpGet ("/api/propertytypes")]
        public async Task<IEnumerable<PropertyTypeResource>> GetPropertyTypes () {
            var propertytype = await context.PropertyType.Include (m => m.Units).ToListAsync ();

            return mapper.Map<List<PropertyType>, List<PropertyTypeResource>> (propertytype);
        }
    }
}