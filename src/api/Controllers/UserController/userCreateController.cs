using System;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Models.Users;
using Asgard.Web.Persistance;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Asgard.Web.Controllers {
    // [Route ("api/[controller]")]
    public class userCreateController : Controller {
        private readonly EramsUsersContext context;
        private readonly ILogger<userCreateController> log;
        private readonly IMapper mapper;
        private readonly IUserCreateRepository repository;
        public userCreateController (IMapper mapper, EramsUsersContext context, ILogger<userCreateController> log, IUserCreateRepository repository) {
            this.mapper = mapper;
            this.context = context;
            this.log = log;
            this.repository = repository;
        }

        [Route ("api/newUserCreate")]
        [HttpPost]
        public async Task<ActionResult> createUsers ([FromBody] User user) {
            if (!ModelState.IsValid)
                return BadRequest (ModelState);
            try {
                await this.repository.CreateUser (user);
            } catch (Exception) {
                //await ex("existing with exiting");
            }
            return CreatedAtRoute (new { id = user.UserId }, user);
        }

        [Route ("api/getUser/{username}")]
        [HttpGet]
        public IActionResult GetProject (string username) {
            var project = repository.GetUser (username);

            if (project == null) {
                return NotFound ();
            }

            return Ok (project);
        }

        [Route ("api/getAllUser")]
        [HttpGet]
        public IActionResult GetAllUser () {
            var allUserList = repository.GetAllUser ();

            if (allUserList == null) {
                return NotFound ();
            }

            return Ok (allUserList);
        }

         [Route ("api/getSingleUser/{userId}")]
         
        [HttpGet]
        public IActionResult GetSingleUser (System.Guid userId) {
            var singleUserList = repository.GetSingleUser (userId);

            if (singleUserList == null) {
                return NotFound ();
            }

            return Ok (singleUserList);
        }
    }
}