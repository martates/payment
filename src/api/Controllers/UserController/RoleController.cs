using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Models.Users;
using Asgard.Web.Persistance;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Asgard.Web.Controllers {
    // [Route ("api/[controller]")]
    public class RoleController : Controller {
        private readonly EramsUsersContext context;
        private readonly ILogger<RoleController> log;
        private readonly IMapper mapper;
        private readonly IRoleRepository repository;

        public RoleController (IMapper mapper, EramsUsersContext context, ILogger<RoleController> log, IRoleRepository repository) {
            this.mapper = mapper;
            this.context = context;
            this.log = log;
            this.repository = repository;
        }

        [Route ("api/addRole")]
        [HttpPost]
        public async Task<ActionResult> createRoles ([FromBody] Role role) {
            if (!ModelState.IsValid)
                return BadRequest (ModelState);
            await this.repository.CreateRole (role);
            return CreatedAtRoute (new { id = role.RoleId }, role);
        }

        [Route ("api/addUserInRole")]
        [HttpPost]
        public async Task<ActionResult> createUserInRoles ([FromBody] UserInRole userInRole) {
            if (!ModelState.IsValid)
                return BadRequest (ModelState);
            await this.repository.CreateUserInRole (userInRole);
            return CreatedAtRoute (new { id = userInRole.RoleId }, userInRole);
        }

        [Route ("api/getAllRole")]
        [HttpGet]
        public IActionResult GetAllRole () {
            var allRoleList = repository.GetAllRole ();

            if (allRoleList == null) {
                return NotFound ();
            }

            return Ok (allRoleList);
        }

        [Route ("api/getUserInRole/{userId}")]
        [HttpGet]
        public IEnumerable<UserInRoleResource> GetUserInRole(Guid userId)
        {
            var userInrole = repository.GetUserInRole(userId);

            var result = mapper.Map<IEnumerable<UserInRole>, IEnumerable<UserInRoleResource>>(userInrole);

            return result;
        }
    }
}