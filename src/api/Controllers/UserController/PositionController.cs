using Asgard.Web.Core;
using Asgard.Web.Persistance;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Asgard.Web.Controllers {
    public class PositionController : Controller {
        private readonly EramsUsersContext context;
        private readonly ILogger<PositionController> log;
        private readonly IMapper mapper;
        private readonly IPositionRepository repository;
        public PositionController (IMapper mapper, EramsUsersContext context, ILogger<PositionController> log, IPositionRepository repository) {
            this.mapper = mapper;
            this.context = context;
            this.log = log;
            this.repository = repository;
        }

        [Route ("api/getAllPosition")]
        [HttpGet]
        public IActionResult GetAllPosition () {
            var allPositionList = repository.GetAllPostion ();

            if (allPositionList == null) {
                return NotFound ();
            }

            return Ok (allPositionList);
        }

        [Route ("api/getAllDirectorate")]
        [HttpGet]
        public IActionResult GetAllDirectorate () {
            var allDirectorateList = repository.GetAllDirectorate ();

            if (allDirectorateList == null) {
                return NotFound ();
            }

            return Ok (allDirectorateList);
        }
    }
}