using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using Asgard.Web.Persistance;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Asgard.Web.Controllers
{
// [Authorize]
    public class OwnerShipController : Controller
    {
        private readonly IOwnerShipRepository repository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        public OwnerShipController(IMapper mapper, IOwnerShipRepository repository, IUnitOfWork unitOfWork)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
            this.repository = repository;
        }

        [Route("api/ownership")]
        [HttpPost]
        public async Task<IActionResult> CreateAffilate([FromBody] OwnerShipResource ownerShip)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var ownershipres = mapper.Map<OwnerShipResource, Ownership>(ownerShip);
            repository.Add(ownershipres);
            await unitOfWork.Complete();
            var result = mapper.Map<Ownership, OwnerShipResource>(ownershipres);

            return Ok(result);
        }

        [Route("api/projectperkm/{projectperkmId}/ownerShip/{ownerShip}")]
        // [Route ("api/project/{id}/projectperkm/{projectPerKm}")]
        [HttpPut]
        public async Task<ActionResult> UpdateContractPhase([FromBody] OwnerShipResource ownerShip)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var _ownership = repository.UpdateOwnership(ownerShip.OwnershipId);

            mapper.Map<OwnerShipResource, Ownership>(ownerShip, _ownership);

            await unitOfWork.Complete();

            return Ok(mapper.Map<Ownership, OwnerShipResource>(_ownership));
        }

        [HttpDelete("api/deleteownership/{id}")]
        public async Task<IActionResult> DeleteProperty()
        {
            var ownership = int.Parse(RouteData.Values["id"].ToString());
            await repository.Deleteownership(ownership);
            await unitOfWork.Complete();
            return NoContent();
        }

        [HttpGet("api/ownershipList")]
        public IEnumerable<OwnerShipResource> GetOwner()
        {
            // TODO: make call async
            // Map to a given contract
            var ownerList = repository.GetOwnerList();
            var result = mapper.Map<IEnumerable<Ownership>, IEnumerable<OwnerShipResource>>(ownerList);
            return result;
        }

        [HttpGet("api/PropjectPerKMs/{id}/ownerships")]
        public IEnumerable<OwnerShipResource> GetAffiliate()
        {
            var contractId = int.Parse(RouteData.Values["id"].ToString());
            var ownerShip = repository.GetOwnerships(contractId);
            var result = mapper.Map<IEnumerable<Ownership>, IEnumerable<OwnerShipResource>>(ownerShip);
            return result;
        }

        [HttpGet]
        [Route("api/ownershipdrop")]
        public IEnumerable<KeyValuePair<int, string>> GetAffiliateNames()
        {
            return repository.GetOwnershipNames();
        }

        [HttpGet]
        [Route("api/PropjectPerKMs/{id}/ownershipdrops")]
        public IEnumerable<KeyValuePair<int, string>> GetOwnershipswithPerKm()
        {
            var projectPerKm = int.Parse(RouteData.Values["id"].ToString());
            return repository.GetOwnershipswithPerKm(projectPerKm);
        }
    }
}