﻿using System.Collections.Generic;
using System.Text;
using api.Utilities;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using Asgard.Web.Persistance;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace Asgard.Web {
    public class Startup {
        public Startup (IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // Urls values only used if client and server are served separately
        // necessitating cors
        private List<string> Urls;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services) {
            services.AddScoped<IContractRepository, ContractRepository> ();
            services.AddScoped<IContractPhaseRepository, ContractPhaseRepository> ();
            services.AddScoped<IDeliverableLookupRepository, DeliverableLookupRepository> ();
            services.AddScoped<IDeliverableRepository, DeliverableRepository> ();
            services.AddScoped<IConsultantsProgramRepository, ConsultantsProgramRepository> ();
            services.AddScoped<IConsultantsPerformanceRepository, ConsultantsPerformanceRepository> ();
            services.AddScoped<ISupplementRepository, SupplementRepository> ();
            services.AddScoped<IPhaseCurrencyBreakdownRepository, PhaseCurrencyBreakdownRepository> ();
            services.AddScoped<IUnitOfWork, UnitOfWork> ();

            services.AddScoped<IProjectRepository, ProjectRepository> ();
            services.AddScoped<IOwnerShipRepository, OwnerShipRepository> ();
            services.AddScoped<IProjectPerKmRepository, ProjectPerKmRepository> ();
            services.AddScoped<IPropertyRepository, PropertyRepository> ();
            services.AddScoped<IProjectRepository, ProjectRepository> ();
            services.AddScoped<IEstimateCommitteRepository, EstimateCommitteRepository> ();
            services.AddScoped<IFileUploadRepository, FileUploadRepository> ();
            services.Configure<FileUploadSettings> (Configuration.GetSection ("PhotoSettings"));

            services.AddScoped<IContractProgressRepository, ContractProgressRepository> ();
            services.AddScoped<IRoadSegmentListRepository, RoadSegmentListRepository> ();
            services.AddScoped<IrnmdsRepository, rnmdsRepository> ();
            services.AddScoped<IsectionRepository, sectionRepository> ();
            services.AddScoped<IroadClassRepository, roadClassRepository> ();

            services.AddScoped<IPurchaseOrderRepository, PurchaseOrderRepository> ();

            services.AddScoped<IpaymentReposiotry, PaymenetRepository> ();
            services.AddScoped<IfinancedByReposiotry, FinancedByRepository> ();
            services.AddScoped<IremarkReposiotry, RemarkRepository> ();

            services.AddScoped<IUserCreateRepository, UserCreateRepository> ();
            services.AddScoped<IMembershipRepository, MembershipRepository> ();
            services.AddScoped<IRoleRepository, RoleRepository> ();
              services.AddScoped<IPositionRepository, PositionRepository> ();
            // Add service and create Policy with options
            // TODO: AllowAnyOrigin is temporary 
            services.AddCors (options => {
                options.AddPolicy ("CorsPolicy",
                    builder => builder.SetIsOriginAllowed (IsOriginAllowed)
                    .AllowAnyMethod ()
                    .AllowAnyHeader ()
                    .AllowCredentials ());
            });

            var signingKey = new SymmetricSecurityKey (Encoding.UTF8.GetBytes (JwtPassPhrase.PASS_PHRASE));

            services.AddAuthentication (options => {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer (cfg => {
                cfg.RequireHttpsMetadata = false;
                cfg.SaveToken = true;
                cfg.TokenValidationParameters = new TokenValidationParameters () {
                    IssuerSigningKey = signingKey,
                    ValidateAudience = false,
                    ValidateIssuer = false,
                    ValidateLifetime = false,
                    ValidateIssuerSigningKey = true
                };
            });
            services.AddAutoMapper ();
            services.AddDbContext<EramsContext> (opt => opt.UseSqlServer (Configuration.GetConnectionString ("DefaultConnection")));
            services.AddDbContext<EramsUsersContext> (opt => opt.UseSqlServer (Configuration.GetConnectionString ("UsersConnection")));
            services.AddMvc ();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder app, IHostingEnvironment env,
            ILoggerFactory loggerFactory) {
            Urls = new List<string> ();

            if (env.IsDevelopment ()) {
                app.UseDeveloperExceptionPage ();
                Urls.Add ("http://localhost:60");
            }

            if (env.IsProduction ()) {
                Urls.Add ("http://localhost:4200");
                Urls.Add ("http://172.16.1.43:60");
                Urls.Add ("http://erams.era.gov.et:60");
                Urls.Add ("http://213.55.97.172:60");

                //   Urls.Add ("http://localhost:4200");
                // Urls.Add ("http://192.168.10.18:60");
                // Urls.Add ("http://erams.era.gov.et:60");
                // Urls.Add ("http://213.55.97.172:60");
            }

            app.UseAuthentication ();

            app.UseCors ("CorsPolicy");

            // return index.html or similar for host only url
            app.UseDefaultFiles ();

            // enables serving static (html, css, js) files from within this web api.
            // the files served are the angular app assets. This enables serving both
            // client and server from the same server host and port making cors setting 
            // unnecessary for production build. Kept cors only for development.
            app.UseStaticFiles ();

            app.UseMvc ();
            loggerFactory.AddFile ("Logs/erams-sms-{Date}.txt");
        }

        private bool IsOriginAllowed (string url) {
            return Urls.Contains (url);
        }
    }
}