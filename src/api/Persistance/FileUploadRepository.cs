using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Asgard.Web.Persistance;
using System.Collections.Generic;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Persistance
{
    public class FileUploadRepository: IFileUploadRepository
    {
        private readonly EramsContext context;
        public FileUploadRepository(EramsContext context)
        {
            this.context = context;
        }
        public async Task<IEnumerable<FileUpload>> GetFileUploads(int ownershipId)
        {
              return await context.FileUploads
        .Where(p => p.OwnershipId == ownershipId)
        .ToListAsync();
        }
    }
}