using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions.Internal;

namespace Asgard.Web.Persistance
{
    public class ContractRepository : IContractRepository
    {
        private readonly EramsContext context;
        public ContractRepository(EramsContext context)
        {
            this.context = context;
        }

        public Contract GetContract(int id)
        {
            return context.Contracts
                .Include(c => c.Firm)
                .Include(c => c.ContractPhases)
                .Include(c => c.CurrencyBreakdowns)
                .Where(c => c.ContractId == id)
                .FirstOrDefault();
        }

        public IEnumerable<KeyValuePair<int, string>> GetContractNames()
        {
            return context.Contracts
                .Where(c => c.IsAwarded == true && (c.ContractType.Contains("design") && !c.ContractType.Contains("build")))
                .Select(c => new KeyValuePair<int, string>(c.ContractId, c.ContractName));
        }

        public IEnumerable<KeyValuePair<int, string>> GetWorkContractNames()
        {
            return context.Contracts
                .Where(c => c.IsAwarded == true && (c.ContractType.Contains("Construction") ))
                .Select(c => new KeyValuePair<int, string>(c.ContractId, c.ContractName));
        }
        public void UpdateContractAdvanceFigures(int id, decimal advancePercent, decimal advanceAmount)
        {
            var contract = context.Contracts.Find(id);
            contract.AdvanceAmount = advanceAmount;
        }
    }
}