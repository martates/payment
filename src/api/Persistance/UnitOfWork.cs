using System.Threading.Tasks;
using Asgard.Web.Core;

namespace Asgard.Web.Persistance {
    public class UnitOfWork : IUnitOfWork
    {
        private readonly EramsContext context;

        public UnitOfWork(EramsContext context)
        {
            this.context = context;
        }
        public async Task Complete()
        {
            await context.SaveChangesAsync();
        }
    }
}