using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions.Internal;

namespace Asgard.Web.Persistance {
    public class PhaseCurrencyBreakdownRepository : IPhaseCurrencyBreakdownRepository {
        private readonly EramsContext context;

        public PhaseCurrencyBreakdownRepository (EramsContext context) {
            this.context = context;
        }

        public IEnumerable<PhaseCurrencyBreakdown> GetCurrencybreakdowns (int phaseId) {
            return context.PhaseCurrencyBreakdowns
                .Where (p => p.ContractPhaseId == phaseId);
        }

        public void Add (PhaseCurrencyBreakdown breakdown) {
            context.PhaseCurrencyBreakdowns.Add (breakdown);
        }

        public async Task<PhaseCurrencyBreakdown> GetCurrencybreakdown(int breakdownId)
        {
            return await context.PhaseCurrencyBreakdowns
                .FirstOrDefaultAsync(p => p.Id == breakdownId);
        }
    }
}