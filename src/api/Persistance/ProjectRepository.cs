using Asgard.Web.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions.Internal;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Asgard.Web.Persistance
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly EramsContext context;
        public ProjectRepository(EramsContext context)
        {
            this.context = context;
        }

        Project IProjectRepository.GetProject(int id)
        {
            return context.Project
           // .Include(con => con.Contract)
            .Include(p => p.ProjectPerKm)
            .ThenInclude(pk => pk.ownership)
            .ThenInclude(ow => ow.Property)
                .Where(c => c.ProjectID == id)
                .FirstOrDefault();
        }
        public IEnumerable<KeyValuePair<int, string>> GetProjectNames()
        {
            return context.Project
            .OrderBy(pro => pro.Name)
            .Select(c => new KeyValuePair<int, string>(c.ProjectID, c.Name));
        }
    }
}