using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Helpers;
using Asgard.Web.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Asgard.Web.Persistance {

    public class DeliverableRepository : IDeliverableRepository {
        private readonly EramsContext context;

        public DeliverableRepository (EramsContext context) {
            this.context = context;
        }
        public IEnumerable<Deliverable> GetDeliverables (int contractId) {
            var deliverables = context.Deliverables
                .Include (d => d.ContractPhase)
                .Include (d => d.DeliverableLookup)
                .Where (d => d.ContractId == contractId);
            return deliverables;
        }

        public void Add (Deliverable deliverable, int contractId) {
            deliverable.ContractId = contractId;
            context.Deliverables.Add (deliverable);
        }
        public async Task<Deliverable> GetDeliverable (int deliverableId, bool includeRelated) {
            var query = context.Deliverables.Where (d => d.Id == deliverableId);

            if (includeRelated) {
                return await query
                    .Include (d => d.ContractPhase)
                    .Include (d => d.DeliverableLookup)
                    .FirstOrDefaultAsync ();
            }

            return await query.FirstOrDefaultAsync ();
        }

        public async Task<TaskResult> DeleteDeliverable (int id) {
            var deliverable =
                await context.Deliverables
                .Include (d => d.ConsultantsPrograms)
                .Include (d => d.ConsultantsPerformances)
                .Where (d => d.Id == id)
                .SingleOrDefaultAsync ();

            if (deliverable == null)
                return new TaskResult (false, "Contract Phase does not exist");

            if (deliverable.ConsultantsPrograms.Count > 0)
                return new TaskResult (false, "Deliverable contains Consultant's Schedules. Please delete them before deleting the deliverable.");

            if (deliverable.ConsultantsPerformances.Count > 0)
                return new TaskResult (false, "Deliverable contains Consultant's Performances. Please delete them before deleting the deliverable.");

            context.Deliverables.Remove (deliverable);

            return new TaskResult (true, string.Empty);
        }

        public void CopyDeliverables (ContractPhase contractPhase) {
            var deliverables =
                context.Deliverables
                .Where (d => d.ContractPhaseId == contractPhase.ContractPhaseId);

            foreach (var original in deliverables) {
                var clone = original.Clone ();
                clone.ContractPhaseId = contractPhase.Id;
                clone.Id = 0;
                UpdateDeliverableMonetaryValues(contractPhase, clone);
                context.Entry<Deliverable> (clone).State = EntityState.Added;
                context.Deliverables.Add (clone);
            };
        }

        public void UpdateDeliverableMonetaryValues (ContractPhase contractPhase) {
            var deliverables =
                context.Deliverables
                .Where (d => d.ContractPhaseId == contractPhase.Id);

            var phaseTotal = contractPhase.LumpSum;

            phaseTotal = contractPhase.IsAdvanceNotTaken ? phaseTotal : phaseTotal - contractPhase.AdvanceAmount;

            foreach (var deliverable in deliverables) {
                decimal scaler = 0;

                if (!contractPhase.IsAdvanceNotTaken) {
                    scaler = (deliverable.PhasePercentage / 100) / (1 - (contractPhase.AdvancePercent / 100));
                } else {
                    scaler = (deliverable.PhasePercentage / 100);
                }

                deliverable.MonetaryValue = phaseTotal * scaler;
            }
        }

        private void UpdateDeliverableMonetaryValues (ContractPhase contractPhase, Deliverable deliverable) {

            var phaseTotal = contractPhase.LumpSum;

            phaseTotal = contractPhase.IsAdvanceNotTaken ? phaseTotal : phaseTotal - contractPhase.AdvanceAmount;

            decimal scaler = 0;

            if (!contractPhase.IsAdvanceNotTaken) {
                scaler = (deliverable.PhasePercentage / 100) / (1 - (contractPhase.AdvancePercent / 100));
            } else {
                scaler = (deliverable.PhasePercentage / 100);
            }

            deliverable.MonetaryValue = phaseTotal * scaler;
        }

    }
}