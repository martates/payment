using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Asgard.Web.Persistance {

    public class ConsultantsPerformanceRepository : IConsultantsPerformanceRepository {
        private readonly EramsContext context;

        public ConsultantsPerformanceRepository (EramsContext context) {
            this.context = context;
        }

        public void Add (ConsultantsPerformance consultantsPerformance, int contractId) {
            consultantsPerformance.ContractId = contractId;
            context.ConsultantsPerformances.Add (consultantsPerformance);
        }

        public async Task<bool> DeleteConsultantsPerformance (int id) {
            var performance = await context.ConsultantsPerformances.FindAsync (id);

            if (performance == null)
                return false;

            context.ConsultantsPerformances.Remove (performance);
            return true;
        }

        public async Task<ConsultantsPerformance> GetConsultantsPerformance (int performanceId, bool includeRelated) {
            var query = context.ConsultantsPerformances.Where (c => c.Id == performanceId);

            if (includeRelated) {
                var progress = await query
                    .Include (d => d.Deliverable)
                    .ThenInclude (d => d.DeliverableLookup)
                    .FirstOrDefaultAsync ();

                progress.Deliverable.ContractPhase =
                    context.ContractPhases.FirstOrDefault (p => p.Id == progress.Deliverable.ContractPhaseId);
            }

            return await query.FirstOrDefaultAsync ();
        }

        public IEnumerable<ConsultantsPerformance> GetConsultantsPerformances (int contractId) {
            return context.ConsultantsPerformances
                .Include (c => c.Deliverable)
                .ThenInclude (d => d.DeliverableLookup)
                .Where (d => d.ContractId == contractId);
        }
    }
}