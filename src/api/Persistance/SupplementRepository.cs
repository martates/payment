using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Helpers;
using Asgard.Web.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Asgard.Web.Persistance {

    public class SupplementRepository : ISupplementRepository {
        private readonly EramsContext context;

        public SupplementRepository (EramsContext context) {
            this.context = context;
        }

        public IEnumerable<Supplement> GetSupplements (int contractId) {
            return context.Supplements
                .Where (c => c.ContractId == contractId);
        }

        public TaskResult Add (Supplement supplement, int contractId) {
            // verify sequence numbering
            //      get previous supplements
            var result = VerifySequenceNumber (contractId, supplement.Number);

            if (!result.IsSuccessful)
                return result;

            // link with contract
            supplement.ContractId = contractId;

            // audit
            supplement.CreatedBy = ""; // TODO: Add identity here
            supplement.CreatedOn = DateTime.Now;
            supplement.LastEditedBy = ""; // TODO: Add identity here
            supplement.LastModifiedOn = DateTime.Now;

            // add to context
            context.Supplements.Add (supplement);

            return result;
        }

        public async Task<TaskResult> DeleteSupplement (int id) {
            var contractPhase =
                await context.ContractPhases
                .Where (cp => cp.SupplementId == id)
                .SingleOrDefaultAsync ();

            if (contractPhase != null)
                return new TaskResult (false, "Supplement is associated with contract phases. Please delete the associated contract phases before deleting the supplement.");

            var supplement =
                await context.Supplements.FirstOrDefaultAsync(s => s.Id == id);

            context.Supplements.Remove (supplement);

            return new TaskResult (true, string.Empty);
        }

        public TaskResult VerifySequenceNumber (int contractId, int number) {
            var supplements = GetSupplements (contractId);
            if (supplements.Count () == 0)
                return new TaskResult (true, string.Empty);

            var found = supplements.Any (s => s.Number == number);

            return found ? new TaskResult (false, "Supplement sequence number is repeated") : new TaskResult (true, string.Empty);
        }

        public async Task<Supplement> GetSupplement (int supplementId, bool includeRelated) {
            var query = context.Supplements.Where (s => s.Id == supplementId);

            if (includeRelated) {
                return await query
                    .Include (d => d.SupplementPhases)
                    .ThenInclude (cp => cp.Deliverables)
                    .FirstOrDefaultAsync ();
            }

            return await query.FirstOrDefaultAsync ();
        }
    }
}