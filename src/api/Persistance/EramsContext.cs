using System;
using System.Collections.Generic;
using Asgard.Web.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Asgard.Web.Persistance {
    public class EramsContext : DbContext {

        public DbSet<Contract> Contracts { get; set; }
        public DbSet<Supplement> Supplements { get; set; }
        public DbSet<Firm> Firms { get; set; }
        public DbSet<DeliverableLookup> DeliverableLookups { get; set; }
        public DbSet<Deliverable> Deliverables { get; set; }
        public DbSet<ContractPhase> ContractPhases { get; set; }
        public DbSet<ConsultantsProgram> ConsultantsPrograms { get; set; }
        public DbSet<ConsultantsPerformance> ConsultantsPerformances { get; set; }
        public DbSet<CurrencyBreakdown> CurrencyBreakdowns { get; set; }
        public DbSet<PhaseCurrencyBreakdown> PhaseCurrencyBreakdowns { get; set; }

        public DbSet<Ownership> Ownership { get; set; }
        public DbSet<Property> Property { get; set; }
        public DbSet<Project> Project { get; set; }
        public DbSet<ProjectPerKm> ProjectPerKm { get; set; }
        public DbSet<PropertyType> PropertyType { get; set; }
        public DbSet<Unit> Unit { get; set; }
        public DbSet<EstimateCommitte> EstimateCommittes { get; set; }
        public DbSet<FileUpload> FileUploads { get; set; }

        public DbSet<KeyStaffRegister> KeyStaffRegisters { get; set; }
        public DbSet<ContactDetail> ContactDetails { get; set; }
        public DbSet<Measurement> Measurements { get; set; }
        public DbSet<ContractProgressPerformanceScore> ContractProgressPerformanceScore { get; set; }

        public DbSet<RoadSegmentList> RoadSegmentLists { get; set; }
        public DbSet<roadClassesList> roadClassesLists { get; set; }
        public DbSet<rnmdList> rnmdLists { get; set; }
        public DbSet<sectionList> sectionLists { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<FinancedBy> financedBys { get; set; }
        public DbSet<Remark> remarks { get; set; }

        public EramsContext (DbContextOptions<EramsContext> options) : base (options) { }

        protected override void OnModelCreating (ModelBuilder modelBuilder) {
            modelBuilder.Entity<ContractPhase> ().HasOne (p => p.Supplement)
                .WithMany (p => p.SupplementPhases).HasForeignKey (s => s.SupplementId);
            modelBuilder.Entity<ContractPhase> ().HasOne (p => p.ReplacedSupplement)
                .WithMany (p => p.ReplacedSupplementPhases).HasForeignKey (m => m.ReplacedSupplementId);

            modelBuilder.Entity<Contract> (entity => {
                entity.ToTable ("Contract");
                entity.Property (p => p.FirmId).HasColumnName ("Contractor");
            });

            modelBuilder.Entity<Firm> (entity => {
                entity.ToTable ("Firm");
                entity.Property (p => p.Id).HasColumnName ("FirmID");
                entity.Property (p => p.Name).HasColumnName ("FirmName");
            });

            modelBuilder.Entity<CurrencyBreakdown> (entity => {
                entity.ToTable ("CurrencyBreakdown");
                entity.Property (p => p.Id).HasColumnName ("CurrencyBreakDownID");
            });

            modelBuilder.Entity<ContractType> (entity => {
                entity.HasKey (c => c.ContractType1);
            });

            modelBuilder.Entity<ContactDetail> (entity => {
                entity.ToTable ("ContactDetail");
                entity.Property (p => p.StaffID).HasColumnName ("KeyStaffID");
            });
            modelBuilder.Entity<Measurement> (entity => {
                entity.ToTable ("Measurement");
            });
            modelBuilder.Entity<KeyStaffRegister> (entity => {
                entity.ToTable ("KeyStaffRegister");
            });
        }
    }
}