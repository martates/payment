using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Asgard.Web.Persistance;
using System.Collections.Generic;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Persistance
{
    public class FileUploadService :IFileUploadService
    {
        public readonly IUnitOfWork unitOfWork;
        public readonly IFileUploadStorage fileUploadStorage;
        public FileUploadService(IUnitOfWork unitOfWork, IFileUploadStorage fileUploadStorage)
        {
            this.fileUploadStorage = fileUploadStorage;
            this.unitOfWork = unitOfWork;
        }

        public async Task<FileUpload> UploadPhoto(Ownership ownership, IFormFile file, string uploadsFolderPath)
        {
            var fileName = await fileUploadStorage.StorePhoto(uploadsFolderPath, file);

            var fileUpload = new FileUpload { FileName = fileName };
            ownership.FileUploads.Add(fileUpload);
            await unitOfWork.Complete();

        return fileUpload;
        }
    }
}