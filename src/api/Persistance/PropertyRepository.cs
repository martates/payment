using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Asgard.Web.Persistance
{
    public class PropertyRepository : IPropertyRepository
    {
        private readonly EramsContext context;
        public PropertyRepository(EramsContext context)
        {
            this.context = context;
        }

        public void Add(Property property)
        {
            context.Property.Add(property);
        }

        public async Task Deleteproperty(int PropertyId)
        {
            var propety = await context.Property.FindAsync(PropertyId);
            context.Property.Remove(propety);
        }

        public IEnumerable<Property> Getproperty()
        {
            return context.Property;
        }

        public IEnumerable<Property> Getpropertys(int ownershipId)
        {
            return context.Property.Where(cp => cp.OwnershipId == ownershipId);
        }

        public Property GetPropety(int ownershipId)
        {
            return context.Property
                .Include(c => c.Ownership)
                .Where(c => c.OwnershipId == ownershipId)
                .FirstOrDefault();
        }
        public IEnumerable<Property> Getpropertysasfull(int projectId, int projectPerKmId, int ownershipId)
        {
            return context.Property.Where(cp => cp.OwnershipId == ownershipId);
        }

        public IEnumerable<Project> GetPropertyOnSpecificProject(int projectId)
        {
            return context.Project
            .Include(pkm => pkm.ProjectPerKm)
            .ThenInclude(o => o.ownership)
            .ThenInclude(p => p.Property)
            .Where(p => p.ProjectID == projectId);
        }
    }
}