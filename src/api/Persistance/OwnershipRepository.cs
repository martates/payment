using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Asgard.Web.Persistance
{
    public class OwnerShipRepository : IOwnerShipRepository
    {
        private readonly EramsContext context;
        public OwnerShipRepository(EramsContext context)
        {
            this.context = context;
        }

        public void Add(Ownership ownership)
        {
            context.Ownership.Add(ownership);
        }
        public async Task Deleteownership(int ownershipId)
        {
            var ownership = await context.Ownership.FindAsync(ownershipId);
            context.Ownership.Remove(ownership);
        }
        public IEnumerable<Ownership> GetOwnerList()
        {
            return context.Ownership;
        }
        public IEnumerable<KeyValuePair<int, string>> GetOwnershipNames()
        {
            return context.Ownership
                .Select(a => new KeyValuePair<int, string>(a.OwnershipId, a.OwnershipName));
        }
        public IEnumerable<KeyValuePair<int, string>> GetOwnershipswithPerKm(int ProjectPerKmId)
        {
            return context.Ownership.Where(a => a.ProjectPerKmId == ProjectPerKmId)
                .Select(a => new KeyValuePair<int, string>(a.OwnershipId, a.OwnershipName));
        }

        public IEnumerable<Ownership> GetOwnerships(int ProjectPerKmId)
        {
            return context.Ownership.Where(a => a.ProjectPerKmId == ProjectPerKmId);
        }

        public void Insert(int contractId, Ownership Ownership)
        {
            throw new NotImplementedException();
        }

        public Ownership GetOwnership(int OwnershipId, bool includeRelated)
        {
            return context.Ownership.Where(a => a.OwnershipId == OwnershipId).FirstOrDefault();
        }

        public Ownership UpdateOwnership(int ownershipId)
        {           
            return context.Ownership
              .Include(c => c.projectPerKm)
              .Where(c => c.OwnershipId == ownershipId)
              .FirstOrDefault();
        }
    }
}