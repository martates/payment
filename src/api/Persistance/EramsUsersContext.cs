﻿using System;
using Asgard.Web.Core.Models.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Asgard.Web.Persistance {
    public partial class EramsUsersContext : DbContext {
        public EramsUsersContext (DbContextOptions<EramsUsersContext> options) : base (options) { 

        }

        public virtual DbSet<Application> Applications { get; set; }
        public virtual DbSet<Membership> Memberships { get; set; }
        public virtual DbSet<Profile> Profiles { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserInRole> UsersInRoles { get; set; }
        public virtual DbSet<Directorates> Directorates { get; set; }
        public virtual DbSet<MandatoryRolesMatrix> MandatoryRolesMatrix { get; set; }
        public virtual DbSet<Positions> Positions { get; set; }

        protected override void OnModelCreating (ModelBuilder modelBuilder) {
            modelBuilder.Entity<Application> (entity => {
                entity.HasKey (e => e.ApplicationId)
                    .ForSqlServerIsClustered (false);

                entity.ToTable ("aspnet_Applications");

                entity.HasIndex (e => e.ApplicationName)
                    .HasName ("UQ__aspnet_A__3091033107020F21")
                    .IsUnique ();

                entity.HasIndex (e => e.LoweredApplicationName)
                    .HasName ("aspnet_Applications_Index")
                    .ForSqlServerIsClustered ();

                entity.Property (e => e.ApplicationId).HasDefaultValueSql ("(newid())");

                entity.Property (e => e.ApplicationName)
                    .IsRequired ()
                    .HasMaxLength (256);

                entity.Property (e => e.Description).HasMaxLength (256);

                entity.Property (e => e.LoweredApplicationName)
                    .IsRequired ()
                    .HasMaxLength (256);
            });

            modelBuilder.Entity<Membership> (entity => {
                entity.HasKey (e => e.UserId)
                    .ForSqlServerIsClustered (false);

                entity.ToTable ("aspnet_Membership");

                entity.HasIndex (e => new { e.ApplicationId, e.LoweredEmail })
                    .HasName ("aspnet_Membership_index")
                    .ForSqlServerIsClustered ();

                entity.Property (e => e.UserId).ValueGeneratedNever ();

                entity.Property (e => e.Comment).HasColumnType ("ntext(3000)");

                entity.Property (e => e.CreateDate).HasColumnType ("datetime");

                entity.Property (e => e.Email).HasMaxLength (256);

                entity.Property (e => e.FailedPasswordAnswerAttemptWindowStart).HasColumnType ("datetime");

                entity.Property (e => e.FailedPasswordAttemptWindowStart).HasColumnType ("datetime");

                entity.Property (e => e.LastLockoutDate).HasColumnType ("datetime");

                entity.Property (e => e.LastLoginDate).HasColumnType ("datetime");

                entity.Property (e => e.LastPasswordChangedDate).HasColumnType ("datetime");

                entity.Property (e => e.LoweredEmail).HasMaxLength (256);

                entity.Property (e => e.MobilePin)
                    .HasColumnName ("MobilePIN")
                    .HasMaxLength (16);

                entity.Property (e => e.Password)
                    .IsRequired ()
                    .HasMaxLength (128);

                entity.Property (e => e.PasswordAnswer).HasMaxLength (128);

                entity.Property (e => e.PasswordFormat).HasDefaultValueSql ("((0))");

                entity.Property (e => e.PasswordQuestion).HasMaxLength (256);

                entity.Property (e => e.PasswordSalt)
                    .IsRequired ()
                    .HasMaxLength (128);

                entity.HasOne (d => d.Application)
                    .WithMany (p => p.AspnetMembership)
                    .HasForeignKey (d => d.ApplicationId)
                    .OnDelete (DeleteBehavior.ClientSetNull)
                    .HasConstraintName ("FK__aspnet_Me__Appli__21B6055D");

                entity.HasOne (d => d.User)
                    .WithOne (p => p.Membership)
                    .HasForeignKey<Membership> (d => d.UserId)
                    .OnDelete (DeleteBehavior.ClientSetNull)
                    .HasConstraintName ("FK__aspnet_Me__UserI__22AA2996");
            });

            modelBuilder.Entity<Profile> (entity => {
                entity.HasKey (e => e.UserId);

                entity.ToTable ("aspnet_Profile");

                entity.Property (e => e.UserId).ValueGeneratedNever ();

                entity.Property (e => e.LastUpdatedDate).HasColumnType ("datetime");

                entity.Property (e => e.PropertyNames)
                    .IsRequired ()
                    .HasColumnType ("ntext(6000)");

                entity.Property (e => e.PropertyValuesBinary)
                    .IsRequired ()
                    .HasColumnType ("image(6000)");

                entity.Property (e => e.PropertyValuesString)
                    .IsRequired ()
                    .HasColumnType ("ntext(6000)");

                entity.HasOne (d => d.User)
                    .WithOne (p => p.AspnetProfile)
                    .HasForeignKey<Profile> (d => d.UserId)
                    .OnDelete (DeleteBehavior.ClientSetNull)
                    .HasConstraintName ("FK__aspnet_Pr__UserI__38996AB5");
            });

            modelBuilder.Entity<Role> (entity => {
                entity.HasKey (e => e.RoleId)
                    .ForSqlServerIsClustered (false);

                entity.ToTable ("aspnet_Roles");

                entity.HasIndex (e => new { e.ApplicationId, e.LoweredRoleName })
                    .HasName ("aspnet_Roles_index1")
                    .IsUnique ()
                    .ForSqlServerIsClustered ();

                entity.Property (e => e.RoleId).HasDefaultValueSql ("(newid())");

                entity.Property (e => e.Description).HasMaxLength (256);

                entity.Property (e => e.LoweredRoleName)
                    .IsRequired ()
                    .HasMaxLength (256);

                entity.Property (e => e.RoleName)
                    .IsRequired ()
                    .HasMaxLength (256);

                entity.HasOne (d => d.Application)
                    .WithMany (p => p.AspnetRoles)
                    .HasForeignKey (d => d.ApplicationId)
                    .OnDelete (DeleteBehavior.ClientSetNull)
                    .HasConstraintName ("FK__aspnet_Ro__Appli__440B1D61");
            });

            modelBuilder.Entity<User> (entity => {
                entity.HasKey (e => e.UserId)
                    .ForSqlServerIsClustered (false);

                entity.ToTable ("aspnet_Users");

                entity.HasIndex (e => new { e.ApplicationId, e.LastActivityDate })
                    .HasName ("aspnet_Users_Index2");

                entity.HasIndex (e => new { e.ApplicationId, e.LoweredUserName })
                    .HasName ("aspnet_Users_Index")
                    .IsUnique ()
                    .ForSqlServerIsClustered ();

                entity.Property (e => e.UserId).HasDefaultValueSql ("(newid())");

                entity.Property (e => e.BadgeNumber).HasMaxLength (10);

                entity.Property (e => e.Directorate).HasMaxLength (80);

                entity.Property (e => e.FirstName).HasMaxLength (256);

                entity.Property (e => e.LastActivityDate).HasColumnType ("datetime");

                entity.Property (e => e.LastEditedBy).HasMaxLength (256);

                entity.Property (e => e.LastName).HasMaxLength (256);

                entity.Property (e => e.LoweredUserName)
                    .IsRequired ()
                    .HasMaxLength (256);

                entity.Property (e => e.MobileAlias).HasMaxLength (16);

                entity.Property (e => e.Position).HasMaxLength (80);

                entity.Property (e => e.Telephone).HasMaxLength (50);

                entity.Property (e => e.UserName)
                    .IsRequired ()
                    .HasMaxLength (256);

                entity.HasOne (d => d.Application)
                    .WithMany (p => p.AspnetUsers)
                    .HasForeignKey (d => d.ApplicationId)
                    .OnDelete (DeleteBehavior.ClientSetNull)
                    .HasConstraintName ("FK__aspnet_Us__Appli__0DAF0CB0");
            });

            modelBuilder.Entity<UserInRole> (entity => {
                entity.HasKey (e => new { e.UserId, e.RoleId });

                entity.ToTable ("aspnet_UsersInRoles");

                entity.HasIndex (e => e.RoleId)
                    .HasName ("aspnet_UsersInRoles_index");

                entity.HasOne (d => d.Role)
                    .WithMany (p => p.UsersInRoles)
                    .HasForeignKey (d => d.RoleId)
                    .OnDelete (DeleteBehavior.ClientSetNull)
                    .HasConstraintName ("FK__aspnet_Us__RoleI__4AB81AF0");

                entity.HasOne (d => d.User)
                    .WithMany (p => p.Roles)
                    .HasForeignKey (d => d.UserId)
                    .OnDelete (DeleteBehavior.ClientSetNull)
                    .HasConstraintName ("FK__aspnet_Us__UserI__49C3F6B7");
            });

            modelBuilder.Entity<Directorates> (entity => {
                entity.HasKey (e => e.Directorate);

                entity.Property (e => e.Directorate)
                    .HasMaxLength (50)
                    .ValueGeneratedNever ();
            });

            modelBuilder.Entity<MandatoryRolesMatrix> (entity => {
                entity.HasKey (e => e.MandatoryRoleId);

                entity.Property (e => e.MandatoryRoleId).HasColumnName ("MandatoryRoleID");

                entity.Property (e => e.Directorate).HasMaxLength (50);

                entity.Property (e => e.Position).HasMaxLength (50);

                entity.HasOne (d => d.MandatoryRoleNavigation)
                    .WithMany (p => p.MandatoryRolesMatrix)
                    .HasForeignKey (d => d.MandatoryRole)
                    .HasConstraintName ("FK_MandatoryRolesMatrix_aspnet_Roles");
            });

            modelBuilder.Entity<Positions> (entity => {
                entity.HasKey (e => e.Position);

                entity.Property (e => e.Position)
                    .HasMaxLength (50)
                    .ValueGeneratedNever ();
            });
        }
    }
}