using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Models.Users;

namespace Asgard.Web.Persistance
{
    public class MembershipRepository : IMembershipRepository {
    private readonly EramsUsersContext context;

    public MembershipRepository (EramsUsersContext context) {
      this.context = context;
    }
    public async Task<Membership> CreateUser (Membership aspNetUser) {
      await this.context.Memberships.AddAsync (aspNetUser);
      await this.context.SaveChangesAsync ();
      return aspNetUser;
    }
  }
}