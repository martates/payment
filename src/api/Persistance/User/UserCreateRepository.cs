using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Models.Users;

namespace Asgard.Web.Persistance {
    public class UserCreateRepository : IUserCreateRepository {
        private readonly EramsUsersContext context;

        public UserCreateRepository (EramsUsersContext context) {
            this.context = context;
        }

        public async Task<User> CreateUser (User user) {
            await this.context.Users.AddAsync (user);
            await this.context.SaveChangesAsync ();
            return user;
        }
        public IEnumerable<User> GetUser (string username) {
            return context.Users.Where (c => c.UserName == username);
        }
        public IEnumerable<User> GetAllUser () {
            return context.Users.OrderBy (c => c.UserName);
        }

        User IUserCreateRepository.GetSingleUser (System.Guid userId) {
            return context.Users
                .Where (c => c.UserId == userId)
                .FirstOrDefault ();
        }

    }
}