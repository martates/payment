using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Models.Users;
using Microsoft.EntityFrameworkCore;

namespace Asgard.Web.Persistance {
    public class RoleRepository : IRoleRepository {
        private readonly EramsUsersContext context;

        public RoleRepository (EramsUsersContext context) {
            this.context = context;
        }

        public async Task<Role> CreateRole (Role role) {
            await this.context.Roles.AddAsync (role);
            await this.context.SaveChangesAsync ();
            return role;
        }

        public async Task<UserInRole> CreateUserInRole (UserInRole userInRole) {
            await this.context.UsersInRoles.AddAsync (userInRole);
            await this.context.SaveChangesAsync ();
            return userInRole;
        }

        public IEnumerable<Role> GetAllRole () {
            return context.Roles.OrderBy (c => c.RoleName);
        }

        // public UserInRole GetUserInRole(Guid userId)
        // {
        //      return context.UsersInRoles.Where (cp => cp.UserId == userId).FirstOrDefault();
        // }
        public IEnumerable<UserInRole> GetUserInRole (Guid userId) {
            return context.UsersInRoles
               // .Include (d => d.User)
               .Include (d => d.Role)
                .Where (cp => cp.UserId == userId);
        }
    }
}