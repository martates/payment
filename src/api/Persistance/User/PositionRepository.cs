using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Models.Users;

namespace Asgard.Web.Persistance {

    public class PositionRepository : IPositionRepository {
        private readonly EramsUsersContext context;

        public PositionRepository (EramsUsersContext context) {
            this.context = context;
        }

        public IEnumerable<Positions> GetAllPostion () {
            return context.Positions.OrderBy (c => c.Position);
        }
         public IEnumerable<Directorates> GetAllDirectorate () {
            return context.Directorates.OrderBy (c => c.Directorate);
        }
    }
}