using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Persistance {
    public class RemarkRepository : IremarkReposiotry {
        private readonly EramsContext context;
        public RemarkRepository (EramsContext context) {
            this.context = context;
        }

        public void Add (Remark remark) {
            context.remarks.Add (remark);
        }

        public async Task DeleteRemark (int id) {
            var remark = await context.remarks.FindAsync (id);
            context.remarks.Remove (remark);
        }

        public IEnumerable<Remark> GetRemarkList () {
            return context.remarks;
        }

        public Remark UpdateRemark (int id) {
            // throw new System.NotImplementedException();
            return context.remarks
                //  .Include(c => c.projectPerKm)
                .Where (c => c.remarkId == id)
                .FirstOrDefault ();
        }
    }
}