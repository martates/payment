using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Persistance {
    public class FinancedByRepository : IfinancedByReposiotry {
        private readonly EramsContext context;
        public FinancedByRepository (EramsContext context) {
            this.context = context;
        }

        public void Add (FinancedBy financedBy) {
            context.financedBys.Add (financedBy);
        }

        public async Task DeletefinancedBy (int id) {
            var financedBy = await context.financedBys.FindAsync (id);
            context.financedBys.Remove (financedBy);
        }

        public IEnumerable<FinancedBy> GetfinancedBy () {
            return context.financedBys;
        }

        public IEnumerable<FinancedBy> GetfinancedByList () {
            return context.financedBys;
        }

        public FinancedBy UpdatefinancedBy (int id) {
            // throw new System.NotImplementedException();
            return context.financedBys
                //  .Include(c => c.projectPerKm)
                .Where (c => c.financedById == id)
                .FirstOrDefault ();
        }
        public IEnumerable<KeyValuePair<int, string>> GetFinancedByNames () {
            return context.financedBys
                .OrderBy (pro => pro.financedByname)
                .Select (c => new KeyValuePair<int, string> (c.financedById, c.financedByname));
        }
    }
}