using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Persistance
{
    public class PaymenetRepository : IpaymentReposiotry
    {
        private readonly EramsContext context;
        public PaymenetRepository(EramsContext context)
        {
            this.context = context;
        }
        public void Add(Payment payment)
        {
            context.Payments.Add(payment);
        }
        public IEnumerable<Payment> GetPaymentList()
        {
            return context.Payments.Where(c => c.paidstatus == "0");
        }
         public IEnumerable<Payment> GetPaymentListPaid()
        {
            return context.Payments.Where(c => c.paidstatus == "1");
        }
        public IEnumerable<Payment> GetPayment(int projectId)
        {
            return context.Payments.Where(cp => cp.ProjectID == projectId);
        }

        public Payment UpdatePayment(int id)
        {
            // throw new System.NotImplementedException();
            return context.Payments
            //  .Include(c => c.projectPerKm)
            .Where(c => c.id == id)
            .FirstOrDefault();
        }

        public async Task DeletePayment(int id)
        {
            var payment = await context.Payments.FindAsync(id);
            context.Payments.Remove(payment);
        }

        // public Task DeletePayment(int id)
        // {
        //     var payment = await context.Payments.FindAsync(id);
        //     context.Payments.Remove(payment);
        // }
    }
}