using System.Collections.Generic;
using System.Threading.Tasks;
using Asgard.Web.Core.Models;
using Asgard.Web.Core;

namespace Asgard.Web.Persistance {

    public class DeliverableLookupRepository: IDeliverableLookupRepository {
        private readonly EramsContext context;

        public DeliverableLookupRepository(EramsContext context)
        {
            this.context = context;
        }

        public IEnumerable<DeliverableLookup> GetDeliverableLookups() {
            return context.DeliverableLookups;
        }

        public void Add(DeliverableLookup lookup)
        {
            context.DeliverableLookups.Add(lookup);
        }

    }
}