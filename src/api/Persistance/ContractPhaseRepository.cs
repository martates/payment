using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Helpers;
using Asgard.Web.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Asgard.Web.Persistance {
    public class ContractPhaseRepository : IContractPhaseRepository {
        private readonly EramsContext context;
        public ContractPhaseRepository (EramsContext context) {
            this.context = context;
        }

        public void Add (int contractId, ContractPhase contractPhase) {
            contractPhase.ContractId = contractId;

            context.ContractPhases.Add (contractPhase);
        }

        public async Task<TaskResult> DeleteContractPhase (int contractPhaseId) {
            var contractPhase =
                await context.ContractPhases
                .Include (c => c.Deliverables)
                .Where (cp => cp.Id == contractPhaseId)
                .SingleOrDefaultAsync ();

            if (contractPhase == null)
                return new TaskResult (false, "Contract Phase does not exist");

            if (contractPhase.Deliverables.Count > 0)
                return new TaskResult (false, "Contract Phase contains deliverbales. Please delete deliverables before deleting the phase.");

            context.ContractPhases.Remove (contractPhase);

            return new TaskResult (true, string.Empty);
        }

        public async Task<ContractPhase> GetContractPhase (int contractPhaseId) {
            return await context.ContractPhases.FindAsync (contractPhaseId);
        }

        public IEnumerable<ContractPhase> GetContractPhases (int contractId) {
            return context.ContractPhases.Where (cp => cp.ContractId == contractId);
        }

    }
}