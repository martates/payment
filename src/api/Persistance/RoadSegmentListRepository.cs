using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Persistance {
    public class RoadSegmentListRepository : IRoadSegmentListRepository {
        private readonly EramsContext context;
        public RoadSegmentListRepository (EramsContext context) {
            this.context = context;
        }

         public void Add(RoadSegmentList rooadSegmentLists)
        {
            context.RoadSegmentLists.Add(rooadSegmentLists);
        }
        public IEnumerable<RoadSegmentList> GetroadSegmentList () {
            return context.RoadSegmentLists;
        }
        public List<RoadSegmentList> GroupParts () {
            var totals = context.RoadSegmentLists
                .GroupBy (o => o.region)
                .Select (g => new RoadSegmentList {
                    region = g.Key,
                       gravellength = g.Sum (row => row.gravellength),
                        asphaltlength = g.Sum (row => row.asphaltlength)
                })
                .ToList ();
            return totals;
        }

        public RoadSegmentList UpdateRoadSegementList(int rnmdId)
        {
           return context.RoadSegmentLists
                // .Include(c => c.Ownership)
                .Where(c => c.rnmdId == rnmdId)
                .FirstOrDefault();
        }
    }
}

// public IQueryable<IGrouping<string , int>> GetroadSegmentGroupSum () {
//     var totals = context.RoadSegmentLists
//         .GroupBy (row => row.region)
//         .Select (g => new {
//             Name = g.Key,
//                 Total = g.Sum (row => row.gravellength)
//         }).ToList();
//     return (IQueryable<IGrouping<string , int>>)totals;
// }
// public IEnumerable<IGrouping<string, RoadSegmentList>> GetroadSegmentList () {
//     var allList = context.RoadSegmentLists.GroupBy (x => x.region);
//     return allList;
// }

// IQueryable<<anonymous type: int totalgrave>>
//  IQueryable<IGrouping<<anonymous type: string reg>, RoadSegmentList>>
//  .Select<IGrouping<<anonymous type: string reg>, RoadSegmentList>, 
// <anonymous type: int totalgrave>>(System.Linq.Expressions.Expression<Func<IGrouping<<anonymous type: string reg>, RoadSegmentList>, <anonymous type: int totalgrave>>> selector)

// public IEnumerable<string> GetroadSegmentSum () {
//     return context.RoadSegmentLists
//         .Select (o => o.region ).Distinct();            
// }

// public int GetroadSegmentSum () {
//     return context.RoadSegmentLists
//         .Select (x => new {
//             total = x.gravellength
//         })
//         .Sum (y => y.total);
// }