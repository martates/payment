using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core.Helpers;
using Asgard.Web.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Asgard.Web.Persistance
{
    public class EstimateCommitteRepository : IEstimateCommitteRepository
    {

        private readonly EramsContext context;
        public EstimateCommitteRepository(EramsContext context)
        {
            this.context = context;
        }
        public void Add(int ProjectPerKmId, EstimateCommitte estimateCommitte)
        {
           estimateCommitte.ProjectPerKmId = ProjectPerKmId;
            context.EstimateCommittes.Add(estimateCommitte);
        }
       public IEnumerable<EstimateCommitte> GetEstimateCommittes(int ProjectPerKmId )
        {
            return context.EstimateCommittes.Where( ec => ec.ProjectPerKmId == ProjectPerKmId);
        }
           public async Task<EstimateCommitte> GetEstimateCommitte(int Id)
        {
            return await context.EstimateCommittes.FindAsync(Id);
        }
                public async Task<TaskResult> DeleteEstimateCommitte(int Id)
        {             
            var estimateCommitte = 
                await context.EstimateCommittes
                    //.Include(c => c.)
                    .Where(cp => cp.Id == Id)
                    .SingleOrDefaultAsync();

            if(estimateCommitte == null)
                return new TaskResult(false, "Estimate Committe does not exist");       
            
            // if(estimateCommitte.Deliverables.Count > 0)
            //     return new TaskResult(false, "Contract Phase contains deliverbales. Please delete deliverables before deleting the phase.");
            
            context.EstimateCommittes.Remove(estimateCommitte);

            return new TaskResult(true, string.Empty);
        }

    }
}