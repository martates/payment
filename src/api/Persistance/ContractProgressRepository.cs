using System.Collections.Generic;
using System.Linq;
using Asgard.Web.Controllers.Resources;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Asgard.Web.Persistance
{
    public class ContractProgressRepository : IContractProgressRepository
    {
        private readonly IMapper mapper;
        // public ContractProgressRepository(IMapper mapper)
        // {
        //     this.mapper = mapper;
        // }
        private readonly EramsContext context;
        public ContractProgressRepository(EramsContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }
        public ContractProgressResource GetContractProgress(int contractId)
        {
            var contract = context.Contracts.Include(c => c.Firm).FirstOrDefault(c => c.ContractId == contractId);


            if (contract == null)
            {
                return null;
            }

            var contactDetails = context.ContactDetails.Include(cd => cd.KeyStaffRegister).Where(cd => cd.ContractId == contractId && cd.LeavingDate == null);

            if (contactDetails == null || contactDetails.Count() == 0)
            {
                return null;
            }

            var measurement = context.Measurements
                .Include(m => m.ContractProgressPerformanceScore)
                .Where(m => m.ContractId == contractId)
                .OrderBy(m => m.Date)
                .LastOrDefault();

            if (measurement == null)
            {
                return null;
            }

            var contractProgressResource = new ContractProgressResource();


            contractProgressResource.ContractName = contract.ContractName;
            contractProgressResource.ContractType = contract.ContractType;
            contractProgressResource.Firm = contract.Firm.Name;

            var scores = measurement.ContractProgressPerformanceScore;
            decimal? score = (scores != null && scores.Count() > 0) ? scores.First().ProgressPerformance : null;
            contractProgressResource.ProgressPerformance = score;

            var staff = new List<KeyStaffRegisterResource>();

            foreach (var contact in contactDetails)
            {
                staff.Add(mapper.Map<KeyStaffRegister, KeyStaffRegisterResource>(contact.KeyStaffRegister));
            }

            contractProgressResource.Staff = staff.AsEnumerable();

            return contractProgressResource;
        }
        public IEnumerable<ContractProgressResource> GetContractByStaff(int staffId)
        {
            var keyStaff = context.KeyStaffRegisters.Include(ks => ks.ContactDetails).FirstOrDefault(ks => ks.StaffID == staffId);

            if (keyStaff == null)
            {
                return null;
            }

            var contracts = new List<Contract>();

            foreach (var cd in keyStaff.ContactDetails)
            {
                contracts.Add(context.Contracts.Include(c => c.Firm).FirstOrDefault(c => c.ContractId == cd.ContractId));
            }

            var progressResources = new List<ContractProgressResource>();

            foreach (var c in contracts)
            {
                var measurement = context.Measurements
                    .Include(m => m.ContractProgressPerformanceScore)
                    .Where(m => m.ContractId == c.ContractId)
                    .OrderBy(m => m.Date)
                    .LastOrDefault();

                if (measurement == null)
                    continue;

                var progressResource = new ContractProgressResource();

                var scores = measurement.ContractProgressPerformanceScore;
                decimal? score = (scores != null && scores.Count() > 0) ? scores.First().ProgressPerformance : null;
                progressResource.ContractName = c.ContractName;
                progressResource.ContractType = c.ContractType;
                progressResource.Firm = c.Firm.Name;
                progressResource.ProgressPerformance = score != null ? score : new decimal?();
                progressResource.Staff = new List<KeyStaffRegisterResource>() { mapper.Map<KeyStaffRegister, KeyStaffRegisterResource>(keyStaff) };

                progressResources.Add(progressResource);
            }

            return progressResources;
        }
      public IEnumerable<KeyValuePair<int, string>> getkeyStaff(){
            
             return context.KeyStaffRegisters
                 .Where (ks => ks.IsLocalStaff == true)
                .Select (ks => new KeyValuePair<int, string> (ks.StaffID, ks.FullName));
        }
    }
}