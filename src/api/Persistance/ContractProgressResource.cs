using System.Collections.Generic;

namespace Asgard.Web.Controllers.Resources
{
    public class ContractProgressResource
    {
        public string ContractName { get; set; }
        public string Firm { get; set; }
        public string ContractType { get; set; }
        public IEnumerable<KeyStaffRegisterResource> Staff { get; set; }
        public decimal? ProgressPerformance { get; set; }  
          
    }
}