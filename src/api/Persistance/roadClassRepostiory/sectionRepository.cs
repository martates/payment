using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Persistance {
    public class sectionRepository : IsectionRepository {
        private readonly EramsContext context;
        public sectionRepository (EramsContext context) {
            this.context = context;
        }

         public void Add(sectionList sectionList)
        {
            context.sectionLists.Add(sectionList);
        }

        public IEnumerable<KeyValuePair<int, string>> GetSectionList()
        {
            return context.sectionLists
               .Select(a => new KeyValuePair<int, string>(a.Id, a.section));
        }
    }
}

