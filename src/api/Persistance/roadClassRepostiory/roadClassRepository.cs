using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Persistance {
    public class roadClassRepository : IroadClassRepository {
        private readonly EramsContext context;
        public roadClassRepository (EramsContext context) {
            this.context = context;
        }

         public void Add(roadClassesList roadClassesList)
        {
            context.roadClassesLists.Add(roadClassesList);
        }

        public IEnumerable<KeyValuePair<int, string>> GetroadClassList()
        {
            return context.roadClassesLists
               .Select(a => new KeyValuePair<int, string>(a.Id, a.roadClass));
        }
    }
}

