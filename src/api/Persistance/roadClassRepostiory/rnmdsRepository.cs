using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Persistance {
    public class rnmdsRepository : IrnmdsRepository {
        private readonly EramsContext context;
        public rnmdsRepository (EramsContext context) {
            this.context = context;
        }

         public void Add(rnmdList rnmdList)
        {
            context.rnmdLists.Add(rnmdList);
        }

          public IEnumerable<KeyValuePair<int, string>> GetrnmdsList()
        {
            return context.rnmdLists
                .Select(a => new KeyValuePair<int, string>(a.Id, a.rnmd));
        }

    }
}

