using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;

namespace Asgard.Web.Persistance {
    public class PurchaseOrderRepository: IPurchaseOrderRepository
    {
         private readonly EramsContext context;
        public PurchaseOrderRepository (EramsContext context) {
            this.context = context;
        }

        public void Add(Purchase_Order purchase_Order)
        {
            throw new System.NotImplementedException();
        }

        public Task DeletePurchaseOrder(int projectPerKmId)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Purchase_Order> GetPurchaseOrder()
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Purchase_Order> GetPurchaseOrders(int projectId)
        {
            throw new System.NotImplementedException();
        }

        public Purchase_Order UpdatePurchaseOrder(int projectPerKmId)
        {
            throw new System.NotImplementedException();
        }
    }
}