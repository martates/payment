using System;
using System.Collections.Generic;
using Asgard.Web.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Asgard.Web.Persistance {
    public class EramsLogisticContext : DbContext {
         public EramsLogisticContext (DbContextOptions<EramsLogisticContext> options) : base (options) { }
        public DbSet<Purchase_Order> Purchase_Orders { get; set; }
        public DbSet<Requestion_Item> Requestion_Items { get; set; }
    }
}