using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Asgard.Web.Persistance {
    public class ProjectPerKmRepository : IProjectPerKmRepository {
        private readonly EramsContext context;
        public ProjectPerKmRepository (EramsContext context) {
            this.context = context;
        }
        public void Add (ProjectPerKm projectPerKm) {
            context.ProjectPerKm.Add (projectPerKm);
        }

        public async Task DeleteProjectPerKm (int projectPerKmId) {
            var projectperkm = await context.ProjectPerKm.FindAsync (projectPerKmId);
            context.ProjectPerKm.Remove (projectperkm);
        }

        public IEnumerable<ProjectPerKm> GetProjectPerKm () {
            return context.ProjectPerKm;
        }
        public IEnumerable<KeyValuePair<int, string>> GetProjectPerKmsdrop (int projectId) {
            return context.ProjectPerKm
                .Where (cp => cp.ProjectID == projectId)
                .Select (a => new KeyValuePair<int, string> (a.ProjectPerKmId, "From" + a.KMStart + "Km" + "To" + a.KmEnd + "Km"));
        }
        public IEnumerable<ProjectPerKm> GetProjectPerKms (int projectId) {
            return context.ProjectPerKm.Where (cp => cp.ProjectID == projectId);
        }

        public ProjectPerKm UpdateProjectPerKm(int projectPerKmId)
        {
             return context.ProjectPerKm
                .Include (c => c.project)
                .Where (c => c.ProjectPerKmId == projectPerKmId)
                .FirstOrDefault ();
        }
    }
}