namespace Asgard.Web.Controllers.Resources
{
    public class UnitResource
    {
        public int  Id { get; set; }
        public string Name { get; set; } 
    }
}