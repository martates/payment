using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asgard.Web.Core;
using Asgard.Web.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Asgard.Web.Persistance {

    public class ConsultantsProgramRepository : IConsultantsProgramRepository {
        private readonly EramsContext context;

        public ConsultantsProgramRepository (EramsContext context) {
            this.context = context;
        }

        public void Add (ConsultantsProgram consultantsProgram, int contractId) {
            consultantsProgram.ContractId = contractId;
            context.ConsultantsPrograms.Add (consultantsProgram);
        }

        public async Task<ConsultantsProgram> GetConsultantsProgram (int programId, bool includeRelated) {
            var query = context.ConsultantsPrograms.Where (c => c.Id == programId);

            if (includeRelated) {
                var program = await query
                    .Include (d => d.Deliverable)
                    .ThenInclude (d => d.DeliverableLookup)
                    .FirstOrDefaultAsync ();

                program.Deliverable.ContractPhase =
                    context.ContractPhases.FirstOrDefault (p => p.Id == program.Deliverable.ContractPhaseId);
            }

            return await query.FirstOrDefaultAsync ();
        }

        public async Task<bool> DeleteConsultantsProgram (int id) {
            var program = await context.ConsultantsPrograms.FindAsync (id);

            if (program == null)
                return false;

            context.ConsultantsPrograms.Remove (program);
            return true;
        }

        public IEnumerable<ConsultantsProgram> GetConsultantsPrograms (int contractId) {
            return context.ConsultantsPrograms
                .Include (c => c.Deliverable)
                .ThenInclude (d => d.DeliverableLookup)
                .Where (d => d.ContractId == contractId);
        }
    }
}