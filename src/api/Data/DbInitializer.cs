using System;
using System.Linq;
using Asgard.Web.Core.Models;
using Asgard.Web.Persistance;

namespace Asgard.Web.data
{
    public static class DbInitializer
    {
        public static void Initialize(EramsContext context)
        {
            context.Database.EnsureCreated();

            // Look for any deliverable.
            if (context.DeliverableLookups.Any())
            {
                return; // DB has been seeded
            }
            if (context.PropertyType.Any())
            {
                return; // DB has been seeded
            }

            if (context.Unit.Any())
            {
                return; // DB has been seeded
            }

            var plant = new PropertyType() { Name = "Plants" };
            var Farm = new PropertyType() { Name = "Farms" };
            var pernennial_croops = new PropertyType() { Name = "Pernennial Croops" };
            var Trees = new PropertyType() { Name = "Trees" };
            var protectedgrass = new PropertyType() { Name = "Protected Grass" };
            var rural_land = new PropertyType() { Name = "Permanent Improvement on rural land" };
            var relocation = new PropertyType() { Name = "Relocation of Property" };
            var mining = new PropertyType() { Name = "Payable to Mining Licencse" };
            var burial = new PropertyType() { Name = "For Burial Ground" };
            var fences = new PropertyType() { Name = "For Fences" };
            var building = new PropertyType() { Name = "For Buildings" };

            var propertytypes = new PropertyType[] {
                plant,
                Farm,
                pernennial_croops,
                Trees,
                protectedgrass,
                rural_land,
                relocation,
                mining,
                burial,
                fences,
                building,

            };

            var units = new Unit[] {
                new Unit { Name = "Number", PropertyType = plant },
                new Unit { Name = "Hectar", PropertyType = Farm },
                new Unit { Name = "Square Area", PropertyType = pernennial_croops },
                new Unit { Name = "Linear", PropertyType = Trees },
                new Unit { Name = "Linear", PropertyType = protectedgrass },
                new Unit { Name = "Linear", PropertyType = rural_land },
                new Unit { Name = "Linear", PropertyType = relocation },
                new Unit { Name = "Linear", PropertyType = mining },
                new Unit { Name = "Linear", PropertyType = burial },
                new Unit { Name = "Linear", PropertyType = fences },
                new Unit { Name = "Linear", PropertyType = building },
            };

            // var core = new Firm () { Name = "Core Consulting Engineers", FirmType = "Consultant" };
            // var beza = new Firm () { Name = "Beza Engineers", FirmType = "Consultant" };

            // var firms = new Firm[] {
            //     core,
            //     beza
            // };

            // var contracts = new Contract[] {
            //     new Contract {
            //     ContractNumber = "0001", ContractName = @"Consultancy Services of Feasibility And EIA Study, Rap, Detailed Engineering And Tender Document Preparation
            //     For Mota-Jara-Gedo Link Road Project", ContractSignedOn = new DateTime (2015, 1, 1), Firm = core, OriginalContractAmount = 8000000,
            //     DeductRetentionBeforeVatCalc = false, IsContractInactive = false, IsCommencementDateModified = false, ContractType = "Design"
            //     },
            //     new Contract {
            //     ContractNumber = "0002", ContractName = "Test Contract 2", ContractSignedOn = new DateTime (2017, 1, 1),
            //     Firm = beza, OriginalContractAmount = 6000000, DeductRetentionBeforeVatCalc = false, IsContractInactive = false, IsCommencementDateModified = false,
            //     ContractType = "Design & Supervision"
            //     },

            // };

            // var currencyBreakdowns = new CurrencyBreakdown[] {
            //     new CurrencyBreakdown { Contract = contracts[0], Currency = "ETB", ExchangeRate = 1 },
            //     new CurrencyBreakdown { Contract = contracts[0], Currency = "USD", ExchangeRate = 29.75M },
            //     new CurrencyBreakdown { Contract = contracts[0], Currency = "GBP", ExchangeRate = 36M }
            // };

            // var supplement = new Supplement {
            //     Contract = contracts[0],
            //     Number = 2,
            //     Name = "Supplementary Agreement to Consultancy Services of Feasibility And EIA Study, Rap, Detailed Engineering And Tender Document Preparation For Mota-Jara-Gedo Link Road Project",
            //     AgreementDate = new DateTime (2018 - 1 - 20),
            //     Purpose = "Adjcent link road design",
            //     SupplementAmount = 100000
            // };

            var delDesc = new DeliverableLookup[] {
                new DeliverableLookup { Description = "Inception" },
                new DeliverableLookup { Description = "Preliminary EIA Report" },
                new DeliverableLookup { Description = "Route Selection Report" },
                new DeliverableLookup { Description = "Monthly Progress Report" },
                new DeliverableLookup { Description = "Design Standard Review Report" },
                new DeliverableLookup { Description = "Engineering Report" },
                new DeliverableLookup { Description = "Solis and Materials Report" },
                new DeliverableLookup { Description = "Preliminary Hydrological/Hydraulics Report" },
            };

            var contractTypes = new ContractType[] {
                new ContractType { ContractType1 = "Construction (Bridges and other structures)", Category = "Work" },
                new ContractType { ContractType1 = "Design" }
            };
            foreach (var propertytype in propertytypes)
                context.PropertyType.Add(propertytype);

            foreach (var Unit in units)
                context.Unit.Add(Unit);


            // foreach (var d in delDesc)
            //     context.DeliverableLookups.Add (d);

            // foreach (var firm in firms)
            //     context.Firms.Add (firm);

            // foreach (var contract in contracts)
            //     context.Contracts.Add (contract);

            // context.Supplements.Add (supplement);

            context.SaveChanges();
        }
    }
}